IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES T
        WHERE T.TABLE_SCHEMA = 'dbo'
        AND T.TABLE_NAME = 'tbl_OMS_FeedbackDetail')
    BEGIN

        CREATE TABLE dbo.tbl_OMS_FeedbackDetail
        (
			[FeedbackDetailId] int identity(1,1) primary key,
            [BrokerId] varchar(10),
			[BrokerName] varchar (100),
			[BrokerEmail] varchar (100),
			[BrokerPhoneNumber] varchar(100),
			[CustomerId] varchar(100),
			[CustomerName] varchar(100),
			[Comments] varchar(max),
			[FileName] varchar(max),
			[FileURL] varchar(max),
			[CreatedDate] datetime,
			[CreatedBy] varchar(10),
			[ModifiedDate] datetime,
			[ModifiedBy] varchar(10)
        )
    END
GO