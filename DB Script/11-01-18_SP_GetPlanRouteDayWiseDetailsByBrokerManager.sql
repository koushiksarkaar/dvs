ALTER PROCEDURE [dbo].[SP_GetPlanRouteDayWiseDetailsByBrokerManager]
(
	@CompanyId nvarchar(10),
	@BrokerID  nvarchar(50)	 
)
AS

BEGIN

	SET NOCOUNT ON;
    Declare @groupID nvarchar(50);
    Declare @NewSalesmanID nvarchar(50);
    Declare @GoyaCompanyId nvarchar(5);
    
    /* Check for Super user and give access to all customer under Super User */
	  SET @groupID = (SELECT SUBSTRING(@BrokerID,5,2)) ;	  
	  set @GoyaCompanyId = (SELECT SUBSTRING(@BrokerID,1,2)) ;	  
	   if(@groupID = '01')
	     set @NewSalesmanID = (SELECT SUBSTRING(@BrokerID,3,2));
	   ELSE
	       set @NewSalesmanID  = @BrokerID
	
      Select USERNO as UserID,USERNAME AS Name into #Brokers from [Goya_UtilDB].[dbo].[vw_User_Master_Data] 
      where (BRROUT like  @NewSalesmanID+'%%' or @BrokerID='' or @BrokerID is null)   and @CompanyId = COMPANY

 --  SP_GetPlanRouteDayWiseDetailsByBrokerManager '01', ''

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) as [Date], ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) as 'Count', (B.UserID+ '- ' + B.Name) as BrokerName into #tempMonday From tbl_OMS_Order O 
   inner join #Brokers B on o.SalesmanId = B.UserID collate  Latin1_General_CS_AS
   Where  DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0)
   Group By(B.UserID+ '- ' + B.Name) 

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 1) as [Date], ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) as 'Count', (B.UserID+ '- ' + B.Name) as BrokerName into #tempTuesday From tbl_OMS_Order O 
   inner join #Brokers B on o.SalesmanId = B.UserID collate  Latin1_General_CS_AS
   Where  DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 1)
   Group By(B.UserID+ '- ' + B.Name) 

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 2) as [Date], ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) as 'Count', (B.UserID+ '- ' + B.Name) as BrokerName into #tempWednesday From tbl_OMS_Order O 
   inner join #Brokers B on o.SalesmanId = B.UserID collate  Latin1_General_CS_AS
   Where  DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 2)
   Group By(B.UserID+ '- ' + B.Name) 

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 3) as [Date], ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) as 'Count', (B.UserID+ '- ' + B.Name) as BrokerName into #tempThursday From tbl_OMS_Order O 
   inner join #Brokers B on o.SalesmanId = B.UserID collate  Latin1_General_CS_AS
   Where  DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 3)
   Group By(B.UserID+ '- ' + B.Name) 

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 4) as [Date], ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) as 'Count', (B.UserID+ '- ' + B.Name) as BrokerName into #tempFriday From tbl_OMS_Order O 
   inner join #Brokers B on o.SalesmanId = B.UserID collate  Latin1_General_CS_AS
   Where  DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 4)
   Group By(B.UserID+ '- ' + B.Name) 

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 5) as [Date], ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) as 'Count', (B.UserID+ '- ' + B.Name) as BrokerName into #tempSaturday From tbl_OMS_Order O 
   inner join #Brokers B on o.SalesmanId = B.UserID collate  Latin1_General_CS_AS
   Where  DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 5)
   Group By(B.UserID+ '- ' + B.Name) 

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 6) as [Date], ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) as 'Count', (B.UserID+ '- ' + B.Name) as BrokerName into #tempSunday From tbl_OMS_Order O 
   inner join #Brokers B on o.SalesmanId = B.UserID collate  Latin1_General_CS_AS
   Where  DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 6)
   Group By(B.UserID+ '- ' + B.Name) 

   Select * from #tempMonday
   UNION ALL
   Select * from #tempTuesday 
   UNION ALL
   Select * from #tempWednesday 
   UNION ALL 
   Select * from #tempThursday   
   UNION ALL
   Select * from #tempFriday   
   UNION ALL
   Select * from #tempSaturday  
   UNION ALL
   Select * from #tempSunday 
END