IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[tbl_OMS_PlanRouteCustomerDetails]') 
         AND name = 'MondayPriority'
)
BEGIN
	ALTER TABLE [dbo].[tbl_OMS_PlanRouteCustomerDetails] ADD MondayPriority INT
END

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[tbl_OMS_PlanRouteCustomerDetails]') 
         AND name = 'TuesdayPriority'
)
BEGIN
	ALTER TABLE [dbo].[tbl_OMS_PlanRouteCustomerDetails] ADD TuesdayPriority INT
END

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[tbl_OMS_PlanRouteCustomerDetails]') 
         AND name = 'WednesdayPriority'
)
BEGIN
	ALTER TABLE [dbo].[tbl_OMS_PlanRouteCustomerDetails] ADD WednesdayPriority INT
END

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[tbl_OMS_PlanRouteCustomerDetails]') 
         AND name = 'ThursdayPriority'
)
BEGIN
	ALTER TABLE [dbo].[tbl_OMS_PlanRouteCustomerDetails] ADD ThursdayPriority INT
END

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[tbl_OMS_PlanRouteCustomerDetails]') 
         AND name = 'FridayPriority'
)
BEGIN
	ALTER TABLE [dbo].[tbl_OMS_PlanRouteCustomerDetails] ADD FridayPriority INT
END

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[tbl_OMS_PlanRouteCustomerDetails]') 
         AND name = 'SaturdayPriority'
)
BEGIN
	ALTER TABLE [dbo].[tbl_OMS_PlanRouteCustomerDetails] ADD SaturdayPriority INT
END

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[tbl_OMS_PlanRouteCustomerDetails]') 
         AND name = 'SundayPriority'
)
BEGIN
	ALTER TABLE [dbo].[tbl_OMS_PlanRouteCustomerDetails] ADD SundayPriority INT
END