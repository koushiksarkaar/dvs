/****** Object:  StoredProcedure [dbo].[SProc_OMS_InsertUpdateCreditFormBusinessBankRef]    Script Date: 9/20/2017 1:01:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 18 Sep 2017
-- Description:	Insert / Update Creadit Form Business Bank Reference
-- =============================================
CREATE PROCEDURE [dbo].[SProc_OMS_InsertUpdateCreditFormBusinessBankRef]
(
	@BankId int,
	@CreditId int,
	@BankName nvarchar(100),
	@StreetAddress  nvarchar(500),
	@City nvarchar(100),
	@State nvarchar(100),
	@Zip nvarchar(50),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@AccountInName nvarchar(100),
	@ContactPhone nvarchar(50),
	@CreatedBy nvarchar(100),
	@UpdatedBy nvarchar(100),
	@Message int output
)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM tbl_OMS_CreditFormBusinessBankRef WHERE [BankId]=@BankId)
	BEGIN
		SET @Message=1

		UPDATE tbl_OMS_CreditFormBusinessBankRef
		SET 
			BankName = @BankName,
			StreetAddress = @StreetAddress,
			City = @City,
			[State] = @State,
			Zip = @Zip,
			Phone = @Phone,
			Fax = @Fax,
			AccountInName = @AccountInName,
			ContactPhone = @ContactPhone,
			UpdatedDate = SYSDATETIME(),
			UpdatedBy = @UpdatedBy
		WHERE
			BankId = @BankId
	END
	ELSE
	BEGIN
		SET @Message=0

		INSERT INTO tbl_OMS_CreditFormBusinessBankRef
		(
			CreditId,
			BankName,
			StreetAddress,
			City,
			[State],
			Zip,
			Phone,
			Fax,
			AccountInName,
			ContactPhone,
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@CreditId,
			@BankName,
			@StreetAddress,
			@City,
			@State,
			@Zip,
			@Phone,
			@Fax,
			@AccountInName,
			@ContactPhone,
			SYSDATETIME(),
			@CreatedBy
		)
	END
END
