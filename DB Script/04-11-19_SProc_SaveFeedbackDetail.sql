IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[SProc_SaveFeedbackDetail]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[SProc_SaveFeedbackDetail]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SProc_SaveFeedbackDetail]        
	(
		@CompanyId varchar(10),
		@BrokerId varchar(10),
		@BrokerName varchar(100),
		@BrokerEmail varchar(100),
		@BrokerPhoneNumber varchar(100),
		@CustomerId varchar(100),
		@CustomerName varchar(100),
		@Comments varchar(100),
		@FileName varchar(100),
		@CreatedDate datetime,
		@CreatedBy varchar(100)
	)
AS
BEGIN
	declare @FileURL nvarchar(max)
	set @FileURL = 'https://portal.goya.com/OMSDocuments/FeedbackForm/' + @CompanyId + '/' + @FileName

	if(@FileName is null or @FileName = '')
	begin
		insert into dbo.tbl_OMS_FeedbackDetail
			(
				[BrokerId],
				[BrokerName],
				[BrokerEmail],
				[BrokerPhoneNumber],
				[CustomerId],
				[CustomerName],
				[Comments],
				[FileName],
				[FileURL],
				[CreatedDate],
				[CreatedBy],
				[ModifiedDate],
				[ModifiedBy]
			)
		values
			(
				@BrokerId,
				@BrokerName,
				@BrokerEmail,
				@BrokerPhoneNumber,
				@CustomerId,
				@CustomerName,
				@Comments,
				@FileName,
				'',
				@CreatedDate,
				@CreatedBy,
				null,
				null
			)
	end
	else
	begin
		insert into dbo.tbl_OMS_FeedbackDetail
			(
				[BrokerId],
				[BrokerName],
				[BrokerEmail],
				[BrokerPhoneNumber],
				[CustomerId],
				[CustomerName],
				[Comments],
				[FileName],
				[FileURL],
				[CreatedDate],
				[CreatedBy],
				[ModifiedDate],
				[ModifiedBy]
			)
		values
			(
				@BrokerId,
				@BrokerName,
				@BrokerEmail,
				@BrokerPhoneNumber,
				@CustomerId,
				@CustomerName,
				@Comments,
				@FileName,
				@FileURL,
				@CreatedDate,
				@CreatedBy,
				null,
				null
			)
	end
	
	select top 1 * 
	from [dbo].[tbl_OMS_FeedbackDetail]
	where [FileName] = @FileName
END