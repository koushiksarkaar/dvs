USE [OAuthServiceDB]
GO
/****** Object:  StoredProcedure [dbo].[sp_ResetReleaseUpdateFlag]    Script Date: 9/30/2017 3:55:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[sp_ResetReleaseUpdateFlag]  
(
	@userId nvarchar(50) 
)
as  
begin 
   UPDATE [dbo].[AspNetUsers]
   SET IsNewReleaseFlag = 0
   WHERE UserName = @userId
	
       
End
 