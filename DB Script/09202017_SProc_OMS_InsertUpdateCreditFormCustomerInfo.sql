
/****** Object:  StoredProcedure [dbo].[SProc_OMS_InsertUpdateCreditFormCustomerInfo]    Script Date: 10/5/2017 11:38:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 4th Oct 2017
-- Description:	Insert Update Creadit Form Customer Info 
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_InsertUpdateCreditFormCustomerInfo]
(
	@CreditId int,
	@BrokerId nvarchar(50),
	@CorporateName nvarchar(100),
	@TradeName nvarchar(100),
	@WeeklyOrderAmount decimal(18, 2),
	@FirstOrderAmount decimal(18, 2),
	@DeliveryStreetAddress nvarchar(500),
	@DeliveryCity nvarchar(100),
	@DeliveryState nvarchar(100),
	@DeliveryZip nvarchar(50),
	@DeliveryFax nvarchar(50),
	@DeliveryEmail nvarchar(100),
	@FederalTaxIdNo nvarchar(100),
	@StateResaleCertificateNo nvarchar(100),
	@StoreTelephone nvarchar(50),
	@CellPhoneNo nvarchar(50),
	@ReceivingDaysHours nvarchar(100),
	@BillingStreetAdress nvarchar(500),
	@BillingCity nvarchar(100),
	@BillingState nvarchar(100),
	@BillingZip nvarchar(50),
	@BillingTelephone nvarchar(50),
	@BillingFax nvarchar(50),
	@AccountPayableManager nvarchar(100),
	@AccountPayablePhone nvarchar(50),
	@BusinessType nvarchar(100),
	@PeriodAtLocation nvarchar(100),
	@PeriodInBusiness nvarchar(100),
	@CorporationYear nvarchar(100),
	@InsuranceCompany nvarchar(100),
	@CompanySreetAddress nvarchar(500),
	@CompanyCity nvarchar(100),
	@CompanyState nvarchar(100),
	@CompanyZip nvarchar(50),
	@Policy nvarchar(max),
	@Status nvarchar(200),
	@CreatedBy nvarchar(100),
	@UpdatedBy nvarchar(100),
	@Message int output
)
AS
BEGIN
	
	IF EXISTS(SELECT 1 FROM tbl_OMS_CreditFormCustomerInfo WHERE [CreditId]=@CreditId)
	BEGIN
		SET @Message=1

		UPDATE tbl_OMS_CreditFormCustomerInfo
		SET
			BrokerId = @BrokerId,
			CorporateName = @CorporateName,
			TradeName = @TradeName,
			WeeklyOrderAmount = @WeeklyOrderAmount,
			FirstOrderAmount = @FirstOrderAmount,
			DeliveryStreetAddress = @DeliveryStreetAddress,
			DeliveryCity = @DeliveryCity,
			DeliveryState = @DeliveryState,
			DeliveryZip = @DeliveryZip,
			DeliveryFax = @DeliveryFax,
			DeliveryEmail = @DeliveryEmail,
			FederalTaxIdNo = @FederalTaxIdNo,
			StateResaleCertificateNo = @StateResaleCertificateNo,
			StoreTelephone = @StoreTelephone,
			CellPhoneNo = @CellPhoneNo,
			ReceivingDaysHours = @ReceivingDaysHours,
			BillingStreetAdress = @BillingStreetAdress,
			BillingCity = @BillingCity,
			BillingState = @BillingState,
			BillingZip = @BillingZip,
			BillingTelephone = @BillingTelephone,
			BillingFax = @BillingFax,
			AccountPayableManager = @AccountPayableManager,
			AccountPayablePhone = @AccountPayablePhone,
			BusinessType = @BusinessType,
			PeriodAtLocation = @PeriodAtLocation,
			PeriodInBusiness = @PeriodInBusiness,
			CorporationYear = @CorporationYear,
			InsuranceCompany = @InsuranceCompany,
			CompanySreetAddress = @CompanySreetAddress,
			CompanyCity = @CompanyCity,
			CompanyState = @CompanyState,
			CompanyZip = @CompanyZip,
			Policy = @Policy,
			[Status] = @Status,
			UpdatedDate = SYSDATETIME(),
			UpdatedBy = @UpdatedBy
		WHERE
			CreditId = @CreditId
	END
	ELSE
	BEGIN
		SET @Message=0

		INSERT INTO tbl_OMS_CreditFormCustomerInfo
		(
			BrokerId,
			CorporateName,
			TradeName,
			WeeklyOrderAmount,
			FirstOrderAmount,
			DeliveryStreetAddress,
			DeliveryCity,
			DeliveryState,
			DeliveryZip,
			DeliveryFax,
			DeliveryEmail,
			FederalTaxIdNo,
			StateResaleCertificateNo,
			StoreTelephone,
			CellPhoneNo,
			ReceivingDaysHours,
			BillingStreetAdress,
			BillingCity,
			BillingState,
			BillingZip,
			BillingTelephone,
			BillingFax,
			AccountPayableManager,
			AccountPayablePhone,
			BusinessType,
			PeriodAtLocation,
			PeriodInBusiness,
			CorporationYear,
			InsuranceCompany,
			CompanySreetAddress,
			CompanyCity,
			CompanyState,
			CompanyZip,
			Policy,
			[Status],
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@BrokerId,
			@CorporateName,
			@TradeName,
			@WeeklyOrderAmount,
			@FirstOrderAmount,
			@DeliveryStreetAddress,
			@DeliveryCity,
			@DeliveryState,
			@DeliveryZip,
			@DeliveryFax,
			@DeliveryEmail,
			@FederalTaxIdNo,
			@StateResaleCertificateNo,
			@StoreTelephone,
			@CellPhoneNo,
			@ReceivingDaysHours,
			@BillingStreetAdress,
			@BillingCity,
			@BillingState,
			@BillingZip,
			@BillingTelephone,
			@BillingFax,
			@AccountPayableManager,
			@AccountPayablePhone,
			@BusinessType,
			@PeriodAtLocation,
			@PeriodInBusiness,
			@CorporationYear,
			@InsuranceCompany,
			@CompanySreetAddress,
			@CompanyCity,
			@CompanyState,
			@CompanyZip,
			@Policy,
			@Status,
			SYSDATETIME(),
			@CreatedBy
		)
	END
END
