IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[SProc_GetFeedbackDetailByBrokerId]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[SProc_GetFeedbackDetailByBrokerId]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SProc_GetFeedbackDetailByBrokerId]        
	(
		@BrokerId varchar(10)
	)
AS
BEGIN
	select top 1 * 
	from dbo.tbl_OMS_FeedbackDetail
	where BrokerId = @BrokerId
	order by CreatedDate desc
END