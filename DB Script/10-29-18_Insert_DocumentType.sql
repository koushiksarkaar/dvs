If NOT EXISTS (SELECT 1 FROM tbl_oms_DocumentType WHERE Description = 'Report')
BEGIN
INSERT INTO tbl_oms_DocumentType
           (Description
           ,CreatedDate
           ,UpdatedDate
           ,CreatedBy
           ,UpdatedBy
           ,IsActive
           ,ModuleId)
     VALUES
           ('Report'
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
END