ALTER PROCEDURE [dbo].[SP_GetPlanRouteCustomerDetails]
 (
    @CompanyId nvarchar(10),
	@BrokerId nvarchar(50)
 )
AS
BEGIN

		SET NOCOUNT ON;
	Declare @UserId nvarchar(10);
	set @UserId = (select substring (@BrokerId,3,4));
 
	Select C.CSNO AS CustomerId, C.CSNAME AS CustomerName, 
	ISNULL(C.CSADR2+', ','')
	+ISNULL(C.CSCITY+', ','')
	+ISNULL(C.CSST+', ','')
	+LTRIM(ISNULL(C.CSZIP,'')) AS Address,
	Monday,Tuesday, Wednesday,Thursday,Friday,Saturday,Sunday
	From [Goya_UtilDB].[dbo].[vw_CustomerDetail] C
	left join TBL_OMS_PlanRouteCustomerDetails P
	  on C.CSNO collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
	Where csco=@CompanyId and BRROUT = @UserId AND CSACT='A';
END