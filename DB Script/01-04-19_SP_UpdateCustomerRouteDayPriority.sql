SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_UpdateCustomerRouteDayPriority]
(
   @BrokerId nvarchar(50),
   @CustomerId nvarchar(50),
   @Priority int,
   @Day nvarchar(20)
)
AS
BEGIN
	SET NOCOUNT ON;

	if(@Day = 'Monday')
		update tbl_OMS_PlanRouteCustomerDetails
		set MondayPriority = @Priority
		where BrokerId = @BrokerId and CustomerId = @CustomerId
	else if(@Day = 'Tuesday')
		update tbl_OMS_PlanRouteCustomerDetails
		set TuesdayPriority = @Priority
		where BrokerId = @BrokerId and CustomerId = @CustomerId
	else if(@Day = 'Wednesday')
		update tbl_OMS_PlanRouteCustomerDetails
		set WednesdayPriority = @Priority
		where BrokerId = @BrokerId and CustomerId = @CustomerId
	else if(@Day = 'Thursday')
		update tbl_OMS_PlanRouteCustomerDetails
		set ThursdayPriority = @Priority
		where BrokerId = @BrokerId and CustomerId = @CustomerId
	else if(@Day = 'Friday')
		update tbl_OMS_PlanRouteCustomerDetails
		set FridayPriority = @Priority
		where BrokerId = @BrokerId and CustomerId = @CustomerId
	else if(@Day = 'Saturday')
		update tbl_OMS_PlanRouteCustomerDetails
		set SaturdayPriority = @Priority
		where BrokerId = @BrokerId and CustomerId = @CustomerId
	else if(@Day = 'Sunday')
		update tbl_OMS_PlanRouteCustomerDetails
		set SundayPriority = @Priority
		where BrokerId = @BrokerId and CustomerId = @CustomerId

END

 -- [SP_UpdateCustomerRouteDayPriority] '013506', '712450', 1, 'Tuesday'