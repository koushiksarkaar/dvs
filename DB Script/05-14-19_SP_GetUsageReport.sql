IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[SP_GetUsageReport]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[SP_GetUsageReport]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetUsageReport]        
	(
		 @CompanyId nvarchar(10),
		 @DivisionId nvarchar(10) = null,
		 @GroupId nvarchar(10) = null,
		 @BrokerId nvarchar(10)=null,
		 @FromDate nvarchar(100),
		 @ToDate nvarchar(100)
	)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @BrokerString VARCHAR(MAX), @Sql VARCHAR(MAX);

	--   exec [SP_GetUsageReport] '01',null, null, null, '04/01/2018', '04/24/2019'

	Select  @BrokerString = COALESCE(@BrokerString +',','') +''''+ rtrim(B.USERNO) +''''
         From [Goya_UtilDB].[dbo].[vw_User_Master_Data] B
         where B.COMPANY = @CompanyId         
           AND (( B.[BrokerDivisionCode]  = @DivisionId OR @DivisionId is NULL OR @DivisionId=''))
             AND (( B.[BrokerGroupCode] = @GroupId OR @GroupId is NULL OR @GroupId=''))
         AND (( B.[BRROUT] = @BrokerId OR @BrokerId is NULL OR @BrokerId=''))

		 CREATE TABLE #tbl_OMS_Order_temp(
			[PONumber] [nvarchar](50) NOT NULL,
			[ClientOPNumber] [nvarchar](50) NULL,
			[BasketID] [nvarchar](25) NOT NULL,
			[BasketName] [nvarchar](200) NULL,
			[OrderDate] [datetime] NOT NULL,
			[ShippingDate] [datetime] NULL,
			[CancelDate] [datetime] NULL,
			[ActualAmount] [numeric](18, 2) NULL,
			[BillAmount] [numeric](18, 2) NULL,
			[TotalDiscount] [numeric](18, 2) NULL,
			[CanceledBy] [nvarchar](30) NULL CONSTRAINT [DF_tbl_OMS_Order_CanceledBy]  DEFAULT ((0)),
			[CanceledDate] [datetime] NULL CONSTRAINT [DF_tbl_OMS_Order_CanceledDate]  DEFAULT ((0)),
			[DeletedBy] [nvarchar](30) NULL CONSTRAINT [DF_tbl_OMS_Order_DeletedBy]  DEFAULT ((0)),
			[DeletedDate] [datetime] NULL,
			[Comments] [nvarchar](500) NULL,
			[SBTag] [bit] NULL,
			[ReciveBy] [nvarchar](50) NULL,
			[ReceiveDate] [datetime] NULL,
			[PLPass] [int] NULL,
			[PLAppBy] [nvarchar](30) NULL,
			[PLAppDate] [datetime] NULL,
			[INVPass] [int] NULL,
			[INVAppBy] [nvarchar](30) NULL,
			[INVAppDate] [datetime] NULL,
			[DespatchBy] [nvarchar](30) NULL,
			[DespatchDate] [datetime] NULL,
			[DeliveredBy] [nvarchar](30) NULL,
			[DeliveredDate] [datetime] NULL,
			[Status] [nvarchar](50) NULL CONSTRAINT [DF_tbl_OMS_Order_Status]  DEFAULT ('Pending'),
			[OrderDay] [nvarchar](3) NULL,
			[NotDay] [nvarchar](3) NULL,
			[Pickup] [bit] NULL,
			[OrderQty] [int] NULL,
			[ProductType] [nvarchar](50) NULL DEFAULT (''),
			[OrderNo] [nvarchar](50) NULL DEFAULT (''),
			[OrderStatus] [varchar](50) NULL DEFAULT (''),
			[ID] [int] NOT NULL,
			[OrderSent] [datetime] NULL,
			[Invoice_Number] [varchar](25) NULL,
			[TotUnits] [int] NULL,
			[Latitude] [decimal](12, 9) NULL,
			[Longitude] [decimal](12, 9) NULL,
			[Location] [varchar](max) NULL,
			[SalesmanId] [nvarchar](200) NULL,
			[CustomerId] [nvarchar](200) NULL,
			[OrderAmount] [decimal](18, 2) NULL,
			[ErrorDescription] [nvarchar](max) NULL,
			[Deviceinfo] [nvarchar](max) NULL,
			[OrderStartDate] [datetime] NULL
		)

		CREATE TABLE #tbl_OMS_SalesCommunicator_temp(
			[Srl] [int] NOT NULL,
			[WeekNo] [int] NOT NULL,
			[Year] [varchar](10) NOT NULL,
			[StartDate] [datetime] NULL,
			[EndDate] [datetime] NULL,
			[Active] [bit] NULL,
			[PdfFile] [nvarchar](1000) NULL,
			[FileName] [varchar](500) NULL,
			[CreatedBy] [varchar](50) NULL,
			[CreatedOn] [datetime2](7) NULL,
			[UpdatedBy] [varchar](50) NULL,
			[UpdatedOn] [datetime2](7) NULL,
			[IsDelete] [bit] NULL DEFAULT ((0)),
			[Comments] [nvarchar](200) NULL DEFAULT (''),
			[DocumentType] [nvarchar](max) NULL,
			[Zone] [nvarchar](10) NULL,
			[CompanyId] [nvarchar](5) NULL,
			[Tags] [nvarchar](max) NULL,
			[DivisionId] [nvarchar](500) NULL,
			[GroupId] [nvarchar](500) NULL,
			[BrokerId] [varchar](50) NULL
		)

		CREATE TABLE #tbl_OMS_LOG_temp(
			[TrackID] [int] NOT NULL,
			[UserID] [varchar](50) NULL,
			[ControlType] [nvarchar](max) NULL,
			[ActionName] [nvarchar](max) NULL,
			[ActionDate] [datetime] NULL,
			[DeviceType] [nvarchar](50) NULL,
			[Value] [nvarchar](max) NULL,
			[PageURL] [nvarchar](max) NULL,
			[PageName] [nvarchar](50) NULL,
			[ControlID] [nvarchar](50) NULL,
			[ControlName] [nvarchar](50) NULL,
			[BrowserName] [nvarchar](50) NULL,
			[BrowserVersion] [nvarchar](50) NULL,
			[Note] [nvarchar](max) NULL,
			[Duration] [nvarchar](500) NULL,
			[Functionality] [nvarchar](max) NULL,
			[OrderNo] [nvarchar](200) NULL
		) 

		CREATE TABLE #tbl_OMS_PlanRouteCustomerDetails_temp(
			[RouteId] [int] NOT NULL,
			[CompanyId] [nvarchar](50) NULL,
			[BrokerId] [nvarchar](50) NULL,
			[CustomerId] [nvarchar](50) NULL,
			[Monday] [bit] NULL,
			[Tuesday] [bit] NULL,
			[Wednesday] [bit] NULL,
			[Thursday] [bit] NULL,
			[Friday] [bit] NULL,
			[Saturday] [bit] NULL,
			[Sunday] [bit] NULL,
			[Plandate] [datetime] NULL,
			[CreatedDate] [datetime] NULL,
			[UpdatedDate] [datetime] NULL,
			[CreatedBy] [nvarchar](50) NULL,
			[UpdatedBy] [nvarchar](50) NULL,
			[MondayPriority] [int] NULL,
			[TuesdayPriority] [int] NULL,
			[WednesdayPriority] [int] NULL,
			[ThursdayPriority] [int] NULL,
			[FridayPriority] [int] NULL,
			[SaturdayPriority] [int] NULL,
			[SundayPriority] [int] NULL
		)
	
	set @sql = '
	insert into #tbl_OMS_Order_temp 
	select *
	from GoyaDB2.dbo.tbl_OMS_Order
	where OrderDate between ' + '''' + @FromDate + '''' + ' and ' + '''' + @ToDate + '''' + ' 
	and SalesmanId in(' + @BrokerString +')' --Create filtered temp order table

	exec(@sql)

	set @sql = '
	insert into #tbl_OMS_SalesCommunicator_temp 
	select *
	from GoyaDB2.dbo.tbl_OMS_SalesCommunicator
	where ((CreatedOn between ' + '''' + @FromDate + '''' + ' and ' + '''' + @ToDate + '''' + ')
	or (UpdatedOn between ' + '''' + @FromDate + '''' + ' and ' + '''' + @ToDate + '''' + '))
	and BrokerId in(' + @BrokerString +')
	and BrokerId <> null and BrokerId !=''''' --Create filtered temp sales communicator table

	exec(@sql)

	set @sql = '
	insert into #tbl_oms_log_temp 
	select *
	from GoyaDB2.dbo.tbl_oms_log
	where ActionDate between ' + '''' + @FromDate + '''' + ' and ' + '''' + @ToDate + '''' + ' 
	and Userid in(' + @BrokerString +')' --Create filtered temp log table

	exec(@sql)

	set @sql = '
	insert into #tbl_OMS_PlanRouteCustomerDetails_temp 
	select *
	from GoyaDB2.dbo.tbl_OMS_PlanRouteCustomerDetails
	where ((CreatedDate between ' + '''' + @FromDate + '''' + ' and ' + '''' + @ToDate + '''' + ')
	or (UpdatedDate between ' + '''' + @FromDate + '''' + ' and ' + '''' + @ToDate + '''' + '))
	and BrokerId in(' + @BrokerString +')' --Create filtered temp route planning table

	exec(@sql)

	/* Get Order Count from Smart Order Page UAT */
	Select O.SalesmanId,Count(*) SmartOrder into #SmartOrderCount from #tbl_OMS_Order_temp O 
    Where O.Deviceinfo like('%Smart Order%')
	  --and O.SalesmanId in (@BrokerId, null,'')
	Group By SalesmanId;

    /*  Get Order Count from Create Order Page UAT */
	Select O.SalesmanId,Count(*) CreateOrderUAT into #CreateOrderCountUAT from #tbl_OMS_Order_temp O 
    Where O.Deviceinfo like('%Create Order%') AND O.Deviceinfo like('%OMSUAT%')
	Group By SalesmanId;

	/*  Get Order Count from Create Order Prod */
	Select O.SalesmanId,Count(*) as CreateOrderProd into #CreateOrderCountProd from #tbl_OMS_Order_temp O 
    Where O.Deviceinfo not like('%Create Order%') AND O.Deviceinfo not like('%http%')
	  --and O.OrderDate > ('03/21/2019')
	Group By SalesmanId;
	
    /*  Get Order Count from Create Order Tablet */
	Select O.SalesmanId,Count(*) CreateOrderTablet into #CreateOrderCountTablet from #tbl_OMS_Order_temp O 
    Where O.Deviceinfo like '%Chrome 4%' and O.Deviceinfo not like '%Desktop%'
	  --and O.OrderDate > ('03/21/2019')
	Group By SalesmanId;
	
    /*  Get Order Count from Create Order Non-Tablet */
	Select O.SalesmanId,Count(*) CreateOrderNonTablet into #CreateOrderCountNonTablet from #tbl_OMS_Order_temp O 
    Where O.Deviceinfo not like '%Chrome 4%' or O.Deviceinfo like '%Desktop%'
	  --and O.OrderDate > ('03/21/2019')
	Group By SalesmanId;

	/*Add Statement to Briefcase count */ 
	select  BrokerId as SalesmanId ,count(*) as briefStatement  into #briefStatement from #tbl_OMS_SalesCommunicator_temp
	Where DocumentType like '%statement%'
	Group by brokerid;

	 /*Add Price sheet to Briefcase count */ 
	select  BrokerId as SalesmanId ,count(*) as briefPriceList  into #briefPriceList from #tbl_OMS_SalesCommunicator_temp
	Where DocumentType like '%Smart Price Sheet%'
	Group by brokerid;

	/*Smart Order - View and Create Price catalog in PDF */
	Select Userid as SalesmanId,Count(*) SmartPdfView into #smartPDFView from #tbl_oms_log_temp 
	Where Functionality like '%SmartOrder To PDF%'
	Group By UserId; 

	Select Distinct BrokerId as SalesmanId, 'Yes' as Created INTO #routeCreate
	 from #tbl_OMS_PlanRouteCustomerDetails_temp

	/*Route Plan Screen - Brokers visited Plan Tab */
	Select Userid as SalesmanId,Count(*) planRouteTab into #planRouteTab from #tbl_oms_log_temp 
	Where  Functionality like '%RoutePlaning PlanRoute%'
	Group By UserId; 

		/*Route Plan Screen - Brokers visited Calendar Tab */
	Select Userid as SalesmanId,Count(*) calendarRouteTab into #calendarRouteTab from #tbl_oms_log_temp 
	Where  Functionality like '%RoutePlaning Calendar%' OR Functionality like '%Route Page Load%'
	Group By UserId;

    /*Brokers who visited Cash Management Link */
	Select Userid as SalesmanId,Count(*) cashMgtCount into #cashMgtCount from #tbl_oms_log_temp 
	Where  Functionality like '%collect%'
	Group By UserId; 
	
	set @Sql = '
	 Select B.COMPANY,B.USERNO,B.USERNAME,B.BrokerDivisionDesc,B.BrokerGroupCode,
	        SO.SalesmanId, 
	        SO.SmartOrder,
			COU.CreateOrderUAT,
			COP.CreateOrderProd,
			COCT.CreateOrderTablet,
			COCNT.CreateOrderNonTablet,
			BS.briefStatement ,
			BP.briefPriceList,
			SPV.SmartPdfView,
			RC.Created,
			PRT.planRouteTab,
			CRT.calendarRouteTab,
			CMC.cashMgtCount
	 From [Goya_UtilDB].[dbo].[vw_User_Master_Data] B  
        LEFT  JOIN  #SmartOrderCount SO   
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS  = SO.SalesmanId collate SQL_Latin1_General_CP1_CI_AS 
	   LEFT  JOIN  #CreateOrderCountUAT  COU           
	        ON B.USERNO  collate SQL_Latin1_General_CP1_CI_AS= COU.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #CreateOrderCountProd COP            
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = COP.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #CreateOrderCountTablet COCT            
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = COCT.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #CreateOrderCountNonTablet COCNT            
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = COCNT.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #briefStatement BS                  
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = BS.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #briefPriceList BP                   
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = BP.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #smartPDFView SPV                     
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = SPV.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #routeCreate RC                      
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = RC.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #planRouteTab PRT                    
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = PRT.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN  #calendarRouteTab CRT                 
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = CRT.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
		LEFT  JOIN #cashMgtCount  CMC        
		    ON B.USERNO collate SQL_Latin1_General_CP1_CI_AS = CMC.SalesmanId collate SQL_Latin1_General_CP1_CI_AS
	where B.USERNO in(' + @BrokerString + ')'

	exec(@Sql)
END