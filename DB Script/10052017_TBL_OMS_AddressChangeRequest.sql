USE [GoyaProd]
GO

/****** Object:  Table [dbo].[TBL_OMS_AddressChangeRequest]    Script Date: 10/6/2017 2:12:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TBL_OMS_AddressChangeRequest](
	[ReqID] [int] IDENTITY(1,1) NOT NULL,
	[SalesmanID] [nvarchar](50) NULL,
	[CustomerID] [nvarchar](50) NULL,
	[StreetAddress1] [nvarchar](max) NULL,
	[StreetAddress2] [nvarchar](max) NULL,
	[City] [nvarchar](300) NULL,
	[State] [nvarchar](300) NULL,
	[Zip] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Status] [nvarchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedBy] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


