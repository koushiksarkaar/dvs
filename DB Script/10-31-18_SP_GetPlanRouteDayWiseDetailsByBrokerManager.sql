CREATE PROCEDURE [dbo].[SP_GetPlanRouteDayWiseDetailsByBrokerManager]
(
	@CompanyId nvarchar(10),
	@BrokerID  nvarchar(50)	 
)
AS

BEGIN

	SET NOCOUNT ON;
    Declare @groupID nvarchar(50);
    Declare @NewSalesmanID nvarchar(50);
    Declare @GoyaCompanyId nvarchar(5);
    
    /* Check for Super user and give access to all customer under Super User */
	  SET @groupID = (SELECT SUBSTRING(@BrokerID,5,2)) ;	  
	  set @GoyaCompanyId = (SELECT SUBSTRING(@BrokerID,1,2)) ;	  
	   if(@groupID = '01')
	     set @NewSalesmanID = (SELECT SUBSTRING(@BrokerID,3,2));
	   ELSE
	       set @NewSalesmanID  = @BrokerID
	
      Select (COMPANY+''+BRROUT) as UserID,USERNAME AS Name into #Brokers from [Goya_UtilDB].[dbo].[vw_User_Master_Data] 
      where BRROUT like  @NewSalesmanID+'%%' and @GoyaCompanyId = COMPANY
      
	  
 
	Declare @UserId nvarchar(10);
	Set @UserId = (select substring (@BrokerId,3,4));

	select B.Name, C.CSNO as CustomerId,(C.CSNO+'-'+C.CSNAME) as CustomerName into #CustName From [Goya_UtilDB].[dbo].[vw_CustomerDetail] C 
	inner join #Brokers as B on C.CSCO = @CompanyId and C.BRROUT LIKE '%' + SUBSTRING(B.UserID, 3, 6) + '%' and c.CSACT = 'A'

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) as [Date],C.CustomerName as CustomerInfo, ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) over() as 'Count', B.Name into #tempMonday From TBL_OMS_PlanRouteCustomerDetails P
   inner join #CustName C on P.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%'
   left join tbl_OMS_Order O on O.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%' and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0)
   left join #Brokers B on P.BrokerId collate  Latin1_General_CS_AS like '%' + B.UserID + '%'
   where Monday = 1 and CompanyId = @CompanyId --and BrokerId like '%' + B.UserID collate  Latin1_General_CS_AS + '%' unnecessary
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)),C.CustomerName, B.Name;

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 1) as [Date],C.CustomerName as CustomerInfo, ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) over() as 'Count', B.Name into #tempTuesday From TBL_OMS_PlanRouteCustomerDetails P
   inner join #CustName C on P.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%'
   left join tbl_OMS_Order O on O.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%' and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 1)
   left join #Brokers B on P.BrokerId collate  Latin1_General_CS_AS like '%' + B.UserID + '%'
   where Tuesday = 1 and CompanyId = @CompanyId --and BrokerId like '%' + B.UserID collate  Latin1_General_CS_AS + '%' unnecessary
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)),C.CustomerName, B.Name;

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 2) as [Date],C.CustomerName as CustomerInfo, ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) over() as 'Count', B.Name into #tempWednesday From TBL_OMS_PlanRouteCustomerDetails P
   inner join #CustName C on P.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%'
   left join tbl_OMS_Order O on O.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%' and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 2)
   left join #Brokers B on P.BrokerId collate  Latin1_General_CS_AS like '%' + B.UserID + '%'
   where Wednesday = 1 and CompanyId = @CompanyId --and BrokerId like '%' + B.UserID collate  Latin1_General_CS_AS + '%' unnecessary
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)),C.CustomerName, B.Name;

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 3) as [Date],C.CustomerName as CustomerInfo, ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) over() as 'Count', B.Name into #tempThursday From TBL_OMS_PlanRouteCustomerDetails P
   inner join #CustName C on P.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%'
   left join tbl_OMS_Order O on O.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%' and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 3)
   left join #Brokers B on P.BrokerId collate  Latin1_General_CS_AS like '%' + B.UserID + '%'
   where Thursday = 1 and CompanyId = @CompanyId --and BrokerId like '%' + B.UserID collate  Latin1_General_CS_AS + '%' unnecessary
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)),C.CustomerName, B.Name;

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 4) as [Date],C.CustomerName as CustomerInfo, ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) over() as 'Count', B.Name into #tempFriday From TBL_OMS_PlanRouteCustomerDetails P
   inner join #CustName C on P.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%'
   left join tbl_OMS_Order O on O.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%' and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 4)
   left join #Brokers B on P.BrokerId collate  Latin1_General_CS_AS like '%' + B.UserID + '%'
   where Friday = 1 and CompanyId = @CompanyId --and BrokerId like '%' + B.UserID collate  Latin1_General_CS_AS + '%' unnecessary
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)),C.CustomerName, B.Name;

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 5) as [Date],C.CustomerName as CustomerInfo, ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) over() as 'Count', B.Name into #tempSaturday From TBL_OMS_PlanRouteCustomerDetails P
   inner join #CustName C on P.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%'
   left join tbl_OMS_Order O on O.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%' and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 5)
   left join #Brokers B on P.BrokerId collate  Latin1_General_CS_AS like '%' + B.UserID + '%'
   where Saturday = 1 and CompanyId = @CompanyId --and BrokerId like '%' + B.UserID collate  Latin1_General_CS_AS + '%' unnecessary
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)),C.CustomerName, B.Name;

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 6) as [Date],C.CustomerName as CustomerInfo, ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, COUNT(*) over() as 'Count', B.Name into #tempSunday From TBL_OMS_PlanRouteCustomerDetails P
   inner join #CustName C on P.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%'
   left join tbl_OMS_Order O on O.CustomerId collate  Latin1_General_CS_AS like '%' + c.CustomerId + '%' and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 6)
   left join #Brokers B on P.BrokerId collate  Latin1_General_CS_AS like '%' + B.UserID + '%'
   where Sunday = 1 and CompanyId = @CompanyId --and BrokerId like '%' + B.UserID collate  Latin1_General_CS_AS + '%' unnecessary
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)),C.CustomerName, B.Name;

  Select * from #tempMonday
  UNION ALL
  Select * from #tempTuesday 
  UNION ALL
  Select * from #tempWednesday 
  UNION ALL 
  Select * from #tempThursday   
  UNION ALL
  Select * from #tempFriday   
  UNION ALL
  Select * from #tempSaturday  
  UNION ALL
  Select * from #tempSunday 
END

 --SP_GetPlanRouteDayWiseDetailsByBrokerManager '01', '013501'