IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[SP_UpdateNotificationReadCountByBrokerId]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[SP_UpdateNotificationReadCountByBrokerId]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_UpdateNotificationReadCountByBrokerId]   
	(
		@BrokerId nvarchar(10),
		@NotificationTypeId int
	)     
AS
BEGIN
	SET NOCOUNT ON;

	update tbl_OMS_NotificationDetail
	set NotificationReadCount = NotificationCount
	where BrokerId = @BrokerId
	and NotificationTypeId = @NotificationTypeId
END

--[SP_UpdateNotificationReadCountByBrokerId] '013506', 1