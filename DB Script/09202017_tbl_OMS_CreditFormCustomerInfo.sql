

/****** Object:  Table [dbo].[tbl_OMS_CreditFormCustomerInfo]    Script Date: 10/5/2017 11:36:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_OMS_CreditFormCustomerInfo](
	[CreditId] [int] IDENTITY(1,1) NOT NULL,
	[BrokerId] [nvarchar](50) NULL,
	[CorporateName] [nvarchar](100) NULL,
	[TradeName] [nvarchar](100) NULL,
	[WeeklyOrderAmount] [decimal](18, 2) NULL,
	[FirstOrderAmount] [decimal](18, 2) NULL,
	[DeliveryStreetAddress] [nvarchar](500) NULL,
	[DeliveryCity] [nvarchar](100) NULL,
	[DeliveryState] [nvarchar](100) NULL,
	[DeliveryZip] [nvarchar](50) NULL,
	[DeliveryFax] [nvarchar](50) NULL,
	[DeliveryEmail] [nvarchar](100) NULL,
	[FederalTaxIdNo] [nvarchar](100) NULL,
	[StateResaleCertificateNo] [nvarchar](100) NULL,
	[StoreTelephone] [nvarchar](50) NULL,
	[CellPhoneNo] [nvarchar](50) NULL,
	[ReceivingDaysHours] [nvarchar](100) NULL,
	[BillingStreetAdress] [nvarchar](500) NULL,
	[BillingCity] [nvarchar](100) NULL,
	[BillingState] [nvarchar](100) NULL,
	[BillingZip] [nvarchar](50) NULL,
	[BillingTelephone] [nvarchar](50) NULL,
	[BillingFax] [nvarchar](50) NULL,
	[AccountPayableManager] [nvarchar](100) NULL,
	[AccountPayablePhone] [nvarchar](50) NULL,
	[BusinessType] [nvarchar](100) NULL,
	[PeriodAtLocation] [nvarchar](100) NULL,
	[PeriodInBusiness] [nvarchar](100) NULL,
	[CorporationYear] [nvarchar](100) NULL,
	[InsuranceCompany] [nvarchar](100) NULL,
	[CompanySreetAddress] [nvarchar](500) NULL,
	[CompanyCity] [nvarchar](100) NULL,
	[CompanyState] [nvarchar](100) NULL,
	[CompanyZip] [nvarchar](50) NULL,
	[Policy] [nvarchar](max) NULL,
	[Status] [nvarchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_OMS_CreditFormCustomerInfo] PRIMARY KEY CLUSTERED 
(
	[CreditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


