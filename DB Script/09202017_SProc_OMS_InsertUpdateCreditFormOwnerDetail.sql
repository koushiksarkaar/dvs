/****** Object:  StoredProcedure [dbo].[SProc_OMS_InsertUpdateCreditFormOwnerDetail]    Script Date: 9/20/2017 1:04:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 18 Sep 2017
-- Description:	Insert / Update Creadit Form Owner Detail
-- =============================================
CREATE PROCEDURE [dbo].[SProc_OMS_InsertUpdateCreditFormOwnerDetail]
(
	@OwnerId int,
	@CreditId int,
	@PrincipalName nvarchar(100),
	@StreetAddress nvarchar(500),
	@City nvarchar(100),
	@State nvarchar(100),
	@Zip nvarchar(50),
	@Time time(7),
	@SocialSecurity nvarchar(100),
	@Telephone nvarchar(50),
	@CreatedBy nvarchar(100),
	@UpdatedBy nvarchar(100),
	@Message int output
)
AS
BEGIN

	IF EXISTS(SELECT 1 FROM tbl_OMS_CreditFormOwnerDetail WHERE [OwnerId]=@OwnerId)
	BEGIN
		SET @Message=1

		UPDATE tbl_OMS_CreditFormOwnerDetail
		SET
			PrincipalName = @PrincipalName,
			StreetAddress = @StreetAddress,
			City = @City,
			[State] = @State,
			Zip = @Zip,
			[Time] = @Time,
			SocialSecurity = @SocialSecurity,
			Telephone = @Telephone,
			UpdateDate = SYSDATETIME(),
			UpdatedBy = @UpdatedBy
		WHERE
			OwnerId = @OwnerId
	END
	ELSE
	BEGIN
		SET @Message=0

		INSERT INTO tbl_OMS_CreditFormOwnerDetail
		(
			CreditId,
			PrincipalName,
			StreetAddress,
			City,
			[State],
			Zip,
			[Time],
			SocialSecurity,
			Telephone,
			CreateDate,
			CreatedBy
		)
		VALUES
		(
			@CreditId,
			@PrincipalName,
			@StreetAddress,
			@City,
			@State,
			@Zip,
			@Time,
			@SocialSecurity,
			@Telephone,
			SYSDATETIME(),
			@CreatedBy
		)
	END
END
