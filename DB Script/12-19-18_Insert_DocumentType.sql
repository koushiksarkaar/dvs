If NOT EXISTS (SELECT 1 FROM tbl_oms_DocumentType WHERE Description = 'Price List')
BEGIN
INSERT INTO tbl_oms_DocumentType
           (Description
           ,CreatedDate
           ,UpdatedDate
           ,CreatedBy
           ,UpdatedBy
           ,IsActive
           ,ModuleId)
     VALUES
           ('Price List'
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
END

If NOT EXISTS (SELECT 1 FROM tbl_oms_DocumentType WHERE Description = 'Sales Communicator')
BEGIN
INSERT INTO tbl_oms_DocumentType
           (Description
           ,CreatedDate
           ,UpdatedDate
           ,CreatedBy
           ,UpdatedBy
           ,IsActive
           ,ModuleId)
     VALUES
           ('Sales Communicator'
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
END

If NOT EXISTS (SELECT 1 FROM tbl_oms_DocumentType WHERE Description = 'Marketing')
BEGIN
INSERT INTO tbl_oms_DocumentType
           (Description
           ,CreatedDate
           ,UpdatedDate
           ,CreatedBy
           ,UpdatedBy
           ,IsActive
           ,ModuleId)
     VALUES
           ('Marketing'
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
END

If NOT EXISTS (SELECT 1 FROM tbl_oms_DocumentType WHERE Description = 'Other')
BEGIN
INSERT INTO tbl_oms_DocumentType
           (Description
           ,CreatedDate
           ,UpdatedDate
           ,CreatedBy
           ,UpdatedBy
           ,IsActive
           ,ModuleId)
     VALUES
           ('Other'
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
END