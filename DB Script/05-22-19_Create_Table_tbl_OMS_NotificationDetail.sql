IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES T
        WHERE T.TABLE_SCHEMA = 'dbo'
        AND T.TABLE_NAME = 'tbl_OMS_NotificationDetail')
    BEGIN

        CREATE TABLE dbo.tbl_OMS_NotificationDetail
        (
			[NotificationDetailId] int identity(1,1) primary key,
            [BrokerId] nvarchar(10),
			[NotificationTypeId] int,
			[NotificationCount] int,
			[NotificationReadCount] int,
			[CreatedDate] datetime,
			[CreatedBy] varchar(10),
			[ModifiedDate] datetime,
			[ModifiedBy] varchar(10)
        )
    END
GO