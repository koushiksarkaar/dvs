IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[SP_GetAllBrokers]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[SP_GetAllBrokers]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetAllBrokers]        
AS
BEGIN
	SET NOCOUNT ON;
      Select COMPANY as Companyid, (COMPANY+''+BRROUT) as UserID,USERNAME AS Name from [Goya_UtilDB].[dbo].[vw_User_Master_Data]
END