/****** Object:  StoredProcedure [dbo].[SProc_OMS_InsertUpdateCreditFormBusinessTradeRef]    Script Date: 9/20/2017 1:02:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 18 Sep 2017
-- Description:	Insert / Update Creadit Form Business Trade
-- =============================================
CREATE PROCEDURE [dbo].[SProc_OMS_InsertUpdateCreditFormBusinessTradeRef]
(
	@TradeId int,
	@CreditId int,
	@TradeName nvarchar(100),
	@StreetAddress nvarchar(500),
	@City nvarchar(100),
	@State nvarchar(100),
	@Zip nvarchar(50),
	@PhoneNo nvarchar(50),
	@FaxNo nvarchar(50),
	@Account nvarchar(100),
	@ContactName nvarchar(100),
	@ContactPhoneNo nvarchar(50),
	@RelationshipPeriod nvarchar(100),
	@CreditLimitAmount decimal(18, 2),
	@CreatedBy nvarchar(100),
	@UpdatedBy nvarchar(100),
	@Message int output
)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM tbl_OMS_CreditFormBusinessTradeRef WHERE [TradeId]=@TradeId)
	BEGIN
		SET @Message=1

		UPDATE tbl_OMS_CreditFormBusinessTradeRef
		SET
			TradeName = @TradeName,
			StreetAddress = @StreetAddress,
			City = @City,
			[State] = @State,
			Zip = @Zip,
			PhoneNo = @PhoneNo,
			FaxNo = @FaxNo,
			Account = @Account,
			ContactName = @ContactName,
			ContactPhoneNo = @ContactPhoneNo,
			RelationshipPeriod = @RelationshipPeriod,
			CreditLimitAmount = @CreditLimitAmount,
			UpdatedDate = SYSDATETIME(),
			UpdatedBy = @UpdatedBy
		WHERE
			TradeId = @TradeId
	END
	ELSE
	BEGIN
		SET @Message=0

		INSERT INTO tbl_OMS_CreditFormBusinessTradeRef
		(
			CreditId,
			TradeName,
			StreetAddress,
			City,
			[State],
			Zip,
			PhoneNo,
			FaxNo,
			Account,
			ContactName,
			ContactPhoneNo,
			RelationshipPeriod,
			CreditLimitAmount,
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@CreditId,
			@TradeName,
			@StreetAddress,
			@City,
			@State,
			@Zip,
			@PhoneNo,
			@FaxNo,
			@Account,
			@ContactName,
			@ContactPhoneNo,
			@RelationshipPeriod,
			@CreditLimitAmount,
			SYSDATETIME(),
			@CreatedBy
		)
	END
END
