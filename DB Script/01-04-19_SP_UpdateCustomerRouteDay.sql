ALTER PROCEDURE [dbo].[SP_UpdateCustomerRouteDay]
(
   @CompanyId nvarchar(10),
   @BrokerId nvarchar(50),
   @CustomerId nvarchar(50),
   @Monday bit,
   @Tuesday bit, 
   @Wednesday bit,
   @Thursday bit,
   @Friday bit,
   @Saturday bit,
   @Sunday bit


)
AS
BEGIN

	SET NOCOUNT ON;

	if Exists(select customerid from TBL_OMS_PlanRouteCustomerDetails Where CompanyId = @CompanyId and BrokerId = @BrokerId and CustomerId = @CustomerId) -- If row alrady exists
		BEGIN
				Update TBL_OMS_PlanRouteCustomerDetails
				 SET 
					  Monday=@Monday,
					  Tuesday=@Tuesday,
					  Wednesday=@Wednesday,
					  Thursday=@Thursday,
					  Friday=@Friday,
					  Saturday=@Saturday,
					  Sunday=@Sunday
				Where CompanyId = @CompanyId and BrokerId = @BrokerId and CustomerId = @CustomerId
		END
	ELSE   -- If New customer is checked for route planning
	BEGIN

	     insert into TBL_OMS_PlanRouteCustomerDetails
		 values( @CompanyId,@BrokerId,@CustomerId, @Monday ,@Tuesday ,@Wednesday ,  @Thursday, @Friday , @Saturday, @Sunday ,null,null,null,null,null, null, null, null, null, null, null, null)

	END
END

 