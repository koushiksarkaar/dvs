/****** Object:  Table [dbo].[tbl_OMS_CreditFormExistingAccountDetail]    Script Date: 9/20/2017 1:07:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_OMS_CreditFormExistingAccountDetail](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[CreditId] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[StreetAddress] [nvarchar](500) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Zip] [nvarchar](50) NULL,
	[Account] [nvarchar](100) NULL,
	[Open] [bit] NULL,
	[Closed] [bit] NULL,
	[Sold] [bit] NULL,
	[RelationshipToAccount] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_OMS_CreditFormExistingAccountDetail] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormExistingAccountDetail]  WITH CHECK ADD  CONSTRAINT [FK_tbl_OMS_CreditFormExistingAccountDetail_tbl_OMS_CreditFormCustomerInfo] FOREIGN KEY([CreditId])
REFERENCES [dbo].[tbl_OMS_CreditFormCustomerInfo] ([CreditId])
GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormExistingAccountDetail] CHECK CONSTRAINT [FK_tbl_OMS_CreditFormExistingAccountDetail_tbl_OMS_CreditFormCustomerInfo]
GO


