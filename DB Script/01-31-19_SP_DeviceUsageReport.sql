CREATE PROCEDURE [dbo].[SP_DeviceUsageReport]
(
	@FromDate nvarchar(50),
	@ToDate nvarchar(50),
	@CompanyId nvarchar(50),
	@Division nvarchar(50),
	@GroupId nvarchar(50),
	@BrokerId nvarchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;
	select
	U.COMPANY as Company, 
	O.SalesmanId as 'Broker #', 
	U.USERNAME as Name, 
	U.BrokerGroupCode as 'Group',
	U.BrokerDivisionDesc,
	convert(varchar(10), round((cast(count(case when O.Deviceinfo like '%Chrome 4%' and O.Deviceinfo not like '%Desktop%' then 1 end) as float) / (count(case when O.Deviceinfo like '%Chrome 4%' then 1 end) + count(case when O.Deviceinfo like '%Safari%' then 1 end) + count(case when O.Deviceinfo like '%Android' and O.Deviceinfo not like '%Chrome 4%' then 1 end)) * 100), 2)) + '%' as 'Tablet Orders',
	convert(varchar(10), round((cast(count(case when O.Deviceinfo like '%Safari%' then 1 end) as float) / (count(case when O.Deviceinfo like '%Chrome 4%' then 1 end) + count(case when O.Deviceinfo like '%Safari%' then 1 end) + count(case when O.Deviceinfo like '%Android' and O.Deviceinfo not like '%Chrome 4%' then 1 end)) * 100), 2)) + '%' as 'iOS Orders',
	convert(varchar(10), round((cast(count(case when O.Deviceinfo like '%Android%' and O.Deviceinfo not like '%Chrome 4%' then 1 end) as float) / (count(case when O.Deviceinfo like '%Chrome 4%' then 1 end) + count(case when O.Deviceinfo like '%Safari%' then 1 end) + count(case when O.Deviceinfo like '%Android' and O.Deviceinfo not like '%Chrome 4%' then 1 end)) * 100), 2)) + '%' as 'Android Orders',
	convert(varchar(10), round((cast(count(case when O.Deviceinfo like '%Desktop%' then 1 end) as float) / (count(case when O.Deviceinfo like '%Chrome 4%' then 1 end) + count(case when O.Deviceinfo like '%Safari%' then 1 end) + count(case when O.Deviceinfo like '%Android' and O.Deviceinfo not like '%Chrome 4%' then 1 end)) * 100), 2)) + '%' as 'Desktop Orders',
	count(case when O.Deviceinfo like '%Chrome 4%' and O.Deviceinfo not like '%Desktop%' then 1 end) as 'Number of Tablet Orders',
	count(case when O.Deviceinfo like '%Safari%' and O.Deviceinfo not like '%Desktop%' then 1 end) as 'Number of iOS Number Orders',
	count(case when O.Deviceinfo like '%Android' and O.Deviceinfo not like '%Chrome 4%' then 1 end) as 'Number of Android Orders',
	count(case when O.Deviceinfo like '%Desktop%' then 1 end) as 'Number of Desktop Orders'
	from [GoyaDB2].[dbo].[tbl_OMS_Order] O
	inner join [Goya_UtilDB].[dbo].[vw_User_Master_Data] U on U.USERNO collate SQL_Latin1_General_CP1_CI_AS = O.SalesmanId
	where OrderDate between @FromDate and @ToDate and COMPANY is not null and O.Deviceinfo not like '%en-US%'
	and substring(SalesmanId,1,2) in (select * from fnSplitString(@CompanyId, ','))
	and (@Division is null or BrokerDivisionCode = @Division)
	and (@GroupId is null or BrokerGroupCode = @GroupId)
	and (@BrokerId is null or SalesmanId = @BrokerId)
	group by U.COMPANY, 
	O.SalesmanId, 
	U.USERNAME, 
	U.BrokerGroupCode, 
	U.BrokerDivisionDesc
END

--SP_DeviceUsageReport '2019-01-21', '2019-01-26', '15,20', '1', '10', '151020'