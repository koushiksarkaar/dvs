/****** Object:  StoredProcedure [dbo].[SProc_OMS_InsertUpdateAddressChangeRequest]    Script Date: 10/6/2017 12:38:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 5th Oct 2017
-- Description:	Insert Update Address Change Request
-- =============================================
ALTER procedure [dbo].[SProc_OMS_InsertUpdateAddressChangeRequest]
(
	@ReqID INT = NULL,
	@UserID NVARCHAR(50) = NULL,
	@CustomerID NVARCHAR(50) = NULL,
	@StreetAddress1 NVARCHAR(MAX) = NULL,
	@StreetAddress2 NVARCHAR(MAX) = NULL,
	@City NVARCHAR(300) = NULL,
	@State NVARCHAR(300) = NULL,
	@Zip NVARCHAR(50) = NULL,
	@Phone NVARCHAR(50) = NULL,
	@Status NVARCHAR(200) = NULL,
	@CreatedBy NVARCHAR(100) = NULL,
	@UpdatedBy NVARCHAR(100) = NULL
 )
AS
BEGIN 
	IF EXISTS(SELECT 1 FROM TBL_OMS_AddressChangeRequest WHERE [ReqID]=@ReqID)
	BEGIN
		UPDATE 
			TBL_OMS_AddressChangeRequest
		SET
			SalesmanID = @UserID,
			CustomerID = @CustomerID,
			StreetAddress1 = @StreetAddress1,
			StreetAddress2 = @StreetAddress2,
			City = @City,
			[State] = @State,
			Zip = @Zip,
			Phone = @Phone,
			[Status] = @Status,
			UpdatedDate = SYSDATETIME(),
			UpdatedBy = @UpdatedBy
		WHERE 
			[ReqID]=@ReqID
	END
	ELSE
	BEGIN
		INSERT INTO TBL_OMS_AddressChangeRequest
		(
			SalesmanID,
			CustomerID,
			StreetAddress1,
			StreetAddress2,
			City,
			[State],
			Zip,
			Phone,
			[Status],
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@UserID,
			@CustomerID,
			@StreetAddress1,
			@StreetAddress2,
			@City,
			@State,
			@Zip,
			@Phone,
			@Status,
			SYSDATETIME(),
			@CreatedBy
		)
 
		SELECT @ReqID = CAST(SCOPE_IDENTITY() AS INT); -- Get Inserted ID
		
	END
	SELECT * FROM TBL_OMS_AddressChangeRequest WHERE REQID = @ReqID;
END
 
 
