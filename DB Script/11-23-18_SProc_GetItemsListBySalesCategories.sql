USE [PriceCatalog]
GO
/****** Object:  StoredProcedure [dbo].[SProc_GetItemsListBySalesCategories]    Script Date: 11/21/2018 5:24:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SProc_GetItemsListBySalesCategories]
(
	@SalesCategoryCode nvarchar(10),
	@SalesSubCategoryCode nvarchar(50)
)
AS
BEGIN

SELECT CatId, SubCatID, ProductCode from  [tbl_Goya_CatalogDetail]
where (CatID = @SalesCategoryCode and @SalesCategoryCode is not null) or (SubCatID = @SalesSubCategoryCode and @SalesSubCategoryCode is not null)

END

--[SProc_GetItemsListBySalesCategories] '26', ''