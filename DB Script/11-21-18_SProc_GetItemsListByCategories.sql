USE [GOYA_BI]
GO
/****** Object:  StoredProcedure [dbo].[SProc_GetItemsListByCategories]    Script Date: 11/21/2018 5:24:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SProc_GetItemsListByCategories]
(
	@CategoryCode nvarchar(10),
	@SubCategoryCode nvarchar(10)
)
AS
BEGIN

SELECT l1.Level1 as Category_Code,l1.Lev1Desc as Category_Name, l2.Level2 as SubCategory_Code,
l2.Lev2Desc as SubCategory_Name, l3.Level3 as SubSubCategory_Code, l3.Lev3Desc as SubSubCategory_Name,
i.Item as Item_Code, i.Item_Description as Item_Name
FROM dimSalesCodes sc inner join dimSalesLevel1 l1 
ON sc.Level1=l1.Level1
inner join dimSalesLevel2 l2 
ON sc.Level2=l2.Level2
inner join dimSalesLevel3 l3
ON sc.Level3=l3.Level3
inner join dimItems i
ON sc.Level4=i.Item
where (sc.Level2 = @SubCategoryCode and @SubCategoryCode is not null) or (sc.level1 = @CategoryCode and @CategoryCode is not null)

END

--[SProc_GetItemsListBySubCategory] '', 'aa'