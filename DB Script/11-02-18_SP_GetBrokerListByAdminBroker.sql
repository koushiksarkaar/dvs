ALTER PROCEDURE [dbo].[SP_GetBrokerListByAdminBroker]
(
	@BrokerID  nvarchar(50)	 
)
AS
BEGIN

	SET NOCOUNT ON;
    Declare @groupID nvarchar(50);
    Declare @NewSalesmanID nvarchar(50);
    Declare @GoyaCompanyId nvarchar(5);
	 Declare @IsNumericID nvarchar(5);

    
    /* Check for Super user and give access to all customer under Super User */
	  SET @groupID = (SELECT SUBSTRING(@BrokerID,5,2)) ;	  
	  set @GoyaCompanyId = (SELECT SUBSTRING(@BrokerID,1,2)) ;	  
	  SET @IsNumericID =  (SELECT SUBSTRING(@BrokerID,3,2)) ;	  


	   if(@groupID = '01')
	     set @NewSalesmanID = (SELECT SUBSTRING(@BrokerID,3,2));
	   ELSE
	       set @NewSalesmanID  = @BrokerID

		   IF(ISNUMERIC(@IsNumericID) =0)
		   SET @NewSalesmanID =''
	
     -- Select UserID,Name from [Goya_UtilDB].[dbo].[vw_User_Master_Data] where UserID like  @NewSalesmanID+'%%'
      Select (COMPANY+''+BRROUT) as UserID,USERNAME AS Name from [Goya_UtilDB].[dbo].[vw_User_Master_Data] 
      where (BRROUT like  @NewSalesmanID+'%%' OR @NewSalesmanID=''  )and @GoyaCompanyId = COMPANY
      
      --select * from [Goya_UtilDB].[dbo].[vw_User_Master_Data]
END