USE [PriceCatalog]
GO
/****** Object:  StoredProcedure [dbo].[SProc_GetItemsListByTag]    Script Date: 11/21/2018 5:24:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SProc_GetItemsListByTag]
(
	@TagName nvarchar(100)
)
AS
BEGIN

SELECT NewProduct, PriceChanges, SizeChanges, PackChanges, FoodService, OrganicProduct, MercDisplay from  [tbl_Goya_CatalogDetail]
where MercDisplay = 1 and @TagName = 'Display Items' 
or FoodService = 1 and @TagName = 'Food Service'
or OrganicProduct = 1 and @TagName = 'Organic'
or NewProduct = 1 and @TagName = 'New Item'
or PriceChanges = 1 and @TagName = 'Price Change'
--where (CatID = @SalesCategoryCode and @SalesCategoryCode is not null) or (SubCatID = @SalesSubCategoryCode and @SalesSubCategoryCode is not null)

END

--[SProc_GetItemsListByTag] 'Display Items'