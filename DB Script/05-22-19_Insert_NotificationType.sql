If NOT EXISTS (SELECT 1 FROM tbl_OMS_NotificationType WHERE NotificationTypeName = 'Document Briefcase')
BEGIN
INSERT INTO tbl_OMS_NotificationType
           (NotificationTypeName
		   ,IsActive
           ,CreatedDate
           ,CreatedBy
           ,ModifiedDate
           ,ModifiedBy)
     VALUES
           ('Document Briefcase'
		   ,1
           ,GETDATE()
           ,null
           ,null
           ,null)

INSERT INTO tbl_OMS_NotificationType
           (NotificationTypeName
		   ,IsActive
           ,CreatedDate
           ,CreatedBy
           ,ModifiedDate
           ,ModifiedBy)
     VALUES
           ('Credit Hold'
          ,1
           ,GETDATE()
           ,null
           ,null
           ,null)
END