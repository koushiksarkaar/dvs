
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetCreditFormDetailsByCreditId]    Script Date: 10/4/2017 11:35:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 4th Oct 2017
-- Description:	Get Creadit Form detail by creadit id
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetCreditFormDetailsByCreditId]
(
	@CreditId INT = null
)
AS
BEGIN
		SELECT
			--Credir form Customer Info
			CI.CreditId,
			CI.BrokerId,
			CI.CorporateName,
			CI.TradeName,
			CI.WeeklyOrderAmount,
			CI.FirstOrderAmount,
			CI.DeliveryStreetAddress,
			CI.DeliveryCity,
			CI.DeliveryState,
			CI.DeliveryZip,
			CI.DeliveryFax,
			CI.DeliveryEmail,
			CI.FederalTaxIdNo,
			CI.StateResaleCertificateNo,
			CI.StoreTelephone,
			CI.CellPhoneNo,
			CI.ReceivingDaysHours,
			CI.BillingStreetAdress,
			CI.BillingCity,
			CI.BillingState,
			CI.BillingZip,
			CI.BillingTelephone,
			CI.BillingFax,
			CI.AccountPayableManager,
			CI.AccountPayablePhone,
			CI.BusinessType,
			CI.PeriodAtLocation,
			CI.PeriodInBusiness,
			CI.CorporationYear,
			CI.InsuranceCompany,
			CI.CompanySreetAddress,
			CI.CompanyCity,
			CI.CompanyState,
			CI.CompanyZip,
			CI.Policy,
			CI.[Status],
			CI.CreatedDate,
			CI.UpdatedDate,
			CI.UpdatedBy,
			CI.CreatedBy,

			--Credit form trade reference
			TR.TradeId AS TradeRefTradeId,
			TR.CreditId AS TradeRefCreditId,
			TR.TradeName AS TradeRefTradeName,
			TR.StreetAddress AS TradeRefStreetAddress,
			TR.City AS TradeRefCity,
			TR.[State] AS TradeRefState,
			TR.Zip AS TradeRefZip,
			TR.PhoneNo AS TradeRefPhoneNo,
			TR.FaxNo AS TradeRefFaxNo,
			TR.Account AS TradeRefAccount,
			TR.ContactName AS TradeRefContactName,
			TR.ContactPhoneNo AS TradeRefContactPhoneNo,
			TR.RelationshipPeriod AS TradeRefRelationshipPeriod,
			TR.CreditLimitAmount AS TradeRefCreditLimitAmount,
			TR.CreatedDate AS TradeRefCreatedDate,
			TR.UpdatedDate AS TradeRefUpdatedDate,
			TR.CreatedBy AS TradeRefCreatedBy,
			TR.UpdatedBy AS TradeRefUpdatedBy,

			--Credit form Bank Refernce
			BR.BankId AS BankRefBankId,
			BR.CreditId AS BankRefCreditId,
			BR.BankName AS BankRefBankName,
			BR.StreetAddress AS BankRefStreetAddress,
			BR.City AS BankRefCity,
			BR.[State] AS BankRefState,
			BR.Zip AS BankRefZip,
			BR.Phone AS BankRefPhone,
			BR.Fax AS BankRefFax,
			BR.AccountInName AS BankRefAccountInName,
			BR.ContactPhone AS BankRefContactPhone,
			BR.CreatedDate AS BankRefCreatedDate,
			BR.UpdatedDate AS BankRefUpdatedDate,
			BR.CreatedBy AS BankRefCreatedBy,
			BR.UpdatedBy AS BankRefUpdatedBy,

			--Credit form Existing Account Detail
			AD.AccountId AS ACDetailAccountId,
			AD.CreditId AS ACDetailCreditId,
			AD.Name AS ACDetailName,
			AD.StreetAddress AS ACDetailStreetAddress,
			AD.City AS ACDetailCity,
			AD.[State] AS ACDetailState,
			AD.Zip AS ACDetailZip,
			AD.Account AS ACDetailAccount,
			AD.[Open] AS ACDetailOpen,
			AD.Closed AS ACDetailClosed,
			AD.Sold AS ACDetailSold,
			AD.RelationshipToAccount AS ACDetailRelationshipToAccount,
			AD.CreatedDate AS ACDetailCreatedDate,
			AD.UpdateDate AS ACDetailUpdateDate,
			AD.CreatedBy AS ACDetailCreatedBy,
			AD.UpdatedBy AS ACDetailUpdatedBy,

			--Credit form Owner Detail
			OD.OwnerId AS OwnerDetailOwnerId,
			OD.CreditId AS OwnerDetailCreditId,
			OD.PrincipalName AS OwnerDetailPrincipalName,
			OD.StreetAddress AS OwnerDetailStreetAddress,
			OD.City AS OwnerDetailCity,
			OD.[State] AS OwnerDetailState,
			OD.Zip AS OwnerDetailZip,
			OD.[Time] AS OwnerDetailTime,
			OD.SocialSecurity AS OwnerDetailSocialSecurity,
			OD.Telephone AS OwnerDetailTelephone,
			OD.CreateDate AS OwnerDetailCreateDate,
			OD.UpdateDate AS OwnerDetailUpdateDate,
			OD.CreatedBy AS OwnerDetailCreatedBy,
			OD.UpdatedBy AS OwnerDetailUpdatedBy
		FROM 
			--Credit form Customer Info
			tbl_OMS_CreditFormCustomerInfo CI INNER JOIN 
			--Credit form trade reference
			tbl_OMS_CreditFormBusinessTradeRef TR ON CI.CreditId = TR.CreditId INNER JOIN
			--Credit form Bank Refernce
			tbl_OMS_CreditFormBusinessBankRef BR ON CI.CreditId = BR.CreditId INNER JOIN
			--Credit form Existing Account Detail
			tbl_OMS_CreditFormExistingAccountDetail AD ON CI.CreditId = AD.CreditId INNER JOIN
			--Credit form Owner Detail
			tbl_OMS_CreditFormOwnerDetail OD ON CI.CreditId = OD.CreditId 
		WHERE
			(( CI.CreditId = @CreditId) OR @CreditId IS NULL)
END
