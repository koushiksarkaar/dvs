USE [GoyaDB2]
GO
/****** Object:  StoredProcedure [dbo].[SP_GetInvoiceScanCopyByOrderNumber]    Script Date: 5/9/2019 11:42:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--            exec [SP_GetInvoiceScanCopyByOrderNumber]  '771330','14804174'

Alter PROCEDURE [dbo].[SP_GetInvoiceScanCopyByOrderNumber]
(
 @CustomerId nvarchar(50),
 @OrderNumber nvarchar(50) 
 
  
)
AS
BEGIN

    
    DECLARE @BrokerString nvarchar(max);   
      
	SET NOCOUNT ON;
 
	
/****** Script for SelectTopNRows command from SSMS  ******/
  SELECT  [Order Number],[Invorder Number],Invoice,*
  FROM [Goya_UtilDB].[dbo].[vw_METAVIWER_Invoice_Image]
  Where Customer =@CustomerId  and [Order Number]=@OrderNumber
     order by [Delivery Date] desc
 
  
          
END

 