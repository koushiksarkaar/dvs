/****** Object:  Table [dbo].[tbl_OMS_CreditFormOwnerDetail]    Script Date: 9/20/2017 1:08:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_OMS_CreditFormOwnerDetail](
	[OwnerId] [int] IDENTITY(1,1) NOT NULL,
	[CreditId] [int] NOT NULL,
	[PrincipalName] [nvarchar](100) NULL,
	[StreetAddress] [nvarchar](500) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Zip] [nvarchar](50) NULL,
	[Time] [time](7) NULL,
	[SocialSecurity] [nvarchar](100) NULL,
	[Telephone] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_OMS_CreditFormOwnerDetail] PRIMARY KEY CLUSTERED 
(
	[OwnerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormOwnerDetail]  WITH CHECK ADD  CONSTRAINT [FK_tbl_OMS_CreditFormOwnerDetail_tbl_OMS_CreditFormCustomerInfo] FOREIGN KEY([CreditId])
REFERENCES [dbo].[tbl_OMS_CreditFormCustomerInfo] ([CreditId])
GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormOwnerDetail] CHECK CONSTRAINT [FK_tbl_OMS_CreditFormOwnerDetail_tbl_OMS_CreditFormCustomerInfo]
GO


