/****** Object:  StoredProcedure [dbo].[SProc_OMS_UpdateAddressChangeRequestStatus]    Script Date: 10/6/2017 1:04:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 5th Oct 2017
-- Description:	Update Address Change Request Status
-- =============================================
ALTER procedure [dbo].[SProc_OMS_UpdateAddressChangeRequestStatus]
(
	@ReqID INT = NULL,
	@Status NVARCHAR(200) = NULL
 )
AS
BEGIN 
	UPDATE
		TBL_OMS_AddressChangeRequest
	SET
		[Status] = @Status
	WHERE 
		ReqID = @ReqID
END
 
 
