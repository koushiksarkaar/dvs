CREATE PROCEDURE [dbo].[SP_GetPlanRouteDayWiseCustomerDetailsByBroker]
 (
    @CustomerId nvarchar(10),
	@BrokerId nvarchar(50),
	@OrderDate nvarchar(50)
 )
AS
BEGIN
    SET NOCOUNT ON;
	
	select SalesmanId, CustomerId, cast(OrderDate as date) as OrderDate, cast(OrderDate as time(0)) as OrderTime, OrderQty, OrderAmount 
	into #Orders
	from tbl_OMS_Order 
	where CustomerId = @CustomerId and SalesmanId = @BrokerId and cast(OrderDate as date) = @OrderDate
	order by OrderTime desc

	select top 1 *
	into #OrderTime
	from #Orders

	select CustomerId, sum(OrderQty) as OrderQty, sum(OrderAmount) as OrderAmount 
	into #OrderSumAmount
	from #Orders
	group by CustomerId

	select OSA.OrderQty, OSA.OrderAmount,  OT.OrderTime
	from #OrderSumAmount OSA
	join #OrderTime OT on OT.CustomerId = OSA.CustomerId
END

--  exec [SP_GetPlanRouteDayWiseCustomerDetailsByBroker] '712450','013506', '2018-12-18'





 
 
 

 

 