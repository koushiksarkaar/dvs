USE [GoyaProd]
GO
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetAddressChangeRequest]    Script Date: 10/6/2017 12:57:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 5th Oct 2017
-- Description:	Search Address Change Request
-- =============================================
ALTER procedure [dbo].[SProc_OMS_GetAddressChangeRequest]
(
	@BrokerID NVARCHAR(50) = NULL,
	@CustomerID NVARCHAR(50) = NULL,
	@Status NVARCHAR(200) = NULL,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL
 )
AS
BEGIN 
	SELECT 
		ACR.ReqID,
		ACR.SalesmanID,
		ACR.CustomerID,
		ACR.StreetAddress1,
		ACR.StreetAddress2,
		ACR.City,
		ACR.[State],
		ACR.Zip,
		ACR.Phone,
		ACR.[Status],
		ACR.CreatedDate,
		ACR.UpdatedDate,
		ACR.CreatedBy,
		ACR.UpdatedBy
	FROM 
		TBL_OMS_AddressChangeRequest ACR
	WHERE 
		((ACR.SalesmanID LIKE '%' + @BrokerID + '%') OR @BrokerID IS NULL) AND
		((ACR.CustomerID LIKE '%' + @CustomerID + '%') OR @CustomerID IS NULL) AND
		((ACR.[Status] = @Status) OR @Status IS NULL) AND
		((ACR.CreatedDate >= @FromDate) OR @FromDate IS NULL) AND
		((ACR.CreatedDate <= @ToDate) OR @ToDate IS NULL)
END
 
 
