
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetCreditFormCustomerInfo]    Script Date: 10/4/2017 10:45:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 4th Oct 2017
-- Description:	Get Creadit Form Customer Info by BrokerID or Status
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetCreditFormCustomerInfo]
(
	@BrokerId nvarchar(50) = null,
	@Status nvarchar(200) = null,
	@Fromdate datetime = null,
	@Todate datetime = null
)
AS
BEGIN
		SELECT
			CI.CreditId,
			CI.BrokerId,
			CI.CorporateName,
			CI.TradeName,
			CI.WeeklyOrderAmount,
			CI.FirstOrderAmount,
			CI.DeliveryStreetAddress,
			CI.DeliveryCity,
			CI.DeliveryState,
			CI.DeliveryZip,
			CI.DeliveryFax,
			CI.DeliveryEmail,
			CI.FederalTaxIdNo,
			CI.StateResaleCertificateNo,
			CI.StoreTelephone,
			CI.CellPhoneNo,
			CI.ReceivingDaysHours,
			CI.BillingStreetAdress,
			CI.BillingCity,
			CI.BillingState,
			CI.BillingZip,
			CI.BillingTelephone,
			CI.BillingFax,
			CI.AccountPayableManager,
			CI.AccountPayablePhone,
			CI.BusinessType,
			CI.PeriodAtLocation,
			CI.PeriodInBusiness,
			CI.CorporationYear,
			CI.InsuranceCompany,
			CI.CompanySreetAddress,
			CI.CompanyCity,
			CI.CompanyState,
			CI.CompanyZip,
			CI.Policy,
			CI.[Status],
			CI.CreatedDate,
			CI.UpdatedDate,
			CI.UpdatedBy,
			CI.CreatedBy
		FROM 
			tbl_OMS_CreditFormCustomerInfo CI
		WHERE
			(( CI.BrokerId Like '%'+@BrokerId+'%' ) OR @BrokerId IS NULL) AND
			(( CI.[Status] = @Status) OR @Status IS NULL) AND
			(( CI.CreatedDate >= @Fromdate) OR @Fromdate IS NULL) AND
			(( CI.CreatedDate <= @Todate) OR @Todate IS NULL) 
END
