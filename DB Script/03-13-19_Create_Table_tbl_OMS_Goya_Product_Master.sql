IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.TABLES T
        WHERE T.TABLE_SCHEMA = 'dbo'
        AND T.TABLE_NAME = 'tbl_OMS_Goya_Product_Master'  )
    BEGIN

        CREATE TABLE dbo.tbl_OMS_Goya_Product_Master
        (
            ItemCode int,
			Category varchar(100),
			Subcategory varchar(100),
			Name1 varchar(250),
			Name2 varchar(250),
			Country varchar(max),
			Region varchar(100),
			NetWeight varchar(100),
			PackQuantity varchar(100)
        )
    END

GO