ALTER PROCEDURE [dbo].[SP_GetPlanRouteDayWiseDetailsByBroker]
 (
    @CompanyId nvarchar(10),
	@BrokerId nvarchar(50)
 )
AS
BEGIN

    SET NOCOUNT ON;
	Declare @UserId nvarchar(10);
	Set @UserId = (select substring (@BrokerId,3,4));
 

--select C.CSNO as CustomerId,(C.CSNO+'-'+C.CSNAME) as CustomerName into #CustName From [Goya_UtilDB].[dbo].[vw_CustomerDetail] C 
--where csco=@CompanyId and BRROUT = @UserId AND CSACT='A';

-- Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 1) MondayOfCurrentWeek
 -- exec [SP_GetPlanRouteDayWiseDetailsByBroker] '01','013506'


 
   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) as [Date],--C.CustomerName as CustomerInfo, 
   cast(O.OrderDate as time) OrderTime,
   ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, P.CustomerId, COUNT(*) over() as 'Count' into #tempMonday 
   From TBL_OMS_PlanRouteCustomerDetails P
   --inner join #CustName C on c.CustomerId collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
   left join tbl_OMS_Order O on P.CustomerId collate  Latin1_General_CS_AS = O.CustomerId collate  Latin1_General_CS_AS and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0)
   where Monday = 1 and CompanyId = @CompanyId and BrokerId = @BrokerId
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)), P.CustomerId, O.OrderDate--,C.CustomerName, C.CustomerId;

   Select  DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 1) as [Date],--C.CustomerName as CustomerInfo, 
   cast(O.OrderDate as time) OrderTime,
   ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, P.CustomerId, COUNT(*) over() as 'Count' into #tempTuesday From TBL_OMS_PlanRouteCustomerDetails P
   --inner join #CustName C on c.CustomerId collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
   left join tbl_OMS_Order O on P.CustomerId collate  Latin1_General_CS_AS = O.CustomerId collate  Latin1_General_CS_AS and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 1)
   where Tuesday = 1 and CompanyId = @CompanyId and BrokerId = @BrokerId
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)), P.CustomerId, O.OrderDate--,C.CustomerName, C.CustomerId;

   Select  DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 2) as [Date],--C.CustomerName as CustomerInfo, 
   cast(O.OrderDate as time) OrderTime,
   ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, P.CustomerId, COUNT(*) over() as 'Count' into #tempWednesday From TBL_OMS_PlanRouteCustomerDetails P
   --inner join #CustName C on c.CustomerId collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
   left join tbl_OMS_Order O on P.CustomerId collate  Latin1_General_CS_AS = O.CustomerId collate  Latin1_General_CS_AS and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 2)
   where Wednesday = 1 and CompanyId = @CompanyId and BrokerId = @BrokerId
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)), P.CustomerId, O.OrderDate--,C.CustomerName, C.CustomerId;

   Select  DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 3) as [Date],--C.CustomerName as CustomerInfo, 
   cast(O.OrderDate as time) OrderTime,
   ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, P.CustomerId, COUNT(*) over() as 'Count' into #tempThursday From TBL_OMS_PlanRouteCustomerDetails P
   --inner join #CustName C on c.CustomerId collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
   left join tbl_OMS_Order O on P.CustomerId collate  Latin1_General_CS_AS = O.CustomerId collate  Latin1_General_CS_AS and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 3)
   where Thursday = 1 and CompanyId = @CompanyId and BrokerId = @BrokerId
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)), P.CustomerId, O.OrderDate--,C.CustomerName, C.CustomerId;

   Select  DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 4) as [Date],--C.CustomerName as CustomerInfo, 
   cast(O.OrderDate as time) OrderTime,
   ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, P.CustomerId, COUNT(*) over() as 'Count' into #tempFriday From TBL_OMS_PlanRouteCustomerDetails P
   --inner join #CustName C on c.CustomerId collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
   left join tbl_OMS_Order O on P.CustomerId collate  Latin1_General_CS_AS = O.CustomerId collate  Latin1_General_CS_AS and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 4)
   where Friday = 1 and CompanyId = @CompanyId and BrokerId = @BrokerId
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)), P.CustomerId, O.OrderDate--,C.CustomerName, C.CustomerId;

   Select  DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 5) as [Date],--C.CustomerName as CustomerInfo, 
   cast(O.OrderDate as time) OrderTime,
   ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, P.CustomerId, COUNT(*) over() as 'Count' into #tempSaturday From TBL_OMS_PlanRouteCustomerDetails P
   --inner join #CustName C on c.CustomerId collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
   left join tbl_OMS_Order O on P.CustomerId collate  Latin1_General_CS_AS = O.CustomerId collate  Latin1_General_CS_AS and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 5)
   where Saturday = 1 and CompanyId = @CompanyId and BrokerId = @BrokerId
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)), P.CustomerId, O.OrderDate--,C.CustomerName, C.CustomerId;

   Select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 6) as [Date],--C.CustomerName as CustomerInfo, 
   cast(O.OrderDate as time) OrderTime,
   ISNULL(SUM(O.OrderAmount), 0) as OrderAmount, P.CustomerId, COUNT(*) over() as 'Count' into #tempSunday From TBL_OMS_PlanRouteCustomerDetails P
   --inner join #CustName C on c.CustomerId collate  Latin1_General_CS_AS = P.CustomerId collate  Latin1_General_CS_AS
   left join tbl_OMS_Order O on P.CustomerId collate  Latin1_General_CS_AS = O.CustomerId collate  Latin1_General_CS_AS and DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)) = DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 6)
   where Sunday = 1 and CompanyId = @CompanyId and BrokerId = @BrokerId
   Group By DATEADD(dd, 0, DATEDIFF(dd, 0, O.OrderDate)), P.CustomerId, O.OrderDate--,C.CustomerName, C.CustomerId;

  Select * from #tempMonday
  UNION ALL
  Select * from #tempTuesday 
  UNION ALL
  Select * from #tempWednesday 
  UNION ALL 
  Select * from #tempThursday   
  UNION ALL
  Select * from #tempFriday   
  UNION ALL
  Select * from #tempSaturday  
  UNION ALL
  Select * from #tempSunday 

END

--  exec  [SP_GetPlanRouteDayWiseDetailsByBroker] '01','013506'





 
 
 

 

 