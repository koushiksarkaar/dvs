/****** Object:  Table [dbo].[tbl_OMS_CreditFormBusinessBankRef]    Script Date: 9/20/2017 1:05:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_OMS_CreditFormBusinessBankRef](
	[BankId] [int] IDENTITY(1,1) NOT NULL,
	[CreditId] [int] NOT NULL,
	[BankName] [nvarchar](100) NULL,
	[StreetAddress] [nvarchar](500) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Zip] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[AccountInName] [nvarchar](100) NULL,
	[ContactPhone] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_OMS_CreditFormBusinessBankRef] PRIMARY KEY CLUSTERED 
(
	[BankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormBusinessBankRef]  WITH CHECK ADD  CONSTRAINT [FK_tbl_OMS_CreditFormBusinessBankRef_tbl_OMS_CreditFormCustomerInfo] FOREIGN KEY([CreditId])
REFERENCES [dbo].[tbl_OMS_CreditFormCustomerInfo] ([CreditId])
GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormBusinessBankRef] CHECK CONSTRAINT [FK_tbl_OMS_CreditFormBusinessBankRef_tbl_OMS_CreditFormCustomerInfo]
GO


