SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suvhra
-- Create date: 20 Dec 2018
-- Description:	Insert / Update Creadit Form DeliveryLocation
-- =============================================
CREATE PROCEDURE [dbo].[SProc_OMS_GetCustomerContact]
(

	@CompanyId nvarchar(10),
	@BrokerId  nvarchar(50),
	@CustomerId nvarchar(50)
	

)
AS
BEGIN
	SELECT FirstName, 
	LastName, 
	Position, 
	PhNo 
	FROM Tbl_OMS_CustomerContact 
	WHERE CompanyId = @CompanyId 
	and CustomerId = @CustomerId 
	and BrokerId = @BrokerId
END
