
/****** Object:  StoredProcedure [dbo].[SProc_OMS_UpdateCreditFormCustomerInfoStatus]    Script Date: 10/5/2017 1:56:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 4th Oct 2017
-- Description:	Update Credit Form Customer Info Status by Credit Id
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_UpdateCreditFormCustomerInfoStatus]
(
	@CreditId INT = NULL,
	@Status NVARCHAR(200) = NULL,
	@Message NVARCHAR(50) OUTPUT
)
AS
BEGIN
	SET @Message = 0
	UPDATE 
		tbl_OMS_CreditFormCustomerInfo
	SET 
		[Status] = @Status 
	WHERE
		CreditId = @CreditId
		
END
