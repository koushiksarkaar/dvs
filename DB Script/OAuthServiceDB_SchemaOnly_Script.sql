USE [OAuthServiceDB]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applications](
	[ApplicationId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[CurrentVersion] [nvarchar](6) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreatedBy] [nchar](10) NULL,
	[UpdatedBy] [nchar](10) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationRoleMapping]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationRoleMapping](
	[ApplicationId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsAccess] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ApplicationRoleMapping] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationAccessMapping]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationAccessMapping](
	[AccessId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[ParentAccessId] [int] NULL,
	[AccessName] [nvarchar](50) NULL,
	[Description] [nvarchar](250) NULL,
	[SortOrder] [smallint] NULL,
	[AccessType] [smallint] NULL,
	[AccessControl] [smallint] NULL,
	[PageURL] [nvarchar](256) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ApplicationAccessMaping] PRIMARY KEY CLUSTERED 
(
	[AccessId] ASC,
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - Common; 2 - Req. Access -- From LookupMappingTable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApplicationAccessMapping', @level2type=N'COLUMN',@level2name=N'AccessType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'3 - Menu; 4 - Link; 5 - Button; 6- Process; 7- Report -- From LookupMappingTable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApplicationAccessMapping', @level2type=N'COLUMN',@level2name=N'AccessControl'
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserType]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserType](
	[UserTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserTypeCode] [nvarchar](100) NULL,
	[Description] [nvarchar](256) NULL,
	[Comments] [nvarchar](256) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UserTypeCode] PRIMARY KEY CLUSTERED 
(
	[UserTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDivisonMgrMapping]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDivisonMgrMapping](
	[UserName] [nvarchar](256) NOT NULL,
	[CompanyCode] [nvarchar](2) NOT NULL,
	[BrokerDivisonCode] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Combination of CompanyCode and BrokerDivisonCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserDivisonMgrMapping', @level2type=N'COLUMN',@level2name=N'BrokerDivisonCode'
GO
/****** Object:  Table [dbo].[UserCompanyMapping]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserCompanyMapping](
	[UserName] [nvarchar](128) NULL,
	[CompanyCode] [varchar](2) NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_OMS_User]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OMS_User](
	[UserID] [nvarchar](50) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[UserGUID] [nvarchar](100) NULL,
	[Name] [nvarchar](50) NULL,
	[Street] [nvarchar](100) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Zip] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[Region] [nvarchar](100) NULL,
	[ContactNo] [nvarchar](50) NULL,
	[EmailID] [nvarchar](50) NULL,
	[Organization] [nvarchar](100) NULL,
	[ContactPersonName] [nvarchar](50) NULL,
	[ContactPersonEmail] [nvarchar](50) NULL,
	[WHCode] [nvarchar](50) NULL,
	[TelxonNo] [nvarchar](50) NULL,
	[Password] [nvarchar](500) NULL,
	[UserType] [int] NULL,
	[IMEINumber] [varchar](50) NULL,
	[SpotID] [int] NULL,
	[DepartmentID] [int] NULL,
	[SalesManID] [int] NULL,
	[OrderAcknowledgement] [bit] NULL,
	[AttachOrderConfirmation] [bit] NULL,
	[RegisteredOn] [datetime] NULL,
	[IsAuthenticated] [bit] NULL,
	[AuthenticatedOn] [datetime] NULL,
	[UserIsDeleted] [bit] NULL,
	[AuthenticatedBy] [nvarchar](50) NULL,
	[CreditLimit] [numeric](18, 2) NULL,
	[ContractNo] [varchar](50) NULL,
	[BRROUT] [varchar](20) NULL,
	[CSAccessFlag] [varchar](10) NULL,
	[WHCompany] [varchar](10) NOT NULL,
	[WHCO2] [varchar](10) NULL,
	[LastTimeUpdate] [datetime] NULL,
	[LastCustUpdate] [datetime] NULL,
	[LastBillUpdate] [datetime] NULL,
	[UserFlag] [char](1) NULL,
 CONSTRAINT [PK_tbl_OMS_User_1] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[CompanyID] ASC,
	[WHCompany] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoleApplicationAccessMapping]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleApplicationAccessMapping](
	[RoleId] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[AccessId] [int] NOT NULL,
	[IsAccess] [bit] NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_RoleApplicationAccessMapping] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[ApplicationId] ASC,
	[AccessId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupMappingTable]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupMappingTable](
	[CodeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Value] [nvarchar](100) NULL,
	[Description] [nvarchar](250) NULL,
	[ParentCodeId] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreatedBy] [datetime] NULL,
	[UpdatedBy] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_LookupMappingTable] PRIMARY KEY CLUSTERED 
(
	[CodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyCode] [varchar](2) NOT NULL,
	[CompanyDescription] [varchar](50) NULL,
 CONSTRAINT [PK_dimCompany_1] PRIMARY KEY CLUSTERED 
(
	[CompanyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BrokerPhoto]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BrokerPhoto](
	[CompanyCode] [nchar](20) NULL,
	[BrokerCode] [nvarchar](20) NULL,
	[PhotoName] [nvarchar](250) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[UserWindowName] [nvarchar](256) NULL,
	[Domain] [nvarchar](256) NULL,
	[UserTypeCode] [nvarchar](6) NULL,
	[UserFullName] [nvarchar](256) NULL,
	[CreateDate] [datetime] NULL,
	[CreatedBy] [nvarchar](250) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](250) NULL,
	[Comments] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC,
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUserAppRoleMap]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_InsertUserAppRoleMap]  
(  
   @RoleId varchar (50),
   @ApplicationId varchar (50),  
   @UserName varchar (256)
)  
as  
begin  
	-- Delete from RoleApplicationAccessMapping where RoleId = @RoleId and ApplicationId = @ApplicationId;

   Insert into AspNetUserRoles 
   (UserId, ApplicationId, RoleId, UserName) 
   values ((select Id from AspNetUsers where UserName = @UserName), 
   @ApplicationId, @RoleId, @UserName)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertRoleAppAccessMap]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_InsertRoleAppAccessMap]  
(  
   @RoleId varchar (50),
   @ApplicationId varchar (50),  
   @AccessId varchar (50),
   @IsAccess bit
)  
as  
begin  
	-- Delete from RoleApplicationAccessMapping where RoleId = @RoleId and ApplicationId = @ApplicationId;

   Insert into RoleApplicationAccessMapping 
   (RoleId, ApplicationId, AccessId, IsAccess) 
   values (@RoleId, @ApplicationId, @AccessId, @IsAccess)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertRole]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertRole](
@RoleName NVARCHAR(MAX),
@Description nvarchar(250) = null,
@IsActive bit
)
AS
BEGIN
   DECLARE @RoleId nvarchar(MAX);
   SET @RoleId = (SELECT MAX(CONVERT(INT,Id)) FROM [dbo].[AspNetRoles])
   SET @RoleId = @RoleId + 1;
   
   

   IF NOT EXISTS (SELECT * FROM [dbo].[AspNetRoles]
                   WHERE Name = @RoleName)
   BEGIN
       INSERT INTO [dbo].[AspNetRoles] (Id,Name,Description,IsActive)
       VALUES (ISNULL(@RoleId, 1),@RoleName,@Description,@IsActive)
   END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertAppRoleMap]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_InsertAppRoleMap]  
(  
   @RoleId varchar (50),
   @ApplicationId varchar (50),
   @IsAccess bit
)  
as  
begin  
	-- Delete from RoleApplicationAccessMapping where RoleId = @RoleId and ApplicationId = @ApplicationId;

   Insert into ApplicationRoleMapping 
   (RoleId, ApplicationId, IsAccess) 
   values (@RoleId, @ApplicationId, @IsAccess)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertApplication]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_InsertApplication]  
(  
   @ApplicationName varchar (50),  
   @Description varchar (50) = null,
   @CreateDate datetime,
   @IsActive bit
)  
as  
begin  
   Insert into Applications 
   (ApplicationName, Description, CreateDate, IsActive) 
   values (@ApplicationName, @Description, @CreateDate, @IsActive)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertAppAccessMap]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_InsertAppAccessMap]  
(  
   @ApplicationId varchar (50),  
   @AccessName varchar (50),
   @Description varchar (250) = null,
   @SortOrder int,
   @AccessType int,
   @AccessControl int,
   @CreateDate datetime,
   @IsActive bit
)  
as  
begin  
   Insert into ApplicationAccessMapping 
   (ApplicationId, AccessName, Description, SortOrder, AccessType, AccessControl, CreateDate, IsActive) 
   values (@ApplicationId, @AccessName, @Description, @SortOrder, @AccessType, @AccessControl, @CreateDate, @IsActive)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserSearchList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetUserSearchList]  
(  
   @UserSearchStr nvarchar(256)
)
as  
begin  
	select A.UserName, A.UserFullName from [OAuthServiceDB].[dbo].[AspNetUsers] A
	where UserName like @UserSearchStr or UserFullName like @UserSearchStr;
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserMyProfile]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetUserMyProfile]  
(
	@userId nvarchar(50)
)
as  
begin  
   select A.UserName, A.UserFullName, A.Email, A.PhoneNumber, A.Domain,
   (select Name from AspNetRoles where Id = (select RoleId from AspNetUserRoles where UserName=@userId)) as Role,
   LEFT(@userId,2) as CompanyId,
   (select CompanyDescription from [dbo].[Company] where CompanyCode = LEFT(@userId,2)) as CompanyName,
   (select Description from [dbo].[UserType] where UserTypeCode = A.UserTypeCode) as UserType
     from AspNetUsers A where UserName = @userId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserIdFromUserName]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[sp_GetUserIdFromUserName]  
(
	@userName nvarchar(50)
)
as  
begin  
   select Id from AspNetUsers where UserName =  @userName 
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserFullName]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetUserFullName]  
(
	@userId nvarchar(50)
)
as  
begin  
   select UserFullName from AspNetUsers where UserName = @userId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserCompany]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetUserCompany](
@Domain NVARCHAR(256),
@UserName NVARCHAR(256)
)
AS
BEGIN
	SET NOCOUNT ON
	Declare @UsrTyp NVARCHAR(6)
	Declare @cnt int
	set @cnt = 1;
	set @UsrTyp =(Select a.UserTypeCode from AspNetUsers a where a.Domain = @Domain and a.UserWindowName =@UserName)
	if(@UsrTyp = 'GB')
	begin
		set @cnt = (Select COUNT(*)  from UserCompanyMapping c where c.IsActive=1 AND c.UserName = @UserName);
		if(@cnt > 0)
			Select distinct c.CompanyCode, 'GB' as UsrTyp from UserCompanyMapping c where c.IsActive=1 AND c.UserName = @UserName
		else
			Select '0' as CompanyCode,'GB' as UsrTyp 
		--Declare @CompList nvarchar(max)
		--Select @CompList = Coalesce(@CompList+',','') + c.CompanyCode from UserCompanyMapping c where c.IsActive=1 AND c.UserName = @UserName  
		--Select @CompList as CompanyCode, 'GB' as UsrTyp
	end
	else if(@UsrTyp = 'DM')
	begin
		Select d.BrokerDivisonCode, d.CompanyCode, 'DM' as UsrTyp from dbo.UserDivisonMgrMapping d Where d.IsActive=1 AND d.UserName=@UserName
	end
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserAppRoleNotAssignedList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetUserAppRoleNotAssignedList]  
(  
   @ApplicationId int,
   @RoleId int
)
as  
DECLARE @roleCnt AS INT
begin  
	select UserName, UserFullName from [OAuthServiceDB].[dbo].[AspNetUsers]
	where UserName not in (select UserName from [OAuthServiceDB].[dbo].[AspNetUserRoles] where ApplicationId = @ApplicationId and RoleId = @RoleId)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserAppRoleAssignedList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetUserAppRoleAssignedList]  
(  
   @ApplicationId int,
   @RoleId int
)
as  
DECLARE @roleCnt AS INT
begin  
	select A.UserName, B.UserFullName from [OAuthServiceDB].[dbo].[AspNetUserRoles] A
	inner join [OAuthServiceDB].[dbo].[AspNetUsers] B on A.UserName = B.UserName
	where A.ApplicationId = @ApplicationId and A.RoleId = @RoleId 
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRolesList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetRolesList]  
as  
begin  
   select * from AspNetRoles 
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRoleLookupListForAppId]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetRoleLookupListForAppId]  
(
	@applicationId int
)
as  
begin 
	select A.RoleId, B.Name as RoleName from ApplicationRoleMapping A
	inner join AspNetRoles B on A.RoleId = B.Id
	where A.ApplicationId = @applicationId and A.IsAccess = 1 
	order by A.RoleId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRoleIdFromUserId]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetRoleIdFromUserId]  
(
	@userId nvarchar(50)
)
as  
begin  
   select RoleId from AspNetUserRoles where UserName = @userId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRoleApplicationAccessMappingList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetRoleApplicationAccessMappingList]  
(  
   @ApplicationId int,
   @RoleId int
)
as  
DECLARE @roleCnt AS INT
begin  
	select A.*, isnull(B.RoleId,0) as RoleId, case A.AccessTypeName when 'Common' then 1 else  isnull(B.IsAccess,0) end as IsAccess  
	from
			(select U.AccessId, U.ApplicationId, U.AccessName, U.Description, V.Value as AccessTypeName, W.Value as AccessControlName, PageURL
			from ApplicationAccessMapping U
			inner join LookupMappingTable V on U.AccessType = V.CodeId
			inner join LookupMappingTable W on U.AccessControl = W.CodeId where U.IsActive = 1
			) A 
			left outer join 
			RoleApplicationAccessMapping B on A.AccessId = B.AccessId where A.ApplicationId = @ApplicationId  and B.RoleId = @RoleId
union
	select distinct A.* , 0 as RoleId, case A.AccessTypeName when 'Common' then 1 else 0 end as IsAccess 
	from
			(select U.AccessId, U.ApplicationId, U.AccessName, U.Description,   V.Value as AccessTypeName, W.Value as AccessControlName, PageURL
			from ApplicationAccessMapping U
			inner join LookupMappingTable V on U.AccessType = V.CodeId
			inner join LookupMappingTable W on U.AccessControl = W.CodeId  where U.ApplicationId = @ApplicationId and  U.IsActive = 1
			) A  where AccessId not in (select X.AccessId from ApplicationAccessMapping X left outer join RoleApplicationAccessMapping Y
			on X.AccessId = Y.AccessId
			 where X.ApplicationId = @ApplicationId and Y.RoleId = @RoleId)
			
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetOMSUserForOAuthSync]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[sp_GetOMSUserForOAuthSync]  
as  
begin  
   select UserID, Name from dbo.tbl_OMS_User where UserID not in (select UserName from AspNetUsers);
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetLookupMappingTableList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetLookupMappingTableList]  
(  
   @pName nvarchar(50)
) 
as  
begin  
   select CodeId, Value from LookupMappingTable where Name = @pName
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetIfUserOrEmailExist]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetIfUserOrEmailExist] 
(
	@userName nvarchar(256),
	@email  nvarchar(256) = null
) 
as  
begin  
   select COUNT(*) from dbo.AspNetUsers where UPPER(UserName) = UPPER(@userName) or UPPER(UserWindowName) = UPPER(@userName)  or UPPER(Email) = UPPER(@email);
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetIfUserExist]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetIfUserExist] 
(
	@userName nvarchar(256)
	-- @email  nvarchar(256) = null
) 
as  
begin  
   select COUNT(*) from dbo.AspNetUsers where UPPER(UserName) = UPPER(@userName) or UPPER(UserWindowName) = UPPER(@userName); -- or UPPER(Email) = UPPER(@email)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetGeneralLookupList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetGeneralLookupList]  
(
	@tblName nvarchar(50)
)
as  
begin 
	if(@tblName = 'Applications') 
		begin
			select ApplicationId as ID, ApplicationName as VALUE from Applications where IsActive = 1
		end
    else if (@tblName = 'Roles')
		begin
			select Id as ID, Name as VALUE from AspNetRoles where IsActive = 1
		end
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetBrokerPhoto]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetBrokerPhoto](
@CompanyCode NVARCHAR(2),
@BrokerCode NVARCHAR(20)
)
AS
BEGIN
	SET NOCOUNT ON
	
	Declare @cnt int
	
	set @cnt = (SELECT BrokerCode FROM BrokerPhoto where CompanyCode = @CompanyCode AND BrokerCode = @BrokerCode)
	if( @cnt > 0)
		SELECT PhotoName as BrkrPhoto FROM BrokerPhoto where CompanyCode = @CompanyCode AND BrokerCode = @BrokerCode
	else
		SELECT '' as BrkrPhoto
	
	 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetApplicationsList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetApplicationsList]  
as  
begin  
   select * from Applications 
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetApplicationRoleMappingList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetApplicationRoleMappingList]  
(  
   @ApplicationId int
)
as  
begin  
	select U.RoleId,IsAccess, V.Name as RoleName
	from ApplicationRoleMapping U
	inner join AspNetRoles V on U.RoleId = V.Id where V.IsActive = 1 and U.ApplicationId = @ApplicationId
	union 
	select Id as RoleId, 0 as IsAccess, Name as RoleName
	from AspNetRoles where Id not in (select RoleId from ApplicationRoleMapping where ApplicationId = @ApplicationId)
End
GO
/****** Object:  StoredProcedure [dbo].[sp_GetApplicationAccessMappingList]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetApplicationAccessMappingList]  
(  
   @pAppId int
)
as  
begin
	if(@pAppId = 0)  
		select APP.ApplicationName, AT.Value as AccessTypeName, AC.Value as AccessControlName, AAM.* from ApplicationAccessMapping AAM 
		inner join Applications APP on AAM.ApplicationId = APP.ApplicationId
		inner join LookupMappingTable AT on AAM.AccessType = AT.CodeId
		inner join LookupMappingTable AC on AAM.AccessControl = AC.CodeId where APP.IsActive = 1;
	else
		select APP.ApplicationName, AT.Value as AccessTypeName, AC.Value as AccessControlName, AAM.* from ApplicationAccessMapping AAM 
		inner join Applications APP on AAM.ApplicationId = APP.ApplicationId
		inner join LookupMappingTable AT on AAM.AccessType = AT.CodeId
		inner join LookupMappingTable AC on AAM.AccessControl = AC.CodeId where APP.IsActive = 1 and AAM.ApplicationId = @pAppId;
End
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUserAppRoleMapByIds]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DeleteUserAppRoleMapByIds]  
(  
   @RoleId int,
   @ApplicationId int
)  
as  
begin  
   Delete from AspNetUserRoles where RoleId = @RoleId and ApplicationId = @ApplicationId;
End
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteRoleById]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DeleteRoleById]  
(  
   @RoleId int
)  
as  
begin  
   Delete from AspNetRoles where Id=@RoleId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteRoleAppAccessMapByIds]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DeleteRoleAppAccessMapByIds]  
(  
   @RoleId int,
   @ApplicationId int
)  
as  
begin  
   Delete from RoleApplicationAccessMapping where RoleId = @RoleId and ApplicationId = @ApplicationId;
End
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAppRoleMapByIds]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DeleteAppRoleMapByIds]  
(  
   @ApplicationId int
)  
as  
begin  
   Delete from ApplicationRoleMapping where ApplicationId = @ApplicationId;
End
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteApplicationById]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DeleteApplicationById]  
(  
   @ApplicationId int
)  
as  
begin  
   Delete from Applications where ApplicationId=@ApplicationId;
   Delete from ApplicationAccessMapping where ApplicationId=@ApplicationId;
   Delete from RoleApplicationAccessMapping where ApplicationId=@ApplicationId;
End
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAppAccessMapById]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DeleteAppAccessMapById]  
(  
   @AccessId int
)  
as  
begin  
   Delete from ApplicationAccessMapping where AccessId=@AccessId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckIfValidEmailExist]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_CheckIfValidEmailExist] 
(
	@userName nvarchar(256),
	@email  nvarchar(256) = null
) 
as  
begin
	DECLARE @defaultEmail nvarchar(256);  
	DECLARE @dbEmail nvarchar(256);
	SET @defaultEmail = @userName + '@' + @userName + '.com';
	SET @dbEmail = (SELECT Email FROM [dbo].[AspNetUsers] where UserName = @userName);
	
	print @defaultEmail;
	print @dbEmail;
	
	IF(@dbEmail = @defaultEmail and @email <> @defaultEmail and @email is not null)
	BEGIN
		Update AspNetUsers set 
	   Email = COALESCE(@email, Email)
	   where UserName=@userName;
	END

   select COUNT(*) from dbo.AspNetUsers where UPPER(UserName) = UPPER(@userName) and UPPER(Email) = UPPER(@email);
End
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 02/09/2017 15:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserProfileFromAdmin]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_UpdateUserProfileFromAdmin]  
(  
   @userName nvarchar (50),
   @userFulName nvarchar (250),
   @email nvarchar (250),  
   @phone nvarchar (50),
   @updatedBy nvarchar(250) = null,
   @comments nvarchar(MAX)
)  
as  
begin  
   Update AspNetUsers set 
   UserFullName = COALESCE(@userFulName, UserFullName), 
   Email = COALESCE(@email, Email), 
   PhoneNumber = COALESCE(@phone,PhoneNumber),
   UpdateDate = GETDATE(),
   UpdatedBy = @updatedBy,
   Comments = @comments
   where UserName=@userName
End
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUser]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateUser](
@UserName NVARCHAR(256),
@UserFullName nvarchar(256),
@Domain nvarchar(256) = null,
@UserTypeCode nvarchar(6)
)
AS
BEGIN

   IF EXISTS (SELECT * FROM [dbo].[AspNetUsers]
                   WHERE UserName = @UserName)
   BEGIN
       UPDATE [dbo].[AspNetUsers]
       SET UserWindowName = @UserName, UserFullName = @UserFullName, Domain = @Domain, UserTypeCode = @UserTypeCode
       WHERE UserName = @UserName
   END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUpdatedBy]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_UpdateUpdatedBy]  
(  
   @userName nvarchar (50),
   @comments nvarchar (MAX)
)  
as  
begin  
   Update AspNetUsers set 
   UpdateDate = GETDATE(),
   UpdatedBy = @userName,
   Comments = @comments
   where UserName=@userName
End

----


/****** Object:  StoredProcedure [dbo].[sp_UpdateMyProfile]    Script Date: 12/08/2016 14:28:42 ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateRolesById]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_UpdateRolesById]  
(  
   @RoleId int,
   @RoleName nvarchar (50),  
   @IsActive bit
)  
as  
begin  
   Update AspNetRoles set 
   Name = COALESCE(@RoleName, Name), 
   IsActive=COALESCE(@IsActive,IsActive)
   where Id=@RoleId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateMyProfile]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_UpdateMyProfile]  
(  
   @userName nvarchar (50),
   @email nvarchar (50),  
   @phone nvarchar (50),
   @comments nvarchar(MAX)
)  
as  
begin  
   Update AspNetUsers set 
   Email = COALESCE(@email, Email), 
   PhoneNumber = COALESCE(@phone,PhoneNumber),
   UpdateDate = GETDATE(),
   UpdatedBy = @userName,
   Comments = @comments
   where UserName=@userName
End

------------------------------
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateMenuById]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_UpdateMenuById]  
(  
   @AccessId int,
   @ApplicationId int, 
   @AccessName nvarchar (50),
   @Description nvarchar (250),
   @SortOrder int,
   @AccessType int,
   @AccessControl int,
   @IsActive bit
)  
as  
begin  
   Update ApplicationAccessMapping set 
   ApplicationId = COALESCE(@ApplicationId, ApplicationId), 
   AccessName = COALESCE(@AccessName, AccessName),
   Description = COALESCE(@Description, Description),
   SortOrder = COALESCE(@SortOrder, SortOrder),
   AccessType = COALESCE(@AccessType, AccessType),
   AccessControl = COALESCE(@AccessControl, AccessControl),
   IsActive=COALESCE(@IsActive,IsActive)
   where AccessId=@AccessId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateApplicationById]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_UpdateApplicationById]  
(  
   @ApplicationId int,
   @ApplicationName varchar (50),  
   @Description varchar (50) = null,
  -- @CurrentVersion nvarchar(6),
  -- @StartDate datetime,
 --  @EndDate datetime,
 --  @CreateDate datetime,
   @UpdateDate datetime,
 --  @CreatedBy nchar(10),
 --  @UpdatedBy nchar(10),
   @IsActive bit
)  
as  
begin  
   Update Applications set 
   ApplicationName = COALESCE(@ApplicationName,ApplicationName), 
   Description =COALESCE(@Description,Description),
  -- CurrentVersion=COALESCE(@CurrentVersion,CurrentVersion),
 --  StartDate=COALESCE(@StartDate,StartDate),
 --  EndDate=COALESCE(@EndDate,EndDate),
--   CreateDate=COALESCE(@CreateDate,CreateDate),
   UpdateDate=COALESCE(@UpdateDate,UpdateDate),
 --  CreatedBy=COALESCE(@CreatedBy,CreatedBy),
 --  UpdatedBy=COALESCE(@UpdatedBy,UpdatedBy),
   IsActive=COALESCE(@IsActive,IsActive)
   where ApplicationId=@ApplicationId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_OMS_GetUserInfo]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_OMS_GetUserInfo](
@pEmailId NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON
	
	 SELECT  UserID, CompanyID, UserGUID,UserType,
             Name,EmailID,  DATEADD(MINUTE, 20, GETDATE()) AS TokeValidTime,
	             CASE UserType 
				 WHEN  0  THEN  'Admin' 
				 WHEN  1  THEN  'Salesman'
				 WHEN  2  THEN  'Dealer/Retaler' 
				 WHEN  3  THEN  'Customer' 
				 END   AS Role	 
	 FROM        tbl_OMS_User 
	 WHERE       EmailID=@pEmailId
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OMS_AddUserProfile]    Script Date: 02/09/2017 15:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_OMS_AddUserProfile](
@pUserID NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON
	
	 DECLARE @UserId nvarchar(MAX);
	 DECLARE @UserFullName nvarchar(MAX);
	 SET @UserId = (SELECT [Id] FROM [dbo].[AspNetUsers] where UserName = @pUserID)
	 SET @UserFullName = (SELECT [Name] FROM [dbo].[tbl_OMS_User] where UserID = @pUserID)
	 
		 IF(@UserId <> '')
			BEGIN
		-- 	INSERT INTO [dbo].[AspNetUserRoles] ( [UserId],[RoleId] ) VALUES(@UserId,'1') -- RK comment for test, originally not commented.

			INSERT INTO [dbo].[AspNetUserClaims] ( [UserId],[ClaimType],[ClaimValue] )
			VALUES(@UserId,'BILLINGCO',LEFT(@pUserID,2));

			INSERT INTO [dbo].[AspNetUserClaims] ( [UserId],[ClaimType],[ClaimValue] )
			VALUES(@UserId,'FULLNAME',RTRIM(@UserFullName));

			INSERT INTO [dbo].[AspNetUserClaims] ( [UserId],[ClaimType],[ClaimValue] )
			VALUES(@UserId,'BROKERID',RIGHT(@pUserID,4));	   

		  END

--print(LEFT(@pUserID,2))
--print(RTRIM(@UserFullName))
--print(RIGHT(@pUserID,4))
END
GO
/****** Object:  Default [DF_ApplicationAccessMaping_ParentAccessId]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[ApplicationAccessMapping] ADD  CONSTRAINT [DF_ApplicationAccessMaping_ParentAccessId]  DEFAULT ((0)) FOR [ParentAccessId]
GO
/****** Object:  Default [DF_ApplicationAccessMaping_SortOrder]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[ApplicationAccessMapping] ADD  CONSTRAINT [DF_ApplicationAccessMaping_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
/****** Object:  Default [DF_ApplicationAccessMaping_CreateDate]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[ApplicationAccessMapping] ADD  CONSTRAINT [DF_ApplicationAccessMaping_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_ApplicationAccessMaping_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[ApplicationAccessMapping] ADD  CONSTRAINT [DF_ApplicationAccessMaping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_ApplicationRoleMapping_CreatedDate]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[ApplicationRoleMapping] ADD  CONSTRAINT [DF_ApplicationRoleMapping_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_ApplicationRoleMapping_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[ApplicationRoleMapping] ADD  CONSTRAINT [DF_ApplicationRoleMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Applications_CreateDate]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[Applications] ADD  CONSTRAINT [DF_Applications_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_Applications_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[Applications] ADD  CONSTRAINT [DF_Applications_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_AspNetUserRoles_CreateDate]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[AspNetUserRoles] ADD  CONSTRAINT [DF_AspNetUserRoles_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_AspNetUserRoles_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[AspNetUserRoles] ADD  CONSTRAINT [DF_AspNetUserRoles_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_AspNetUsers_CreateDate]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[AspNetUsers] ADD  CONSTRAINT [DF_AspNetUsers_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_LookupMappingTable_ParentCodeId]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[LookupMappingTable] ADD  CONSTRAINT [DF_LookupMappingTable_ParentCodeId]  DEFAULT ((0)) FOR [ParentCodeId]
GO
/****** Object:  Default [DF_LookupMappingTable_CreateDate]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[LookupMappingTable] ADD  CONSTRAINT [DF_LookupMappingTable_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_LookupMappingTable_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[LookupMappingTable] ADD  CONSTRAINT [DF_LookupMappingTable_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_RoleApplicationAccessMapping_IsAccess]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[RoleApplicationAccessMapping] ADD  CONSTRAINT [DF_RoleApplicationAccessMapping_IsAccess]  DEFAULT ((0)) FOR [IsAccess]
GO
/****** Object:  Default [DF_RoleApplicationAccessMapping_CreateDate]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[RoleApplicationAccessMapping] ADD  CONSTRAINT [DF_RoleApplicationAccessMapping_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_RoleApplicationAccessMapping_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[RoleApplicationAccessMapping] ADD  CONSTRAINT [DF_RoleApplicationAccessMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_tbl_OMS_User_UserType]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[tbl_OMS_User] ADD  CONSTRAINT [DF_tbl_OMS_User_UserType]  DEFAULT ((0)) FOR [UserType]
GO
/****** Object:  Default [DF_tbl_OMS_User_OrderAcknowledgement]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[tbl_OMS_User] ADD  CONSTRAINT [DF_tbl_OMS_User_OrderAcknowledgement]  DEFAULT ((0)) FOR [OrderAcknowledgement]
GO
/****** Object:  Default [DF_tbl_OMS_User_AttachOrderConfirmation]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[tbl_OMS_User] ADD  CONSTRAINT [DF_tbl_OMS_User_AttachOrderConfirmation]  DEFAULT ((0)) FOR [AttachOrderConfirmation]
GO
/****** Object:  Default [DF_tbl_OMS_User_IsAuthenticated]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[tbl_OMS_User] ADD  CONSTRAINT [DF_tbl_OMS_User_IsAuthenticated]  DEFAULT ((0)) FOR [IsAuthenticated]
GO
/****** Object:  Default [DF_tbl_OMS_User_UserIsDeleted]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[tbl_OMS_User] ADD  CONSTRAINT [DF_tbl_OMS_User_UserIsDeleted]  DEFAULT ((0)) FOR [UserIsDeleted]
GO
/****** Object:  Default [DF_UserCompanyMapping_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[UserCompanyMapping] ADD  CONSTRAINT [DF_UserCompanyMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_UserDivisonMgrMapping_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[UserDivisonMgrMapping] ADD  CONSTRAINT [DF_UserDivisonMgrMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_UserTypeCode_IsActive]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[UserType] ADD  CONSTRAINT [DF_UserTypeCode_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]    Script Date: 02/09/2017 15:02:48 ******/
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
