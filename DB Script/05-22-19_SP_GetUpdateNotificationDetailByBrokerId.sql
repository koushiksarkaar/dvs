IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[SP_GetUpdateNotificationDetailByBrokerId]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[SP_GetUpdateNotificationDetailByBrokerId]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetUpdateNotificationDetailByBrokerId]   
	(
		@CompanyId nvarchar(10),
		@BrokerId nvarchar(10)
	)     
AS
BEGIN
	SET NOCOUNT ON;
	declare @DivisionId nvarchar(10)
	declare @GroupId nvarchar(10)
	declare @DocumentBriefcaseCount nvarchar(10)
	declare @CreditHoldOrderCount nvarchar(10)
	declare @BrokerIdDb2 nvarchar(10)
	declare @Db2Query varchar(max)

	create table #CreditHoldOrderCount(
		CREDITHOLDORDERCOUNT nvarchar(10)
	)

	set @BrokerIdDb2 = SUBSTRING(@BrokerId, 3, 4) 

	if not exists(
      Select * from tbl_OMS_NotificationDetail where BrokerId = @BrokerId
	)

	begin
		declare @idColumn int
		select @idColumn = min( NotificationTypeId ) from tbl_OMS_NotificationType

		while @idColumn is not null -- Loop through NoticationType detail to add new rows since broker is new
		begin
			insert into tbl_OMS_NotificationDetail(
				BrokerId,
				NotificationTypeId,
				NotificationCount,
				NotificationReadCount,
				CreatedDate,
				CreatedBy,
				ModifiedDate,
				ModifiedBy
			)
			values(
				@BrokerId,
				@idColumn,
				0,
				0,
				GETDATE(),
				NULL,
				NULL,
				NULL
			)

			select @idColumn = min( NotificationTypeId ) from tbl_OMS_NotificationType where NotificationTypeId > @idColumn
		end
	end

	select -- Convert to db2 parameters
		@DivisionId = BrokerDivisionCode,
		@GroupId=BrokerGroupCode 
	from  [Goya_UtilDB].[dbo].[vw_User_Master_Data] 
	where 
		COMPANY = @CompanyId 
		and USERNO = @BrokerId

	select -- Populate briefcase count
		@DocumentBriefcaseCount =
			count(*)
			from tbl_OMS_SalesCommunicator
			where 
				CompanyId = @CompanyId and 
				IsDelete = 0 and 
				(DivisionId in (@DivisionId, null,'') and GroupId in (@GroupId, null,'') or BrokerId = @BrokerId)

	set @Db2Query = ( -- populate credit hold order count
		select 'SELECT COUNT ( * ) AS CREDITHOLDORDERCOUNT FROM GOYA . ORVHEAD03
		WHERE ORHCO = '''''+@CompanyId+'''''
		AND BRROUT = '''''+@BrokerIdDb2+'''''
		AND ORHSTS = 11 '
	)

	set @Db2Query = (select 'select * from openquery(goya04_oledb ,'+'''' +@DB2Query+''''+')')

	insert into #CreditHoldOrderCount exec (@DB2Query)

	select
		@CreditHoldOrderCount =
			CreditHoldOrderCount
			from #CreditHoldOrderCount

	update tbl_OMS_NotificationDetail
	set NotificationCount = @DocumentBriefcaseCount
	where BrokerId = @BrokerId
	and NotificationTypeId = 1

	update tbl_OMS_NotificationDetail
	set NotificationCount = @CreditHoldOrderCount
	where BrokerId = @BrokerId
	and NotificationTypeId = 2

	select * from tbl_OMS_NotificationDetail where BrokerId = @BrokerId
END

--[SP_GetUpdateNotificationDetailByBrokerId] '01', '013506'