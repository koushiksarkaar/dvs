CREATE PROCEDURE [dbo].[SP_GetPlanRouteCustomerDetailsPriority]
 (
    @CompanyId nvarchar(10),
	@BrokerId nvarchar(50),
	@Day nvarchar(50)
 )
AS
BEGIN
	SET NOCOUNT ON;

	if(@Day = 'Monday')
		begin
			select 
			CustomerId, 
			BrokerId,
			Monday,
			Tuesday, 
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday,
			MondayPriority,
			TuesdayPriority,
			WednesdayPriority,
			ThursdayPriority,
			FridayPriority,
			SaturdayPriority,
			SundayPriority
			from  TBL_OMS_PlanRouteCustomerDetails  
			where BrokerId = @BrokerId
			and Monday = 1
		end
	else if(@Day = 'Tuesday')
		begin
			select 
			CustomerId, 
			BrokerId,
			Monday,
			Tuesday, 
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday,
			MondayPriority,
			TuesdayPriority,
			WednesdayPriority,
			ThursdayPriority,
			FridayPriority,
			SaturdayPriority,
			SundayPriority
			from  TBL_OMS_PlanRouteCustomerDetails  
			where BrokerId = @BrokerId
			and Tuesday = 1
		end
	else if(@Day = 'Wednesday')
		begin
			select 
			CustomerId, 
			BrokerId,
			Monday,
			Tuesday, 
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday,
			MondayPriority,
			TuesdayPriority,
			WednesdayPriority,
			ThursdayPriority,
			FridayPriority,
			SaturdayPriority,
			SundayPriority
			from  TBL_OMS_PlanRouteCustomerDetails  
			where BrokerId = @BrokerId
			and Wednesday = 1
		end
	else if(@Day = 'Thursday')
		begin
			select 
			CustomerId, 
			BrokerId,
			Monday,
			Tuesday, 
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday,
			MondayPriority,
			TuesdayPriority,
			WednesdayPriority,
			ThursdayPriority,
			FridayPriority,
			SaturdayPriority,
			SundayPriority
			from  TBL_OMS_PlanRouteCustomerDetails  
			where BrokerId = @BrokerId
			and Thursday = 1
		end
	else if(@Day = 'Friday')
		begin
			select 
			CustomerId, 
			BrokerId,
			Monday,
			Tuesday, 
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday,
			MondayPriority,
			TuesdayPriority,
			WednesdayPriority,
			ThursdayPriority,
			FridayPriority,
			SaturdayPriority,
			SundayPriority
			from  TBL_OMS_PlanRouteCustomerDetails  
			where BrokerId = @BrokerId
			and Friday = 1
		end
	else if(@Day = 'Saturday')
		begin
			select 
			CustomerId, 
			BrokerId,
			Monday,
			Tuesday, 
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday,
			MondayPriority,
			TuesdayPriority,
			WednesdayPriority,
			ThursdayPriority,
			FridayPriority,
			SaturdayPriority,
			SundayPriority
			from  TBL_OMS_PlanRouteCustomerDetails  
			where BrokerId = @BrokerId
			and Saturday = 1
		end
	else if (@Day = 'Sunday')
		begin
			select 
			CustomerId, 
			BrokerId,
			Monday,
			Tuesday, 
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday,
			MondayPriority,
			TuesdayPriority,
			WednesdayPriority,
			ThursdayPriority,
			FridayPriority,
			SaturdayPriority,
			SundayPriority
			from  TBL_OMS_PlanRouteCustomerDetails  
			where BrokerId = @BrokerId
			and Sunday = 1
		end
END

-- [SP_GetPlanRouteCustomerDetails] '01','013506'

