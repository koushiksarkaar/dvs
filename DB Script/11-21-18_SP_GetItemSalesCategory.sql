USE [PriceCatalog]
GO
/****** Object:  StoredProcedure [dbo].[SP_GetItemSalesCategory]    Script Date: 11/21/2018 3:27:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_GetItemSalesCategory]
 @CompanyId NVARCHAR(100)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT   C.SalesCategoryID,replace(replace(replace(c.Description,'“',''),'”',''),'"','') as SalesCategoryDescription,
	replace(replace(replace(sc.Description,'“',''),'”',''),'"','') as SalesSubCategoryDescription,
	sc.SalesSubCategoryID 

  FROM  [tbl_OMS_SalesCategory] C
    inner join   [tbl_OMS_SalesSubCategory] SC ON C.SalesCategoryID = SC.SalesCategoryID
END


 
 