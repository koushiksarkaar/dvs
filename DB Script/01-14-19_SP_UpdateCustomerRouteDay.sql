ALTER PROCEDURE [dbo].[SP_UpdateCustomerRouteDay]
(
   @CompanyId nvarchar(10),
   @BrokerId nvarchar(50),
   @CustomerId nvarchar(50),
   --@Monday bit,
   --@Tuesday bit, 
   --@Wednesday bit,
   --@Thursday bit,
   --@Friday bit,
   --@Saturday bit,
   --@Sunday bit
   @Day nvarchar(20),
   @DayBool bit
)
AS
BEGIN

	SET NOCOUNT ON;

	declare @Priority int

	if Exists(select customerid from TBL_OMS_PlanRouteCustomerDetails Where CompanyId = @CompanyId and BrokerId = @BrokerId and CustomerId = @CustomerId) -- If row alrady exists
		BEGIN
				--Update TBL_OMS_PlanRouteCustomerDetails
				-- SET 
				--	  Monday=@Monday,
				--	  Tuesday=@Tuesday,
				--	  Wednesday=@Wednesday,
				--	  Thursday=@Thursday,
				--	  Friday=@Friday,
				--	  Saturday=@Saturday,
				--	  Sunday=@Sunday
				--Where CompanyId = @CompanyId and BrokerId = @BrokerId and CustomerId = @CustomerId

				if(@Day = 'Monday')
					begin
						set @Priority = (select max(MondayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@DayBool = 1) -- Day was checked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = @Priority + 1
									end
								else -- Priority does not exist
									begin
										set @Priority = 0
									end

								update tbl_OMS_PlanRouteCustomerDetails
								set MondayPriority = @Priority, Monday = @DayBool
								where BrokerId = @BrokerId and CustomerId = @CustomerId
							end
						else -- Day was unchecked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = (select MondayPriority from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId and CustomerId = @CustomerId)

										update tbl_OMS_PlanRouteCustomerDetails
										set MondayPriority = null, Monday = @DayBool
										where MondayPriority = @Priority and BrokerId = @BrokerId and CustomerId = @CustomerId

										update tbl_OMS_PlanRouteCustomerDetails
										set MondayPriority = MondayPriority - 1
										where MondayPriority > @Priority and BrokerId = @BrokerId
									end -- No else condition for priority does not exist because if day was unchecked, priority will always exist
							end
					end
				else if(@Day = 'Tuesday')
					begin
						set @Priority = (select max(TuesdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@DayBool = 1) -- Day was checked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = @Priority + 1
									end
								else -- Priority does not exist
									begin
										set @Priority = 0
									end

								update tbl_OMS_PlanRouteCustomerDetails
								set TuesdayPriority = @Priority, Tuesday = @DayBool
								where BrokerId = @BrokerId and CustomerId = @CustomerId
							end
						else -- Day was unchecked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = (select TuesdayPriority from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId and CustomerId = @CustomerId)

										update tbl_OMS_PlanRouteCustomerDetails
										set TuesdayPriority = null, Tuesday = @DayBool
										where TuesdayPriority = @Priority and BrokerId = @BrokerId and CustomerId = @CustomerId

										update tbl_OMS_PlanRouteCustomerDetails
										set TuesdayPriority = TuesdayPriority - 1
										where TuesdayPriority > @Priority and BrokerId = @BrokerId
									end -- No else condition for priority does not exist because if day was unchecked, priority will always exist
							end
					end
				else if(@Day = 'Wednesday')
					begin
						set @Priority = (select max(WednesdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@DayBool = 1) -- Day was checked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = @Priority + 1
									end
								else -- Priority does not exist
									begin
										set @Priority = 0
									end

								update tbl_OMS_PlanRouteCustomerDetails
								set WednesdayPriority = @Priority, Wednesday = @DayBool
								where BrokerId = @BrokerId and CustomerId = @CustomerId
							end
						else -- Day was unchecked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = (select WednesdayPriority from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId and CustomerId = @CustomerId)

										update tbl_OMS_PlanRouteCustomerDetails
										set WednesdayPriority = null, Wednesday = @DayBool
										where WednesdayPriority = @Priority and BrokerId = @BrokerId and CustomerId = @CustomerId

										update tbl_OMS_PlanRouteCustomerDetails
										set WednesdayPriority = WednesdayPriority - 1
										where WednesdayPriority > @Priority and BrokerId = @BrokerId
									end -- No else condition for priority does not exist because if day was unchecked, priority will always exist
							end
					end
				else if(@Day = 'Thursday')
					begin
						set @Priority = (select max(ThursdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@DayBool = 1) -- Day was checked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = @Priority + 1
									end
								else -- Priority does not exist
									begin
										set @Priority = 0
									end

								update tbl_OMS_PlanRouteCustomerDetails
								set ThursdayPriority = @Priority, Thursday = @DayBool
								where BrokerId = @BrokerId and CustomerId = @CustomerId
							end
						else -- Day was unchecked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = (select ThursdayPriority from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId and CustomerId = @CustomerId)

										update tbl_OMS_PlanRouteCustomerDetails
										set ThursdayPriority = null, Thursday = @DayBool
										where ThursdayPriority = @Priority and BrokerId = @BrokerId and CustomerId = @CustomerId

										update tbl_OMS_PlanRouteCustomerDetails
										set ThursdayPriority = ThursdayPriority - 1
										where ThursdayPriority > @Priority and BrokerId = @BrokerId
									end -- No else condition for priority does not exist because if day was unchecked, priority will always exist
							end
					end
				else if(@Day = 'Friday')
					begin
						set @Priority = (select max(FridayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@DayBool = 1) -- Day was checked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = @Priority + 1
									end
								else -- Priority does not exist
									begin
										set @Priority = 0
									end

								update tbl_OMS_PlanRouteCustomerDetails
								set FridayPriority = @Priority, Friday = @DayBool
								where BrokerId = @BrokerId and CustomerId = @CustomerId
							end
						else -- Day was unchecked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = (select FridayPriority from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId and CustomerId = @CustomerId)

										update tbl_OMS_PlanRouteCustomerDetails
										set FridayPriority = null, Friday = @DayBool
										where FridayPriority = @Priority and BrokerId = @BrokerId and CustomerId = @CustomerId

										update tbl_OMS_PlanRouteCustomerDetails
										set FridayPriority = FridayPriority - 1
										where FridayPriority > @Priority and BrokerId = @BrokerId
									end -- No else condition for priority does not exist because if day was unchecked, priority will always exist
							end
					end
				else if(@Day = 'Saturday')
					begin
						set @Priority = (select max(SaturdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@DayBool = 1) -- Day was checked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = @Priority + 1
									end
								else -- Priority does not exist
									begin
										set @Priority = 0
									end

								update tbl_OMS_PlanRouteCustomerDetails
								set SaturdayPriority = @Priority, Saturday = @DayBool
								where BrokerId = @BrokerId and CustomerId = @CustomerId
							end
						else -- Day was unchecked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = (select SaturdayPriority from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId and CustomerId = @CustomerId)

										update tbl_OMS_PlanRouteCustomerDetails
										set SaturdayPriority = null, Saturday = @DayBool
										where SaturdayPriority = @Priority and BrokerId = @BrokerId and CustomerId = @CustomerId

										update tbl_OMS_PlanRouteCustomerDetails
										set SaturdayPriority = SaturdayPriority - 1
										where SaturdayPriority > @Priority and BrokerId = @BrokerId
									end -- No else condition for priority does not exist because if day was unchecked, priority will always exist
							end
					end
				else if(@Day = 'Sunday')
					begin
						set @Priority = (select max(SundayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@DayBool = 1) -- Day was checked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = @Priority + 1
									end
								else -- Priority does not exist
									begin
										set @Priority = 0
									end

								update tbl_OMS_PlanRouteCustomerDetails
								set SundayPriority = @Priority, Sunday = @DayBool
								where BrokerId = @BrokerId and CustomerId = @CustomerId
							end
						else -- Day was unchecked
							begin
								if(@Priority is not null) -- Priority exists
									begin
										set @Priority = (select SundayPriority from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId and CustomerId = @CustomerId)

										update tbl_OMS_PlanRouteCustomerDetails
										set SundayPriority = null, Sunday = @DayBool
										where SundayPriority = @Priority and BrokerId = @BrokerId and CustomerId = @CustomerId

										update tbl_OMS_PlanRouteCustomerDetails
										set SundayPriority = SundayPriority - 1
										where SundayPriority > @Priority and BrokerId = @BrokerId
									end -- No else condition for priority does not exist because if day was unchecked, priority will always exist
							end
					end
					END
	ELSE   -- If New customer is checked for route planning
	BEGIN

	  --   insert into TBL_OMS_PlanRouteCustomerDetails
		 --values( @CompanyId,@BrokerId,@CustomerId, @Monday ,@Tuesday ,@Wednesday ,  @Thursday, @Friday , @Saturday, @Sunday ,null,null,null,null,null, null, null, null, null, null, null, null)

		 if(@Day = 'Monday')
			begin
				if(@DayBool = 1) -- Day was checked
					begin
						set @Priority = (select max(MondayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@Priority is not null) -- Priority exists
							begin
								set @Priority = @Priority + 1
							end
						else -- Priority does not exist
							begin
								set @Priority = 0
							end

						insert into tbl_OMS_PlanRouteCustomerDetails
						values (@CompanyId, @BrokerId, @CustomerId, @DayBool, 0, 0, 0, 0, 0, 0 , null, null, null, null, null, @Priority, null, null, null, null, null, null)
					end -- No else condition for day was unchecked because if customer does not exist, day can never be unchecked
			end
		else if(@Day = 'Tuesday')
			begin
				if(@DayBool = 1) -- Day was checked
					begin
						set @Priority = (select max(TuesdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@Priority is not null) -- Priority exists
							begin
								set @Priority = @Priority + 1
							end
						else -- Priority does not exist
							begin
								set @Priority = 0
							end

						insert into tbl_OMS_PlanRouteCustomerDetails
						values (@CompanyId, @BrokerId, @CustomerId, 0, @DayBool, 0, 0, 0, 0, 0 , null, null, null, null, null, null, @Priority, null, null, null, null, null)
					end -- No else condition for day was unchecked because if customer does not exist, day can never be unchecked
			end
		else if(@Day = 'Wednesday')
			begin
				if(@DayBool = 1) -- Day was checked
					begin
						set @Priority = (select max(WednesdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@Priority is not null) -- Priority exists
							begin
								set @Priority = @Priority + 1
							end
						else -- Priority does not exist
							begin
								set @Priority = 0
							end

						insert into tbl_OMS_PlanRouteCustomerDetails
						values (@CompanyId, @BrokerId, @CustomerId, 0, 0, @DayBool, 0, 0, 0, 0 , null, null, null, null, null, null, null, @Priority, null, null, null, null)
					end -- No else condition for day was unchecked because if customer does not exist, day can never be unchecked
			end
		else if(@Day = 'Thursday')
			begin
				if(@DayBool = 1) -- Day was checked
					begin
						set @Priority = (select max(ThursdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@Priority is not null) -- Priority exists
							begin
								set @Priority = @Priority + 1
							end
						else -- Priority does not exist
							begin
								set @Priority = 0
							end

						insert into tbl_OMS_PlanRouteCustomerDetails
						values (@CompanyId, @BrokerId, @CustomerId, 0, 0, 0, @DayBool, 0, 0, 0 , null, null, null, null, null, null, null, null, @Priority, null, null, null)
					end -- No else condition for day was unchecked because if customer does not exist, day can never be unchecked
			end
		else if(@Day = 'Friday')
			begin
				if(@DayBool = 1) -- Day was checked
					begin
						set @Priority = (select max(FridayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@Priority is not null) -- Priority exists
							begin
								set @Priority = @Priority + 1
							end
						else -- Priority does not exist
							begin
								set @Priority = 0
							end

						insert into tbl_OMS_PlanRouteCustomerDetails
						values (@CompanyId, @BrokerId, @CustomerId, 0, 0, 0, 0, @DayBool, 0, 0 , null, null, null, null, null, null, null, null, null, @Priority, null, null)
					end -- No else condition for day was unchecked because if customer does not exist, day can never be unchecked
			end
		else if(@Day = 'Saturday')
			begin
				if(@DayBool = 1) -- Day was checked
					begin
						set @Priority = (select max(SaturdayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@Priority is not null) -- Priority exists
							begin
								set @Priority = @Priority + 1
							end
						else -- Priority does not exist
							begin
								set @Priority = 0
							end

						insert into tbl_OMS_PlanRouteCustomerDetails
						values (@CompanyId, @BrokerId, @CustomerId, 0, 0, 0, 0, 0, @DayBool, 0 , null, null, null, null, null, null, null, null, null, null, @Priority, null)
					end -- No else condition for day was unchecked because if customer does not exist, day can never be unchecked
			end
		else if(@Day = 'Sunday')
			begin
				if(@DayBool = 1) -- Day was checked
					begin
						set @Priority = (select max(SundayPriority) from tbl_OMS_PlanRouteCustomerDetails where BrokerId = @BrokerId)

						if(@Priority is not null) -- Priority exists
							begin
								set @Priority = @Priority + 1
							end
						else -- Priority does not exist
							begin
								set @Priority = 0
							end

						insert into tbl_OMS_PlanRouteCustomerDetails
						values (@CompanyId, @BrokerId, @CustomerId, 0, 0, 0, 0, 0, 0, @DayBool , null, null, null, null, null, null, null, null, null, null, null, @Priority)
					end -- No else condition for day was unchecked because if customer does not exist, day can never be unchecked
			end
		END
END

 