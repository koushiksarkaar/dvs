/****** Object:  Table [dbo].[tbl_OMS_CreditFormBusinessTradeRef]    Script Date: 9/20/2017 1:06:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_OMS_CreditFormBusinessTradeRef](
	[TradeId] [int] IDENTITY(1,1) NOT NULL,
	[CreditId] [int] NOT NULL,
	[TradeName] [nvarchar](100) NULL,
	[StreetAddress] [nvarchar](500) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Zip] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[Account] [nvarchar](100) NULL,
	[ContactName] [nvarchar](100) NULL,
	[ContactPhoneNo] [nvarchar](50) NULL,
	[RelationshipPeriod] [nvarchar](100) NULL,
	[CreditLimitAmount] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_OMS_CreditFormBusinessTradeRef] PRIMARY KEY CLUSTERED 
(
	[TradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormBusinessTradeRef]  WITH CHECK ADD  CONSTRAINT [FK_tbl_OMS_CreditFormBusinessTradeRef_tbl_OMS_CreditFormBusinessTradeRef] FOREIGN KEY([CreditId])
REFERENCES [dbo].[tbl_OMS_CreditFormCustomerInfo] ([CreditId])
GO

ALTER TABLE [dbo].[tbl_OMS_CreditFormBusinessTradeRef] CHECK CONSTRAINT [FK_tbl_OMS_CreditFormBusinessTradeRef_tbl_OMS_CreditFormBusinessTradeRef]
GO


