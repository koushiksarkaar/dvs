ALTER PROCEDURE [dbo].[SP_GetOrderListRapidFireOrder]
(
 @CompanyId nvarchar(10),
 @DivisionId nvarchar(10) = null,
 @GroupId nvarchar(10) = null,
 @BrokerId nvarchar(10)=null,
 @FromDate DateTime,
 @ToDate DateTime,
 @Hour nvarchar(10)
 --@TimeDifference int -- in seconds
)
AS
BEGIN

       DECLARE @BrokerString VARCHAR(MAX);
       DECLARE @FromDate1 VARCHAR(50);
       DECLARE @ToDate1 VARCHAR(50);
       DECLARE @DB2Query VARCHAR(MAX);
      
     
      
	SET NOCOUNT ON;
	
	set @FromDate1 = (select CONVERT(DATE,@FromDate));
	set @ToDate1 = (select CONVERT(DATE,@ToDate));
	
	
----------------------- Get Broker List-------------------------------------
Select B.COMPANY,b.BrokerDivisionCode, b.BrokerDivisionDesc, B.BrokerGroupCode,  B.USERNO,B.BRROUT,B.USERNAME  INTO #tempBroker
from     [Goya_UtilDB].[dbo].[vw_User_Master_Data] B 
         where B.COMPANY = @CompanyId         
           AND (( B.[BrokerDivisionCode]  = @DivisionId OR @DivisionId is NULL OR @DivisionId=''))
             AND (( B.[BrokerGroupCode] = @GroupId OR @GroupId is NULL OR @GroupId=''))
         AND (( B.[BRROUT] = @BrokerId OR @BrokerId is NULL OR @BrokerId=''))

		 -- SELECT * FROM   [Goya_UtilDB].[dbo].[vw_User_Master_Data];
Select  @BrokerString = COALESCE(@BrokerString +',','') +''''+ @CompanyId +''+B.BRROUT +''''
   --B.BRROUT as BrokerId,B.USERNAME as BrokerName
         From #tempBroker B
------------------------Get Orders List for By Hour----------------------------------------------------
 declare @sql varchar(max);
  declare @sql1 varchar(max);

 set @sql ='
SELECT  O.SalesmanId,CustomerId,Latitude,Longitude,
 (CASE WHEN DATEPART(HOUR, O.OrderDate) BETWEEN 0 AND 11 THEN CONVERT(VARCHAR(10),DATEPART(HOUR, O.OrderDate))+'' AM''
      WHEN DATEPART(HOUR, O.OrderDate) = 12 THEN CONVERT(VARCHAR(10),DATEPART(HOUR, O.OrderDate))+'' PM''
  ELSE CONVERT(VARCHAR(10),DATEPART(HOUR, O.OrderDate)-12)+'' PM'' END) as OrderHour,
  OrderQty,
  OrderAmount
From 
GOYADB2..tbl_OMS_Order O 
WHERE SalesmanId IN ('+@BrokerString+')' +'
AND DATEADD(dd, 0, DATEDIFF(dd, 0, OrderDate)) Between '+ ''''+@FromDate1+ ''''+  'and ' + ''''+ @ToDate1+ '''';

CREATE TABLE #Temp1 
(SalesmanId nvarchar(20),
 CustomerId nvarchar(50),
Latitude nvarchar(200),
Longitude nvarchar(200),
 OrderHour NVARCHAR(10),
 OrderQty nvarchar(20),
 OrderAmount nvarchar(20))
 
    --  exec (@sql);

 Insert into #Temp1 EXEC (@sql);
 
   SELECT t1.SalesmanId,(t1.CustomerId+' - '+c.CSNAME) as CustomerInfo,t1.Latitude,t1.Longitude,t1.OrderHour, t1.OrderQty, t1.OrderAmount, 
	ISNULL(C.CSADR2+', ','')
	+ISNULL(C.CSCITY+', ','')
	+ISNULL(C.CSST+', ','')
	+LTRIM(ISNULL(C.CSZIP,'')) AS Address
   FROM #temp1 t1 
      inner join [Goya_UtilDB].[dbo].[vw_CustomerDetail] C on c.CSNO = t1.CustomerId
    WHERE   (( T1.OrderHour = @Hour OR @Hour is NULL OR @Hour='')) 
        and  t1.Latitude != '0.000000000' ;
          
END

 