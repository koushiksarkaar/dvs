IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[SProc_GetItemsListTagSalesCategoryData]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[SProc_GetItemsListTagSalesCategoryData]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SProc_GetItemsListTagSalesCategoryData]
AS
BEGIN
	SELECT PRODUCTCODE, NEWPRODUCT, PRICECHANGES, SIZECHANGES, PACKCHANGES, FOODSERVICE, ORGANICPRODUCT, MERCDISPLAY, CATID, SUBCATID from  [tbl_Goya_CatalogDetail]
END