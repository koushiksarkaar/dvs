IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES T
        WHERE T.TABLE_SCHEMA = 'dbo'
        AND T.TABLE_NAME = 'tbl_OMS_NotificationType')
    BEGIN

        CREATE TABLE dbo.tbl_OMS_NotificationType
        (
			[NotificationTypeId] int identity(1,1) primary key,
            [NotificationTypeName] varchar(100),
			[IsActive] bit,
			[CreatedDate] datetime,
			[CreatedBy] varchar(10),
			[ModifiedDate] datetime,
			[ModifiedBy] varchar(10)
        )
    END
GO