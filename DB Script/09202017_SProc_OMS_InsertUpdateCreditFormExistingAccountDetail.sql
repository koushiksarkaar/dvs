/****** Object:  StoredProcedure [dbo].[SProc_OMS_InsertUpdateCreditFormExistingAccountDetail]    Script Date: 9/20/2017 1:03:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Moiz Ahmed
-- Create date: 18 Sep 2017
-- Description:	Insert / Update Creadit Form Existing Account Detail
-- =============================================
CREATE PROCEDURE [dbo].[SProc_OMS_InsertUpdateCreditFormExistingAccountDetail]
(
	@AccountId int,
	@CreditId int,
	@Name nvarchar(100),
	@StreetAddress nvarchar(500),
	@City nvarchar(100),
	@State nvarchar(100),
	@Zip nvarchar(50),
	@Account nvarchar(100),
	@Open bit,
	@Closed bit,
	@Sold bit,
	@RelationshipToAccount nvarchar(100),
	@CreatedBy nvarchar(100),
	@UpdatedBy nvarchar(100),
	@Message int output
)
AS
BEGIN

	IF EXISTS(SELECT 1 FROM tbl_OMS_CreditFormExistingAccountDetail WHERE [AccountId]=@AccountId)
	BEGIN
		SET @Message=1

		UPDATE tbl_OMS_CreditFormExistingAccountDetail
		SET
			Name = @Name,
			StreetAddress = @StreetAddress,
			City = @City,
			[State] = @State,
			Zip = @Zip,
			Account = @Account,
			[Open] = @Open,
			Closed = @Closed,
			Sold = @Sold,
			RelationshipToAccount = @RelationshipToAccount,
			UpdateDate = SYSDATETIME(),
			UpdatedBy = @UpdatedBy
		WHERE
			AccountId = @AccountId
	END
	ELSE
	BEGIN
		SET @Message=0

		INSERT INTO tbl_OMS_CreditFormExistingAccountDetail
		(
			CreditId,
			Name,
			StreetAddress,
			City,
			[State],
			Zip,
			Account,
			[Open],
			Closed,
			Sold,
			RelationshipToAccount,
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@CreditId,
			@Name,
			@StreetAddress,
			@City,
			@State,
			@Zip,
			@Account,
			@Open,
			@Closed,
			@Sold,
			@RelationshipToAccount,
			SYSDATETIME(),
			@CreatedBy
		)
	END
END
