USE [SB3OMS2017]
GO
/****** Object:  StoredProcedure [dbo].[SProc_OMS_ProductSearch]    Script Date: 4/4/2017 11:20:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_ProductSearch] 
(
	 @CustomerId NVARCHAR(30)
	,@VendorId NVARCHAR(30)=NULL
	,@CategoryId NVARCHAR(30)=NULL
	,@WarehouseId NVARCHAR(30)
	,@GroupId NVARCHAR(30)=NULL
	,@SearchText NVARCHAR(30)=NULL
	,@MfgUpc NVARCHAR(30)=NULL

)
AS
BEGIN
	
	SET NOCOUNT ON;
	 
	 DECLARE @Contractno NVARCHAR(30)

		SELECT @Contractno=CSContractNo FROM tbl_OMS_MCustomer WHERE CustomerId=@CustomerId
		DECLARE @SQL NVARCHAR(MAX)

		SET @SQL=''
		SET @SQL=@SQL+'
		SELECT PM.[ProductId],[DisplayName],[Description],[MfgCode],[Manufacture],
		[Uom],[UPC1],[UPC2],[UPC3],[UPC4],[UPC5],
		ISNULL(PR.CasePrice, [ListPrice])Price,PR.RetPrice,	  
		[Brand],[Zone],[PackedType],[PackSize],[GrossWeight],[Height],[Width]
		,[Length],[CategoryId],[VendorId],[VendorGroupId],PM.[CompanyID],PM.[WarehouseId]
		,[OHQty],[VendorName],[ProductCategory],[VendorGroupDescription],[IsDeleted],[IsActive]
		FROM [VW_OMS_ProductMaster] PM LEFT OUTER JOIN tbl_OMS_MPrice PR
		ON PM.ProductId=PR.ProductId AND PR.CSContractNo='''+ @Contractno +'''
		WHERE PM.WarehouseId='''+ @WarehouseId+''''

		IF(@VendorId IS NOT NULL)
		BEGIN 
		 SET @SQL=@SQL+	' AND PM.VendorId='''+ @VendorId+''''
		END
		IF(@GroupId IS NOT NULL)
		BEGIN
			 SET @SQL=@SQL+	' AND PM.VendorGroupId='''+@GroupId+''''
		END
		IF(@CategoryId IS NOT NULL)
		BEGIN
			 SET @SQL=@SQL+	' AND PM.CategoryId='''+@CategoryId+''''
		END
		IF(@SearchText IS NOT NULL)
		BEGIN
			SET @SQL=@SQL+	' AND (PM.DisplayName LIKE ''% '+ @SearchText +'%'' OR PM.[Description] LIKE''%'+ @SearchText+'%'' OR [MfgCode] LIKE ''%'+ @SearchText +'%'')'
		END
		IF(@MfgUpc IS NOT NULL)
		BEGIN
			SET @SQL=@SQL+	'AND (UPC1='''+ @MfgUpc +''' OR MfgCode='''+ @MfgUpc+''')'
		END
		
		--PRINT(@SQL)
		EXEC(@SQL)
   
END
