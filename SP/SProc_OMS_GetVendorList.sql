USE [SB3OMS2017]
GO
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetVendorList]    Script Date: 4/4/2017 11:20:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author Aparesh >
-- Create date: <Create Date 2017-04-04>
-- Description:	<Description Procedure for generate vendor list>
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetVendorList]

AS
BEGIN
	
	SET NOCOUNT ON;

     SELECT [VendorId] AS [ValueField] ,[VNDName] AS[TextField]
	 FROM tbl_OMS_MVendor WITH(NOLOCK) ORDER BY [VNDName]
END
