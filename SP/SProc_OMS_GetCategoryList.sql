USE [SB3OMS2017]
GO
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetCategoryList]    Script Date: 4/4/2017 11:18:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author Aparesh >
-- Create date: <Create Date 2017-04-04>
-- Description:	<Description Procedure for generate Category/Product Class List>
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetCategoryList]

AS
BEGIN
	
	SET NOCOUNT ON;

     SELECT [CategoryId] AS [ValueField] , [CatDesc] AS [TextField]
	 FROM [tbl_OMS_MCategory] WITH(NOLOCK) ORDER BY [CatDesc]
END
