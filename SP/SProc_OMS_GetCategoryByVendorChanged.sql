
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetCategoryByVendorChanged]    Script Date: 4/4/2017 11:17:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author Aparesh >
-- Create date: <Create Date 2017-04-04>
-- Description:	<Description Procedure for generate Category By Vendor Changed>
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetCategoryByVendorChanged]
(
	@VendorId nvarchar(30)
)
AS
BEGIN
	
	SET NOCOUNT ON;
       SELECT DISTINCT  tbl_OMS_ProductHierarchy.CategoryId AS [ValueField],tbl_OMS_MCategory.CatDesc AS [TextField]
		FROM   tbl_OMS_MCategory WITH(NOLOCK) INNER JOIN tbl_OMS_ProductHierarchy WITH(NOLOCK)
        ON     tbl_OMS_MCategory.CategoryId =tbl_OMS_ProductHierarchy.CategoryId
		WHERE tbl_OMS_ProductHierarchy.VendorId=@VendorId ORDER BY CatDesc
END
