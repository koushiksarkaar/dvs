USE [SB3OMS2017]
GO
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetWarehouseList]    Script Date: 4/4/2017 11:20:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author Aparesh >
-- Create date: <Create Date 2017-04-04>
-- Description:	<Description Procedure for generate warehouse list>
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetWarehouseList]

AS
BEGIN
	
	SET NOCOUNT ON;

     SELECT [WarehouseId] AS [ValueField] , [WHName] AS [TextField]
	 FROM [tbl_OMS_MWareHouse] WITH(NOLOCK) 
END
