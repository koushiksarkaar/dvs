USE [SB3OMS2017]
GO
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetVendorByCategoryChanged]    Script Date: 4/4/2017 11:19:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author Aparesh >
-- Create date: <Create Date 2017-04-04>
-- Description:	<Description Procedure for generate Category By Vendor Changed>
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetVendorByCategoryChanged]
(
	@CategoryId nvarchar(30)
)
AS
BEGIN
	
	SET NOCOUNT ON;
        SELECT DISTINCT  tbl_OMS_ProductHierarchy.VendorId AS [ValueField],tbl_OMS_MVendor.VNDName AS [TextField]
		FROM   tbl_OMS_MVendor WITH(NOLOCK) INNER JOIN tbl_OMS_ProductHierarchy WITH(NOLOCK)
        ON     tbl_OMS_MVendor.VendorId =tbl_OMS_ProductHierarchy.VendorId
		WHERE tbl_OMS_ProductHierarchy.CategoryId=@CategoryId ORDER BY VNDName
END
