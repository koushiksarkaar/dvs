USE [SB3OMS2017]
GO
/****** Object:  StoredProcedure [dbo].[SProc_OMS_GetVendorGroup]    Script Date: 4/4/2017 11:19:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author Aparesh >
-- Create date: <Create Date 2017-04-04>
-- Description:	<Description Procedure for generate vendor group on vendor selection>
-- =============================================
ALTER PROCEDURE [dbo].[SProc_OMS_GetVendorGroup]
(
	@VendorId nvarchar(30)
)
AS
BEGIN
	
	SET NOCOUNT ON;

        SELECT [VendorGroupId] AS [ValueField] , [GroupDescription] AS [TextField]
	    FROM  [dbo].[tbl_OMS_MVendorGroup] WITH(NOLOCK) 
	    WHERE VendorId=@VendorId ORDER BY [GroupDescription]
END
