﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DVSAdminPortal.Commons
{
    public static class AppConstants
    {
        public static string baseApiUri = WebConfigurationManager.AppSettings["baseApiUri"];
        public static string apiUriUserLogin = baseApiUri + WebConfigurationManager.AppSettings["apiUserLogin"];
        public static string apiUriVerifyAdminToken = baseApiUri + WebConfigurationManager.AppSettings["apiVerifyAdminToken"];
        public static string apiUriGetAllApplicationList = baseApiUri + WebConfigurationManager.AppSettings["apiGetAllApplicationList"];
        public static string apiUriUpdateApplicationById = baseApiUri + WebConfigurationManager.AppSettings["apiUpdateApplicationById"];
        public static string apiUriAddNewApplication = baseApiUri + WebConfigurationManager.AppSettings["apiAddNewApplication"];
        public static string apiUriDeleteApplication = baseApiUri + WebConfigurationManager.AppSettings["apiDeleteApplication"];

        public static string apiUriGetAllRoleList = baseApiUri + WebConfigurationManager.AppSettings["apiGetAllRoleList"];
        public static string apiUriUpdateRoleById = baseApiUri + WebConfigurationManager.AppSettings["apiUpdateRoleById"];
        public static string apiUriAddNewRole = baseApiUri + WebConfigurationManager.AppSettings["apiAddNewRole"];
        public static string apiUriDeleteRole = baseApiUri + WebConfigurationManager.AppSettings["apiDeleteRole"];

       // public static string apiUriGetAllApplicationAccessMappingList = baseApiUri + WebConfigurationManager.AppSettings["apiGetAllApplicationAccessMappingList"];
        public static string apiUriGetAppAccessMapViewList = baseApiUri + WebConfigurationManager.AppSettings["apiGetAppAccessMapViewList"];
        public static string apiUriUpdateMenuById = baseApiUri + WebConfigurationManager.AppSettings["apiUpdateMenuById"];
        public static string apiUriAddNewMenu = baseApiUri + WebConfigurationManager.AppSettings["apiAddNewMenu"];
        public static string apiUriDeleteMenu = baseApiUri + WebConfigurationManager.AppSettings["apiDeleteMenu"];

        public static string apiUriGetRoleAppAccessMapViewList = baseApiUri + WebConfigurationManager.AppSettings["apiGetRoleAppAccessMapViewList"];
        public static string apiUriGetRoleDropDownListForAppId = baseApiUri + WebConfigurationManager.AppSettings["apiGetRoleDropDownListForAppId"];
        public static string apiUriUpdateRoleAppAccessList = baseApiUri + WebConfigurationManager.AppSettings["apiUpdateRoleAppAccessList"];
        public static string apiUriUpdateUserAppRoleList = baseApiUri + WebConfigurationManager.AppSettings["apiUpdateUserAppRoleList"];
        public static string GetDefaultScreen = baseApiUri + WebConfigurationManager.AppSettings["apiGetDefaultScreen"];
        public static string apiInsertRollBaseScreen = baseApiUri + WebConfigurationManager.AppSettings["apiInsertRollBaseScreen"]; 

         public static string apiInsertDefaultScreen = baseApiUri + WebConfigurationManager.AppSettings["InsertDefaultScreen"];

        public static string GetScreenIDByAppId = baseApiUri + WebConfigurationManager.AppSettings["GetScreenIDByAppId"];

        public static string GetRoldetailsById = baseApiUri + WebConfigurationManager.AppSettings["apiGetRoldetailsById"];


        public static string apiUriGetAppRoleMapViewList = baseApiUri + WebConfigurationManager.AppSettings["apiGetAppRoleMapViewList"];
        public static string apiUriUpdateAppRoleList = baseApiUri + WebConfigurationManager.AppSettings["apiUpdateAppRoleList"];

        public static string apiUriGetUserAuthorization = baseApiUri + WebConfigurationManager.AppSettings["apiGetUserAuthorization"];

        public static string apiUriapiCreateUser = baseApiUri + WebConfigurationManager.AppSettings["apiCreateUser"];
        public static string apiUriGetUserSearchList = baseApiUri + WebConfigurationManager.AppSettings["apiGetUserSearchList"];
        public static string apiUriGetUserProfileDetails = baseApiUri + WebConfigurationManager.AppSettings["apiGetUserProfileDetails"];
        public static string apiUriUpdateUserProfileDetails = baseApiUri + WebConfigurationManager.AppSettings["apiUpdateUserProfileDetails"];
        public static string apiUriUserResetPwdFromAdmin = baseApiUri + WebConfigurationManager.AppSettings["apiUserResetPwdFromAdmin"];

        public static string apiUriImprotFromOMSUserTbl = baseApiUri + WebConfigurationManager.AppSettings["apiImprotFromOMSUserTbl"];
    }
}