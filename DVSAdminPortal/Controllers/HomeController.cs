﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DVSAdminPortal.Commons;
using DVSAdminPortal.Models;
using DVSWebApi.Models;
using System.Web.Mvc.Html;

namespace DVSAdminPortal.Controllers
{
    public class HomeController : Controller
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ActionResult Index()
        {
            return View();
        }

        // GET: /Home/VerifyAdminRoleToken
        [HttpGet]
        public bool VerifyAdminRoleToken(UserProfile usrObj)
        {
            log.Debug("VerifyAdminRoleToken: Started");

            try
            {
                string strToken = Request.Headers.GetValues("Authorization")[0].ToString();

                log.Debug("VerifyAdminRoleToken: Token: " + strToken);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(AppConstants.baseApiUri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + strToken);
                    log.Debug("VerifyAdminRoleToken: APIUri: " + AppConstants.apiUriVerifyAdminToken);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriVerifyAdminToken).Result;
                    log.Debug("VerifyAdminRoleToken: response: " + response);
                    if (response.IsSuccessStatusCode)
                    {
                        log.Debug("VerifyAdminRoleToken: True ... ");

                        Session["OMSGoyaAdminToken"] = strToken;
                        Session["UserName"] = usrObj.UserName;
                        Session["UserFullName"] = usrObj.UserFullName;
                        Session["UserRole"] = usrObj.Role;

                        return true;
                    }
                    else
                    {
                        log.Debug("VerifyAdminRoleToken: False ... ");

                        return false;
                    }
                }
            }
            catch(Exception ex)
            {
                log.Error(ex.Message + "; " + ex.InnerException + "; " + ex.StackTrace);
            }

            return false;
        }


        #region Manage Application Master Entry

        /// <summary>
        /// This is called OnLoad from Home page when "Manage Application" menu is been clicked.
        /// It will return list of all application from database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Application()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty){ return RedirectToAction("Login", "Account");}
            
            Applications appObj = null;
            List<Applications> appObjLst = null;

            try
            {
                appObjLst = new List<Applications>();
                using (var client = new HttpClient()) 
                {
                    client.BaseAddress = new Uri(AppConstants.baseApiUri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetAllApplicationList).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;

                        var collection = new JavaScriptSerializer().Deserialize<IEnumerable<Applications>>(responseString);

                       foreach(var obj in collection)
                       {
                           appObj = new Applications() 
                           { 
                                ApplicationId = Convert.ToInt32(obj.ApplicationId),
                                ApplicationName = Convert.ToString(obj.ApplicationName),
                                Description = Convert.ToString(obj.Description),
                              //  CurrentVersion = Convert.ToString(obj.CurrentVersion),
                               // StartDate = obj.StartDate == null ? (DateTime?)null : (DateTime)obj.StartDate,
                               // EndDate = obj.EndDate == null ? (DateTime?)null : (DateTime)obj.EndDate,
                               // CreateDate = obj.CreateDate == null ? (DateTime?)null : (DateTime)obj.CreateDate,
                               // UpdateDate = obj.UpdateDate == null ? (DateTime?)null : (DateTime)obj.UpdateDate,
                              //  StartDate = Convert.ToString(obj.StartDate),
                              //  EndDate = Convert.ToString(obj.EndDate),
                                CreateDate = Convert.ToString(obj.CreateDate),
                                UpdateDate = Convert.ToString(obj.UpdateDate),
                                CreatedBy = Convert.ToString(obj.CreatedBy),
                                UpdatedBy = Convert.ToString(obj.UpdatedBy),
                                IsActive = Convert.ToBoolean(obj.IsActive)
                           };

                           appObjLst.Add(appObj);
                       }
                    }
                }

                log.Debug("Get all application list successfully  " + appObjLst.ToString());
                return View(appObjLst);
            }
            catch(Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                appObjLst = null;
                appObj = null;
            }

            return View(appObjLst);
        }

        /// <summary>
        /// Update Application
        /// </summary>
        /// <param name="applicationObj"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateApplication(Applications applicationObj) 
        {
            HttpClient client = new HttpClient();
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                client.BaseAddress = new Uri(AppConstants.baseApiUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                // Populate update date
                applicationObj.UpdateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesSuccessPost = new Dictionary<string, string>()
                {
                    {"ApplicationId", applicationObj.ApplicationId.ToString() },
                    {"ApplicationName", applicationObj.ApplicationName},
                    {"Description", applicationObj.Description},
                  //  {"CurrentVersion", applicationObj.CurrentVersion},
                 //   {"StartDate", applicationObj.StartDate.ToString()},
                  //  {"EndDate", applicationObj.EndDate.ToString()},
                  //  {"CreateDate", applicationObj.CreateDate.ToString()},
                    {"UpdateDate", applicationObj.UpdateDate.ToString()},
                  //  {"CreatedBy", applicationObj.CreatedBy},
                 //   {"UpdatedBy",applicationObj.UpdatedBy},
                    {"IsActive",applicationObj.IsActive.ToString()},
                    {"Message","success"}
                };

                content = new FormUrlEncodedContent(valuesSuccessPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriUpdateApplicationById, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    log.Debug("Method: HomeController/UpdateApplication: Application updated successfully.");

                    // This JSON object will be send to view if DB insert is successful.
                    jsonResponseObj = "[{"
                        + "\"ApplicationId\":\"" + applicationObj.ApplicationId.ToString()
                        + "\", \"ApplicationName\":\"" + applicationObj.ApplicationName
                        + "\", \"Description\":\"" + applicationObj.Description
                        + "\", \"UpdateDate\":\"" + applicationObj.UpdateDate
                        + "\", \"IsActive\":\"" + applicationObj.IsActive
                        + "\", \"Message\":\"success"
                        + "\"}]";
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";
                }

                // This JSON object will be send to view
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddNewApplication(Applications applicationObj)
        {
            HttpClient client = new HttpClient();
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                client.BaseAddress = new Uri(AppConstants.baseApiUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);

                // Populate create date
                applicationObj.CreateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesForPost = new Dictionary<string, string>()
                {
                    {"ApplicationId", applicationObj.ApplicationId.ToString() },
                    {"ApplicationName", applicationObj.ApplicationName},
                    {"Description", applicationObj.Description},
                    {"CreateDate", applicationObj.CreateDate.ToString()},
                    //{"UpdateDate", applicationObj.UpdateDate.ToString()},
                    {"IsActive",applicationObj.IsActive.ToString()},
                    {"Message","success"}
                };

                 
                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriAddNewApplication, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    jsonResponseObj = "[{"
                    + "\", \"Message\":\"success"
                    + "\"}]";

                    log.Debug("Method: HomeController/AddNewApplication: Application created successfully.");
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    // Return json objet to view for fail/error
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";

                    log.Debug("Method: HomeController/AddNewApplication: Application creation fails.");
                }

                // Return json objet to view for successful DB insert
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult DeleteApplication(Applications applicationObj)
        {
            HttpClient client = new HttpClient();
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                client.BaseAddress = new Uri(AppConstants.baseApiUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);

                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesForPost = new Dictionary<string, string>()
            {
                {"ApplicationId", applicationObj.ApplicationId.ToString() },
                {"Message","success"}
            };

                
                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriDeleteApplication, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    jsonResponseObj = "[{"
                    + "\", \"Message\":\"success"
                    + "\"}]";

                    log.Debug("Method: HomeController/DeleteApplication: Application deleted successfully.");
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";

                    log.Debug("Method: HomeController/DeleteApplication: Application deletion fails.");
                }

                // Return json objet to view for successful DB insert
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);

        }

        #endregion Manage Application Master Entry

        #region Manage Role Master Entry

        public ActionResult Role()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            Roles appObj = null;
            List<Roles> appObjLst = null;

            try
            {
                appObjLst = new List<Roles>();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(AppConstants.baseApiUri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetAllRoleList).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;

                        var collection = new JavaScriptSerializer().Deserialize<IEnumerable<Roles>>(responseString);

                        foreach (var obj in collection)
                        {
                            appObj = new Roles()
                            {
                                Id = Convert.ToString(obj.Id),
                                Name = Convert.ToString(obj.Name),
                                IsActive = Convert.ToBoolean(obj.IsActive)
                            };

                            appObjLst.Add(appObj);
                        }
                    }
                }
                return View(appObjLst);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                appObjLst = null;
                appObj = null;
            }

            return View(appObjLst);
        }


        [HttpPost]
        public JsonResult UpdateRole(Roles pObj)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesForPost = new Dictionary<string, string>()
            {
                {"Id", pObj.Id.ToString() },
                {"Name", pObj.Name},
                {"IsActive",pObj.IsActive.ToString()}
            };
 
                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriUpdateRoleById, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    // This JSON object will be send to view if DB insert is successful.
                    jsonResponseObj = "[{"
                       + "\"Id\":\"" + pObj.Id.ToString()
                       + "\", \"Name\":\"" + pObj.Name
                       + "\", \"IsActive\":\"" + pObj.IsActive
                       + "\", \"Message\":\"success"
                       + "\"}]";

                    log.Debug("Method: HomeController/UpdateRole: successfully.");
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";

                    log.Debug("Method: HomeController/UpdateRole: fails.");
                }

                // Return json objet to view for successful/fail/error 
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult AddNewRole(Roles pObj)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesForPost = new Dictionary<string, string>()
                {
                    {"Id", pObj.Id.ToString() },
                    {"Name", pObj.Name},
                    {"IsActive",pObj.IsActive.ToString()}
                };

                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriAddNewRole, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    jsonResponseObj = "[{"
                    + "\", \"Message\":\"success"
                    + "\"}]";
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";
                }

                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult DeleteRole(Roles pObj)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesForPost = new Dictionary<string, string>()
                {
                    {"Id", pObj.Id.ToString() }
                };

                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriDeleteRole, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    jsonResponseObj = "[{"
                    + "\", \"Message\":\"success"
                    + "\"}]";
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";
                }

                // Return json objet to view for successful DB insert
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            // Return json objet to view for successful DB insert
            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
        }

        #endregion Manage Role Master Entry

        #region Manage App Role Mapping

        public ActionResult AppRoleMappings()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

            return View("AppRoleMappings", AppRoleModel(0));
        }

        [HttpPost]
        public ActionResult AppRoleMapping(int AppId)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            return PartialView("_AppRoleMappings", AppRoleModel(AppId));
        }

        public RoleApplicationAccessMapping AppRoleModel(int AppId)
        {
            RoleApplicationAccessMapping applicationAccessMapping = null;
            List<RoleApplicationAccessMapping> appObjLst = null;

            try
            {
                appObjLst = new List<RoleApplicationAccessMapping>();

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetAppRoleMapViewList + "/" + AppId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        applicationAccessMapping = new JavaScriptSerializer().Deserialize<RoleApplicationAccessMapping>(responseString);
                    }
                }

                //   return applicationAccessMapping;
            }
            catch (Exception ex)
            { 
                log.Error("Error: " + ex); 
            }
            return applicationAccessMapping;
        }

        [HttpPost]
        public JsonResult UpdateAppRoleMap(RoleApplicationAccessMapping SelectedRoleIdsObj)
        {
            RoleApplicationAccessMapping RoleAppAccessMapObj = null;
            //  RoleApplicationAccessMapping RoleAppAccessMapListObj = new RoleApplicationAccessMapping();
            List<RoleApplicationAccessMapping> RoleAppAccessMapListObj = new List<RoleApplicationAccessMapping>();

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
            var jsonResponseObj = "[{}]";

            try
            {
                string[] selectedAccessId = SelectedRoleIdsObj.SelectedRoleIds.Split(',');

                for (int i = 0; i < selectedAccessId.Count(); i++)
                {
                    RoleAppAccessMapObj = new RoleApplicationAccessMapping();
                    //RoleAppAccessMapObj.RoleId = SelectedRoleIdsObj.RoleId;
                    RoleAppAccessMapObj.ApplicationId = SelectedRoleIdsObj.ApplicationId;
                    RoleAppAccessMapObj.RoleId = Convert.ToInt32(selectedAccessId[i]);
                    RoleAppAccessMapObj.IsAccess = true;

                    RoleAppAccessMapListObj.Add(RoleAppAccessMapObj);
                }

                if (SelectedRoleIdsObj.NotSelectedRoleIds != null)
                {
                    string[] notSelectedAccessId = SelectedRoleIdsObj.NotSelectedRoleIds.Split(',');
                    for (int i = 0; i < notSelectedAccessId.Count(); i++)
                    {
                        RoleAppAccessMapObj = new RoleApplicationAccessMapping();
                        //RoleAppAccessMapObj.RoleId = SelectedRoleIdsObj.RoleId;
                        RoleAppAccessMapObj.ApplicationId = SelectedRoleIdsObj.ApplicationId;
                        RoleAppAccessMapObj.RoleId = Convert.ToInt32(notSelectedAccessId[i]);
                        RoleAppAccessMapObj.IsAccess = false;

                        RoleAppAccessMapListObj.Add(RoleAppAccessMapObj);
                    }
                }

                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                String contentStr = serializer.Serialize(RoleAppAccessMapListObj);
                StringContent content = new StringContent(contentStr, Encoding.UTF8, "application/json");

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriUpdateAppRoleList, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    // This JSON object will be send to view if DB insert is successful.
                    jsonResponseObj = "[{\"Message\":\"success\"}]";
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";
                }

                // Return json objet to view for successful/fail/error DB insert
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                client = null;
                jsonResponseObj = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            
        }

        #endregion Manage App Role Mapping

        #region Manage Navigation Master Entry

        public ActionResult Menu()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            return View("Menu", GetNavigationForApp(0));
        }

        [HttpPost]
        public ActionResult Menu(int AppId)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            return PartialView("_Menu", GetNavigationForApp(AppId));
        }

        public ApplicationAccessMapping GetNavigationForApp(int appId)
        {
            ApplicationAccessMapping applicationAccessMapping = null;
            // ApplicationAccessMapping appObj = null;
            List<ApplicationAccessMapping> appObjLst = null;

            try
            {
                appObjLst = new List<ApplicationAccessMapping>();

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetAppAccessMapViewList + "/" + appId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;

                        applicationAccessMapping = new JavaScriptSerializer().Deserialize<ApplicationAccessMapping>(responseString);

                        // appAccessMapViewList.ApplicationId = -1;

                    }
                }
                return applicationAccessMapping;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                appObjLst = null;
                // appObj = null;
            }

            return applicationAccessMapping;
        }


        [HttpPost]
        public ActionResult UpdateMenu(ApplicationAccessMapping pObj)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
            // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
            var valuesForPost = new Dictionary<string, string>()
            {
                {"AccessId", pObj.AccessId.ToString() },
                {"ApplicationId", pObj.ApplicationId.ToString()},
                {"AccessName", pObj.AccessName.ToString()},
                {"Description", pObj.Description.ToString()},
                {"SortOrder", pObj.SortOrder.ToString()},
                {"AccessType", pObj.AccessType.ToString()},
                {"AccessControl", pObj.AccessControl.ToString()},
                {"IsActive",pObj.IsActive.ToString()}
            };

            // This JSON object will be send to view if DB insert is successful.
            var jsonSuccessObj = "[{\"Message\":\"success\"}]";

            // This JSON object will be send to view if DB insert fails.
            var jsonFailObj = "[{\"Message\":\"fail\"}]";
            HttpContent content = new FormUrlEncodedContent(valuesForPost);

            // This will map call to WebApi
            var response = client.PostAsync(AppConstants.apiUriUpdateMenuById, content);

            // This will actually call to WebApi
            if (response.Result.IsSuccessStatusCode)
            {
                // Return json objet to view for successful DB insert
               //// return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);

                return PartialView("_Menu", GetNavigationForApp(pObj.ApplicationId));
            }
            else
            {
                // Return json objet to view for fail/error
                return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult AddNewMenu(ApplicationAccessMapping pObj)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);

            if (pObj.Description == null)
                pObj.Description = string.Empty;
            //var pDescription = string.Empty;
            //if (pObj.Description.ToString() != null)
            //    pDescription = pObj.Description.ToString();

            // Populate create date
            pObj.CreateDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
            var valuesForPost = new Dictionary<string, string>()
            {
                {"ApplicationId", pObj.ApplicationId.ToString()},
                {"AccessName", pObj.AccessName.ToString()},
                {"Description", pObj.Description.ToString()},
                {"SortOrder", pObj.SortOrder.ToString()},
                {"AccessType", pObj.AccessType.ToString()},
                {"AccessControl", pObj.AccessControl.ToString()},
                {"CreateDate", pObj.CreateDate.ToString()},
                {"IsActive",pObj.IsActive.ToString()}
            };

            // This JSON object will be send to view if DB insert is successful.
            var jsonSuccessObj = "[{\"Message\":\"success\"}]";

            // This JSON object will be send to view if DB insert fails.
            var jsonFailObj = "[{\"Message\":\"fail\"}]";
            HttpContent content = new FormUrlEncodedContent(valuesForPost);

            // This will map call to WebApi
            var response = client.PostAsync(AppConstants.apiUriAddNewMenu, content);

            // This will actually call to WebApi
            if (response.Result.IsSuccessStatusCode)
            {
                // Return json objet to view for successful DB insert
              ////  return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);

                return PartialView("_Menu", GetNavigationForApp(pObj.ApplicationId));
            }
            else
            {
                // Return json objet to view for fail/error
                return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteMenu(ApplicationAccessMapping pObj)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);

            // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
            var valuesForPost = new Dictionary<string, string>()
            {
                {"AccessId", pObj.AccessId.ToString() }
            };

            var jsonSuccessObj = "[{"
                + "\", \"Message\":\"success"
                + "\"}]";

            // This JSON object will be send to view if DB insert fails.
            var jsonFailObj = "[{\"Message\":\"fail\"}]";
            HttpContent content = new FormUrlEncodedContent(valuesForPost);

            // This will map call to WebApi
            var response = client.PostAsync(AppConstants.apiUriDeleteMenu, content);

            // This will actually call to WebApi
            if (response.Result.IsSuccessStatusCode)
            {
                // Return json objet to view for successful DB insert
                return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Return json objet to view for fail/error
                return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Manage Navigation Master Entry

        #region Manage Role Application Navigation Mapping

        /// <summary>
        /// This Action is called from menu link of View
        /// </summary>
        /// <returns></returns>
        public ActionResult RoleAppAccessMappings()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            return View("RoleAppAccessMappings", RoleAppAccessModel(0,0,0));
        }




        /// <summary>
        /// This Action is called when both dropdownList is selected, ApplicationId and RoleId from View.
        /// </summary>
        /// <param name="AppId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RoleAppAccessMapping(int AppId, int RoleId, int PageFlag)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            //int PageFlag = 0;
            //if (strPageFlag == "RoleAppAccess")
            //    PageFlag = 1;
            //else if (strPageFlag == "UserAppRole")
            //    PageFlag = 2;

            if (PageFlag == 1)
                return PartialView("_RoleAppAccessMapping", RoleAppAccessModel(AppId, RoleId, PageFlag));
            else if (PageFlag == 2)
                return PartialView("_UserAppRoleMapping", RoleAppAccessModel(AppId, RoleId, PageFlag));
            else
                return null;
        }

        [HttpPost]
        public ActionResult FillRoleDDL(int AppId)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            return PartialView("_RoleDropDownList", GetFillRoleDDL(AppId));
        }

        /// <summary>
        /// Return model for RoleAppAccess...
        /// </summary>
        /// <param name="AppId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public RoleApplicationAccessMapping RoleAppAccessModel(int AppId, int RoleId, int PageFlag)
        {
            RoleApplicationAccessMapping applicationAccessMapping = null;
            //// List<RoleApplicationAccessMapping> appObjLst = null;
            Session["RoleId"] = RoleId;
            try
            {
             ////   appObjLst = new List<RoleApplicationAccessMapping>();

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetRoleAppAccessMapViewList + "/" + AppId + "/" + RoleId + "/" + PageFlag).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        applicationAccessMapping = new JavaScriptSerializer().Deserialize<RoleApplicationAccessMapping>(responseString);
                    }
                }

             //   return applicationAccessMapping;
            }
            catch (Exception ex)
            {
            }
            return applicationAccessMapping;
        }

        public RoleApplicationAccessMapping GetFillRoleDDL(int AppId)
        {
            RoleApplicationAccessMapping applicationAccessMapping = null;
          ////  List<RoleApplicationAccessMapping> appObjLst = null;

            try
            {
            ////    appObjLst = new List<RoleApplicationAccessMapping>();

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetRoleDropDownListForAppId + "/" + AppId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        applicationAccessMapping = new JavaScriptSerializer().Deserialize<RoleApplicationAccessMapping>(responseString);
                    }
                }

                //   return applicationAccessMapping;
            }
            catch (Exception ex)
            { }
            return applicationAccessMapping;
        }

        [HttpPost]
        public JsonResult UpdateRoleAppAccess(RoleApplicationAccessMapping SelectedRoleIdsObj)
        {

            RoleApplicationAccessMapping RoleAppAccessMapObj = null;
          //  RoleApplicationAccessMapping RoleAppAccessMapListObj = new RoleApplicationAccessMapping();
            List<RoleApplicationAccessMapping> RoleAppAccessMapListObj = new List<RoleApplicationAccessMapping>();

            
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);

            string[] selectedAccessId = SelectedRoleIdsObj.SelectedRoleIds.Split(',');

            for(int i=0; i < selectedAccessId.Count(); i++)
            {
                RoleAppAccessMapObj = new RoleApplicationAccessMapping();
                RoleAppAccessMapObj.RoleId = SelectedRoleIdsObj.RoleId;
                RoleAppAccessMapObj.ApplicationId = SelectedRoleIdsObj.ApplicationId;
                RoleAppAccessMapObj.AccessId = Convert.ToInt32(selectedAccessId[i]);
                RoleAppAccessMapObj.IsAccess = true;

                RoleAppAccessMapListObj.Add(RoleAppAccessMapObj);
            }

            if (SelectedRoleIdsObj.NotSelectedRoleIds != null)
            {
                string[] notSelectedAccessId = SelectedRoleIdsObj.NotSelectedRoleIds.Split(',');
                for (int i = 0; i < notSelectedAccessId.Count(); i++)
                {
                    RoleAppAccessMapObj = new RoleApplicationAccessMapping();
                    RoleAppAccessMapObj.RoleId = SelectedRoleIdsObj.RoleId;
                    RoleAppAccessMapObj.ApplicationId = SelectedRoleIdsObj.ApplicationId;
                    RoleAppAccessMapObj.AccessId = Convert.ToInt32(notSelectedAccessId[i]);
                    RoleAppAccessMapObj.IsAccess = false;

                    RoleAppAccessMapListObj.Add(RoleAppAccessMapObj);
                }
            }

            // This JSON object will be send to view if DB insert is successful.
            var jsonSuccessObj = "[{\"Message\":\"success\"}]";

            // This JSON object will be send to view if DB insert fails.
            var jsonFailObj = "[{\"Message\":\"fail\"}]";

            // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            String contentStr = serializer.Serialize(RoleAppAccessMapListObj);
            StringContent content = new StringContent(contentStr, Encoding.UTF8, "application/json");

            // This will map call to WebApi
            var response = client.PostAsync(AppConstants.apiUriUpdateRoleAppAccessList, content);

            // This will actually call to WebApi
            if (response.Result.IsSuccessStatusCode)
            {
                // Return json objet to view for successful DB insert
                return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Return json objet to view for fail/error
                return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion Manage Role Application Navigation Mapping

        #region Manage Screen Control

        [HttpPost]
        public ActionResult InsertVisibility(FormCollection Fc)
        {

            
            int count = 1;
            PageDetailList page =(PageDetailList)Session["Pagelist"];
            foreach(var v in page.pagecontent)
            {
                string stcount = count.ToString();
                v.Visibility= Fc[stcount];
                v.RollID = page.RoleId.ToString();
                count++;
            }

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);


            List<PageDetails> pObj = new List<PageDetails>();
            pObj = page.pagecontent;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            String contentStr = serializer.Serialize(pObj);
            StringContent content = new StringContent(contentStr, Encoding.UTF8, "application/json");

            // This will map call to WebApi

            
            var response = client.PostAsync(AppConstants.apiInsertRollBaseScreen, content);

            // This will actually call to WebApi
            if (response.Result.IsSuccessStatusCode)
            {
                TempData["var"] = "Data Inserted Successfully";

                string pageId = Session["pageid"].ToString();
                if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

                return View("Defaultscreenpage", GetAll(pageId));
                // Return json objet to view for successful DB insert
                // return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["var"] = "Server Busy Please Try Again Later";
                string pageId = Session["pageid"].ToString();
                if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

                return View("Defaultscreenpage", GetAll(pageId));

                // Return json objet to view for fail/error
                // return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
            }
            return null;

        }



        [HttpPost]
        public ActionResult InsertDefaultScreen(PageDetailList pd)
        {
            TempData["var"] = "Click save to continue data insertion";
            TempData["newvar"] = "Click To Save Data";

            //PageDetailList page = (PageDetailList)Session["Pagelist"];

            //page.ColumnID = pd.ColumnID;
            //page.ControlID = pd.ControlID;
            //page.ControlType = pd.ControlType;
            //page.Description = pd.Description;


            PageDetails _pageDetails = new PageDetails();
            
            _pageDetails.AccessID = pd.AccessID;
           
            _pageDetails.ApplicationID = pd.ApplicationID;
           // _pageDetails.ApplicationName = pd.;
            _pageDetails.ScreenID = pd.ScreenID;
            _pageDetails.ColumnID = pd.ColumnID;
            _pageDetails.ControlID = pd.ControlID;
            _pageDetails.ControlType = pd.ControlType;
            _pageDetails.Description =pd.Description;

            //apiInsertDefaultScreen
            //int count = 1;
            //PageDetailList page = (PageDetailList)Session["Pagelist"];
            //foreach (var v in page.pagecontent)
            //{
            //    string stcount = count.ToString();
            //    v.Visibility = Fc[stcount];
            //    v.RollID = page.RoleId.ToString();
            //    count++;
            //}

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);


            //List<PageDetails> pObj = new List<PageDetails>();
            //pObj = page.pagecontent;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            String contentStr = serializer.Serialize(_pageDetails);
            StringContent content = new StringContent(contentStr, Encoding.UTF8, "application/json");

            // This will map call to WebApi


            var response = client.PostAsync(AppConstants.apiInsertDefaultScreen, content);

            // This will actually call to WebApi
            if (response.Result.IsSuccessStatusCode)
            {
                TempData["newvar"] = "Data Inserted Successfully";

                string pageId = Session["pageid"].ToString();
                if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

                return View("Defaultscreenpage", GetAll(pageId));
                // Return json objet to view for successful DB insert
                // return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["newvar"] = "Server Busy Please Try Again Later";
                string pageId = Session["pageid"].ToString();
                if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

                return View("Defaultscreenpage", GetAll(pageId));

                // Return json objet to view for fail/error
                // return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
            }
            return null;

        }



        public ActionResult ManageScreenSettings()
        {
            TempData["var"] = "Click save to continue data insertion";
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

            return View("ManageScreenControl", RoleAppAccessModel(0, 0, 0));
        }
        public ActionResult Rollbaseddl(int AppId)
        {
            Session["AppId"] = AppId;
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

            return PartialView("_ManageScreenControl", GetFillRoleDDL(AppId));
        }


        public ActionResult ManageAppAccessMapping(int AppId, int RoleId, int PageFlag)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

            //int PageFlag = 0;
            //if (strPageFlag == "RoleAppAccess")
            //    PageFlag = 1;
            //else if (strPageFlag == "UserAppRole")
            //    PageFlag = 2;

            if (PageFlag == 1)
                return PartialView("_ManageAppAccessMapping", RoleAppAccessModel(AppId, RoleId, PageFlag));
            else if (PageFlag == 2)
                return PartialView("_UserAppRoleMapping", RoleAppAccessModel(AppId, RoleId, PageFlag));
            else
                return null;
        }


        public ActionResult RedirectPageDetails(string pageId)
        {
            //GetDefaultScreen//Defaultscreenpage
            Session["pageid"] = pageId;
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

            return View("Defaultscreenpage", GetAll(pageId));

            return null;
        }
       
        public PageDetailList GetAll(string pageID)
        {
            List<PageDetails> page = new List<PageDetails>();
            RoleDetails role = new RoleDetails();
            PageDetailList pagelist = new PageDetailList();
            string RoleId = Convert.ToString(Session["RoleId"]);

            string accessid = pageID;
            string appid = Session["AppId"].ToString();


            try
            {
                ////    appObjLst = new List<RoleApplicationAccessMapping>();

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.GetDefaultScreen + "/" + pageID + "/" + RoleId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        page= new JavaScriptSerializer().Deserialize<List<PageDetails>>(responseString);
                        pagelist.pagecontent = page;
                    }

                    


                }

                using (var Rolclient = new HttpClient())
                {
                    Rolclient.DefaultRequestHeaders.Accept.Clear();
                    Rolclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    Rolclient.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = Rolclient.GetAsync(AppConstants.GetRoldetailsById + "/" + RoleId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        role = new JavaScriptSerializer().Deserialize<RoleDetails>(responseString);
                        pagelist.pagecontent = page;
                        pagelist.RoleId = role.RoleId;
                        pagelist.Rolename = role.RoleName;
                    }
                }

                PageDetails pagedet = new PageDetails();
                using (var Appclient = new HttpClient())
                {
                    Appclient.DefaultRequestHeaders.Accept.Clear();
                    Appclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    Appclient.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = Appclient.GetAsync(AppConstants.GetScreenIDByAppId + "/" + accessid + "/" + appid).Result; 
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        pagedet = new JavaScriptSerializer().Deserialize<PageDetails>(responseString);
                       
                        pagelist.AccessID = pagedet.AccessID;
                        pagelist.ApplicationID = pagedet.ApplicationID;
                        pagelist.ScreenID = pagedet.ScreenID;
                    }
                }

                //HttpResponseMessage response = client.GetAsync(AppConstants.GetScreenIDByAppId + "/" + accessid + "/" + appid).Result;
                //if (response.IsSuccessStatusCode)
                //{
                //    string responseString = response.Content.ReadAsStringAsync().Result;
                //    page = new JavaScriptSerializer().Deserialize<List<PageDetails>>(responseString);
                //    pagelist.pagecontent = page;
                //}
                Session["Pagelist"] = pagelist;

                return pagelist;
            }
            catch (Exception ex)
            { }
            return pagelist;
        }

        #endregion

        #region Manage User Application Role Mapping

        public ActionResult UserAppRoleMaping()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            return View("UserAppRoleMapping", RoleAppAccessModel(0, 0, 0));
        }


        [HttpPost]
        public JsonResult UpdateUserAppRole(RoleApplicationAccessMapping SelectedIdsObj)
        {

            RoleApplicationAccessMapping UserAppRoleMapObj = null;
            //  RoleApplicationAccessMapping RoleAppAccessMapListObj = new RoleApplicationAccessMapping();
            List<RoleApplicationAccessMapping> UserAppRoleMapListObj = new List<RoleApplicationAccessMapping>();


            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);

            try
            {
                string[] selectedUserName = SelectedIdsObj.UserListToAssigne.Split(',');

                for (int i = 0; i < selectedUserName.Count(); i++)
                {
                    UserAppRoleMapObj = new RoleApplicationAccessMapping();
                    UserAppRoleMapObj.RoleId = SelectedIdsObj.RoleId;
                    UserAppRoleMapObj.ApplicationId = SelectedIdsObj.ApplicationId;
                    UserAppRoleMapObj.UserName = Convert.ToString(selectedUserName[i]);
                    UserAppRoleMapListObj.Add(UserAppRoleMapObj);
                }

                // This JSON object will be send to view if DB insert is successful.
                var jsonSuccessObj = "[{\"Message\":\"success\"}]";

                // This JSON object will be send to view if DB insert fails.
                var jsonFailObj = "[{\"Message\":\"fail\"}]";

                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                String contentStr = serializer.Serialize(UserAppRoleMapListObj);
                StringContent content = new StringContent(contentStr, Encoding.UTF8, "application/json");

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriUpdateUserAppRoleList, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    // Return json objet to view for successful DB insert
                    return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    // Return json objet to view for fail/error
                    return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {

            }

            return null;
        }

        #endregion  Manage User Application Role Mapping

        #region Import OMS User to OAuth


        public ActionResult ImportOMSUserToOAuth()
        {
            return View();
        }


        [HttpPost]
        public JsonResult ImportOMSUserToOAuthDB()
        {

            HttpClient client = new HttpClient();

            // This JSON object will be send to view if DB insert is successful.
            var jsonSuccessObj = "[{\"Message\":\"success\"}]";

            // This JSON object will be send to view if DB insert fails.
            var jsonFailObj = "[{\"Message\":\"fail\"}]";

            try
            {

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                String contentStr = serializer.Serialize(null);
                StringContent content = new StringContent(contentStr, Encoding.UTF8, "application/json");

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriImprotFromOMSUserTbl, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    // Return json objet to view for successful DB insert
                    return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    // Return json objet to view for fail/error
                    return Json(jsonFailObj, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {

            }

            return Json(jsonSuccessObj, JsonRequestBehavior.AllowGet);
        }

        #endregion  Import OMS User to OAuth


        #region Manage User Authentication

        [HttpGet]
        [Route("api/UserAuthorization")]
        public ActionResult UserAuthorization(RoleApplicationAccessMapping obj)
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            RoleApplicationAccessMapping applicationAccessMapping = null;
            List<RoleApplicationAccessMapping> appObjLst = null;

            try
            {
                appObjLst = new List<RoleApplicationAccessMapping>();

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetUserAuthorization).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;

                        applicationAccessMapping = new JavaScriptSerializer().Deserialize<RoleApplicationAccessMapping>(responseString);
                    }
                }
                return View(applicationAccessMapping);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                appObjLst = null;
            }

            return View(appObjLst);
        }

        #endregion Manage User Authentication

        #region Manage User Creation

        public ActionResult UserCreation()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }
            
            return View();
        }

        [HttpPost]
        public JsonResult CreateNewUser(UserLoginDetails pObj)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);

            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
            var valuesForPost = new Dictionary<string, string>()
            {
                {"Domain", pObj.Domain },
                {"SelectedUserInputType", pObj.SelectedUserInputType},
                {"SelectedUserInputValue", pObj.SelectedUserInputValue},
                {"UserType", pObj.UserType},
                {"UserFullName", pObj.UserFullName},
                {"EmailId", pObj.EmailId}
            };

                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriapiCreateUser, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    // This JSON object will be send to view if DB insert is successful.
                    //jsonResponseObj = "[{"
                    //   + "\", \"Message\":\"success"
                    //   + "\"}]";
                    jsonResponseObj = "[{\"Message\":\"success\"}]";

                    log.Debug("Method: HomeController/UpdateRole: successfully.");
                }
                else
                {

                   // string strMsg = pObj.ResponseMessage.ToString();
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";

                    log.Debug("Method: HomeController/UpdateRole: fails.");
                }

                // Return json objet to view for successful/fail/error 
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Manage User Profile

        public ActionResult UserProfile()
        {
            /// If session expired OR logout, redirect to login page
            if (Session["OMSGoyaAdminToken"] == null || Session["OMSGoyaAdminToken"].ToString() == string.Empty) { return RedirectToAction("Login", "Account"); }

            return View();
        }

        [HttpPost]
        public ActionResult GetUserSearch(UserProfile pObj)
        {
            return PartialView("_UserProfile", GetUserSearchList(pObj));
        }

        [HttpPost]
        public ActionResult GetUserProfile(UserProfile pObj)
        {
            return PartialView("_UserProfileDetails", GetUserProfileDetails(pObj));
        }

        public UserProfile GetUserSearchList(UserProfile pObj)
        {
            UserProfile userProfileObj = null;

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetUserSearchList + "/" + pObj.UserName).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        userProfileObj = new JavaScriptSerializer().Deserialize<UserProfile>(responseString);
                    }
                }

                //   return applicationAccessMapping;
            }
            catch (Exception ex)
            { }
            return userProfileObj;
        }

        public UserProfile GetUserProfileDetails(UserProfile pObj)
        {
            UserProfile userProfileObj = null;

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
                    HttpResponseMessage response = client.GetAsync(AppConstants.apiUriGetUserProfileDetails + "/" + pObj.UserName).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        userProfileObj = new JavaScriptSerializer().Deserialize<UserProfile>(responseString);
                    }
                }

                //   return applicationAccessMapping;
            }
            catch (Exception ex)
            { }
            return userProfileObj;
        }

        [HttpPost]
        public JsonResult UpdateUserProfileDetails(UserProfile pObj)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            if (pObj.Email == null)
                pObj.Email = "";
            if (pObj.Phone == null)
                pObj.Phone = "";

            try
            {
                pObj.UpdateBy = Session["UserName"].ToString();
                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesForPost = new Dictionary<string, string>()
            {
                {"UserName", pObj.UserName.ToString() },
                {"UserFullName", pObj.UserFullName},
                {"Email",pObj.Email.ToString()},
                {"Phone",pObj.Phone.ToString()},
                {"UpdateBy",pObj.UpdateBy.ToString()}
            };

                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
                var response = client.PostAsync(AppConstants.apiUriUpdateUserProfileDetails, content);

                // This will actually call to WebApi
                if (response.Result.IsSuccessStatusCode)
                {
                    // This JSON object will be send to view if DB insert is successful.
                    jsonResponseObj = "[{\"Message\":\"success\"}]";

                    log.Debug("Method: HomeController/UpdateRole: successfully.");
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";

                    log.Debug("Method: HomeController/UpdateRole: fails.");
                }

                // Return json objet to view for successful/fail/error 
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UserResetPwd(UserProfile pObj)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["OMSGoyaAdminToken"]);
            var jsonResponseObj = "[{}]";
            HttpContent content = null;

            try
            {
                pObj.UpdateBy = Session["UserName"].ToString();
                // Create Dictionary key value objet for the HttpContent object creation which will be send to WebApi for DB insert.
                var valuesForPost = new Dictionary<string, string>()
            {
                {"UserName", pObj.UserName.ToString() },
                {"Role", pObj.Role},
                {"UpdateBy", pObj.UpdateBy}
            };

                content = new FormUrlEncodedContent(valuesForPost);

                // This will map call to WebApi
              //  var response = client.PostAsync(AppConstants.apiUriUserResetPwdFromAdmin, content);

                HttpResponseMessage response = client.PostAsync(AppConstants.apiUriUserResetPwdFromAdmin, content).Result;

                // This will actually call to WebApi
                //if (response.Result.IsSuccessStatusCode)
                //{
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    pObj = new JavaScriptSerializer().Deserialize<UserProfile>(responseString);
                    
                    if(pObj.SuccessStatus)
                    {
                        jsonResponseObj = "[{"
                        + "\"ResetPwd\":\"" + pObj.Password
                        + "\", \"Message\":\"success"
                        + "\"}]";
                    }

                   // log.Debug("Method: HomeController/UpdateRole: successfully.");
                }
                else
                {
                    // This JSON object will be send to view if DB insert fails.
                    jsonResponseObj = "[{\"Message\":\"fail\"}]";

                 //   log.Debug("Method: HomeController/UpdateRole: fails.");
                }

                // Return json objet to view for successful/fail/error 
                return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                client = null;
                jsonResponseObj = null;
                content = null;
            }

            return Json(jsonResponseObj, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}