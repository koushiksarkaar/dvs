﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DVSWebApi.Models;
using System.Web.Mvc.Html;

namespace DVSAdminPortal.Models
{
    public class PageDetailList
    {
        public List<PageDetails> pagecontent { get; set; }
        public int RoleId { get; set; }
        public string Rolename { get; set; }

        public string ControlID { get; set; }
        public string ControlType { get; set; }
        public string Description { get; set; }
        public string ColumnID { get; set; }

        public int ApplicationID { get; set; }
        public int AccessID { get; set; }

        public string ScreenID { get; set; }







    }
}