﻿using Microsoft.Owin;
using Owin;

//[assembly: OwinStartupAttribute(typeof(OMSAdminPortal.Startup))]
namespace DVSAdminPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
