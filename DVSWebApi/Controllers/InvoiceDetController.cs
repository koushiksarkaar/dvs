﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Repository;
using System.Web.Http.Results;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class InvoiceDetController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // To save invoice data
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/SaveInvoice")]
        public HttpResponseMessage SaveInvoice(InvoiceModels objInv)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invoiceRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceRepository.SaveInvoiceData(objInv));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:SaveInvoice " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GenerateInvoice

        // To generate invoice data
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/InvoiceGeneration")]
        public HttpResponseMessage InvoiceGeneration(GenerateInvoice objInv)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invoiceRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceRepository.InvoiceGenerationData(objInv.OrderNo, objInv.InvoiceNo, objInv.CustomerID, objInv.TripID, objInv.DriverID, objInv.CompanyID, objInv.StopLocation, objInv.Latitude, objInv.Longitude));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:InvoiceGeneration " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//InvoiceGeneration

        // To get delivery records
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/ViewDelivery")]
        public HttpResponseMessage ViewDelivery(DeliveryView objDeliveryView)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invoiceRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceRepository.GetDeliveryViewByOrderNo(objDeliveryView.OrderNumber, objDeliveryView.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:ViewDelivery " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//ViewDelivery

        // To view the invoice list which are already saved
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/ViewSavedInvoice")]
        public HttpResponseMessage ViewSavedInvoice(InvoiceView objInvView)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invRepository.ViewSavedInvoiceData(objInvView.TripID, objInvView.InvoiceNo, objInvView.CustomerID, objInvView.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:ViewSavedInvoice " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//ViewSavedInvoice

        // To view invocie details by particular invoice no
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/ViewInvoiceDetailsByNo")]
        public HttpResponseMessage ViewInvoiceDetailsByNo(InvoiceView objInvView)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invRepository.ViewInvoiceDetailsByNo(objInvView.TripID, objInvView.InvoiceNo, objInvView.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:ViewInvoiceDetailsByNo " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//ViewInvoiceDetailsByNo

        // To get invocie details by particular invoice no
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/GetInvoiceDetailByInvoiceNo")]
        public HttpResponseMessage GetInvoiceDetailByInvoiceNo(OrderView objOrdView)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invRepository.GetInvoiceDetailsByInvoiceNo(objOrdView.OrderNo, objOrdView.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetInvoiceDetailsByInvoiceNo " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;
        }//GetInvoiceDetailByInvoiceNo

        // To generate DSD for specific invoice
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/GenerateDSD")]
        public HttpResponseMessage GenerateDSD(GenerateDSD objGenerateDSD)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invRepository.InsertDSDGenerate(objGenerateDSD));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:InsertDSDGenerate " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GenerateDSD 

        // To add / insert signature
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/AddSignature")]
        public HttpResponseMessage AddSignature(AddSignature objAddSign)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invoiceRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceRepository.AddSignatureData(objAddSign));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:AddSignatureData " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GenerateDSD

        // To get invoice list
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/InvoiceList")]
        public HttpResponseMessage InvoiceList(InvoiceView objInvoiceVwe)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invoiceRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceRepository.GetInvoiceListByDriverCompany(objInvoiceVwe.CompanyID, objInvoiceVwe.DriverID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:AddSignatureData " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GenerateDSD

        // To get Payment Term
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/GetPaymentTerm")]
        public HttpResponseMessage GetPaymentTerm(InvoiceView objPayTerm)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceDetRepository _invoiceRepository = new InvoiceDetRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceRepository.GetPaymentTermListData(objPayTerm.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetPaymentTerm " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GetPaymentTerm
        /// <summary>
        /// UploadInvoiceDSD
        /// </summary>
        /// <param name="invModel"></param>
        /// <returns></returns>
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/InvoiceDet/UploadInvoiceDSD")]
        public HttpResponseMessage UploadInvoiceDSD(InvoiceUploadModel invModel)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceUploadRepository _invoiceupRepository = new InvoiceUploadRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceupRepository.UploadDVSInvoice(invModel));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:UploadInvoiceDSD " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/InvoiceDet/GetUplodedInvoiceList")]
        public HttpResponseMessage GetUplodedInvoiceList(SearchUplodedInvoice search)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            InvoiceUploadRepository _invoiceupRepository = new InvoiceUploadRepository();
            try
            {
                StringContent sc = new StringContent(_invoiceupRepository.GetUplodedInvoiceList(search));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetUplodedInvoiceList " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }
    }
}
