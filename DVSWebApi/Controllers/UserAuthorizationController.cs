﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.DirectoryServices;
using System.Web.Configuration;
using System.Text;
using System.IO;
//using System.Web.Script.Serialization;

namespace DVSWebApi.Controllers
{
    public class UserAuthorizationController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //RoleApplicationAccessMapRepository repositoryObj = null;
        UserAuthorizationRepository repositoryUserObj = null;
        string LdapURIPath = WebConfigurationManager.AppSettings["LdapURI"];

        //[HttpGet]
        //[Route("api/UserAuthorization/{UserId}/{AppId}")]
        //public HttpResponseMessage UserAuthorization(string UserId, string AppId)
        //{

        [HttpPost]
        [Route("api/GetUserFullName")]
        public HttpResponseMessage GetUserFullName(UserLoginDetails objUser)
        {
            // string userFullName = string.Empty;

            repositoryUserObj = new UserAuthorizationRepository();
            objUser.UserName = repositoryUserObj.GetUserFullNameFromUserId(objUser.UserId);

            HttpResponseMessage response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.OK, objUser);

            return response;
        }

        [HttpPost]
        [Route("api/UserAuthorization/CreateUser")]
        public HttpResponseMessage CreateUser(UserLoginDetails pObj)
        {
           // bool result = false;
            string strAccountName = string.Empty;
            string strFullName = string.Empty;
            string strEmail = string.Empty;            
            string strDomain = pObj.Domain;
            string strUserType = pObj.UserType;

            if (pObj.Domain != null)
                strDomain = pObj.Domain.Trim();

            if (pObj.UserType != null)
                strUserType = pObj.UserType.Trim();

            HttpResponseMessage response = null;
            repositoryUserObj = new UserAuthorizationRepository();

            try
            {
                if (pObj.SelectedUserInputType == "Email")
                {
                    //   Find Windows accountname from email address.
                    strEmail = pObj.SelectedUserInputValue;
                    strAccountName = GetSAMAccountNameByEmail(strEmail);
                    strFullName = GetFullName(strAccountName);
                }
                else if (pObj.SelectedUserInputType == "AccountName")
                {
                    strAccountName = pObj.SelectedUserInputValue;
                    strEmail = GetEmailBySAMAccountName(strAccountName);
                    strFullName = GetFullName(strAccountName);
                }
                else
                {
                    strAccountName = pObj.SelectedUserInputValue;
                    strEmail = pObj.EmailId;
                    strFullName = pObj.UserFullName;
                }

                if (!repositoryUserObj.IfUserExistInOAuthDB(strAccountName, strEmail))
                {
                    if (strFullName != null)
                        strFullName = strFullName.Trim();

                    if (strEmail != string.Empty)
                        strEmail = strEmail.Trim();

                    if (strAccountName != string.Empty)
                        strAccountName = strAccountName.Trim();

                    AddUser(strAccountName, strFullName, strDomain, strEmail, pObj.SelectedUserInputType, pObj.UserType);
                    response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.OK, pObj);
                   // result = true;
                }
                else
                {
                    pObj.ResponseMessage = "User already exist.";
                    response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.BadRequest, pObj);
                   // result = false;
                }
                    

                
            }
            catch(Exception ex)
            {
                pObj.ResponseMessage = ex.Message;
                response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.BadRequest, pObj);
            }
            finally
            {

            }

            return response;
        }


        public string GetSAMAccountNameByEmail(string email)
        {
            var oroot = new System.DirectoryServices.DirectoryEntry(LdapURIPath);
            var osearcher = new System.DirectoryServices.DirectorySearcher(oroot);
            osearcher.Filter = string.Format("(&(mail={0}))", email);
            var oresult = osearcher.FindAll();

            if (oresult.Count == 0) throw new InvalidOperationException(string.Format("Cannot find mail {0} in LDAP.", email));
            if (oresult.Count > 1) throw new InvalidOperationException(string.Format("There are {0} items with mail {1} in LDAP.", oresult.Count, email));

            return oresult[0].Properties["sAMAccountName"][0].ToString();
        }

        public string GetEmailBySAMAccountName(string sAMAccountName)
        {
            var oroot = new System.DirectoryServices.DirectoryEntry(LdapURIPath);
            var osearcher = new System.DirectoryServices.DirectorySearcher(oroot);
            osearcher.Filter = string.Format("(&(sAMAccountName={0}))", sAMAccountName);
            var oresult = osearcher.FindAll();

            if (oresult.Count == 0) throw new InvalidOperationException(string.Format("Cannot find sAMAccountName {0} in LDAP.", sAMAccountName));
            if (oresult.Count > 1) throw new InvalidOperationException(string.Format("There are {0} items with sAMAccountName {1} in LDAP.", oresult.Count, sAMAccountName));

            return oresult[0].Properties["mail"][0].ToString();
        }

        public string GetFullName(string AccountName)
        {
            DirectoryEntry entry = new System.DirectoryServices.DirectoryEntry(LdapURIPath);
            object obj = entry.NativeObject;

            DirectorySearcher search = new DirectorySearcher(entry);

            search.Filter = "(SAMAccountName=" + AccountName + ")";
            search.PropertiesToLoad.Add("cn");
            SearchResult result = search.FindOne();

            if (null == result)
            {
                return "";
            }

            //Update the new path to the user in the directory.
            LdapURIPath = result.Path;
            return (string)result.Properties["cn"][0];

            //return "";
        }


        [HttpPost]
        [Route("api/UserAuthorization/LDAPAuthentication")]
        public HttpResponseMessage LDAPAuthentication(UserLoginDetails objUser)
        {
          //  string _path = WebConfigurationManager.AppSettings["LdapURI"];
            string _filterAttribute;
            string emailID = string.Empty;

            string domainAndUsername = objUser.Domain + @"\" + objUser.UserName;
            DirectoryEntry entry = new DirectoryEntry(LdapURIPath, domainAndUsername, objUser.Password);

            HttpResponseMessage response = new HttpResponseMessage();
            repositoryUserObj = new UserAuthorizationRepository();

            try
            {
                //Bind to the native AdsObject to force authentication.
                object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + objUser.UserName + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return response;
                }

                //Update the new path to the user in the directory.
                LdapURIPath = result.Path;
                _filterAttribute = (string)result.Properties["cn"][0];

                response = Request.CreateResponse(HttpStatusCode.OK, "Authentication is success.");
                log.Debug("Method: LDAPAuthentication(UserLoginDetails objUser) -> Windows authentication is successful...");

                if (!repositoryUserObj.IfUserExistInOAuthDB(objUser.UserName, string.Empty))
                {

                    // get a DirectorySearcher object
                    // DirectorySearcher search1 = new DirectorySearcher(entry);
                    try
                    {
                        // specify the search filter
                        search.Filter = "(&(objectClass=user)(anr=" + objUser.UserName + "))";

                        // specify which property values to return in the search
                        search.PropertiesToLoad.Add("givenName");   // first name
                        search.PropertiesToLoad.Add("sn");          // last name
                        search.PropertiesToLoad.Add("mail");        // smtp mail address

                        // perform the search
                        result = search.FindOne();
                      //  emailID = result.Properties["mail"][0].ToString();
                        emailID = "abc@dummyEmail.com";
                    }
                    catch (Exception ex)
                    {
                        emailID = objUser.UserName + "@dummyEmail.com";
                    }



                    AddUser(objUser.UserName, _filterAttribute, objUser.Domain, emailID, string.Empty, string.Empty);
                    log.Debug("Method: LDAPAuthentication(UserLoginDetails objUser) -> " + objUser.Domain + "\\" + objUser.UserName + " has been created in OAuthDB.");
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Error: The user name or password is incorrect.");
                log.Error("Method: LDAPAuthentication(UserLoginDetails objUser) -> Error: The user name or password is incorrect." + ex);
                return response;
            }

            log.Debug("Method: LDAPAuthentication(UserLoginDetails objUser) -> TRUE");
            return response;
        }


        public void AddUser(string userName, string userFullName, string Domain, string emailId, string SelectedUserInputValue, string userType)
        {
            string strPWD = string.Empty;
           // if (Domain.ToUpper() == "SECAUCUS" || SelectedUserInputValue.ToUpper() == "GENERALUSER")
           // {
           //     strPWD = ApiConstant.DefaultPwdPrefix + userName.ToLower();
           // }
           //else
            {
                strPWD = ApiConstant.DefaultBrokerPwdPrefix + userName.ToLower();
            }
            
            string email = emailId;

            try
            {
                string data = string.Empty;
                // data = "UserName=" + txtUserId.Text.Trim() + "&Password=" + strOAuthPassword + "&Email=" + txtEmailId.Text.Trim();
                data = "UserName=" + userName + "&Password=" + strPWD + "&Email=" + email.Trim() + "&UserFullName=" + userFullName + "&Domain=" + Domain + "&UserTypeCode=" + userType;
                string apiNewUserUri = ApiConstant.apiNewUserUri;  //WebConfigurationManager.AppSettings["apiNewUserUri"];

                System.Net.HttpWebRequest request;
                request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(apiNewUserUri);
                request.Method = "POST";

                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                //  dataStream.Close();


                System.Net.HttpWebResponse myHttpWebResponse = (System.Net.HttpWebResponse)request.GetResponse();
                if (myHttpWebResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        }

        //[HttpPost]
        //[Route("api/UserAuthorization/NavigationAccessList")]
        //public HttpResponseMessage UserAuthorization(RoleApplicationAccessMapping obj)
        //{
        //    int RoleId = 0;
        //    string loginName = string.Empty;
        //    string userName4rpt = string.Empty;

        //    RoleApplicationAccessMapping listObj = new RoleApplicationAccessMapping();
        //    HttpResponseMessage data = new HttpResponseMessage();

        //    try
        //    {
        //        repositoryObj = new RoleApplicationAccessMapRepository();
        //        repositoryUserObj = new UserAuthorizationRepository();

        //        string[] arrUserName = obj.UserId.Trim().Split('\\');
        //        if (arrUserName.Length == 2)
        //        {
        //            // domainName = arrUserName[0].Trim();
        //            loginName = arrUserName[1].Trim();
        //            userName4rpt = obj.UserId.Replace("\\", "\\\\");
        //        }
        //        else
        //        {
        //            loginName = arrUserName[0].Trim();
        //            userName4rpt = obj.UserId;
        //        }

                   
        //        // Get RoleId of UserId
        //        RoleId = repositoryUserObj.GetRoleIdFromUserId(loginName);


        //        listObj = (repositoryObj.GetRoleAppAccessMapListFromDb(Convert.ToInt16(obj.AppId), RoleId, userName4rpt));

        //        var jsonObj = JsonConvert.SerializeObject(listObj.RoleAppAccessMapList);

        //        StringContent sc = new StringContent(jsonObj);
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        //        data.Content = sc;

        //        log.Debug("Method: api/UserAuthorization;  User Id:" + obj.UserId);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        data = null;
        //    }
        //    return data;
        //}

        // Reset Release Update flag
        [HttpPost]
        [Route("api/ResetReleaseUpdateFlag")]
        public HttpResponseMessage ResetReleaseUpdateFlag(UserLoginDetails objUser)
        {
            // string userFullName = string.Empty;

            repositoryUserObj = new UserAuthorizationRepository();
            objUser.UserName = repositoryUserObj.ResetReleaseUpdateFlag(objUser.UserId);

            HttpResponseMessage response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.OK, objUser);

            return response;
        }


        [HttpPost]
        [Route("api/UserAuthorization/ResetPassword")]
        public HttpResponseMessage ResetPassword(UserLoginDetails pObj)
        {
            string strAccountName = string.Empty;
            string strFullName = string.Empty;
            string strEmail = string.Empty;
            string strDomain = pObj.Domain;
            string strUserType = pObj.UserType;
            UserProfile userObj = new UserProfile();
            HttpResponseMessage response = null;
            repositoryUserObj = new UserAuthorizationRepository();

            try
            {
                if (repositoryUserObj.IfUserExistInOAuthDB(pObj.UserId, pObj.EmailId))
                {
                    if (pObj.NewPassword.Length >= 6)
                    {
                        userObj = repositoryUserObj.GetUserInfoByUserID(pObj.UserId, pObj.CompanyID);
                        if (userObj.Domain != null)
                            strDomain = userObj.Domain.Trim();

                        if (userObj.UserType != null)
                            strUserType = userObj.UserType.Trim();
                        strAccountName = pObj.UserId;
                        strEmail = userObj.Email;
                        strFullName = userObj.UserFullName;

                        if (strFullName != null)
                            strFullName = strFullName.Trim();

                        if (strEmail != string.Empty)
                            strEmail = strEmail.Trim();

                        if (strAccountName != string.Empty)
                            strAccountName = strAccountName.Trim();

                        ResetUserPassword(strAccountName, strFullName, strDomain, strEmail, pObj.NewPassword, strUserType, pObj.CompanyID);
                        response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.OK, pObj);
                    }
                    else
                    {
                        pObj.ResponseMessage = "User Password Length Must Be At Least 6 Characters.";
                        response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.BadRequest, pObj);
                    }
                }
                else
                {
                    pObj.ResponseMessage = "User Does Not Exist.";
                    response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.BadRequest, pObj);
                }
            }
            catch (Exception ex)
            {
                pObj.ResponseMessage = ex.Message;
                response = Request.CreateResponse<UserLoginDetails>(HttpStatusCode.BadRequest, pObj);
            }
            finally
            {
                userObj = null;
            }
            return response;
        }

        public void ResetUserPassword(string userName, string userFullName, string Domain, string emailId, string NewPassword, string userType, String CompanyID)
        {
            string strPWD = string.Empty;
            strPWD = NewPassword;// ApiConstant.DefaultBrokerPwdPrefix + userName.ToLower();
            string email = emailId;

            try
            {
                string data = string.Empty;
                //data = "UserName=" + userName + "&NewPassword=" + strPWD + "&ConfirmPassword=" + strPWD;
                data = "UserName=" + userName + "&Password=" + strPWD + "&Email=" + email.Trim() + "&UserFullName=" + userFullName + "&Domain=" + Domain + "&UserTypeCode=" + userType + "&CompanyID=" + CompanyID;
                string apiResetUserUri = ApiConstant.apiResetUserUri;  //WebConfigurationManager.AppSettings["apiNewUserUri"];

                System.Net.HttpWebRequest request;
                request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(apiResetUserUri);
                request.Method = "POST";

                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);

                System.Net.HttpWebResponse myHttpWebResponse = (System.Net.HttpWebResponse)request.GetResponse();
                if (myHttpWebResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }



    }
}
