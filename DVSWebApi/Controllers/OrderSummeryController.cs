﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Repository;
using System.Web.Http.Results;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class OrderSummeryController : ApiController
    {

        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // To get order details summery
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderSummery/GetOrderDetailSummery")]
        public HttpResponseMessage GetOrderDetailSummery(OrderSummeryModels objOrderSumry)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            OrderSummeryRepository _orderSumry = new OrderSummeryRepository();
            try
            {
                StringContent sc = new StringContent(_orderSumry.GetOrderSummeryData(objOrderSumry.OrderNo, objOrderSumry.CompnayID, objOrderSumry.TripID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetOrderDetailSummery " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;
        }//GetOrderDetailSummery

        // To insert stop order summery
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderSummery/InsertStopOrderSummery")]
        public HttpResponseMessage InsertStopOrderSummery(StopOrderSummeryModels objOrdSummry)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            OrderSummeryRepository _orderSumry = new OrderSummeryRepository();
            try
            {
                StringContent sc = new StringContent(_orderSumry.InsertStopOrderSummery(objOrdSummry));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK; 
            }
            catch (Exception ex)
            {
                log.Error("Method:InsertStopOrderSummery " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;
        }//InsertStopOrderSummery 


        // To get cancel reason of an item
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderSummery/GetCancelReasonText")]
        public HttpResponseMessage GetCancelReasonText(OrderSummeryModels objOrderSumry)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            OrderSummeryRepository _orderSumry = new OrderSummeryRepository();
            try
            {
                StringContent sc = new StringContent(_orderSumry.GetCancelReasonTextData(objOrderSumry.CompnayID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetCancelReasonText " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;
        }//GetCancelReasonText



    }
}
