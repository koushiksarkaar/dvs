﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web;
//using System.Web.Http.Cors;
using System.Web.Http.Filters;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.IO;
using System.Data;

namespace DVSWebApi.Controllers
{
    public class DashboardController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // To get Dashboard all information ( Header, Menu, Footer )
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Dashboard/GetDVSAllInformation")]
        public HttpResponseMessage GetDVSAllInformation(LoginInfo objLogin)
        {
            DashboardRepository repositoryObj = new DashboardRepository();
            DataSet dsData = new DataSet();
            DataTable dtLogin = new DataTable();
            dtLogin = repositoryObj.GetLoginInfoFromDb(objLogin.CompanyID); //Login
            dsData.Tables.Add(dtLogin);

            DataTable dtHeader = new DataTable();
            dtHeader = repositoryObj.GetHeaderFromDb(objLogin.CompanyID); //Header
            dsData.Tables.Add(dtHeader);

            DataTable dtFooter = new DataTable();
            dtFooter = repositoryObj.GetFooterFromDb(objLogin.CompanyID); //Footer
            dsData.Tables.Add(dtFooter);

            DataTable dtMenu = new DataTable();
            dtMenu = repositoryObj.GetMenuFromDb(objLogin.CompanyID); //Menu
            dsData.Tables.Add(dtMenu);

            DataTable dtTable = new DataTable();
            dtTable = repositoryObj.GetDashboardTopListFromDb(objLogin.CompanyID); //Dashboard top , middle & bottom
            dsData.Tables.Add(dtTable);

            //DataTable dtTable2 = new DataTable();
            //dtTable2 = repositoryObj.GetDashboardMiddleListFromDb(objLogin.CompanyID); //Dashboard Middle
            //dsData.Tables.Add(dtTable2);

            //DataTable dtTable3 = new DataTable();
            //dtTable3 = repositoryObj.GetDashboardBottomListFromDb(objLogin.CompanyID); //Dashboard Bottom
            //dsData.Tables.Add(dtTable3);

            DataTable dtConfig = new DataTable();
            dtConfig = repositoryObj.GetCompanyConfigFromDb(objLogin.CompanyID); //Dashboard top , middle & bottom
            dsData.Tables.Add(dtConfig);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }


        //[HttpGet]
        //[Route("api/Dashboard/GetCompanyConfigParams/{CompanyID}")]
        //public HttpResponseMessage GetCompanyConfigParams(String CompanyID)
        //{
        //    DashboardRepository repositoryObj = new DashboardRepository();
        //    StringContent sc = new StringContent(repositoryObj.GetCompanyConfigParamsData(CompanyID));
        //    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //    HttpResponseMessage data = new HttpResponseMessage();
        //    data.Content = sc;

        //    return data;
        //}

        // get List Of Completed Trips for Dashboard

        [Route("api/Dashboard/ListOfCompletedTrips_forDashboard")]
        [HttpPost]
        public HttpResponseMessage ListOfCompletedTrips_forDashboard()
        {
            DashboardRepository objRep = new Repository.DashboardRepository();
            try
            {
                return objRep.ListOfCompletedTrips_forDashboard();
            }
            catch (Exception ex)
            {
                log.Error("-> api/Dashboard/ListOfCompletedTrips_forDashboard", ex);
            }
            finally
            {
                objRep = null;
            }
            return null;
        }

        // get List of On Going Trip for Dashboard

        [Route("api/Dashboard/ListofOnGoingTrip_forDashboard")]
        [HttpPost]
        public HttpResponseMessage ListofOnGoingTrip_forDashboard()
        {
            DashboardRepository objRep = new Repository.DashboardRepository();
            try
            {
                return objRep.ListofOnGoingTrip_forDashboard();
            }
            catch (Exception ex)
            {
                log.Error("-> api/Dashboard/ListofOnGoingTrip_forDashboard", ex);
            }
            finally
            {
                objRep = null;
            }
            return null;
        }


        // Get List Of Recent Trips for Dashboard

        [Route("api/Dashboard/ListOfRecentTrips_forDashboard")]
        [HttpPost]
        public HttpResponseMessage ListOfRecentTrips_forDashboard()
        {
            DashboardRepository objRep = new Repository.DashboardRepository();
            try
            {
                return objRep.ListOfRecentTrips_forDashboard();
            }
            catch (Exception ex)
            {
                log.Error("-> api/Dashboard/ListOfRecentTrips_forDashboard", ex);
            }
            finally
            {
                objRep = null;
            }
            return null;
        }


        // get Total Order Count for Dashboard

        [Route("api/Dashboard/TotalOrderCountforDashboard")]
        [HttpPost]
        public HttpResponseMessage TotalOrderCountforDashboard()
        {
            DashboardRepository objRep = new Repository.DashboardRepository();
            try
            {
                return objRep.TotalOrderCountforDashboard();
            }
            catch (Exception ex)
            {
                log.Error("-> api/Dashboard/TotalOrderCountforDashboard", ex);
            }
            finally
            {
                objRep = null;
            }
            return null;
        }


        // get Trip Count For Dashboard

        [Route("api/Dashboard/TripCountForDashboard")]
        [HttpPost]
        public HttpResponseMessage TripCountForDashboard()
        {
            DashboardRepository objRep = new Repository.DashboardRepository();
            try
            {
                return objRep.TripCountForDashboard();
            }
            catch (Exception ex)
            {
                log.Error("-> api/Dashboard/TripCountForDashboard", ex);
            }
            finally
            {
                objRep = null;
            }
            return null;
        }
    }
}
