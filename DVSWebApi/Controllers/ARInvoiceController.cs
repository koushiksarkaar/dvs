﻿using DVSWebApi.Models;
using DVSWebApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace DVSWebApi.Controllers
{
    public class ARInvoiceController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ARInvoiceRepository custRepo = null;

        /// <summary>
        /// -> api/AR/customer/Get Customer List
        /// </summary>
        /// <param name="Customer"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/AR/customer/GetCustomerList")]
        public HttpResponseMessage GetCustomerList(CustomerModels Customer)
        {
            try
            {
                custRepo = new ARInvoiceRepository();
                StringContent sc = new StringContent(custRepo.GetCustomerList(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/AR/customer/GetCustomerList", ex);
            }
            finally
            {
                custRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Get Order Invoice List By Customer Code
        /// </summary>
        /// <param name="Customer"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/AR/customer/GetOrderInvoiceListByCustCode")]
        public HttpResponseMessage GetOrderInvoiceListByCustCode(CustomerModels Customer)
        {
            try
            {
                custRepo = new ARInvoiceRepository();
                StringContent sc = new StringContent(custRepo.GetOrderInvoiceListByCustCode(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/AR/customer/GetOrderInvoiceListByCustCode", ex);
            }
            finally
            {
                custRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Get Trip Details By Order No
        /// </summary>
        /// <param name="Customer"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/AR/customer/GetTripDetailsByOrderNo")]
        public HttpResponseMessage GetTripDetailsByOrderNo(CustomerModels Customer)
        {
            try
            {
                custRepo = new ARInvoiceRepository();
                StringContent sc = new StringContent(custRepo.GetTripDetailsByOrderNo(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/AR/customer/GetTripDetailsByOrderNo", ex);
            }
            finally
            {
                custRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Insert AR Invoice Data
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/AR/invoice/InsertARInvoiceData")]
        public HttpResponseMessage InsertARInvoiceData(InvoiceModel invoice)
        {
            try
            {
                custRepo = new ARInvoiceRepository();
                StringContent sc = new StringContent(custRepo.InsertARInvoiceData(invoice));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/AR/customer/InsertARInvoiceData", ex);
            }
            finally
            {
                custRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/AR/invoice/GetInsertedInvoiceList")]
        public HttpResponseMessage GetInsertedInvoiceList(SearchInvoiceList search)
        {
            try
            {
                custRepo = new ARInvoiceRepository();
                StringContent sc = new StringContent(custRepo.GetInsertedInvoiceList(search));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/AR/customer/GetInsertedInvoiceList", ex);
            }
            finally
            {
                custRepo = null;
            }
            return null;
        }

        /// <summary>
        /// get AR invoice status master
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/AR/invoice/GetOrderInvoiceStatus")]
        public HttpResponseMessage GetOrderInvoiceStatus()
        {
            try
            {
                custRepo = new ARInvoiceRepository();
                StringContent sc = new StringContent(custRepo.GetOrderInvoiceStatus());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/AR/customer/GetOrderInvoiceStatus", ex);
            }
            finally
            {
                custRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/AR/invoice/InsertApproveReject")]
        public HttpResponseMessage InsertApproveReject(ApproveReject approve)
        {
            try
            {
                custRepo = new ARInvoiceRepository();
                StringContent sc = new StringContent(custRepo.InsertApproveReject(approve));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/AR/customer/InsertApproveReject", ex);
            }
            finally
            {
                custRepo = null;
            }
            return null;
        }
    }
}
