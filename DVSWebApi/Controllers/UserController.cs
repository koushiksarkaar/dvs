﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DVSWebApi.Repository;
using System.Data;
using DVSWebApi.Models;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Security.Claims;
using System.IO;
using System.Web.Configuration;

namespace DVSWebApi.Controllers
{
    public class UserController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        HttpClient client = null;

        /// <summary>
        /// EndPoint for User authentication and when successfully authenticated it will return Token in response.
        /// This EndPoint can be called for Form user (Broker Id) as well as Windows user.
        /// BrokerId like "013030" and Windows user like "SECAUCUS\JSHSH"
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User")]
        public async Task<IHttpActionResult> GetToken([FromBody] LoginUser user)
        {
            
            JObject jsonObj = null;
            int companyId = 0;
            string userName = string.Empty;
            JObject jsonString = null;
            DataTable dt = null;
            DataToJson dtToJson = null;
            string domainName = string.Empty;
            string loginName = string.Empty;
            string[] arrUserName = {};
            string[] arrUserNameAdmin = {};

            UserAuthorizationRepository repositoryUserObj = new UserAuthorizationRepository();
            UserProfile userObj = new UserProfile();

            try
            {
                log.Debug(" Method: GetToken([FromBody] LoginUser user) [api/User] - Started -> UserId - " + user.UserId);

                client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                /// Check if UserId is Windows id OR Broker Id.
                /// If arrUserName.Length == 2, means its Windows Id else its Broker Id
                arrUserName = user.UserId.Trim().Split('\\');

                #region Authenticate for Windows username from Active Directory (LDAP)
                /// If UserId is Windows Id, authenticate it from active directory
                if (arrUserName.Length == 2)
                {
                    domainName = arrUserName[0].Trim();
                    loginName = arrUserName[1].Trim();


                    //string domain, string username, string pwd
                    var valuesSuccessPost1 = new Dictionary<string, string>()
                    {
                        {"Domain", domainName},
                        {"UserName", loginName},
                        {"Password", user.Password.Trim()}
                    };

                    HttpContent content1 = new FormUrlEncodedContent(valuesSuccessPost1);

                    // This will map call to WebApi
                    HttpResponseMessage response1 = await client.PostAsync(ApiConstant.ldapAuthenticationApiUri, content1);

                    if (!response1.IsSuccessStatusCode)
                    {
                        dt = new DataTable();
                        dt.Columns.Add("Authorization", typeof(string));
                        dt.Columns.Add("Message", typeof(string));
                        dt.Rows.Add("Fail", "Authorization has been denied for this request.");

                        dtToJson = new DataToJson();
                        // Convert Datatable to JSON string
                        jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "GetToken", true));

                        log.Debug("Method: -> GetToken([FromBody] LoginUser user) [api/User] -> (Fail) Windows Authorization (Active Directory login) has been denied for this request.");
                        return Ok(jsonString);
                    }
                    else
                    {
                        /// Once windows authentication is done set password which is in OAuth Database for token generation.
                        user.Password = ApiConstant.DefaultPwdPrefix + loginName.ToLower();
                    }
                }
                else
                {
                    // Split for example id login username is 013030.omsadmin, so that omsadmin can login as broker on his behalf. But omsadmin should have Admin role. 
                    arrUserNameAdmin = user.UserId.Trim().Split('.');

                    if (arrUserNameAdmin.Length == 2)
                    {
                        loginName = arrUserNameAdmin[1];
                        userObj = repositoryUserObj.GetUserProfile(loginName, user.CompanyID, user.DeviceID, user.TokenID);
                        // check for admin role.
                        if(userObj.Role != "Admin")
                        {
                            dt = new DataTable();
                            dt.Columns.Add("Authorization", typeof(string));
                            dt.Columns.Add("Message", typeof(string));
                            dt.Rows.Add("Fail", "Authorization has been denied for this request.");

                            dtToJson = new DataToJson();
                            // Convert Datatable to JSON string
                            jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "GetToken", true));

                            return Ok(jsonString);
                        }
                        else
                        {
                            user.Password = ApiConstant.DefaultPwdPrefix + loginName.ToLower();
                        }
                    }
                    else
                        loginName = arrUserName[0];
                }
                    

                #endregion

                #region Authenticate and generate token from OAuth DB
                var valuesSuccessPost2 = new Dictionary<string, string>()
                {
                    {"grant_type", "password" },
                    {"username", loginName},
                    {"password", user.Password.Trim()}
                };

                log.Debug("Called till here 1.");
                HttpContent content2 = new FormUrlEncodedContent(valuesSuccessPost2); 

                // This will map call to WebApi
                HttpResponseMessage response2 = await client.PostAsync(ApiConstant.userTokenApiUri, content2);

              //  log.Debug("Called till here 2.....Response 2:...."+ response2.);
                log.Debug("Method: -> GetToken([FromBody] LoginUser user) [api/User] -> Authentication (response.IsSuccessStatusCode) - " + response2.IsSuccessStatusCode + "; ApiConstant.userTokenApiUri -  " + ApiConstant.userTokenApiUri);
                log.Debug("API PATH:....." + ApiConstant.userTokenApiUri);
                // This will actually call to WebApi
                if (response2.IsSuccessStatusCode)
                {
                    log.Debug("Called till here 2.....Response 2 is success"  );
                    string result = await response2.Content.ReadAsStringAsync();

                    jsonObj = JObject.Parse(result);
                    string token = jsonObj["access_token"].ToString();

                    if (arrUserNameAdmin.Length == 2)
                    {
                        loginName = arrUserNameAdmin[0];
                    }

                    #region To get UserFullName

                    //userName = repositoryUserObj.GetUserFullNameFromUserId(loginName);
                    userObj = repositoryUserObj.GetUserProfile(loginName, user.CompanyID, user.DeviceID, user.TokenID);

                    #endregion To get UserFullName

                    if (userObj.UserName == null)
                    {
                        dt = new DataTable();
                        dt.Columns.Add("Authorization", typeof(string));
                        dt.Columns.Add("Message", typeof(string));
                        dt.Rows.Add("Fail", "Authorization has been denied for this request.");

                        dtToJson = new DataToJson();
                        // Convert Datatable to JSON string
                        jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "GetToken", true));
                    }
                    else
                    {
                        log.Debug("Method: -> GetToken([FromBody] LoginUser user) [api/User] Called GetUserDetails() for userName and CompanyId -> User Name - " + userName + " CompanyId - " + companyId);

                        dt = new DataTable();
                        dt.Columns.Add("Authorization", typeof(string));
                        dt.Columns.Add("access_token", typeof(string));
                        dt.Columns.Add("token_type", typeof(string));
                        dt.Columns.Add("expires_in", typeof(string));
                        dt.Columns.Add("userName", typeof(string));
                        dt.Columns.Add("userFullName", typeof(string));
                        dt.Columns.Add("companyId", typeof(string));
                        dt.Columns.Add("companyName", typeof(string));
                        dt.Columns.Add("role", typeof(string));
                        dt.Columns.Add("email", typeof(string));
                        dt.Columns.Add("phone", typeof(string));
                        dt.Columns.Add("DriverPicUri", typeof(string));
                        dt.Columns.Add("IsNewReleaseFlag", typeof(bool));
                        dt.Columns.Add("UserType", typeof(string));
                        dt.Columns.Add("DriverDivisionCode", typeof(string));
                        dt.Columns.Add("DeliveryCharge", typeof(string));
                        dt.Columns.Add("MinimumDeliveryChargeAmount", typeof(string));
                        dt.Columns.Add("LoginCompanyId", typeof(string));

                        dt.Rows.Add("Success", jsonObj["access_token"].ToString().Trim(), jsonObj["token_type"].ToString().Trim(), jsonObj["expires_in"].ToString().Trim(), userObj.UserName.Trim(), userObj.UserFullName.Trim(), userObj.CompanyId.Trim(), userObj.CompanyName.Trim(), userObj.Role.Trim(), userObj.Email.Trim(), userObj.Phone.Trim(), ApiConstant.brokerPicUri + userObj.BrokerPicName, userObj.IsNewReleaseFlag,userObj.UserType,userObj.BrokerDivisionCode,userObj.DeliveryCharge,userObj.MinimumDeliveryChargeAmount, userObj.LoginCompanyId);

                        dtToJson = new DataToJson();
                        // Convert Datatable to JSON string
                        jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "GetToken", true));
                    }
                }
                else
                {
                    dt = new DataTable();
                    dt.Columns.Add("Authorization", typeof(string));
                    dt.Columns.Add("Message", typeof(string));
                    dt.Rows.Add("Fail", "Authorization has been denied for this request.");

                    dtToJson = new DataToJson();
                    // Convert Datatable to JSON string
                    jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "GetToken", true));

                    log.Debug("Method: -> GetToken([FromBody] LoginUser user) [api/User] -> (Fail) Authorization has been denied for this request.");

                  //  return BadRequest("Authorization has been denied for this request.");
                }

                #endregion
            }
            catch (Exception ex)
            {
                log.Error(" Method: -> GetToken([FromBody] LoginUser user) [api/User] " + ex);
            }
            finally
            {
                repositoryUserObj = null;
                userObj = null;
            }
            return Ok(jsonString);

        }

       // public async Task<IHttpActionResult> GetToken([FromBody] LoginUser user)

        public async Task<IHttpActionResult> GetUserToken(string userName, string pwd)
        {
            JObject jsonObj = null;
            string token = string.Empty;

            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            var valuesSuccessPost = new Dictionary<string, string>()
                {
                    {"grant_type", "password" },
                    {"username", userName},
                    {"password", pwd}
                };

             HttpContent content = new FormUrlEncodedContent(valuesSuccessPost);
             HttpResponseMessage response = await client.PostAsync(ApiConstant.userTokenApiUri, content);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();

                    jsonObj = JObject.Parse(result);
                    token = jsonObj["access_token"].ToString();
                }

                return Ok(token);
        }


        /// <summary>
        /// function to validate token
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/VerifyUserToken/{UserId}")]
        public bool VerifyUserToken(string UserId)
        {
            string token = string.Empty;
            var re = Request;
            var headers = re.Headers;
            var result = false;

            try
            {
                if (headers.Contains("Authorization"))
                {
                    token = headers.GetValues("Authorization").First();
                }

                result = Task.Run(() => TokenValidation(token, UserId)).Result;

                log.Debug(" Method: VerifyUserToken(string UserId) [api/VerifyUserToken/{UserId}] - " + UserId + " Result - " + result);
            }
            catch (Exception ex)
            {
                log.Error(" Method: VerifyUserToken(string UserId) [api/VerifyUserToken/{UserId}]: " + ex);
            }


            return result;

            // {"Message":"Authorization has been denied for this request."}
        }

        /// <summary>
        /// This will check for valid token from the OAuth Web Api Service 
        /// </summary>
        /// <param name="token">Token received from client</param>
        /// <param name="userId">UserId OR BrokerID OR Salesman ID received from cleint</param>
        /// <returns>Return true if token is valid else false if Authorization has been denied</returns>
        public static async Task<bool> TokenValidation(string token, string userId)
        {
            string apiUserUri = ApiConstant.userVerifyTokenApiUri;
            // string apiUserUri = ApiConstant.userVerifyApiUri + "/" + userId; // Not to use this.
            bool result = false;

            try
            {
                using (var client = new HttpClient())
                {
                    //setup client
                    client.BaseAddress = new Uri(apiUserUri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", token);

                    //make request
                    HttpResponseMessage response = await client.GetAsync(apiUserUri);
                    var responseString = await response.Content.ReadAsStringAsync();

                    result = response.IsSuccessStatusCode;
                    log.Debug(" Method: TokenValidation(string token, string userId) -> TokenValidation -> userVerifyTokenApiUri -> " + apiUserUri + " Return - " + result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                log.Error(" Method: TokenValidation(string token, string userId) - " + ex);
            }

            return result;
        }

        /// <summary>
        /// This function is called from Client Portal to validate Token...
        /// </summary>
        /// <param name="token">Token received from client</param>
        /// <param name="userId">UserId OR BrokerID OR Salesman ID received from cleint (Curently not in use)</param>
        /// <returns>Return true if token is valid else false if Authorization has been denied</returns>
        public bool IsValidToken(string token, string userId)
        {
            var result = Task.Run(() => TokenValidation(token, userId)).Result;

            return result;
        }

  
        [HttpPost]
        [Route("api/UserAuthorizationForUserPost")]
        public async Task<IHttpActionResult> UserAuthorizationForUserPost(LoginUser user)
        {

            HttpClient client = new HttpClient();
            string userName = string.Empty;
            JObject jsonString = null;
            string[] arrUserNameAdmin = null;

            try
            {

                arrUserNameAdmin = user.UserId.Trim().Split('.');

                if (arrUserNameAdmin.Length == 2)
                {
                    user.UserId = arrUserNameAdmin[0];
                }

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var valuesSuccessPost = new Dictionary<string, string>()
                {
                    {"UserId", user.UserId},
                    {"AppId", user.AppId}
                };

                HttpContent content = new FormUrlEncodedContent(valuesSuccessPost);

                // This will map call to WebApi
                HttpResponseMessage response = await client.PostAsync(ApiConstant.userAuthorizationApiUri, content);

                DataTable dtMenuAccessObj = new DataTable();

                // This will actually call to WebApi
                if (response.IsSuccessStatusCode)
                {
                    string jsonData = await response.Content.ReadAsStringAsync();
                    // Convert JSON object received from Authorization web api into DataTable object.
                    dtMenuAccessObj = (DataTable)JsonConvert.DeserializeObject(jsonData, (typeof(DataTable)));
                }

                log.Debug(" Method: -> api/UserAuthorizationForUserPost -> ApiConstant.userAuthorizationApiUri - " + ApiConstant.userAuthorizationApiUri + "; response.IsSuccessStatusCode - " + response.IsSuccessStatusCode);

                DataTable dt = new DataTable();
                dt.Columns.Add("AccessId", typeof(int));
                dt.Columns.Add("AccessName", typeof(string));
                dt.Columns.Add("Description", typeof(string));
                dt.Columns.Add("AccessType", typeof(string));
                dt.Columns.Add("AccessControl", typeof(string));
                dt.Columns.Add("PageURL", typeof(string));
                dt.Columns.Add("IsAccess", typeof(bool));

                foreach (DataRow dr in dtMenuAccessObj.Rows)
                {
                    dt.Rows.Add(Convert.ToInt16(dr["AccessId"]), dr["AccessName"].ToString(), dr["Description"].ToString(), dr["AccessTypeName"].ToString(), dr["AccessControlName"].ToString(), dr["PageURL"].ToString(), Convert.ToBoolean(dr["IsAccess"]));
                }

                DataToJson dtToJson = new DataToJson();

                // Convert Datatable to JSON string
                jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "GetToken", true));

                log.Debug(" Method: -> api/UserAuthorizationForUserPost -> jsonString - " + jsonString);

                return Ok(jsonString);

            }
            catch (Exception ex)
            {
                log.Error(" Method: UserAuthorizationForUserPost(LoginUser user) " + ex);
            }
            return Ok(jsonString);
        }


        /// <summary>
        /// Upload User profile image
        /// </summary>
        /// <param name="objSalesCommunicator"></param>
        /// <returns></returns>
        //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/user/uploadProfileImage")]
        public HttpResponseMessage sendFileData(UserProfile userObj)
        {

            // UserController declaration for Token Varification
            UserController userController = null;
            UserAuthorizationRepository repositoryUserObj = new UserAuthorizationRepository();
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                             
                StringContent sc = new StringContent(repositoryUserObj.sendFileData(userObj));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Communicator/getSalesCommunicatorData", ex);
            }
            finally
            {
                repositoryUserObj = null;
            }
            return null;
        }

        /// <summary>
        /// Check User Profie Image
        /// </summary>
        /// <param name="objSalesCommunicator"></param>
        /// <returns></returns>
        //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/user/checkUserProfileImage")]
        public HttpResponseMessage checkUserProfileImage(UserProfile userObj)
        {

            // UserController declaration for Token Varification
            UserController userController = null;
            UserAuthorizationRepository repositoryUserObj = new UserAuthorizationRepository();
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                string userProfileImagePath = WebConfigurationManager.AppSettings["UserProfileImagePath"];
                var files = Directory.GetFiles(userProfileImagePath, userObj.UserId + ".*");
                string fileName;
                if (files.Length > 0)
                {
                    fileName =  files[0].ToString();
                     File.Delete(fileName);
                }

              StringContent sc = new StringContent("True");
              sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
              HttpResponseMessage data = new HttpResponseMessage();
              data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/user/checkUserProfileImage", ex);
            }
            finally
            {
                repositoryUserObj = null;
            }
            return null;
        }



    }
}
