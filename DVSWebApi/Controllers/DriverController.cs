﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Repository;
using System.Web.Http.Results;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class DriverController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public DriverController()
        {

        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        public DriverController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        // To register driver
        //[AllowAnonymous]
        //[HttpPost]
        //[Route("api/Driver/Login")]
        //public HttpResponseMessage Login(Driver model)
        //{
        //    try
        //    {

        //        DriverRepository _driverObj = new DriverRepository();
        //        StringContent sc = new StringContent(_driverObj.DriverLogin(model));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        HttpResponseMessage data = new HttpResponseMessage();
        //        data.Content = sc;
        //        return data;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/Driver/Login", ex);
        //    }
        //    return null;

        //}

        // POST api/ Driver/SignUp
        [AllowAnonymous]
        [HttpPost]
        [Route("api/Driver/Register")]
        public HttpResponseMessage Register(Driver model)
        {
            try
            {
                DriverRepository _driverObj = new DriverRepository();
                StringContent sc = new StringContent(_driverObj.DriverRegistration(model));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

                //var user = new ApplicationUser() { UserName = model.UserId, Email = model.Email };
                //IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                //if (result.Succeeded)
                //{
                //    var _user = await UserManager.FindAsync(model.UserId, model.Password);

                    //var _user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    // var result1 = UserManager.AddToRole(model.UserId, "Driver");

                    //var roleManager = new RoleManager<Microsoft.AspNet.Identity.EntityFramework.IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                    //if (!roleManager.RoleExists("Driver"))
                    //{
                    //    var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                    //    role.Name = "Driver";
                    //    roleManager.Create(role);

                    //}                    

                    // var currentUser = UserManager.FindByName(user.UserName); 
                    // UserManager.AddToRole(currentUser.Id, "Driver");
                    //model.MemberID = _user.Id;
                    //DriverRepository _driverObj = new DriverRepository();
                    //Boolean flag = _driverObj.DriverRegistration(model);

                    //if (flag)
                    //{
                    //    //log.Debug(" Method:SignUp - Registration  successfully :" + model.UserId);
                    //    //// Request.CreateResponse(HttpStatusCode.OK, "Registration  successfully");
                    //    return Ok();
                    //}
                    //else
                    //{
                    //    //log.Debug(" Method:SignUp - Registration Failed :" + model.UserId);
                    //    //// Request.CreateErrorResponse(HttpStatusCode.NotFound,"Registration Failed");
                    //    return NotFound();
                    //}
                //}
                //else
                //{
                //    return GetErrorResult(result);
                //}
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/Register", ex);
                //log.Error(" Method:SignUp " + ex.Message.ToString());

                //return NotFound();
            }

            //if (!result.Succeeded)
            //{
            //    return GetErrorResult(result);
            //}

            //return Ok();
            return null;
        }//

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        // To get refresh token
        [HttpPost]
        [Route("api/Driver/GetRefreshToken")]
        public HttpResponseMessage GetRefreshToken(LoginUser userObj)
        {
            //  declaration for Refresh Token 
            DriverRepository drvRepository = null;
            try
            {
                drvRepository = new DriverRepository();
                StringContent sc = new StringContent(drvRepository.GetRefreshTokenData(userObj.DeviceID, userObj.TokenID, userObj.UserId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/user/GetRefreshToken", ex);
            }
            finally
            {
                drvRepository = null;
            }
            return null;
        }

        // Get All Driver List

        //[Route("api/Driver/GetAllDriverList")]
        [Route("api/Driver/GetAllDriverList/{CompanyID}/{SearchTxt}")]
        [HttpGet]
        public HttpResponseMessage GetAllDriverList(String CompanyID, String SearchTxt)
        {
            try
            {
                DriverRepository objRep = new Repository.DriverRepository();
                return objRep.GetAllDriverList(CompanyID, SearchTxt);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/GetAllDriverList", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }


        [Route("api/Driver/GetDriverDetailByID/{CompanyID}/{DriverLoginID}")]
        [HttpGet]
        public HttpResponseMessage GetDriverDetailByID(String CompanyID, String DriverLoginID)
        {
            try
            {
                DriverRepository objRep = new Repository.DriverRepository();
                return objRep.GetDriverDetailsByIDdata(CompanyID, DriverLoginID);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/GetDriverDetailByID", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }

        // Get All Active Vehicles Type

        [Route("api/Driver/GetAllActiveVehiclesType")]
        [HttpPost]
        public HttpResponseMessage GetAllActiveVehiclesType(SearchBase search)
        {
            try
            {
                DriverRepository objRep = new Repository.DriverRepository();
                return objRep.GetAllActiveVehiclesType(search);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/GetAllActiveVehiclesType", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }


        // Get All Vehicles List

        [Route("api/Driver/GetAllVehiclesList")]
        [HttpPost]
        public HttpResponseMessage GetAllVehiclesList(SearchBase search)
        {
            try
            {
                DriverRepository objRep = new Repository.DriverRepository();
                return objRep.GetAllVehiclesList(search);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/GetAllVehiclesList", ex);
            }
            finally
            {
                //do nathing;
            }
            return null;
        }


        // insert Vehicles Type into database

        [Route("api/Driver/insertVehiclesType")]
        [HttpPost]
        public bool insertVehiclesType(VehiclesType objVehiclesType)
        {
            Boolean flag = false;
            try
            {
                DriverRepository objRep = new Repository.DriverRepository();
                flag = objRep.insertVehiclesType(objVehiclesType);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/insertVehiclesType", ex);
            }
            finally
            {
                //do nathing;
            }
            return flag;
        }

        [Route("api/Driver/insertDriverDetails")]
        [HttpPost]
        public bool insertDriverDetails(DriverModel driver)
        {
            Boolean flag = false;
            try
            {
                DriverRepository objDriverRep = new DriverRepository();
                flag = objDriverRep.insertDriverDetails(driver);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/insertDriverDetails", ex);
            }
            finally
            {
                //do nathing
            }
            return flag;
        }

        [Route("api/Driver/GetWarehouseList/{CompanyID}")]
        [HttpGet]
        public HttpResponseMessage GetWarehouseList(String CompanyID)
        {
            try
            {
                DriverRepository objRep = new Repository.DriverRepository();
                return objRep.GetAllWarehouseList(CompanyID);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/GetWarehouseList", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }

        [Route("api/Driver/ValidateLoginUser/{LoginUserID}/{CompanyID}")]
        [HttpGet]
        public HttpResponseMessage ValidateLoginUser(String LoginUserID, String CompanyID)
        {
            try
            {
                DriverRepository objRep = new Repository.DriverRepository();
                return objRep.ValidateLoginUserData(LoginUserID, CompanyID);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Driver/ValidateLoginUser", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }




    }
}
