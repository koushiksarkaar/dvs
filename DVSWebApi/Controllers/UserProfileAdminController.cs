﻿using DVSWebApi.Models;
using DVSWebApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DVSWebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserProfileAdminController : ApiController
    {
        UserProfileAdminReprository repositoryObj = null;

        [HttpGet]
        [Route("api/UserProfileAdmin/GetUserSearchList/{UserSearchStr}")]
        public UserProfile GetUserSearchList(string UserSearchStr)
        {
            UserProfile listObj = new UserProfile();
            repositoryObj = new UserProfileAdminReprository();
            listObj = (repositoryObj.GetUserSearchListFromDb(UserSearchStr));
            return listObj;
        }

        [HttpGet]
        [Route("api/UserProfileAdmin/GetUserProfileDetails/{UserName}")]
        public UserProfile GetUserProfileDetails(string UserName)
        {
            UserProfile listObj = new UserProfile();
            repositoryObj = new UserProfileAdminReprository();
            listObj = (repositoryObj.GetUserProfileDetailsFromDb(UserName));
            return listObj;
        }

        [HttpPost]
        [Route("api/UserProfileAdmin/UpdateUserProfileDetails")]
        public bool UpdateUserProfileDetails(UserProfile pObj)
        {
            repositoryObj = new UserProfileAdminReprository();
            return repositoryObj.UpdateUserProfileDetailToDb(pObj);
        }

        [HttpPost]
        [Route("api/UserProfileAdmin/UserResetPwdFromAdmin")]
        public UserProfile UserResetPwdFromAdmin(UserProfile pObj)
        {
            AccountController accountObj = new AccountController();
            ForgetPasswordBindingModel resetPwdobj = new ForgetPasswordBindingModel();
            resetPwdobj.UserName = pObj.UserName;
            resetPwdobj.NewPassword = ApiConstant.DefaultBrokerPwdPrefix + pObj.UserName.ToLower();
            resetPwdobj.UpdateBy = pObj.UpdateBy;

            pObj.SuccessStatus = accountObj.ResetPasswordFromAdmin(resetPwdobj);

            if (pObj.SuccessStatus)
            {
                pObj.Password = resetPwdobj.NewPassword;
                return pObj;
            }
            else
                pObj.SuccessStatus = false;

            return pObj;
        }
    }
}
