﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web;
//using System.Web.Http.Cors;
using System.Web.Http.Filters;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.IO;
using System.Data;

namespace DVSWebApi.Controllers
{
    public class CustomerDashController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("api/CustomerDash/GetCustomerDashboardList")]
        [HttpPost]
        public HttpResponseMessage GetCustomerDashboardList()
        {
            CustomerDashRepository objRep = new Repository.CustomerDashRepository();
            try
            {
                return objRep.GetCustomerDashboardData();
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustomerDash/GetCustomerDashboardList", ex);
            }
            finally
            {
                objRep = null;
            }
            return null;
        }

    }
}
