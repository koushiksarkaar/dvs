﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Net.Http.Headers;

namespace DVSWebApi.Controllers
{
    public class FileInvoiceController : ApiController
    {
        [HttpPost()]
        public HttpResponseMessage UploadFiles()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            String sPath = "";
            DataTable dt = new DataTable();
            dt.Columns.Add("ReturnStatus", typeof(string));
            sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/DVSmobileImg/UploadedFiles/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE.
            if (iUploadedCnt > 0)
            {
                dt.Rows.Add("Ok");
            }
            else
            {
                dt.Rows.Add("File Already Exists.");
            }
            DataToJson dtToJson = new DataToJson();
            String jsonString = dtToJson.convertDataTableToJson(dt, "FileInvoice", true);
            StringContent sc = new StringContent(jsonString);
            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage data = new HttpResponseMessage();
            data.Content = sc;
            return data;
        }
    }
}
