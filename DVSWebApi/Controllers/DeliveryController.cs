﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Repository;
using System.Web.Http.Results;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class DeliveryController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Delivery/InsertDelivery")]
        public HttpResponseMessage InsertDelivery(DeliveryModels objDelivery)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            DeliveryRepository _delvryRepository = new DeliveryRepository();
            try
            {
                StringContent sc = new StringContent(_delvryRepository.InsertDeliveryData(objDelivery));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:InsertDelivery " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//InsertDelivery


        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/Delivery/AdjustDeliveryQty")]
        //public HttpResponseMessage AdjustDeliveryQty(DeliveryModels objDelivery)
        //{
        //    HttpResponseMessage data = new HttpResponseMessage();
        //    DeliveryRepository _delvryRepository = new DeliveryRepository();
        //    try
        //    {
        //        StringContent sc = new StringContent(_delvryRepository.AdjustDeliveryQty(objDelivery));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        data.Content = sc;
        //        data.StatusCode = HttpStatusCode.OK;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Method:AdjustDeliveryQty " + ex.Message.ToString());
        //        data.StatusCode = HttpStatusCode.ExpectationFailed;
        //    }
        //    return data;

        //}//AdjustDeliveryQty
        

    }
}
