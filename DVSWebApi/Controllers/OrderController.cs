﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Web.Http.Cors;

namespace DVSWebApi.Controllers
{
    public class OrderController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Get All Order for Order List        
        [Route("api/Order/GetAllOrderforOrderList")]
        [HttpPost]
        public HttpResponseMessage GetAllOrderforOrderList(OrderModel search)
        {
            try
            {
                Repository.OrderRepository objRep = new Repository.OrderRepository();
                return objRep.GetAllOrderforOrderList(search);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/GetAllOrderforOrderList", ex);
            }
            finally
            {
                //do nathing
            }
            return null;
        }

        [Route("api/Order/getAllOrder/{SalesmanID}/{CompanyID}")]
        [HttpGet]
        public HttpResponseMessage getAllOrder(string SalesmanID, string CompanyID)
        {
            Repository.OrderRepository objRep = new Repository.OrderRepository();
            return objRep.getAllOrder(SalesmanID, CompanyID);
        }
    }
}
