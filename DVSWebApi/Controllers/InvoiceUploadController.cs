﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Threading.Tasks;
using System.Web;
//using OMSWebApi.UploadRepo;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Net.Http.Headers;
using System.Data;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class InvoiceUploadController : ApiController
    {
        [Mime]
        public async Task<FileUploadDetails> Post()
        {
            // file path
            var fileuploadPath = HttpContext.Current.Server.MapPath("~/DVSmobileImg/UploadedFiles");
            // 
            var multiFormDataStreamProvider = new MultiFileUploadProvider(fileuploadPath);

            // Read the MIME multipart asynchronously 
            await Request.Content.ReadAsMultipartAsync(multiFormDataStreamProvider);

            String uploadingFileName = multiFormDataStreamProvider
                .FileData.Select(x => x.LocalFileName).FirstOrDefault();

            //var provider = new MultipartFormDataStreamProvider(fileuploadPath);
            //foreach (MultipartFileData forms in multiFormDataStreamProvider.FormData.)
            //{
            //    string str1 = forms.Headers.ContentDisposition.Name;//get FileName
            //    string str2 = forms.LocalFileName;//get File Path
            //}

            String uploadingFormData = multiFormDataStreamProvider.FormData.GetValues(0).FirstOrDefault();//.FirstOrDefault();
            uploadingFormData = uploadingFormData.Replace("\\", "");

            //dynamic stuff = JsonConvert.DeserializeObject("{ 'Name': 'Jon Smith', 'Address': { 'City': 'New York', 'State': 'NY' }, 'Age': 42 }");
            //string name = stuff.Name;            //string address = stuff.Address.City;
            dynamic frmData = JsonConvert.DeserializeObject(uploadingFormData);
            String CustId, CompId, SecImgName, OutsideImgName, InImgName;

            CustId = frmData.CustomerID;
            CompId = frmData.CompanyID;
            InvoiceUploadRepository invProfile = null;
            invProfile = new InvoiceUploadRepository();
            DataTable dtl = new DataTable();
            dtl = invProfile.SaveImageListAll(CustId, CompId, frmData);//SectionSrl, SectionNm, OutsideSrl, OutsideNm, InsideSrl, InsideNm);

            //ImageViewModel imageVwModel = new ImageViewModel();            //var jData = JsonObject.Parse(uploadingFormData); //var formdata = new FormData();
            //Newtonsoft.Json.Linq.JArray a = Newtonsoft.Json.Linq.JArray.Parse(uploadingFormData);
            //{            //    foreach (Newtonsoft.Json.Linq.JObject o in a.Children<Newtonsoft.Json.Linq.JObject>())
            //    {            //        foreach (JProperty p in o.Properties())
            //        {            //            string name = p.Name;            //            imageVwModel.sectionImgs.sec
            //            list_skupin.Add(name);            //            string value = (string)p.Value;            //            Console.WriteLine(name);            //        }            //    }            //}

            // Create response
            return new FileUploadDetails
            {

                FilePath = uploadingFileName,

                FileName = "Message: " + Convert.ToString(dtl.Rows[0]["Msg"]) + ", CustomerId: " + Convert.ToString(dtl.Rows[0]["CustomerId"]) + "",

                FileLength = new FileInfo(uploadingFileName).Length,

                FileCreatedTime = DateTime.Now.ToLongDateString()
            };

        }
    }

    public class FileUploadDetails
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public long FileLength { get; set; }
        public string FileCreatedTime { get; set; }

    }
    public class Mime : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(
                        HttpStatusCode.UnsupportedMediaType)
                );
            }
        }
    }
    public class MultiFileUploadProvider : MultipartFormDataStreamProvider
    {
        public MultiFileUploadProvider(string rootPath) : base(rootPath) { }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            if (headers != null &&
                headers.ContentDisposition != null)
            {
                return headers
                    .ContentDisposition
                    .FileName.TrimEnd('"').TrimStart('"');
            }

            return base.GetLocalFileName(headers);
        }
    }
}
