﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Results;
using DVSWebApi.Repository;
using System.Net.Mail;
using System.Web.Configuration;
using System.IO;
using System.Net.Mime;

namespace DVSWebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        /// <summary>
        /// RK test
        /// </summary>
        /// // GET api/Account/VerifyToken
        [HttpGet]
        [Route("VerifyToken")]
        public bool VerifyToken()
        {
            return true;
        }

        /// // GET api/Account/VerifyAdminToken
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("VerifyAdminToken")]
        public bool VerifyAdminToken()
        {
            return true;
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [AllowAnonymous]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            var jsonResponseObj = "[{}]";

            try
            {
                UserController userObj = new UserController();
                var resStr = await userObj.GetUserToken(model.UserName, model.OldPassword);
                string tokerStr = ((System.Web.Http.Results.OkNegotiatedContentResult<string>)(resStr)).Content;
                
                if(tokerStr != string.Empty)
                {
                    UserAuthorizationRepository repObj = new UserAuthorizationRepository();
                    string userId = repObj.GetUserIDFromUserName(model.UserName);

                    UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

                    userManager.RemovePassword(userId);

                    userManager.AddPassword(userId, model.NewPassword);

                    jsonResponseObj = "[{"
                        + "\", \"Message\":\"Password changed successfully."
                        + "\", \"Status\":\"success"
                        + "\"}]";

                    return Ok(jsonResponseObj);
                }
                else
                {
                    jsonResponseObj = "[{"
                        + "\", \"Message\":\"Wrong old password. Try again..."
                        + "\", \"Status\":\"fail"
                        + "\"}]";

                    BadRequest(jsonResponseObj);
                }
                
            }
            catch (Exception ex)
            {
                jsonResponseObj = "[{"
                        + "\", \"ErrorMessage\":\"" + ex
                        + "\", \"Message\":\"Sorry, some error occur. Please contact support."
                        + "\", \"Status\":\"fail"
                        + "\"}]";
                return BadRequest(jsonResponseObj);
            }

            return BadRequest();
        }


        // POST api/Account/ForgetPassword
        [HttpPost]
        [AllowAnonymous]
        [Route("ForgetPasswordEmail")]
        public async Task<IHttpActionResult> ForgetPasswordEmail(ForgetPasswordBindingModel model)
        {
            UserAuthorizationRepository userRepObj = null;
            UserController userControllerObj = null;
            LoginUser objUser = null;
            bool ifEmailExist = false;
            string regEmail = string.Empty;
            var jsonResponseObj = "[{}]";
            string body = string.Empty;
            
            try
            {
                userRepObj = new UserAuthorizationRepository();
                ifEmailExist = userRepObj.IfUserValidEmailOAuthDB(model.UserName, model.Email);

               // string from = WebConfigurationManager.AppSettings["SMTPServerEmail"];
                string from = WebConfigurationManager.AppSettings["FromEmail"];
                string to = model.Email;
                string cc = WebConfigurationManager.AppSettings["CCedEmail"];
                string subject = WebConfigurationManager.AppSettings["ForgetPwdSubject"];
                string bodyStr1 = WebConfigurationManager.AppSettings["ForgetPwdBody"];
                //string bodyStr1 = "Please confirm your email address to set your password and redirect to ";

                string redirectOMSurl = WebConfigurationManager.AppSettings["RedirectToOMSfromEmailURL"] + model.UserName + "&fogetPwdFlag=true";
                string supportEmail = WebConfigurationManager.AppSettings["SupportEmail"];
                string supportPhone = WebConfigurationManager.AppSettings["SupportPhone"];

                if (ifEmailExist)
                {
                    userControllerObj = new UserController();
                    objUser = new LoginUser();
                    objUser.UserId = "FORGET_BROKER_TOKEN";
                    objUser.Password = "Pwd1@forget_broker_token";
                    var resStr = await userControllerObj.GetUserToken("FORGET_BROKER_TOKEN", "Pwd1@forget_broker_token");

                    string tokerStr = ((System.Web.Http.Results.OkNegotiatedContentResult<string>)(resStr)).Content;
                    redirectOMSurl += "&access_token=" + tokerStr;

                    body = bodyStr1 + "<a href='" + redirectOMSurl + "'> link </a> </br>";
                    body += "For assistance please email to " + supportEmail + " OR call to " + supportPhone;
                }
                else
                {
                   userRepObj = null;
                   userRepObj = new UserAuthorizationRepository();
                   regEmail = userRepObj.GetUserProfile(model.UserName, model.CompanyID, model.DeviceID, model.TokenID).Email;
                   cc += ", " + regEmail;

                  //  body = bodyStr1 + "<a href='" + redirectOMSurl + "'> link </a> </br>";
                  //  body += "For assistance please email to " + supportEmail + " OR call to " + supportPhone;
                    
                    body = "Sorry, your email does not match with registered email, for assistance please email to " + supportEmail + " OR call to " + supportPhone;
                }

                bool isEmailSent = SendEmail(from, to, subject, body, cc);

                if(isEmailSent)
                {
                    jsonResponseObj = "[{"
                        + "\", \"Message\":\"Email sent successfully, please check your email."
                        + "\", \"Status\":\"success"
                        + "\"}]";
                }
                else
                {
                    jsonResponseObj = "[{"
                        + "\", \"Message\":\"Email not sent, something goes wrong. Please contact support."
                        + "\", \"Status\":\"fail"
                        + "\"}]";
                }
              
            }
            catch(Exception ex)
            {
                jsonResponseObj = "[{"
                        + "\", \"ErrorMessage\":\"" + ex
                        + "\", \"Message\":\"Sorry, some error occur. Please contact support."
                        + "\", \"Status\":\"fail"
                        + "\"}]";
                return BadRequest(jsonResponseObj);
            }
            finally
            {
                userRepObj = null;
                userControllerObj = null;
                objUser = null;
            }

            return Ok(jsonResponseObj);
        }

        [AllowAnonymous]
        public bool SendEmail(string from, string to, string subject, string body, string cc)
        {
            bool isEmailSent = false;

            try
            {
                MailMessage email = new MailMessage(from, to, subject, body);

                if (cc != "")
                {
                    email.CC.Add(cc);
                }
                email.IsBodyHtml = true;

                string server = WebConfigurationManager.AppSettings["SMTPServer"];
                SmtpClient smtp = new SmtpClient(server);

                smtp.UseDefaultCredentials = true;

                smtp.Send(email);
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
            return isEmailSent;
        }

        [AllowAnonymous]
        public bool SendEmailWithAttachment(string from, string to, string subject, string body, string cc,string filePath,string OrderNo)
        {
            bool isEmailSent = false;

            try
            {
                MailMessage email = new MailMessage(from, to, subject, body);

          //      email.Attachments.Add(new Attachment(filePath));
           
              byte[] filebytes = Convert.FromBase64String(filePath);
           //   File.WriteAllBytes(@"C:\Triveni\Work\test\" + OrderNo+".pdf", filebytes);

         //    email.Attachments.Add(new Attachment(new MemoryStream(filebytes),"test1"));
              string OrderDataPath = WebConfigurationManager.AppSettings["OrderData"];
                

              FileStream fs = new FileStream(OrderDataPath + OrderNo+".png", FileMode.OpenOrCreate);
              fs.Write(filebytes, 0, filebytes.Length);
              fs.Close();

              Attachment attachment = new Attachment(OrderDataPath + OrderNo + ".png");
            //    attachment.TransferEncoding = TransferEncoding.Base64;
             email.Attachments.Add(attachment);


                if (cc != "")
                {
                    email.CC.Add(cc);
                }
                email.IsBodyHtml = true;

                string server = WebConfigurationManager.AppSettings["SMTPServer"];
                SmtpClient smtp = new SmtpClient(server);

                smtp.UseDefaultCredentials = true;

                smtp.Send(email);
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
            return isEmailSent;
        }
        /*
        [AllowAnonymous]
        public bool SendEmail(string from, string to, string subject, string body, string cc)
        {
            bool isEmailSent = false;

            try
            {
                MailMessage email = new MailMessage(from, to, subject, body);
                email.CC.Add(cc);
                email.IsBodyHtml = true;

                string server = WebConfigurationManager.AppSettings["SMTPServer"]; 
                SmtpClient smtp = new SmtpClient(server);

                smtp.UseDefaultCredentials = false;
                smtp.Port = 2525;
                smtp.EnableSsl = true;

                string smtpServerEmail = WebConfigurationManager.AppSettings["SMTPServerEmail"];
                string smtpServerPassword = WebConfigurationManager.AppSettings["SMTPServerPassword"];
                smtp.Credentials = new System.Net.NetworkCredential(smtpServerEmail, smtpServerPassword);
                //smtp.Credentials = authinfo;

                smtp.Send(email);
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
            return isEmailSent;
        }

        */

        // POST api/Account/ForgetPassword
        [HttpPost]
        [Authorize]
        [Route("ForgetPassword")]
        public async Task<IHttpActionResult> ForgetPassword(ForgetPasswordBindingModel model)
        {
            var jsonResponseObj = "[{}]";
            UserAuthorizationRepository repObj = null;
            UserManager<IdentityUser> userManager = null;

            try
            {
                repObj = new UserAuthorizationRepository();
                string userId = repObj.GetUserIDFromUserName(model.UserName);

                userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
                userManager.RemovePassword(userId);
                userManager.AddPassword(userId, model.NewPassword);

                bool resultState = repObj.UpdateUpdatedBy(model.UserName);

                jsonResponseObj = "[{"
                        + "\", \"Message\":\"Password changed successfully."
                        + "\", \"Status\":\"success"
                        + "\"}]";
            }
            catch(Exception ex)
            {
                jsonResponseObj = "[{"
                        + "\", \"ErrorMessage\":\"" + ex
                        + "\", \"Message\":\"Sorry, some error occur. Please contact support."
                        + "\", \"Status\":\"fail"
                        + "\"}]";

                return BadRequest(jsonResponseObj);
            }
            finally
            {
                repObj = null;
                userManager = null;
            }
            return Ok(jsonResponseObj);
        }

        ///


        public bool ResetPasswordFromAdmin(ForgetPasswordBindingModel model)
        {
            var jsonResponseObj = "[{}]";
            UserAuthorizationRepository repObj = null;
            UserManager<IdentityUser> userManager = null;

            try
            {
                repObj = new UserAuthorizationRepository();
                string userId = repObj.GetUserIDFromUserName(model.UserName);

                userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
                userManager.RemovePassword(userId);
                userManager.AddPassword(userId, model.NewPassword);

                bool resultState = repObj.UpdateUpdatedBy(model.UserName);

                jsonResponseObj = "[{"
                        + "\", \"Message\":\"Password changed successfully."
                        + "\", \"Status\":\"success"
                        + "\"}]";
            }
            catch (Exception ex)
            {
                jsonResponseObj = "[{"
                        + "\", \"ErrorMessage\":\"" + ex
                        + "\", \"Message\":\"Sorry, some error occur. Please contact support."
                        + "\", \"Status\":\"fail"
                        + "\"}]";

            //    return BadRequest(jsonResponseObj);
            }
            finally
            {
                repObj = null;
                userManager = null;
            }
         //   return Ok(jsonResponseObj);

            return true;
        }

        // POST api/Account/UpdateMyProfile
        [HttpPost]
        [Authorize]
        [Route("UpdateMyProfile")]
        public async Task<IHttpActionResult> UpdateMyProfile(UserProfile model)
        {
            var jsonResponseObj = "[{}]";
            
            try
            {
                UserAuthorizationRepository repObj = new UserAuthorizationRepository();
                bool userId = repObj.UpdateUserProfile(model);

                if (userId)
                {
                    jsonResponseObj = "[{"
                        + "\"UserName\":\"" + model.UserName
                        + "\", \"Email\":\"" + model.Email
                        + "\", \"Phone\":\"" + model.Phone
                        + "\", \"Message\":\"My profile updated successfully."
                        + "\", \"Status\":\"success"
                        + "\"}]";
                }
                else
                {
                    jsonResponseObj = "[{"
                        + "\", \"Message\":\"Sorry, something is wrong. Please contact support."
                        + "\", \"Status\":\"fail"
                        + "\"}]";

                    return BadRequest(jsonResponseObj);
                }

            }
            catch (Exception ex)
            {
                jsonResponseObj = "[{"
                        + "\", \"ErrorMessage\":\"" + ex
                        + "\", \"Message\":\"Sorry, some error occur. Please contact support."
                        + "\", \"Status\":\"fail"
                        + "\"}]";

                return BadRequest(jsonResponseObj);
            }
            return Ok(jsonResponseObj);
        }

        // POST api/Account/CreateNewUser
        [AllowAnonymous]
        [Route("CreateNewUser")]
        public async Task<IHttpActionResult> CreateNewUser(CreateNewUserBindingModel model)
        {
            bool updtResult = false;

            try
            {
                var user = new ApplicationUser() { UserName = model.UserName, Email = model.Email };

                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                UserAuthorizationRepository userObj = new UserAuthorizationRepository();

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                else
                {
                    updtResult = userObj.UpdateNewUser(model);
                }
            }
            catch(Exception ex)
            {

            }
            

            return Ok();
        }


        // POST api/Account/SetPassword
        [AllowAnonymous]
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(model.UserName, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [AllowAnonymous]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(CreateNewUserBindingModel model)
        {
            bool updtResult = false;

            try
            {
                var user = new ApplicationUser() { UserName = model.UserName, Email = model.Email };

                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                UserAuthorizationRepository userObj = new UserAuthorizationRepository();

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                else
                {
                    updtResult = userObj.UpdateUserPassword(model);
                }
            }
            catch (Exception ex)
            {

            }
            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                
                 ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result); 
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }


        [Authorize(Roles = "Broker")]
        [HttpGet]
        [Route("TestRole")]
        public bool TestRole()
        {
            return true;
        }

        [Authorize(Roles = "Broker")]
        [HttpPost]
        [Route("TestRoleForPost")]
        public bool TestRoleForPost()
        {
            return true;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("TestAdmin")]
        public bool TestAdmin()
        {
            return true;
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
