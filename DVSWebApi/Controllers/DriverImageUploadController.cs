﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace DVSWebApi.Controllers
{
    public class DriverImageUploadController : ApiController
    {
        [HttpPost()]
        public string UploadFiles()
        {
            int iUploadedCnt = 0;
            string FileName = string.Empty, fullPath = string.Empty;
            try
            {
                // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
                string sPath = "", DataType = String.Empty;
                DataType = "DriversImages/";
                sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/DVSmobileImg/" + DataType);

                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

                // CHECK THE FILE COUNT.
                for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
                {
                    System.Web.HttpPostedFile hpf = hfc[iCnt];

                    if (hpf.ContentLength > 0)
                    {
                        // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                        FileName = hpf.FileName;
                        if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                        {
                            // SAVE THE FILES IN THE FOLDER.
                            hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                            fullPath = sPath + hpf.FileName;
                            iUploadedCnt = iUploadedCnt + 1;
                        }
                        else
                        {
                            File.Delete(Path.Combine(sPath, hpf.FileName));
                            hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                            fullPath = sPath + hpf.FileName;
                            iUploadedCnt = iUploadedCnt + 1;
                            //string[] files = Directory.GetFiles(sPath + hpf.FileName);
                            //foreach (string file in files)
                            //{
                            //File.Delete(file);
                            ////Console.WriteLine("{file} is deleted." );
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            // RETURN A MESSAGE.
            if (iUploadedCnt > 0)
            {
                return FileName;
            }
            else
            {
                return "Upload Failed";
            }
        }
    }
}
