﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Repository;
using System.Web.Http.Results;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class DriverTripController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("api/DriverTrip/GetAllTripInformation")]
        [HttpPost]
        public HttpResponseMessage GetAllTripInformation(TripSearchBase search)
        {
            try
            {
                DriverTripRepository objRep = new DriverTripRepository();
                return objRep.GetAllTripInformation(search);
            }
            catch (Exception ex)
            {
                log.Error("-> api/DriverTrip/GetAllTripInformation", ex);
            }
            finally
            {
                //do nathing;
            }
            return null;
        }


        [Route("api/DriverTrip/getAllInProgressTrip")]
        [HttpPost]
        public HttpResponseMessage getAllInProgressTrip()
        {
            try
            {
                DriverTripRepository objRep = new Repository.DriverTripRepository();
                return objRep.getAllInProgressTrip();
            }
            catch (Exception ex)
            {
                log.Error("-> api/DriverTrip/getAllInProgressTrip", ex);
            }
            finally
            {
                //do nathing;
            }
            return null;
        }

        [Route("api/DriverTrip/searchInProgressTripBySearchText")]
        [HttpPost]
        public HttpResponseMessage searchInProgressTripBySearchText(TripSearchBase search)
        {
            try
            {
                DriverTripRepository objRep = new DriverTripRepository();
                return objRep.searchInProgressTripBySearchText(search);
            }
            catch (Exception ex)
            {
                log.Error("-> api/DriverTrip/searchInProgressTripBySearchText", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }


        [Route("api/DriverTrip/GetTripSummeryBytripId")]
        [HttpPost]
        public HttpResponseMessage GetTripSummeryBytripId(TripModels Trip)
        {
            try
            {
                DriverTripRepository objRep = new DriverTripRepository();
                return objRep.GetTripSummeryBytripId(Trip);
            }
            catch (Exception ex)
            {
                log.Error("-> api/DriverTrip/GetTripSummeryBytripId", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }

        [Route("api/DriverTrip/TripMapViewByTripID")]
        [HttpPost]
        public HttpResponseMessage TripMapViewByTripID(TripModels Trip)
        {
            try
            {
                Repository.DriverTripRepository objRep = new Repository.DriverTripRepository();
                return objRep.getTripMapViewByTripID(Trip);
            }
            catch (Exception ex)
            {
                log.Error("-> api/DriverTrip/TripMapViewByTripID", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }

        [Route("api/DriverTrip/GetTripCustomerCheckIn")]
        [HttpPost]
        public HttpResponseMessage GetTripCustomerCheckIn(TripModels Trip)
        {
            try
            {
                Repository.DriverTripRepository objRep = new Repository.DriverTripRepository();
                return objRep.GetTripCustomerCheckIn(Trip);
            }
            catch (Exception ex)
            {
                log.Error("-> api/DriverTrip/GetTripCustomerCheckIn", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }


        [Route("api/DriverTrip/getDriverTripInformation")]
        [HttpPost]
        public HttpResponseMessage getDriverTripInformation(TripModels Trip)
        {
            try
            {
                Repository.DriverTripRepository objRep = new Repository.DriverTripRepository();
                return objRep.getDriverTripInformation(Trip);
            }
            catch (Exception ex)
            {
                log.Error("-> api/DriverTrip/getDriverTripInformation", ex);
            }
            finally
            {
                // do nathing;
            }
            return null;
        }

    }
}
