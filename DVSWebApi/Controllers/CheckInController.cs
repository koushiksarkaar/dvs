﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Repository;
using System.Web.Http.Results;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class CheckInController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // To get Check In data
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/CheckIn/GetCheckInData")]
        public HttpResponseMessage GetCheckInData(Trip objTrip)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            CheckInRepository _checkIn = new CheckInRepository();
            try
            {
                StringContent sc = new StringContent(_checkIn.GetCheckIn(objTrip.TripID, objTrip.CompanyID, objTrip.CustomerID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetCheckInData " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GetCheckInData

        // To insert / add Stop check in data
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/CheckIn/InsertStopCheckIn")]
        public HttpResponseMessage InsertStopCheckIn(TripStopCheckIn objStopCheckIn)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            CheckInRepository _checkIn = new CheckInRepository();
            try
            {
                StringContent sc = new StringContent(_checkIn.InsertStopCheckIn(objStopCheckIn));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:InsertStopCheckIn" + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//InsertStopCheckIn

        // To get stop summery by customer id
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/CheckIn/GetStopSummeryByCustomer")]
        public HttpResponseMessage GetStopSummeryByCustomer(Trip objTrip)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            CheckInRepository _checkIn = new CheckInRepository();
            try
            {
                StringContent sc = new StringContent(_checkIn.GetStopSummery2Data(objTrip.TripID, objTrip.CustomerID, objTrip.CompanyID, objTrip.UserName));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetStopSummeryByCustomer " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GetStopSummeryByCustomer 


        





    }
}
