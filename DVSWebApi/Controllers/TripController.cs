﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DVSWebApi.Models;
using DVSWebApi.Providers;
using DVSWebApi.Repository;
using System.Web.Http.Results;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace DVSWebApi.Controllers
{
    public class TripController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Trip/GetTripInfo")]
        public HttpResponseMessage GetTripInfo(Trip objTrip)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            try
            {
                TripRepository _trip = new TripRepository();

                StringContent sc = new StringContent(_trip.GetDriverTripInformation(objTrip.UserName, objTrip.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                log.Error(" Method:GetTripInformation " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;
        }//GetTripInformation

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Trip/GetTripStopDetails")]
        public HttpResponseMessage GetTripStopDetails(Trip objTrip)
        {

            HttpResponseMessage data = new HttpResponseMessage();
            TripRepository _trip = new TripRepository();
            try
            {
                StringContent sc = new StringContent(_trip.GetRouteStopDetails(objTrip.RouteID, objTrip.UserName));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error(" Method:GetTripStopDetails " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GetTripStopDetails

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Trip/GetTripStopSummery")]
        public HttpResponseMessage GetTripStopSummery(Trip objTrip)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            TripRepository _driver = new TripRepository();
            try
            {
                StringContent sc = new StringContent(_driver.GetTripStopSummeryInfo(objTrip.TripID, objTrip.CompanyID, objTrip.ActualStartTime));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetTripStopSummery " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;
        }//GetTripStopSummery

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Trip/SkipCustomerOfTripSummery")]
        public HttpResponseMessage SkipCustomerOfTripSummery(Trip objTrip)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            TripRepository _driver = new TripRepository();
            try
            { //IsCustomerSkipped = 0 means  Red  , 1 = Green ,  2 = Skipped
                StringContent sc = new StringContent(_driver.SkipCustomerOfTripSummeryInfo(objTrip.TripID, objTrip.CompanyID, objTrip.CustomerID, objTrip.IsCustomerSkipped));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:SkipCustomerOfTripSummery " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;
        }//SkipCustomerOfTripSummery

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Trip/GetTripMapView")]
        public HttpResponseMessage GetTripMapView(Trip objTrip)
        {
            HttpResponseMessage data = new HttpResponseMessage();
            TripRepository _driver = new TripRepository();
            try
            {
                StringContent sc = new StringContent(_driver.GetTripMapView(objTrip.TripID, objTrip.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error("Method:GetTripMapView " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GetTripMapView

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Trip/GetTripListOrderCount")]
        public HttpResponseMessage GetTripListOrderCount(TripCount objTrip)
        {

            HttpResponseMessage data = new HttpResponseMessage();
            TripRepository _trip = new TripRepository();
            try
            {
                StringContent sc = new StringContent(_trip.GetTripListOrderCountData(objTrip.CompanyID, objTrip.DriverID, objTrip.StartDate, objTrip.EndDate));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                data.Content = sc;
                data.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.Error(" Method:GetTripListOrderCount " + ex.Message.ToString());
                data.StatusCode = HttpStatusCode.ExpectationFailed;
            }
            return data;

        }//GetTripListOrderCount


    }
}
