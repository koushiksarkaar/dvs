﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web;
//using System.Web.Http.Cors;
using System.Web.Http.Filters;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.IO;
using System.Data;

namespace DVSWebApi.Controllers
{
    public class OfflineController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Offline/GetDVSDashboard")]
        public HttpResponseMessage GetDVSDashboard(LoginInfo objLogin)
        {
            DashboardRepository repositoryObj = new DashboardRepository();
            DataSet dsData = new DataSet();

            DataTable dtTable = new DataTable();
            dtTable = repositoryObj.GetDashboardTopListFromDb(objLogin.CompanyID); //Dashboard top , middle & bottom
            dsData.Tables.Add(dtTable);

            DataTable dtFooter = new DataTable();
            dtFooter = repositoryObj.GetFooterFromDb(objLogin.CompanyID); //Footer
            dsData.Tables.Add(dtFooter);

            DataTable dtMenu = new DataTable();
            dtMenu = repositoryObj.GetMenuFromDb(objLogin.CompanyID); //Menu
            dsData.Tables.Add(dtMenu);

            DataTable dtConfig = new DataTable();
            dtConfig = repositoryObj.GetCompanyConfigFromDb(objLogin.CompanyID); //Dashboard top , middle & bottom
            dsData.Tables.Add(dtConfig);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }

         [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Offline/GetAllTrips")]
        public HttpResponseMessage GetAllTrips(InputParameters objTrips)
        {
            OfflineRepository repositoryObj = new OfflineRepository();
            DataSet dsData = new DataSet();
            DataTable dtTrips = new DataTable();
            dtTrips = repositoryObj.GetTripsInfoFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
            dsData.Tables.Add(dtTrips);

            DataTable dtOrders = new DataTable();
            dtOrders = repositoryObj.GetOrdersInfoFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
            dsData.Tables.Add(dtOrders);

            DataTable dtCustomers = new DataTable();
            dtCustomers = repositoryObj.GetCustomersInfoFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
            dsData.Tables.Add(dtCustomers);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }

         [Authorize(Roles = CustomRoles.BrkRole)]
         [HttpPost]
         [Route("api/Offline/GetAllOrderInvoiceDetails")]
         public HttpResponseMessage GetAllOrderInvoiceDetails(InputParameters objTrips)
         {
             OfflineRepository repositoryObj = new OfflineRepository();
             DataSet dsData = new DataSet();
             DataTable dtTrips = new DataTable();
             dtTrips = repositoryObj.GetOrdersDetailsFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
             dsData.Tables.Add(dtTrips);

             //DataTable dtOrders = new DataTable();
             //dtOrders = repositoryObj.GetOrdersInfoFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
             //dsData.Tables.Add(dtOrders);

             //DataTable dtCustomers = new DataTable();
             //dtCustomers = repositoryObj.GetCustomersInfoFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
             //dsData.Tables.Add(dtCustomers);

             return Request.CreateResponse(HttpStatusCode.OK, dsData);
         }

         [Authorize(Roles = CustomRoles.BrkRole)]
         [HttpPost]
         [Route("api/Offline/GetInvoiceDSDImagePDF")]
         public HttpResponseMessage GetInvoiceDSDImagePDF(InputParameters objTrips)
         { 
             OfflineRepository repositoryObj = new OfflineRepository();
             DataSet dsData = new DataSet();
             DataTable dtTrips = new DataTable();
             dtTrips = repositoryObj.GetInvoiceDSDImagePDFFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
             dsData.Tables.Add(dtTrips);

             return Request.CreateResponse(HttpStatusCode.OK, dsData);
         }

        // ----------------------   UPLOAD DVS ALL DATA  BY SYNCING     ----------------------------------------------
         [Authorize(Roles = CustomRoles.BrkRole)]
         [HttpPost]
         [Route("api/Offline/TripDataUpload")]
         public HttpResponseMessage TripDataUpload(TripsModel objTripModel)
         {
             OfflineRepository repositoryObj = new OfflineRepository();
             try
             {
                 StringContent sc = new StringContent(repositoryObj.UploadTripsData(objTripModel));
                 sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                 HttpResponseMessage data = new HttpResponseMessage();
                 data.Content = sc;
                 return data;

             }
             catch (Exception ex)
             {
                 log.Error("-> api/Offline/TripDataUpload", ex);
             }
             finally
             {
                 repositoryObj = null;
             }
             return null;
         }

         [Authorize(Roles = CustomRoles.BrkRole)]
         [HttpPost]
         [Route("api/Offline/CustomerDataUpload")]
         public HttpResponseMessage CustomerDataUpload(CustomersModel objCustomerModel)
         {
             OfflineRepository repositoryObj = new OfflineRepository();
             try
             {
                 StringContent sc = new StringContent(repositoryObj.UploadCustomerData(objCustomerModel));
                 sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                 HttpResponseMessage data = new HttpResponseMessage();
                 data.Content = sc;
                 return data;

             }
             catch (Exception ex)
             {
                 log.Error("-> api/Offline/CustomerDataUpload", ex);
             }
             finally
             {
                 repositoryObj = null;
             }
             return null;
         }

          [Authorize(Roles = CustomRoles.BrkRole)]
         [HttpPost]
         [Route("api/Offline/GetInvoiceNoByOrderNo")]
         public HttpResponseMessage GetInvoiceNoByOrderNo(InputParameters objTrips)
         {
             OfflineRepository repositoryObj = new OfflineRepository();
             DataSet dsData = new DataSet();
             DataTable dtTrips = new DataTable();
             dtTrips = repositoryObj.GetInvoiceNoByOrderNoFromDb(objTrips.CompanyID, objTrips.DriverID, objTrips.StartDate, objTrips.EndDate); //Trips
             dsData.Tables.Add(dtTrips);

             return Request.CreateResponse(HttpStatusCode.OK, dsData);
         }

          [Authorize(Roles = CustomRoles.BrkRole)]
          [HttpPost]
          [Route("api/Offline/StopsCheckInUpload")]
          public HttpResponseMessage StopsCheckInUpload(StopCheckInModel objCheckInModel)
          {
              OfflineRepository repositoryObj = new OfflineRepository();
              try
              {
                  StringContent sc = new StringContent(repositoryObj.StopsCheckInUploadrData(objCheckInModel));
                  sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                  HttpResponseMessage data = new HttpResponseMessage();
                  data.Content = sc;
                  return data;

              }
              catch (Exception ex)
              {
                  log.Error("-> api/Offline/StopsCheckInUpload", ex);
              }
              finally
              {
                  repositoryObj = null;
              }
              return null;
          }

          // To sync Order detail data for uploading from mobile to sql table
          [Authorize(Roles = CustomRoles.BrkRole)]
          [HttpPost]
          [Route("api/Offline/OrderDetailsUpload")]
          public HttpResponseMessage OrderDetailsUpload(OrderDetailsModel objOrdDetModel)
          {
              OfflineRepository repositoryObj = new OfflineRepository();
              try
              {
                  StringContent sc = new StringContent(repositoryObj.OrderDetailsUploadData(objOrdDetModel));
                  sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                  HttpResponseMessage data = new HttpResponseMessage();
                  data.Content = sc;
                  return data;

              }
              catch (Exception ex)
              {
                  log.Error("-> api/Offline/OrderDetailsUpload", ex);
              }
              finally
              {
                  repositoryObj = null;
              }
              return null;
          }

          //To sync Invoice detail and DSD data for uploading from mobile to sql table
          [Authorize(Roles = CustomRoles.BrkRole)]
          [HttpPost]
          [Route("api/Offline/InvoiceDSDUpload")]
          public HttpResponseMessage InvoiceDSDUpload(InvoicDSDModel objInvoiceModel)
          {
              OfflineRepository repositoryObj = new OfflineRepository();
              try
              {
                  StringContent sc = new StringContent(repositoryObj.InvoiceDSDUploadData(objInvoiceModel));
                  sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                  HttpResponseMessage data = new HttpResponseMessage();
                  data.Content = sc;
                  return data;

              }
              catch (Exception ex)
              {
                  log.Error("-> api/Offline/InvoiceDSDUpload", ex);
              }
              finally
              {
                  repositoryObj = null;
              }
              return null;
          }

          //To upload  DSD images data from mobile to sql table
          [Authorize(Roles = CustomRoles.BrkRole)]
          [HttpPost]
          [Route("api/Offline/DSDPhotosUpload")]
          public HttpResponseMessage DSDPhotosUpload(DSDImageModel objInvoiceModel)
          {
              OfflineRepository repositoryObj = new OfflineRepository();
              try
              {
                  StringContent sc = new StringContent(repositoryObj.DSDPhotosUploadData(objInvoiceModel));
                  sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                  HttpResponseMessage data = new HttpResponseMessage();
                  data.Content = sc;
                  return data;

              }
              catch (Exception ex)
              {
                  log.Error("-> api/Offline/DSDPhotosUpload", ex);
              }
              finally
              {
                  repositoryObj = null;
              }
              return null;
          }
            //To upload  DSD images data from mobile to sql table
            [Authorize(Roles = CustomRoles.BrkRole)]
            [HttpPost]
            [Route("api/Offline/DSDMultiplePhotosUpload")]
            public HttpResponseMessage DSDMultiplePhotosUpload(DSDMultipleImageModel objMultipleImgModel)
            {
                OfflineRepository repositoryObj = new OfflineRepository();
                try
                {
                    StringContent sc = new StringContent(repositoryObj.DSDMultiplePhotosUploadData(objMultipleImgModel));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;

                }
                catch (Exception ex)
                {
                    log.Error("-> api/Offline/DSDMultiplePhotosUpload", ex);
                }
                finally
                {
                    repositoryObj = null;
                }
                return null;
            }

        //To upload  Customer Status  ( Status will be Delivered / Pending  from mobile to sql table
        [Authorize(Roles = CustomRoles.BrkRole)]
          [HttpPost]
          [Route("api/Offline/CustomerStatusUpload")]
          public HttpResponseMessage CustomerStatusUpload(CustomerStatusModel objCustStatModel)
          {
              OfflineRepository repositoryObj = new OfflineRepository();
              try
              {
                  StringContent sc = new StringContent(repositoryObj.CustomerStatusUploadData(objCustStatModel));
                  sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                  HttpResponseMessage data = new HttpResponseMessage();
                  data.Content = sc;
                  return data;

              }
              catch (Exception ex)
              {
                  log.Error("-> api/Offline/CustomerStatusUpload", ex);
              }
              finally
              {
                  repositoryObj = null;
              }
              return null;
          }

          [HttpGet]
          [Route("api/Offline/LineChartData")]
          public HttpResponseMessage LineChartData()
          {
              OfflineRepository repositoryObj = new OfflineRepository();
              try
              {
                  StringContent sc = new StringContent(repositoryObj.LineChartData());
                  sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                  HttpResponseMessage data = new HttpResponseMessage();
                  data.Content = sc;
                  return data;

              }
              catch (Exception ex)
              {
                  log.Error("-> api/Offline/LineChartData", ex);
              }
              finally
              {
                  repositoryObj = null;
              }
              return null;
          }


    }
}
