﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

namespace DVSWebApi.Repository
{
    public class OfflineRepository
    {
        DataTable dt = null;
        DBConnConfigSB3 dbConnConfigSB3 = null;
        DataToJson dtToJson = new DataToJson();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnectionSB3 dbConnectionObj = null;
        public DataTable GetFooterFromDb(string CompnayID)
        {
            List<Footer> FooterListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "Footer", "@CompanyID", CompnayID);
                FooterListObj = new List<Footer>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            FooterListObj.Add(new Footer
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(FooterListObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                FooterListObj = null;
            }

            return dt;
        }


        public DataTable GetDashboardTopListFromDb(String CompnayID)
        {
            List<DashBoard> DashTopListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "Dashboard", "@CompanyID", CompnayID);
                DashTopListObj = new List<DashBoard>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DashTopListObj.Add(new DashBoard
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(DashTopListObj);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnConfigSB3 = null;
                DashTopListObj = null;
            }

            return dt;
        }

        public DataTable GetCompanyConfigParamsData(String CompanyId)
        {
            Common common = new Common();
            List<CompanyConfig> compConfigListObj = null;
            DataTable dt = null;
            //SqlDataAdapter da = null;
            //String jsonString, ImgName = string.Empty;
            try
            {
                dt = new DataTable();
                compConfigListObj = new List<CompanyConfig>();
                dbConnConfigSB3 = new DBConnConfigSB3();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetCompanyConfigList, "@CompanyID", CompanyId);

                //Bind LoginImgModel generic list using LINQ   
                compConfigListObj = (from DataRow dr in dt.Rows
                                     select new CompanyConfig()
                                     {
                                         CompanyID = Convert.ToString(dr["CompanyID"]),
                                         ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                         DriverPhoneOnOff = Convert.ToString(dr["DriverPhoneOnOff"]),
                                         DollarSignOnOff = Convert.ToString(dr["DollarSignOnOff"]),
                                         ThemeColor = Convert.ToString(dr["ThemeColor"]),
                                         PaymentDueDateOnOff = Convert.ToString(dr["PaymentDueDateOnOff"]),
                                         PaymentTermOnOff = Convert.ToString(dr["PaymentTermOnOff"]),
                                         PaymentDueDatePeriod = Convert.ToString(dr["PaymentDueDatePeriod"]),
                                         CustDashBackDatePeriod = Convert.ToString(dr["CustDashBackDatePeriod"])
                                     }).ToList();
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(compConfigListObj);
                DataSet dsCompConfigParams = new DataSet();
                dsCompConfigParams.Tables.Add(dt);
                //jsonString = ConvertDataTabletoString(dsCompConfigParams);
                //return jsonString;
                return dt;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                //da = null;
                //compConfigListObj = null;
            }
            return null;// dt;
        }
        public DataTable GetMenuFromDb(string CompnayID)
        {
            //Common common = new Common();
            List<Menu> MenuListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "Menu", "@CompanyID", CompnayID);
                MenuListObj = new List<Menu>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            MenuListObj.Add(new Menu
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(MenuListObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                MenuListObj = null;
            }

            return dt;
        }

        public DataTable GetTripsInfoFromDb(string CompnayID, string DriverID, string StartDate, string EndDate)
        {
            //Common common = new Common();
            List<TripsInfo> TripsObj = null;
            dbConnectionObj = new DBConnectionSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SprocGetAllTrips, "@CompanyID", CompnayID, "@DriverID", DriverID, "@StartDate", StartDate, "@EndDate", EndDate);
                TripsObj = new List<TripsInfo>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TripsObj.Add(new TripsInfo
                            {
                                TripID = Convert.ToString(dt.Rows[i]["TripID"]),
                                TripName = Convert.ToString(dt.Rows[i]["TripName"]),
                                StartDate = Convert.ToString(dt.Rows[i]["StartDate"]),
                                StartTime = Convert.ToString(dt.Rows[i]["StartTime"]),
                                EndDate = Convert.ToString(dt.Rows[i]["EndDate"]),
                                EndTime = Convert.ToString(dt.Rows[i]["EndTime"]),
                                ActualStartTime = Convert.ToString(dt.Rows[i]["ActualStartTime"]),
                                ActualEndTime = Convert.ToString(dt.Rows[i]["ActualEndTime"]),
                                OrdersCount = Convert.ToString(dt.Rows[i]["OrdersCount"]),
                                StopsCount = Convert.ToString(dt.Rows[i]["StopsCount"]),
                                ItemsCount = Convert.ToString(dt.Rows[i]["ItemsCount"]),
                                StatusID = Convert.ToString(dt.Rows[i]["StatusID"]),
                                StatusText = Convert.ToString(dt.Rows[i]["StatusText"])
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(TripsObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                TripsObj = null;
            }

            return dt;
        }

        public DataTable GetOrdersInfoFromDb(string CompnayID, string DriverID, string StartDate, string EndDate)
        {
            //Common common = new Common();
            List<OrdersInfo> TripsObj = null;
            dbConnectionObj = new DBConnectionSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SprocGetAllOrders, "@CompanyID", CompnayID, "@DriverID", DriverID, "@StartDate", StartDate, "@EndDate", EndDate);
                TripsObj = new List<OrdersInfo>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TripsObj.Add(new OrdersInfo
                            {
                                TripID = Convert.ToString(dt.Rows[i]["TripID"]),
                                TripDetailID = Convert.ToString(dt.Rows[i]["TripDetailID"]),
                                StartLocation = Convert.ToString(dt.Rows[i]["StartLocation"]),
                                CustomerNo = Convert.ToString(dt.Rows[i]["CustomerNo"]),

                                ToDoComments = Convert.ToString(dt.Rows[i]["ToDoComments"]),
                                IsStopSummeryDone = Convert.ToString(dt.Rows[i]["IsStopSummeryDone"]),
                                IsToDo = Convert.ToString(dt.Rows[i]["IsToDo"]),
                                IsCustomerSkipped = Convert.ToString(dt.Rows[i]["IsCustomerSkipped"]),
                                OrderDate = Convert.ToString(dt.Rows[i]["OrderDate"]),
                                OrderTotal = Convert.ToString(dt.Rows[i]["OrderTotal"]),
                                TotalDiscount = Convert.ToString(dt.Rows[i]["TotalDiscount"]),
                                OrderQty = Convert.ToString(dt.Rows[i]["OrderQty"]),
                                OrderNo = Convert.ToString(dt.Rows[i]["OrderNo"]),
                                ItemReturn = Convert.ToString(dt.Rows[i]["ItemReturn"]),
                                ItemExtra = Convert.ToString(dt.Rows[i]["ItemExtra"]),
                                DeliveryFlag = Convert.ToString(dt.Rows[i]["DeliveryFlag"])
                                
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(TripsObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                TripsObj = null;
            }

            return dt;
        }

        public DataTable GetCustomersInfoFromDb(string CompnayID, string DriverID, string StartDate, string EndDate)
        {
            //Common common = new Common();
            List<CustomersInfo> TripsObj = null;
            dbConnectionObj = new DBConnectionSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetAllCustomers, "@CompanyID", CompnayID, "@DriverID", DriverID, "@StartDate", StartDate, "@EndDate", EndDate);
                TripsObj = new List<CustomersInfo>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TripsObj.Add(new CustomersInfo
                            {
                                TripID = Convert.ToString(dt.Rows[i]["TripID"]),
                                TripDetailID = Convert.ToString(dt.Rows[i]["TripDetailID"]),
                                //StartDate = Convert.ToString(dt.Rows[i]["StartDate"]),                                //StartTime = Convert.ToString(dt.Rows[i]["StartTime"]),
                                //EndDate = Convert.ToString(dt.Rows[i]["EndDate"]),                                //EndTime = Convert.ToString(dt.Rows[i]["EndTime"]),
                                ActualStartTime = Convert.ToString(dt.Rows[i]["ActualStartTime"]),
                                ActualEndTime = Convert.ToString(dt.Rows[i]["ActualEndTime"]),
                                StartLocation = Convert.ToString(dt.Rows[i]["StartLocation"]),
                                CustomerNo = Convert.ToString(dt.Rows[i]["CustomerNo"]),
                                Name = Convert.ToString(dt.Rows[i]["Name"]),
                                Street = Convert.ToString(dt.Rows[i]["Street"]),
                                City = Convert.ToString(dt.Rows[i]["City"]),
                                State = Convert.ToString(dt.Rows[i]["State"]),
                                Zip = Convert.ToString(dt.Rows[i]["Zip"]),
                                EmailId = Convert.ToString(dt.Rows[i]["EmailId"]),
                                Country = Convert.ToString(dt.Rows[i]["Country"]),
                                ContactNo = Convert.ToString(dt.Rows[i]["ContactNo"]),
                                SalesmanID = Convert.ToString(dt.Rows[i]["SalesmanID"]),
                                Latitude = Convert.ToString(dt.Rows[i]["Latitude"]),
                                Longitude = Convert.ToString(dt.Rows[i]["Longitude"]),
                                SpecificCustomer = Convert.ToString(dt.Rows[i]["SpecificCustomer"]),
                                IsCustomerSkipped = Convert.ToString(dt.Rows[i]["IsCustomerSkipped"]),
                                CustomerCheckInFlag = Convert.ToString(dt.Rows[i]["CustomerCheckInFlag"]),
                                CustomerStatusFlag = Convert.ToString(dt.Rows[i]["CustomerStatusFlag"]),
                                DeliveryCharges = Convert.ToString(dt.Rows[i]["DeliveryCharges"]),
                                CustomerNotes = Convert.ToString(dt.Rows[i]["CustomerNote"]),
                                StopImageName = Convert.ToString(dt.Rows[i]["StopImageName"]),
                                StopImagePath = Convert.ToString(dt.Rows[i]["StopImagePath"]),
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(TripsObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                TripsObj = null;
            }

            return dt;
        }

        public DataTable GetOrdersDetailsFromDb(string CompnayID, string DriverID, string StartDate, string EndDate)
        {
            //Common common = new Common();
            List<OrderDetails> OrderObj = null;
            dbConnectionObj = new DBConnectionSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetAllOrdersDetails, "@CompanyID", CompnayID, "@DriverID", DriverID, "@StartDate", StartDate, "@EndDate", EndDate);
                OrderObj = new List<OrderDetails>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            OrderObj.Add(new OrderDetails
                            {
                                TripID = Convert.ToString(dt.Rows[i]["TripID"]),
                                TripDetailID = Convert.ToString(dt.Rows[i]["TripDetailID"]),
                                OrderNo = Convert.ToString(dt.Rows[i]["OrderNo"]),
                                ProductCode = Convert.ToString(dt.Rows[i]["ProductCode"]),

                                ItemName = Convert.ToString(dt.Rows[i]["ItemName"]),
                                OrdQty = Convert.ToString(dt.Rows[i]["OrdQty"]),
                                CasePrice = Convert.ToString(dt.Rows[i]["CasePrice"]),
                                RetailPrice = Convert.ToString(dt.Rows[i]["RetailPrice"]),
                                CompanyId = Convert.ToString(dt.Rows[i]["CompanyId"]),
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                WHID = Convert.ToString(dt.Rows[i]["WHID"]),
                                Extra = Convert.ToString(dt.Rows[i]["Extra"]),
                                Returned = Convert.ToString(dt.Rows[i]["Returned"]),
                                ProductImage = Convert.ToString(dt.Rows[i]["ProductImage"]),
                                CaseUPC = Convert.ToString(dt.Rows[i]["CaseUPC"])
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(OrderObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                OrderObj = null;
            }

            return dt;
        }

        public DataTable GetInvoiceDSDImagePDFFromDb(string CompnayID, string DriverID, string StartDate, string EndDate)
        {
            //Common common = new Common();
            List<InvoiceDSDImage> InvDSDImageObj = null;
            dbConnectionObj = new DBConnectionSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProc_GetInvoiceDSDImagePDF, "@CompanyID", CompnayID, "@DriverID", DriverID, "@StartDate", StartDate, "@EndDate", EndDate);
                InvDSDImageObj = new List<InvoiceDSDImage>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        String ImagePDFPath = ApiConstant.DVSmobileImage;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            InvDSDImageObj.Add(new InvoiceDSDImage
                            {
                                TripID = Convert.ToString(dt.Rows[i]["TripID"]),
                                DSDHeader_Id = Convert.ToString(dt.Rows[i]["DSDHeader_Id"]),
                                InvoiceNo = Convert.ToString(dt.Rows[i]["InvoiceNo"]),
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),

                                SignatureImageName = "SignatureImages/" + Convert.ToString(dt.Rows[i]["SignatureImageName"]),
                                CaptureImageName = "CaptureImages/" + Convert.ToString(dt.Rows[i]["CaptureImageName"]),
                                InvoiceFilePDF = "UploadedFiles/" + Convert.ToString(dt.Rows[i]["InvoiceFilePDF"]),
                                DSDPDFName = "UploadedFiles/"+ Convert.ToString(dt.Rows[i]["DSDPDFName"]),
                                ImagePath = ImagePDFPath

                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(InvDSDImageObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                InvDSDImageObj = null;
            }

            return dt;
        }


        public string UploadTripsData(TripsModel objTripModel)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<PayLoadTrip> tripLst = objTripModel.payLoadTrips;
                DataTable dtlTrip = new DataTable();
                dtlTrip.Columns.Add("TripID", typeof(string));
                dtlTrip.Columns.Add("TripStatus", typeof(string));
                dtlTrip.Columns.Add("ActualStartDate", typeof(string));
                dtlTrip.Columns.Add("ActualEndDate", typeof(string));

                foreach (var itms in tripLst)
                {
                    dtlTrip.Rows.Add(itms.TripID, itms.TripStatus, itms.ActualStartDate, itms.ActualEndDate);
                }

                TripsModel tripModel = new TripsModel();
                tripModel.payLoadTrips = objTripModel.payLoadTrips;

                DataSet dsTrip = new DataSet();
                dsTrip.Tables.Add(dtlTrip);
                String TripList = dsTrip.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.DVS_TripUpload, new object[] { "@CompanyID", objTripModel.CompanyID, "@XML_TripsDtl", TripList });

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UploadTripsData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UploadTripsData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }


        public string UploadCustomerData(CustomersModel objCustomerModel)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<PayLoadCustomer> customerLst = objCustomerModel.payLoadCustomers;
                DataTable dtlTrip = new DataTable();
                dtlTrip.Columns.Add("TripID", typeof(string));
                dtlTrip.Columns.Add("TripDetailID", typeof(Int64));
                dtlTrip.Columns.Add("CustomerNo", typeof(string));
                dtlTrip.Columns.Add("ActualStartTime", typeof(string));
                dtlTrip.Columns.Add("ActualEndTime", typeof(string));
                dtlTrip.Columns.Add("IsCustomerSkipped", typeof(string));
                dtlTrip.Columns.Add("SpecificCustomer", typeof(Boolean));
                dtlTrip.Columns.Add("ActualStartDate", typeof(string));
                dtlTrip.Columns.Add("ActualEndDate", typeof(string));

                foreach (var itms in customerLst)
                {
                    dtlTrip.Rows.Add(itms.TripID, itms.TripDetailID, itms.CustomerNo, itms.ActualStartTime, itms.ActualEndTime, itms.IsCustomerSkipped, itms.SpecificCustomer, itms.ActualStartDate, itms.ActualEndDate);
                }

                CustomersModel tripModel = new CustomersModel();
                tripModel.payLoadCustomers = objCustomerModel.payLoadCustomers;

                DataSet dsTrip = new DataSet();
                dsTrip.Tables.Add(dtlTrip);
                String TripList = dsTrip.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.DVS_CustomerUpload, new object[] { "@CompanyID", objCustomerModel.CompanyID, "@XML_TripsDtl", TripList });

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UploadCustomerData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UploadCustomerData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public DataTable GetInvoiceNoByOrderNoFromDb(string CompnayID, string DriverID, string StartDate, string EndDate)
        {
            //Common common = new Common();
            List<InvoiceNoByOrderNo> OrderObj = null;
            dbConnectionObj = new DBConnectionSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.GetInvoiceNoByOrderNo, "@CompanyID", CompnayID, "@DriverID", DriverID, "@StartDate", StartDate, "@EndDate", EndDate);
                OrderObj = new List<InvoiceNoByOrderNo>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            OrderObj.Add(new InvoiceNoByOrderNo
                            {
                                TripID = Convert.ToString(dt.Rows[i]["TripID"]),
                                TripDetailID = Convert.ToString(dt.Rows[i]["TripDetailID"]),
                                OrderNo = Convert.ToString(dt.Rows[i]["OrderNo"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                InvoiceNo = Convert.ToString(dt.Rows[i]["InvoiceNo"])
                            });
                        }
                    }
                }
                DVSWebApi.Repository.DashboardRepository.ListtoDataTable lsttodt = new DVSWebApi.Repository.DashboardRepository.ListtoDataTable();
                dt = lsttodt.ToDataTable(OrderObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                OrderObj = null;
            }

            return dt;
        }

        public string StopsCheckInUploadrData(StopCheckInModel objCheckInModel)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<PayLoadCheckIn> chkInLst = objCheckInModel.payLoadCheckIns;
                DataTable dtlTrip = new DataTable();
                dtlTrip.Columns.Add("TripID", typeof(string));
                dtlTrip.Columns.Add("CustomerNo", typeof(string));
                dtlTrip.Columns.Add("Latitude", typeof(string));
                dtlTrip.Columns.Add("Longitude", typeof(string));
                dtlTrip.Columns.Add("Address", typeof(string));
                dtlTrip.Columns.Add("StartingStop", typeof(string));
                dtlTrip.Columns.Add("CurrentStop", typeof(string));
                dtlTrip.Columns.Add("StopDateTime", typeof(string));
                dtlTrip.Columns.Add("Mileage", typeof(Decimal));
                dtlTrip.Columns.Add("StopImageName", typeof(string));
                dtlTrip.Columns.Add("Remarks", typeof(string));
                dtlTrip.Columns.Add("CreatedAt", typeof(string));
                dtlTrip.Columns.Add("CreatedBy", typeof(string));

                foreach (var itms in chkInLst)
                {
                    dtlTrip.Rows.Add(itms.TripID, itms.CustomerNo, itms.Latitude, itms.Longitude, itms.Address, itms.StartingStop, itms.CurrentStop, itms.StopDateTime, itms.Mileage, itms.StopImageName, itms.Remarks, itms.CreatedAt, itms.CreatedBy);
                }

                StopCheckInModel tripModel = new StopCheckInModel();
                tripModel.payLoadCheckIns = objCheckInModel.payLoadCheckIns;

                DataSet dsTrip = new DataSet();
                dsTrip.Tables.Add(dtlTrip);
                String TripList = dsTrip.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.DVS_StopsCheckInUpload, new object[] { "@CompanyID", objCheckInModel.CompanyID, "@XML_TripsDtl", TripList });

                if (Convert.ToString(dt.Rows[0]["ReturnStatus"]) == "Ok")
                {
                    foreach (var itms in chkInLst)
                    {
                        if (!String.IsNullOrEmpty(itms.StopImagePath) && !String.IsNullOrEmpty(itms.StopImageName))
                        {
                            String StopCheckInImageBinary = Convert.ToString(itms.StopImagePath);
                            String StopCheckInImageName = Convert.ToString(itms.StopImageName);
                            //log.Info("-> api/Offline/StopsCheckInUpload : Image Data: " + Convert.ToString(StopCheckInImageBinary));

                            System.Drawing.Image img;

                            String _StopCheckInPath = ApiConstant.ImageData + "\\Stops\\";
                            if (!String.IsNullOrEmpty(StopCheckInImageName) && StopCheckInImageName != "~" && !String.IsNullOrEmpty(StopCheckInImageBinary) && StopCheckInImageBinary != "~")
                            {
                                string extension = Path.GetExtension(StopCheckInImageName);
                                ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                                img = objBase64.Base64ToImage(StopCheckInImageBinary);              //if (extension == ".png" || extension == ".gif")    img = objBase64.Base64ToImage(SignImageBinary.Substring(22)); else    img = objBase64.Base64ToImage(SignImageBinary.Substring(23));
                                img.Save(_StopCheckInPath + StopCheckInImageName, System.Drawing.Imaging.ImageFormat.Png);
                            }
                        }
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "StopsCheckInUploadrData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "StopsCheckInUploadrData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public string OrderDetailsUploadData(OrderDetailsModel objOrderDetModel)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<OrderDetailsUpload> orderDetLst = objOrderDetModel.orderDetailsUploads;
                DataTable dtlOrdDet = new DataTable();
                dtlOrdDet.Columns.Add("TripID", typeof(string));
                dtlOrdDet.Columns.Add("OrderNo", typeof(string));
                dtlOrdDet.Columns.Add("CustomerID", typeof(string));
                dtlOrdDet.Columns.Add("ProductCode", typeof(string));
                dtlOrdDet.Columns.Add("OrdQty", typeof(Int16));
                dtlOrdDet.Columns.Add("CasePrice", typeof(Decimal));
                dtlOrdDet.Columns.Add("TotalPrice", typeof(Decimal));
                dtlOrdDet.Columns.Add("TotalQty", typeof(Int16));
                dtlOrdDet.Columns.Add("Reason", typeof(string));
                dtlOrdDet.Columns.Add("Comments", typeof(string));
                dtlOrdDet.Columns.Add("ExtraQty", typeof(Int16));
                dtlOrdDet.Columns.Add("ReturnQty", typeof(Int16));
                dtlOrdDet.Columns.Add("ReturnDamageImage", typeof(string));

                foreach (var itms in orderDetLst)
                {
                    dtlOrdDet.Rows.Add(itms.TripID, itms.OrderNo, itms.CustomerID, itms.ProductCode, itms.OrdQty, itms.CasePrice, itms.TotalPrice, itms.TotalQty, itms.Reason, itms.Comments, itms.Extra, itms.Return, itms.ReturnDamageImage);
                }

                OrderDetailsModel OrdDetModel = new OrderDetailsModel();
                OrdDetModel.orderDetailsUploads = objOrderDetModel.orderDetailsUploads;

                DataSet dsOrdDet = new DataSet();
                dsOrdDet.Tables.Add(dtlOrdDet);
                String OrdDetList = dsOrdDet.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProc_DVS_OrderDetailsUpload, new object[] { "@CompanyID", objOrderDetModel.CompanyID, "@XML_OrderDtl", OrdDetList });

                if(dt.Rows.Count > 0)
                {
                    String ImagePath = ApiConstant.ImageData;
                    foreach (var itms in orderDetLst)
                    {
                        String ReturnDamageImageBinary = Convert.ToString(itms.ReturnDamageImage);
                        String ReturnDamageImageName = Convert.ToString(itms.ReturnDamageImage);
                        log.Info("-> api/Offline/OrderDetailsUpload : Image Name: " + Convert.ToString(ReturnDamageImageName));

                        System.Drawing.Image img;

                        String _SignPath = ImagePath + "\\ReturnDamageImages\\";
                        if (!String.IsNullOrEmpty(ReturnDamageImageName) && ReturnDamageImageName != "~" && !String.IsNullOrEmpty(ReturnDamageImageBinary) && ReturnDamageImageBinary != "~")
                        {
                            string extension = Path.GetExtension(ReturnDamageImageName);
                            ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                            img = objBase64.Base64ToImage(ReturnDamageImageBinary);              
                            img.Save(_SignPath + ReturnDamageImageName, System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "OrderDetailsUploadData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "OrderDetailsUploadData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }


        public string InvoiceDSDUploadData(InvoicDSDModel objInvoiceModel)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<InvoicDSDUpload> invoiceLst = objInvoiceModel.invoicDSDUploads;
                DataTable dtInvoice = new DataTable();
                dtInvoice.Columns.Add("InvoiceNo", typeof(string));
                dtInvoice.Columns.Add("TripID", typeof(string));
                dtInvoice.Columns.Add("CustomerID", typeof(string));
                dtInvoice.Columns.Add("DriverID", typeof(string));
                dtInvoice.Columns.Add("DSDDate", typeof(DateTime));
                dtInvoice.Columns.Add("SignatureImageName", typeof(string));
                dtInvoice.Columns.Add("CaptureImageName", typeof(string));
                dtInvoice.Columns.Add("WhoSigned", typeof(string));
                dtInvoice.Columns.Add("CleanInvoicePdfName", typeof(string));
                dtInvoice.Columns.Add("DSDPdfName", typeof(string));

                foreach (var itms in invoiceLst)
                {
                    dtInvoice.Rows.Add(itms.InvoiceNo, itms.TripID, itms.CustomerID, itms.DriverID, itms.DSDDate, itms.SignatureImageName, itms.CaptureImageName, itms.WhoSigned, itms.CleanInvoicePdfName, itms.DSDPdfName);
                }

                InvoicDSDModel InvoiceModel = new InvoicDSDModel();
                InvoiceModel.invoicDSDUploads = objInvoiceModel.invoicDSDUploads;

                DataSet dsInvoice = new DataSet();
                dsInvoice.Tables.Add(dtInvoice);
                String InvoiceList = dsInvoice.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProc_DVS_InvoiceDSDUpload, new object[] { "@CompanyID", objInvoiceModel.CompanyID, "@XML_InvoiceDtl", InvoiceList });

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InvoiceDSDUploadData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InvoiceDSDUploadData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public string DSDPhotosUploadData(DSDImageModel objInvoiceModel)
        {
            DataTable dt = new DataTable();
            DataTable dtDSD = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<DSDImage> DSDLst = objInvoiceModel.DSDImages;
                
                dtDSD.Columns.Add("SignatureImageBinaryData", typeof(string));
                dtDSD.Columns.Add("SignatureImageName", typeof(string));
                dtDSD.Columns.Add("CaptureImageBinaryData", typeof(string));
                dtDSD.Columns.Add("CaptureImageName", typeof(string));

                foreach (var itms in DSDLst)
                {
                    dtDSD.Rows.Add(itms.SignatureImageBinaryData, itms.SignatureImageName, itms.CaptureImageBinaryData, itms.CaptureImageName);
                }

                String ImagePath = ApiConstant.ImageData;                        //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";
                for (int i = 0; i < dtDSD.Rows.Count; i++)
                {
                    String SignImageBinary = Convert.ToString(dtDSD.Rows[i]["SignatureImageBinaryData"]);
                    String SignatureImageName = Convert.ToString(dtDSD.Rows[i]["SignatureImageName"]);
                    log.Info("-> api/Offline/DSDPhotosUpload : Image Data: " + Convert.ToString(SignImageBinary));

                    System.Drawing.Image img;

                    String _SignPath = ImagePath + "\\SignatureImages\\";
                    if (!String.IsNullOrEmpty(SignatureImageName) && SignatureImageName != "~" && !String.IsNullOrEmpty(SignImageBinary) && SignImageBinary != "~")
                    {
                        string extension = Path.GetExtension(SignatureImageName);
                        ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                        img = objBase64.Base64ToImage(SignImageBinary);              //if (extension == ".png" || extension == ".gif")    img = objBase64.Base64ToImage(SignImageBinary.Substring(22)); else    img = objBase64.Base64ToImage(SignImageBinary.Substring(23));
                        img.Save(_SignPath + SignatureImageName, System.Drawing.Imaging.ImageFormat.Png);
                    }

                    String CaptureImageBinary = Convert.ToString(dtDSD.Rows[i]["CaptureImageBinaryData"]);
                    String CaptureImageName = Convert.ToString(dtDSD.Rows[i]["CaptureImageName"]);

                    System.Drawing.Image img2;
                    String _CapturePath = ImagePath + "\\CaptureImages\\";
                    if (!String.IsNullOrEmpty(CaptureImageBinary) && CaptureImageBinary != "~" && !String.IsNullOrEmpty(CaptureImageName) && CaptureImageName != "~")
                    {
                        string extension = Path.GetExtension(CaptureImageName);
                        ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                        img2 = objBase64.Base64ToImage(CaptureImageBinary);                        //if (extension == ".png" || extension == ".gif")    img = Base64ToImage(objImg.ImageBinaryData.Substring(22)); else    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));
                        img2.Save(_CapturePath + CaptureImageName, System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
                dt.Columns.Add("ReturnStatus", typeof(string));
                dt.Rows.Add("Ok");
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DSDPhotosUploadData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DSDPhotosUploadData", false);
            }
            finally
            {
                dt = null;
                dtDSD = null;
            }
            return null;
        }

        public string DSDMultiplePhotosUploadData(DSDMultipleImageModel objInvoiceMultipleImgsModel)
        {
            DataTable dt = new DataTable();
            DataTable dtDSD = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<DSDMultipleImage> DSDLst = objInvoiceMultipleImgsModel.DSDMultipleImages;

                dtDSD.Columns.Add("CaptureImageBinaryData", typeof(string));
                dtDSD.Columns.Add("CaptureImageName", typeof(string));

                foreach (var itms in DSDLst)
                {
                    dtDSD.Rows.Add( itms.CaptureImageBinaryData, itms.CaptureImageName);
                }

                String ImagePath = ApiConstant.ImageData;                        //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";
                for (int i = 0; i < dtDSD.Rows.Count; i++)
                {
                    System.Drawing.Image img;

                    String CaptureImageBinary = Convert.ToString(dtDSD.Rows[i]["CaptureImageBinaryData"]);
                    String CaptureImageName = Convert.ToString(dtDSD.Rows[i]["CaptureImageName"]);

                    System.Drawing.Image img2;
                    String _CapturePath = ImagePath + "\\CaptureImages\\";
                    if (!String.IsNullOrEmpty(CaptureImageBinary) && CaptureImageBinary != "~" && !String.IsNullOrEmpty(CaptureImageName) && CaptureImageName != "~")
                    {
                        string extension = Path.GetExtension(CaptureImageName);
                        ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                        img2 = objBase64.Base64ToImage(CaptureImageBinary);                        //if (extension == ".png" || extension == ".gif")    img = Base64ToImage(objImg.ImageBinaryData.Substring(22)); else    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));
                        img2.Save(_CapturePath + CaptureImageName, System.Drawing.Imaging.ImageFormat.Png);
                    }
                }

                IEnumerable<DSDMultipleImage> invoiceLst = objInvoiceMultipleImgsModel.DSDMultipleImages;
                DataTable dtInvoice = new DataTable();
                dtInvoice.Columns.Add("InvoiceNo", typeof(string));
                dtInvoice.Columns.Add("TripID", typeof(string));
                dtInvoice.Columns.Add("CustomerID", typeof(string));
                dtInvoice.Columns.Add("DriverID", typeof(string));
                dtInvoice.Columns.Add("DSDDate", typeof(DateTime));
                dtInvoice.Columns.Add("SignatureImageName", typeof(string));
                dtInvoice.Columns.Add("CaptureImageName", typeof(string));
                dtInvoice.Columns.Add("WhoSigned", typeof(string));
                dtInvoice.Columns.Add("CleanInvoicePdfName", typeof(string));
                dtInvoice.Columns.Add("DSDPdfName", typeof(string));

                foreach (var itms in invoiceLst)
                {
                    dtInvoice.Rows.Add(objInvoiceMultipleImgsModel.InvoiceNo, objInvoiceMultipleImgsModel.TripID, objInvoiceMultipleImgsModel.CustomerID, objInvoiceMultipleImgsModel.DriverID, objInvoiceMultipleImgsModel.DSDDate, 
                        "", itms.CaptureImageName, "", "", "");
                }

                //InvoicDSDModel InvoiceModel = new InvoicDSDModel();
                //InvoiceModel.invoicDSDUploads = objInvoiceModel.invoicDSDUploads;

                DataSet dsInvoice = new DataSet();
                dsInvoice.Tables.Add(dtInvoice);
                String InvoiceList = dsInvoice.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProc_DVS_InvoiceDSDUpload, new object[] { "@CompanyID", objInvoiceMultipleImgsModel.CompanyID, "@XML_InvoiceDtl", InvoiceList });

                //dt.Columns.Add("InsertedStatus", typeof(string));
                //dt.Rows.Add("Ok");
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DSDMultiplePhotosUploadData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DSDMultiplePhotosUploadData", false);
            }
            finally
            {
                dt = null;
                dtDSD = null;
            }
            return null;
        }

        public string CustomerStatusUploadData(CustomerStatusModel objCustStatModel)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<CustomerStatusUpload> custStatLst = objCustStatModel.CustomerStatusUploads;
                DataTable dtCustStat = new DataTable();
                dtCustStat.Columns.Add("TripID", typeof(string));
                dtCustStat.Columns.Add("CustomerID", typeof(string));
                dtCustStat.Columns.Add("CustomerStatus", typeof(string));

                foreach (var itms in custStatLst)
                {
                    dtCustStat.Rows.Add(itms.TripID, itms.CustomerID, itms.CustomerStatus);
                }

                CustomerStatusModel CustStatModel = new CustomerStatusModel();
                CustStatModel.CustomerStatusUploads = objCustStatModel.CustomerStatusUploads;

                DataSet dsCustStat = new DataSet();
                dsCustStat.Tables.Add(dtCustStat);
                String dsCustStatList = dsCustStat.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProc_DVS_CustomerStatusUpload, new object[] { "@CompanyID", objCustStatModel.CompanyID, "@XML_CustomerStatusDtl", dsCustStatList });

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "CustomerStatusUploadData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "CustomerStatusUploadData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public string LineChartData()
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                dt.Columns.Add("associateId", typeof(string));
                dt.Columns.Add("Month", typeof(string));
                dt.Columns.Add("noOfDefects", typeof(Int16));
                dt.Rows.Add("John",  "January", 17);
                dt.Rows.Add("Max", "February", 6);
                dt.Rows.Add("Terry", "March", 11);

                jsonString = dtToJson.convertDataTableToJson(dt, "CustomerStatusUploadData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "CustomerStatusUploadData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }



    }
}