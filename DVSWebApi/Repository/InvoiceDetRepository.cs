﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DVSWebApi.Repository
{
    public class InvoiceDetRepository
    {
        DBConnectionSB3 dbConnectionObj = null;
        DataTable dtl;
        DBConnection dbConnection = null;
        DataToJson dtToJson = new DataToJson();
        SqlCommand SqlCmd = null;
        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string SaveInvoiceData(InvoiceModels objInv)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString = String.Empty;
            int OrdQty = 0, InvoiceTotalQty = 0;
            String TripID = String.Empty, Invoice_Number = String.Empty, CompID = String.Empty, CustID = String.Empty, DueDate = String.Empty, Dept = string.Empty, OrderNo = String.Empty,  DelvryDt = string.Empty, InvoiceDate = string.Empty, DriverID = String.Empty;
            decimal SubTotalAmount = 0, ShippingCharge = 0, Tax = 0, Discount = 0, GrandTotalAmount = 0;

            try
            {
                Invoice_Number = objInv.InvoiceNo;
                OrderNo = objInv.OrderNumber;
                TripID = objInv.TripID;
                CompID = objInv.CompanyID;
                CustID = objInv.CustomerID;
                InvoiceDate = objInv.InvoiceDate;
                DueDate = objInv.DueDate;
                DriverID = objInv.DriverID;
                Dept = objInv.Dept;

                InvoiceTotalQty = objInv.InvoiceTotalQty;
                SubTotalAmount = objInv.SubTotalAmount;
                ShippingCharge = objInv.ShippingCharge;
                Tax = objInv.Tax;
                Discount = objInv.Discount;
                GrandTotalAmount = objInv.GrandTotalAmount;

                IEnumerable<InvoiceLoadItem> invlItemLst = objInv.invoiceLoadItems;
                DataTable dtlInv = new DataTable();
                dtlInv.Columns.Add("ItemCode", typeof(string));
                dtlInv.Columns.Add("ItemName", typeof(string));
                dtlInv.Columns.Add("Qty", typeof(Int32));
                dtlInv.Columns.Add("DespQty", typeof(Int32));
                dtlInv.Columns.Add("DelQty", typeof(Int32));
                dtlInv.Columns.Add("ItemPrice", typeof(decimal));
                dtlInv.Columns.Add("ItemTotalPrice", typeof(decimal));

                foreach (var itms in invlItemLst)
                {
                    dtlInv.Rows.Add(itms.ItemCode, itms.ItemName, itms.Qty, itms.DespQty, itms.DelQty, itms.ItemPrice, itms.ItemTotalPrice);
                    //if (String.IsNullOrWhiteSpace(TripID))
                    //{
                    //}
                }

                InvoiceModels invModel = new InvoiceModels();
                invModel.invoiceLoadItems = objInv.invoiceLoadItems;

                DataSet dsInv = new DataSet();
                dsInv.Tables.Add(dtlInv);
                String invoiceItms = dsInv.GetXml();
                dt = new DataTable();
                dbConnection = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcSaveInvoice, dbConnection.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", Invoice_Number);
                SqlCmd.Parameters.AddWithValue("@OrderNumber", OrderNo);
                SqlCmd.Parameters.AddWithValue("@TripID", TripID);
                SqlCmd.Parameters.AddWithValue("@CustomerID", CustID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);

                SqlCmd.Parameters.AddWithValue("@InvoiceDate", InvoiceDate);
                SqlCmd.Parameters.AddWithValue("@DueDate", DueDate);
                SqlCmd.Parameters.AddWithValue("@DriverID", DriverID);
                SqlCmd.Parameters.AddWithValue("@Dept", Dept);
                SqlCmd.Parameters.AddWithValue("@InvoiceTotalQty", InvoiceTotalQty);
                SqlCmd.Parameters.AddWithValue("@SubTotalAmount", SubTotalAmount);
                SqlCmd.Parameters.AddWithValue("@ShippingCharge", ShippingCharge);
                SqlCmd.Parameters.AddWithValue("@Tax", Tax);
                SqlCmd.Parameters.AddWithValue("@Discount", Discount);
                SqlCmd.Parameters.AddWithValue("@GrandTotalAmount", GrandTotalAmount);

                SqlCmd.Parameters.AddWithValue("@XML_InvoiceDtl", invoiceItms);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnection.ConnectionOpen();
                da.Fill(dt);
                dbConnection.ConnectionClose();

                //dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcInsertDelivery, "@TripID", TripID, "@CompanyID", CompID, "@OrderNo", OrderNo, "@Invoice_Number", Invoice_Number,
                //    "@OrderQty", OrdQty, "@OrderAmount", OrderAmount, "@Latitude", Lat, "@Longitude", Lng, "@Location", Location, "@DeliveredBy", DeliveredBy, "@XML_OrderDtl", DelvryItms);
                try
                {
                    if (dt != null)
                    {
                        String InvoiceNum = Convert.ToString(dt.Rows[0]["InvoiceNo"]);
                        if (!string.IsNullOrEmpty(InvoiceNum))
                        {
                            Zen.Barcode.Code128BarcodeDraw brCode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;                                             //https://www.youtube.com/watch?v=dojOKLaZSDw
                            System.Drawing.Image img = brCode.Draw(InvoiceNum, 60);
                            String ImgPath = ApiConstant.Barcode_imagePath;
                            img.Save(ImgPath + InvoiceNum + ".png", System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }
                catch(Exception ex){}
                jsonString = dtToJson.convertDataTableToJson(dt, "GenerateInvoiceData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GenerateInvoiceData", false);
            }
            finally
            {
                dbConnection = null;
                SqlCmd = null;
                dt.Dispose();
                da = null;
            }
            return null;
        }

        public String InvoiceGenerationData(String OrderNo, String InvoiceNo, String CustomerID, String TripID, String DriverID, String CompnayID, String StopLocation, String Latitude, String Longitude) // fetch Invoice and Trip  View respective Invoice No
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcInvoiceGeneration, "@OrderNo", OrderNo, "@InvoiceNo", InvoiceNo, "@CustomerID", CustomerID, "@TripID", TripID, "@DriverID", DriverID, "@CompanyID", CompnayID, "@StopLocation", @StopLocation, "@Latitude", Latitude, "@Longitude", Longitude);
                // Merging Path with the field
                try
                {
                    if (dtl != null)
                    {
                        if (!string.IsNullOrEmpty(InvoiceNo))
                        {
                            Zen.Barcode.Code128BarcodeDraw brCode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;                                             //https://www.youtube.com/watch?v=dojOKLaZSDw
                            System.Drawing.Image img = brCode.Draw(InvoiceNo, 60);
                            String ImgPath = ApiConstant.Barcode_imagePath;
                            img.Save(ImgPath + InvoiceNo + ".png", System.Drawing.Imaging.ImageFormat.Png);
                        }
                        
                    }
                }
                catch (Exception ex) { }
                jsonString = dtToJson.convertDataTableToJson(dtl, "InvoiceGenerationData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "InvoiceGenerationData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//InvoiceGenerationData

        public String GetDeliveryViewByOrderNo(String InvoiceNo, String CompnayID) // fetch Invoice and Trip  View respective Invoice No
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                // Call Image Path API and merge the path with this API
                String DVSbarcodeImage = ApiConstant.DVSmobileImage + "BarcodeImages/";
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetInvoiceHeaderByNo, "@OrderNumber", InvoiceNo, "@CompanyID", CompnayID, "@BarcodeImage", DVSbarcodeImage);
                // Merging Path with the field

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetInvoiceData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetInvoiceData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetInvoiceData


        public String ViewSavedInvoiceData(String TripID, String OrderNo, String CustomerID, String CompnayID) // fetch Invoice and Trip  View respective Invoice No
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                // Call Image Path API and merge the path with this API
                String DVSbarcodeImage = ApiConstant.DVSmobileImage + "BarcodeImages/";
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcViewSavedInvoice, "@TripID", TripID, "@InvoiceNo", OrderNo, "@CustomerID", CustomerID, "@CompanyID", CompnayID, "@BarcodeImage", DVSbarcodeImage);
                // Merging Path with the field

                jsonString = dtToJson.convertDataTableToJson(dtl, "ViewSavedInvoiceData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "ViewSavedInvoiceData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//ViewInvoiceDetailsByNo
        public String ViewInvoiceDetailsByNo(String TripID, String OrderNo, String CompnayID) // fetch Invoice and Trip  View respective Invoice No
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                // Call Image Path API and merge the path with this API
                String DVSbarcodeImage = ApiConstant.DVSmobileImage + "BarcodeImages/";
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetInvoiceDetail_ByDVSInvoiceNo, "@TripID", TripID, "@InvoiceNo", OrderNo, "@CompanyID", CompnayID, "@BarcodeImage", DVSbarcodeImage);
                // Merging Path with the field

                jsonString = dtToJson.convertDataTableToJson(dtl, "ViewInvoiceDetailsByNo", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "ViewInvoiceDetailsByNo", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//ViewInvoiceDetailsByNo

        public String GetInvoiceDetailsByInvoiceNo(String OrderNo, String CompnayID) // fetch Invoice and Trip  View respective Invoice No
        {
            dbConnectionObj = new DBConnectionSB3();
            String jsonString = string.Empty;
            int PayDueDatePeriod = 0;
            DataTable dashDtl = null;
            try
            {
                DashboardRepository dashbrdRepositoryObj = new DashboardRepository();
                dashDtl = new DataTable();
                dashDtl = dashbrdRepositoryObj.GetCompanyConfigParamsData(CompnayID);
                if(dashDtl != null)
                {
                    if(dashDtl.Rows.Count > 0)
                    {
                        PayDueDatePeriod = Convert.ToInt16(dashDtl.Rows[0]["PaymentDueDatePeriod"]);
                    }
                }
                // Call Image Path API and merge the path with this API
                String DVSbarcodeImage = ApiConstant.DVSmobileImage + "BarcodeImages/";
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetInvoiceHeaderByNo, "@OrderNumber", OrderNo, "@CompanyID", CompnayID, "@BarcodeImage", DVSbarcodeImage, "@PayDueDatePeriod", PayDueDatePeriod);
                // Merging Path with the field

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetInvoiceDetailsByInvoiceNo", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetInvoiceDetailsByInvoiceNo", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetInvoiceDetailsByInvoiceNo

        public string InsertDSDGenerate(GenerateDSD objGenerateDSD)
        {
            SqlDataAdapter da = null; 
            DataTable dt = null;
            String jsonString = String.Empty;

            try
            {
                dbConnection = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertDSD, dbConnection.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", objGenerateDSD.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@TripID", objGenerateDSD.TripID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", objGenerateDSD.CompanyID);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objGenerateDSD.CustomerID);
                SqlCmd.Parameters.AddWithValue("@DriverID", objGenerateDSD.DriverID);
                SqlCmd.Parameters.AddWithValue("@DSDDate", objGenerateDSD.DSDDate);
                SqlCmd.Parameters.AddWithValue("@SignatureImageName", objGenerateDSD.SignatureImageName);
                SqlCmd.Parameters.AddWithValue("@CaptureImageName", objGenerateDSD.CaptureImageName);
                SqlCmd.Parameters.AddWithValue("@WhoSigned", objGenerateDSD.WhoSigned);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnection.ConnectionOpen();
                da.Fill(dt);
                dbConnection.ConnectionClose();

                try
                {
                    if (Convert.ToString(dt.Rows[0]["InsertedStatus"]) == "Ok")
                    {
                        String ModelData = "Name: " + objGenerateDSD.CustomerID + ", Signature Image Name: " + objGenerateDSD.SignatureImageName + ", TripID: " + objGenerateDSD.TripID;
                        log.Info("-> api/InvoiceDet/GenerateDSD : Image Data: " + Convert.ToString(ModelData));

                        System.Drawing.Image img;
                        String path = ApiConstant.StopCheckIn_imagePath;                        //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";
                        String ImgPath = path + "\\SignatureImages\\";
                        if (!String.IsNullOrEmpty(objGenerateDSD.SignatureImageName) && !string.IsNullOrEmpty(objGenerateDSD.SignatureImageBinaryData) && objGenerateDSD.SignatureImageName != "~" && objGenerateDSD.SignatureImageBinaryData != "~")
                        {
                            string extension = Path.GetExtension(ImgPath + objGenerateDSD.SignatureImageName);
                            ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                            img = objBase64.Base64ToImage(objGenerateDSD.SignatureImageBinaryData);                        //if (extension == ".png" || extension == ".gif")    img = Base64ToImage(objImg.ImageBinaryData.Substring(22)); else    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));
                            img.Save(ImgPath + objGenerateDSD.SignatureImageName, System.Drawing.Imaging.ImageFormat.Png);
                        }

                        String CapturePath = path + "\\CaptureImages\\";
                        if (!String.IsNullOrEmpty(objGenerateDSD.CaptureImageName) && !string.IsNullOrEmpty(objGenerateDSD.CaptureImageBinaryData) && objGenerateDSD.CaptureImageName != "~" && objGenerateDSD.CaptureImageBinaryData != "~")
                        {
                            string extension = Path.GetExtension(CapturePath + objGenerateDSD.CaptureImageName);
                            ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                            img = objBase64.Base64ToImage(objGenerateDSD.CaptureImageBinaryData);                        //if (extension == ".png" || extension == ".gif")    img = Base64ToImage(objImg.ImageBinaryData.Substring(22)); else    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));
                            img.Save(CapturePath + objGenerateDSD.CaptureImageName, System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Method:InsertStopCheckIn" + ex.Message.ToString());
                }

                jsonString = dtToJson.convertDataTableToJson(dt, "InsertDSDGenerateData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertDSDGenerateData", false);
            }
            finally
            {
                dbConnection = null;
                SqlCmd = null;
                dt.Dispose();
                da = null;
            }
            return null;
        }

        public string AddSignatureData(AddSignature objAddSign)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString = String.Empty;

            try
            {
                dbConnection = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertDSD, dbConnection.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", objAddSign.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@TripID", objAddSign.TripID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", objAddSign.CompanyID);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objAddSign.CustomerID);
                SqlCmd.Parameters.AddWithValue("@DriverID", objAddSign.DriverID);

                SqlCmd.Parameters.AddWithValue("@SignedForTheDelivery", objAddSign.SignedForTheDelivery);
                SqlCmd.Parameters.AddWithValue("@SignatureImageName", objAddSign.SignatureImageName);
                SqlCmd.Parameters.AddWithValue("@SignatureImageBinaryData", objAddSign.SignatureImageBinaryData);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnection.ConnectionOpen();
                da.Fill(dt);
                dbConnection.ConnectionClose();

                try
                {
                    if (Convert.ToString(dtl.Rows[0]["InsertedStatus"]) == "Ok")
                    {
                        String ModelData = "Name: " + objAddSign.CustomerID + ", Signature Image Name: " + objAddSign.SignatureImageName + ", TripID: " + objAddSign.TripID;
                        log.Info("-> api/InvoiceDet/GenerateDSD : Image Data: " + Convert.ToString(ModelData));

                        System.Drawing.Image img;
                        String path = ApiConstant.StopCheckIn_imagePath;                        //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";
                        String ImgPath = path + "\\SignatureImages\\";
                        if (!String.IsNullOrEmpty(objAddSign.SignatureImageName) && !string.IsNullOrEmpty(objAddSign.SignatureImageBinaryData) && objAddSign.SignatureImageName != "~" && objAddSign.SignatureImageBinaryData != "~")
                        {
                            string extension = Path.GetExtension(ImgPath + objAddSign.SignatureImageName);
                            ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                            img = objBase64.Base64ToImage(objAddSign.SignatureImageBinaryData);                        //if (extension == ".png" || extension == ".gif")    img = Base64ToImage(objImg.ImageBinaryData.Substring(22)); else    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));
                            img.Save(ImgPath + objAddSign.SignatureImageName, System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Method:AddSignatureData" + ex.Message.ToString());
                }

                jsonString = dtToJson.convertDataTableToJson(dt, "AddSignatureData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "AddSignatureData", false);
            }
            finally
            {
                dbConnection = null;
                SqlCmd = null;
                dt.Dispose();
                da = null;
            }
            return null;
        }


        public String GetInvoiceListByDriverCompany(String CompnayID, String DriverID) // fetch Invoice and Trip  View respective Invoice No
        {
            dbConnectionObj = new DBConnectionSB3();
            String jsonString;
            try
            {
                // Call Image Path API and merge the path with this API
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetInvoiceByDriverCompany, "@CompanyID", CompnayID, "@DriverID", DriverID);
                // Merging Path with the field

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetInvoiceListByDriverCompany", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetInvoiceListByDriverCompany", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetInvoiceListByDriverCompany

        public String GetPaymentTermListData(String CompnayID) // fetch Invoice and Trip  View respective Invoice No
        {
            dbConnectionObj = new DBConnectionSB3();
            String jsonString;
            try
            {
                // Call Image Path API and merge the path with this API
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetPaymentTermList, "@CompanyID", CompnayID);
                // Merging Path with the field

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetPaymentTermListData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetPaymentTermListData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetPaymentTermListData

    }
}