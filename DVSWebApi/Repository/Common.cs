﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DVSWebApi.Models;

namespace DVSWebApi.Repository
{
    public class Common
    {
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public List<LookupMappingTable> GetLookupMappingTableList(string pName)
        {
            List<LookupMappingTable> listObj = null;
            try
            {
                listObj = new List<LookupMappingTable>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetLookupMappingTableList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@pName", pName);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                listObj = (from DataRow dr in dt.Rows

                           select new LookupMappingTable()
                           {
                               CodeId = Convert.ToInt32(dr["CodeId"]),
                               Value1 = Convert.ToString(dr["Value"])
                           }).ToList();

                return listObj;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                listObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return listObj;
        }


        public List<LookupMappingTable> GetGeneralLookupList(string selectedValue, string tblName)
        {
            List<LookupMappingTable> listObj = null;
            try
            {
                listObj = new List<LookupMappingTable>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetGeneralLookupList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@tblName", tblName);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                listObj = (from DataRow dr in dt.Rows

                           select new LookupMappingTable()
                           {
                               CodeId = Convert.ToInt32(dr["ID"]),
                               Value1 = Convert.ToString(dr["VALUE"]),
                               SelectedValue = selectedValue
                           }).ToList();

                return listObj;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                listObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return listObj;
        }

        
    }
}