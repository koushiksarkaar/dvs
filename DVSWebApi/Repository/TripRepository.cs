﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Configuration;
using DVSWebApi.Models;

namespace DVSWebApi.Repository
{
    public class TripRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnectionSB3 dbConnectionObj = null;
        DBConnectionOAuth dbOAuthConnectionObj = null;
        SqlCommand OAuthcmd = null;
        SqlCommand SqlCmd = null;
        DataTable dtl;
        DataToJson dtToJson = new DataToJson();

        public String GetDriverTripInformation(String UserName, String CompanyID) // fetch todayTripInformation respective drivers 
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcDriverTripInformation, "@UserName", UserName, "@CompanyID", CompanyID);
                jsonString = dtToJson.convertDataTableToJson(dtl, "getDriverTripInfo", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "getDriverTripInfo", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//

        public String GetRouteStopDetails(String TripRouteID, String UserName) //  Fetch List of Trip Route Stops or warehouse details
        {
            //List<RouteStopDetails> ListObj = new List<RouteStopDetails>();
            dbConnectionObj = new DBConnectionSB3();
            string jsonString = String.Empty;
            try
            {
                SqlDataAdapter da = null;
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcRouteSpotDetail, "@TripRouteID", TripRouteID, "@UserName", UserName, "@Flag", 0);

                jsonString = dtToJson.convertDataTableToJson(dtl, "getTripRouteStopDetails", true);

                //ListObj = (from DataRow dr in dt.Rows

                //           select new RouteStopDetails()
                //           {
                //               StopID = dr["StopID"].ToString(),
                //               SequenceNO = dr["SequenceNO"].ToString(),
                //               Location = dr["Location"].ToString()                              
                //           }).ToList();
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "getTripRouteStopDetails", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
            //  return ListObj;

            return jsonString;
        }//GetRouteStopDetails


        public String GetTripStopSummeryInfo(String TripID, String CompanyID, string ActualStartTime) // fetch Trip Map View respective Trip ID
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetTripSummery, "@CompanyID", CompanyID, "@TripID", TripID, "@ActualStartTime", ActualStartTime);
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetTripStopSummeryInfo", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetTripStopSummeryInfo", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetTripStopsInfo

        public String SkipCustomerOfTripSummeryInfo(String TripID, String CompanyID, String CustomerID, String IsCustomerSkipped) // fetch Trip Map View respective Trip ID
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {       //IsCustomerSkipped = 0 means  Red  , 1 = Green ,  2 = Skipped
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcDVSUpdateSkippedTripSummery, "@CustomerID", CustomerID, "@TripID", TripID, "@CompanyID", CompanyID, "@Action", IsCustomerSkipped);
                jsonString = dtToJson.convertDataTableToJson(dtl, "SkipCustomerOfTripSummeryInfo", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "SkipCustomerOfTripSummeryInfo", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetTripStopsInfo

        public String GetTripMapView(String TripID, String CompnayID) // fetch Trip Map View respective Trip ID
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetTripMapView, "@CompanyID", CompnayID, "@TripID", TripID);
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetTripMapView", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetTripMapView", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetTripMapView


        public String GetTripListOrderCountData(String CompanyID, String DriverID, String StartDate, String EndDate) //  Fetch List of Trip Count
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString = String.Empty;
            try
            {
                SqlDataAdapter da = null;
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetTripListOrderCount, "@CompanyID", CompanyID, "@DriverID", DriverID, "@StartDate", StartDate, "@EndDate", EndDate);

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetTripListOrderCountData", true);
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetTripListOrderCountData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
            //  return ListObj;

            return jsonString;
        }//GetTripListOrderCountData


    }
}