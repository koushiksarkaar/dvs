﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Data.SqlClient;

namespace DVSWebApi.Repository
{
    public class DeliveryRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnectionSB3 dbConnectionObj = null;
        DataTable dtl;
        DBConnection dbConnection = null;
        DataToJson dtToJson = new DataToJson();
        SqlCommand SqlCmd = null;

        public string InsertDeliveryData(DeliveryModels objDeliveryItms)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString = String.Empty;
            Int16 OrdQty=0;
            String TripID = String.Empty, Invoice_Number = String.Empty, CompID = String.Empty, Location = String.Empty, OrderNo = String.Empty, DeliveredBy = string.Empty, DeliveryDate = string.Empty, Remarks = string.Empty;
            decimal Lat = 0, Lng = 0, OrderAmount = 0;
            
            try
            {
                IEnumerable<DeliveryLoadItem> delItemLst = objDeliveryItms.deliveryLoadItems;
                DataTable dtlDelvry = new DataTable();
                dtlDelvry.Columns.Add("ItemCode", typeof(string));
                dtlDelvry.Columns.Add("ItemName", typeof(string));
                dtlDelvry.Columns.Add("Qty", typeof(Int32));
                dtlDelvry.Columns.Add("DelQty", typeof(Int32));
                dtlDelvry.Columns.Add("ItemPrice", typeof(decimal));
                dtlDelvry.Columns.Add("Remarks", typeof(string));
                dtlDelvry.Columns.Add("CancelReason", typeof(string));

                foreach (var itms in delItemLst)
                {
                    dtlDelvry.Rows.Add(itms.ItemCode, itms.ItemName, itms.Qty, itms.DelQty, itms.ItemPrice, itms.Remarks, itms.CancelReason);
                    if (String.IsNullOrWhiteSpace(TripID))
                    {
                        TripID = itms.TripID;
                        CompID = itms.CompanyID;
                        OrderNo = itms.OrderNo;
                        Invoice_Number = itms.Invoice_Number;
                        OrdQty = itms.OrderQty;
                        OrderAmount = itms.OrderAmount;
                        Lat = itms.Latitude;
                        Lng = itms.Longitude;
                        Location = itms.Location;
                        DeliveredBy = itms.DeliveredBy;
                        DeliveryDate = itms.DeliveryDate;
                    }
                }

                DeliveryModels delvryModel = new DeliveryModels();
                delvryModel.deliveryLoadItems = objDeliveryItms.deliveryLoadItems;

                DataSet dsDelvry = new DataSet();
                dsDelvry.Tables.Add(dtlDelvry);
                String DelvryItms = dsDelvry.GetXml();
                dt = new DataTable();
                dbConnection = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertDelivery, dbConnection.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;
                SqlCmd.Parameters.AddWithValue("@TripID", TripID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                SqlCmd.Parameters.AddWithValue("@OrderNo", OrderNo);
                SqlCmd.Parameters.AddWithValue("@Invoice_Number", Invoice_Number);
                SqlCmd.Parameters.AddWithValue("@OrderQty", OrdQty);
                SqlCmd.Parameters.AddWithValue("@OrderAmount", OrderAmount);
                SqlCmd.Parameters.AddWithValue("@Latitude", Lat);
                SqlCmd.Parameters.AddWithValue("@Longitude", Lng);
                SqlCmd.Parameters.AddWithValue("@Location", Location);
                SqlCmd.Parameters.AddWithValue("@DeliveredBy", DeliveredBy);
                SqlCmd.Parameters.AddWithValue("@DeliveryDate", DeliveryDate);
                SqlCmd.Parameters.AddWithValue("@XML_OrderDtl", DelvryItms);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnection.ConnectionOpen();
                da.Fill(dt);
                dbConnection.ConnectionClose();

                //dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcInsertDelivery, "@TripID", TripID, "@CompanyID", CompID, "@OrderNo", OrderNo, "@Invoice_Number", Invoice_Number,
                //    "@OrderQty", OrdQty, "@OrderAmount", OrderAmount, "@Latitude", Lat, "@Longitude", Lng, "@Location", Location, "@DeliveredBy", DeliveredBy, "@XML_OrderDtl", DelvryItms);

                jsonString = dtToJson.convertDataTableToJson(dt, "InsertDeliveryData", true);
                    return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertDeliveryData", false);
            }
            finally
            {
                dbConnection = null;
                SqlCmd = null;
                dt.Dispose();
                da = null;
            }
            return null;
        }


        //public string AdjustDeliveryQty(DeliveryModels objDeliveryItms)
        //{
        //    SqlDataAdapter da = null;
        //    DataTable dt = null;
        //    String jsonString = String.Empty;
        //    Int16 OrdQty = 0;
        //    String TripID = String.Empty, Invoice_Number = String.Empty, CompID = String.Empty, Location = String.Empty, OrderNo = String.Empty, DeliveredBy = string.Empty;
        //    decimal Lat = 0, Lng = 0, OrderAmount = 0;

        //    try
        //    {
        //        DeliveryModels delvryModel = new DeliveryModels();
        //        delvryModel.deliveryLoadItems = objDeliveryItms.deliveryLoadItems;

        //        DataSet dsDelvry = new DataSet();
        //        dsDelvry.Tables.Add(dtlDelvry);
        //        String DelvryItms = dsDelvry.GetXml();
        //        dt = new DataTable();
        //        dbConnection = new DBConnection();
        //        SqlCmd = new SqlCommand(ApiConstant.SProcInsertDelivery, dbConnection.ApiConnection);
        //        SqlCmd.CommandType = CommandType.StoredProcedure;
        //        SqlCmd.CommandTimeout = 0;
        //        SqlCmd.Parameters.AddWithValue("@TripID", TripID);
        //        SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
        //        SqlCmd.Parameters.AddWithValue("@OrderNo", OrderNo);
        //        SqlCmd.Parameters.AddWithValue("@Invoice_Number", Invoice_Number);
        //        SqlCmd.Parameters.AddWithValue("@OrderQty", OrdQty);
        //        SqlCmd.Parameters.AddWithValue("@OrderAmount", OrderAmount);
        //        SqlCmd.Parameters.AddWithValue("@Latitude", Lat);
        //        SqlCmd.Parameters.AddWithValue("@Longitude", Lng);
        //        SqlCmd.Parameters.AddWithValue("@Location", Location);
        //        SqlCmd.Parameters.AddWithValue("@DeliveredBy", DeliveredBy);
        //        SqlCmd.Parameters.AddWithValue("@XML_OrderDtl", DelvryItms);

        //        da = new SqlDataAdapter(SqlCmd);
        //        dt = new DataTable();
        //        dbConnection.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnection.ConnectionClose();

        //        jsonString = dtToJson.convertDataTableToJson(dt, "AdjustDeliveryQty", true);
        //        return jsonString;
        //    }
        //    catch (Exception ex)
        //    {
        //        jsonString = dtToJson.convertDataTableToJson(dt, "AdjustDeliveryQty", false);
        //    }
        //    finally
        //    {
        //        dbConnection = null;
        //        SqlCmd = null;
        //        dt.Dispose();
        //        da = null;
        //    }
        //    return null;
        //}



    }
}