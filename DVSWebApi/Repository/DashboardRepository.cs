﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DVSWebApi.Repository
{
    public class DashboardRepository
    {
        DataTable dt = null;
        DBConnConfigSB3 dbConnConfigSB3 = null;
        DataToJson dtToJson = new DataToJson();

        DBConnectionSB3 dbConnectionObj = null;
        public DataTable GetLoginInfoFromDb(string CompnayID)
        {
            //Common common = new Common();
            List<LoginInfo> DashTopListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "Login", "@CompanyID", CompnayID);
                DashTopListObj = new List<LoginInfo>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DashTopListObj.Add(new LoginInfo
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(DashTopListObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                DashTopListObj = null;
            }

            return dt;
        }

        public DataTable GetMenuFromDb(string CompnayID)
        {
            //Common common = new Common();
            List<Menu> MenuListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "Menu", "@CompanyID", CompnayID);
                MenuListObj = new List<Menu>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            MenuListObj.Add(new Menu
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(MenuListObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                MenuListObj = null;
            }

            return dt;
        }

        public DataTable GetHeaderFromDb(string CompnayID)
        {
            //Common common = new Common();
            List<Header> HeaderListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "Header", "@CompanyID", CompnayID);
                HeaderListObj = new List<Header>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            HeaderListObj.Add(new Header
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(HeaderListObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                HeaderListObj = null;
            }
            return dt;
        }

        public DataTable GetFooterFromDb(string CompnayID)
        {
            List<Footer> FooterListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "Footer", "@CompanyID", CompnayID);
                FooterListObj = new List<Footer>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            FooterListObj.Add(new Footer
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(FooterListObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                FooterListObj = null;
            }

            return dt;
        }


        public DataTable GetDashboardTopListFromDb(String CompnayID)
        {
            List<DashBoard> DashTopListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWiseOffline, "@ScreenName", "Dashboard", "@CompanyID", CompnayID);
                DashTopListObj = new List<DashBoard>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DashTopListObj.Add(new DashBoard
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(DashTopListObj);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnConfigSB3 = null;
                DashTopListObj = null;
            }

            return dt;
        }

        public DataTable GetCompanyConfigParamsData(String CompanyId)
        {
            Common common = new Common();
            List<CompanyConfig> compConfigListObj = null;
            DataTable dt = null;
            //SqlDataAdapter da = null;
            //String jsonString, ImgName = string.Empty;
            try
            {
                dt = new DataTable();
                compConfigListObj = new List<CompanyConfig>();
                dbConnConfigSB3 = new DBConnConfigSB3();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetCompanyConfigList, "@CompanyID", CompanyId);

                //Bind LoginImgModel generic list using LINQ   
                compConfigListObj = (from DataRow dr in dt.Rows
                                     select new CompanyConfig()
                                     {
                                         CompanyID = Convert.ToString(dr["CompanyID"]),
                                         ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                         DriverPhoneOnOff = Convert.ToString(dr["DriverPhoneOnOff"]),
                                         DollarSignOnOff = Convert.ToString(dr["DollarSignOnOff"]),
                                         ThemeColor = Convert.ToString(dr["ThemeColor"]),
                                         PaymentDueDateOnOff = Convert.ToString(dr["PaymentDueDateOnOff"]),
                                         PaymentTermOnOff = Convert.ToString(dr["PaymentTermOnOff"]),
                                         PaymentDueDatePeriod = Convert.ToString(dr["PaymentDueDatePeriod"]),
                                         CustDashBackDatePeriod = Convert.ToString(dr["CustDashBackDatePeriod"])
                                     }).ToList();
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(compConfigListObj);
                DataSet dsCompConfigParams = new DataSet();
                dsCompConfigParams.Tables.Add(dt);
                //jsonString = ConvertDataTabletoString(dsCompConfigParams);
                //return jsonString;
                return dt;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnConfigSB3 = null;
                //da = null;
                //compConfigListObj = null;
            }
            return null;// dt;
        }


        public DataTable GetDashboardMiddleListFromDb(String CompnayID)
        {
            List<DashBoardMiddle> DashMiddleListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "DashboardMiddle", "@CompanyID", CompnayID);
                DashMiddleListObj = new List<DashBoardMiddle>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DashMiddleListObj.Add(new DashBoardMiddle
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(DashMiddleListObj);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnConfigSB3 = null;
                DashMiddleListObj = null;
            }

            return dt;
        }

        public DataTable GetDashboardBottomListFromDb(string CompnayID)
        {
            List<DashBoardBottom> DashBottomListObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetLebelImageByScreenWise, "@ScreenName", "DashboardBottom", "@CompanyID", CompnayID);
                DashBottomListObj = new List<DashBoardBottom>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DashBottomListObj.Add(new DashBoardBottom
                            {
                                ConfigID = Convert.ToString(dt.Rows[i]["ConfigID"]),
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                                LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(DashBottomListObj);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnConfigSB3 = null;
                DashBottomListObj = null;
            }

            return dt;
        }

        public DataTable GetCompanyConfigFromDb(String CompnayID)
        {
            List<CompanyConfig> CompConfigObj = null;
            dbConnConfigSB3 = new DBConnConfigSB3();
            dbConnectionObj = new DBConnectionSB3();
            String CompanyName1 = string.Empty, CompanyAddress1 = string.Empty, CompanyLogo1 = string.Empty;
            try
            {
                dt = new DataTable();
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProc_DVS_GetCompanyByID, "@CompanyId", CompnayID);


                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        CompanyName1 = Convert.ToString(dt.Rows[0]["CompanyName"]);
                        CompanyAddress1 = Convert.ToString(dt.Rows[0]["CompanyAddress"]);
                        CompanyLogo1 = Convert.ToString(dt.Rows[0]["CompanyLogo"]);
                                
                    }
                }
            }
            catch (Exception ex)
            {
            }
            try
            {
                dt = new DataTable();
                dt = dbConnConfigSB3.ExecDataTabletProcConfig(ApiConstant.SProcGetCompanyConfigList, "@CompanyID", CompnayID);
                CompConfigObj = new List<CompanyConfig>();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            CompConfigObj.Add(new CompanyConfig
                            {
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                DriverPhoneOnOff = Convert.ToString(dt.Rows[i]["DriverPhoneOnOff"]),
                                DollarSignOnOff = Convert.ToString(dt.Rows[i]["DollarSignOnOff"]),
                                ThemeColor = Convert.ToString(dt.Rows[i]["ThemeColor"]),
                                PaymentDueDateOnOff = Convert.ToString(dt.Rows[i]["PaymentDueDateOnOff"]),
                                PaymentTermOnOff = Convert.ToString(dt.Rows[i]["PaymentTermOnOff"]),
                                PaymentDueDatePeriod = Convert.ToString(dt.Rows[i]["PaymentDueDatePeriod"]),
                                TripAmountOnOff = Convert.ToString(dt.Rows[i]["TripAmountOnOff"]),
                                TripStopOnOff = Convert.ToString(dt.Rows[i]["TripStopOnOff"]),
                                TripHeaderOnOff = Convert.ToString(dt.Rows[i]["TripHeaderOnOff"]),
                                TripOrderCountOnOff = Convert.ToString(dt.Rows[i]["TripOrderCountOnOff"]),
                                TripDateOnOff = Convert.ToString(dt.Rows[i]["TripDateOnOff"]),
                                CleanInvItemNameOnOff = Convert.ToString(dt.Rows[i]["CleanInvItemNameOnOff"]),
                                CustDashBackDatePeriod = Convert.ToString(dt.Rows[i]["CustDashBackDatePeriod"]),
                                //PaymentDueDatePeriod = Convert.ToString(dt.Rows[i]["PaymentDueDatePeriod"])
                                //LabelText = Convert.ToString(dt.Rows[i]["LabelText"]),
                                //FolderHeaderImage = Convert.ToString(dt.Rows[i]["FolderHeaderImage"]),
                                //SortOrder = Convert.ToString(dt.Rows[i]["SortOrder"]),

                                CompanyName = CompanyName1,
                                CompanyAddress = CompanyAddress1,
                                CompanyLogo = CompanyLogo1

                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dt = lsttodt.ToDataTable(CompConfigObj);

                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnConfigSB3 = null;
                CompConfigObj = null;
                dbConnectionObj = null;
                dbConnectionObj = null;
            }

            return dt;
        }

        public HttpResponseMessage TotalOrderCountforDashboard()
        {
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            string strJsonData = string.Empty;
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                ret = objAccess.GetDataTable(ApiConstant.getTotalOrderCountforDashboard, ApiConstant.apiConnectionString, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "TotalOrderCountforDashboard", false);
            }
            finally
            {
                objJson = null;
                objAccess = null;
                objdt = null;
            }
            return null;
        }


        public HttpResponseMessage TripCountForDashboard()
        {
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            string strJsonData = string.Empty;
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                ret = objAccess.GetDataTable(ApiConstant.getTripCountForDashboard, ApiConstant.apiConnectionString, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "TripCountForDashboard", false);
            }
            finally
            {
                objJson = null;
                objAccess = null;
                objdt = null;
            }
            return null;
        }

        public HttpResponseMessage ListOfCompletedTrips_forDashboard()
        {
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            string strJsonData = string.Empty;
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                ret = objAccess.GetDataTable(ApiConstant.getListOfCompletedTrips_forDashboard, ApiConstant.apiConnectionString, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "ListOfCompletedTrips_forDashboard", false);
            }
            finally
            {
                objJson = null;
                objAccess = null;
                objdt = null;
            }
            return null;
        }

        public HttpResponseMessage ListofOnGoingTrip_forDashboard()
        {
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            string strJsonData = string.Empty;
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                ret = objAccess.GetDataTable(ApiConstant.getListofOnGoingTrip_forDashboard, ApiConstant.apiConnectionString, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "ListofOnGoingTrip_forDashboard", false);
            }
            finally
            {
                objJson = null;
                objAccess = null;
                objdt = null;
            }
            return null;
        }

        public HttpResponseMessage ListOfRecentTrips_forDashboard()
        {
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            string strJsonData = string.Empty;
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                ret = objAccess.GetDataTable(ApiConstant.getListOfRecentTrips_forDashboard, ApiConstant.apiConnectionString, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "ListOfRecentTrips_forDashboard", false);
            }
            finally
            {
                objJson = null;
                objAccess = null;
                objdt = null;
            }
            return null;
        }

        public string ConvertDataTabletoString(DataSet dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Tables[0].Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Tables[0].Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            dt.Dispose();
            return serializer.Serialize(rows);
        }










        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }






    }
}