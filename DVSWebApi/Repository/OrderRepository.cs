﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using DVSWebApi.Models;

namespace DVSWebApi.Repository
{
    public class OrderRepository
    {
        public HttpResponseMessage GetAllOrderforOrderList(OrderModel search)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@SALESMANID", SqlDbType.VarChar, 50);
                objParam[0].Value = search.SalesmanID;

                objParam[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar, 10);
                objParam[1].Value = search.CompanyID;

                ret = objAccess.GetDataTable(ApiConstant.getAllOrders, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetAllOrderforOrderList", false);
            }
            finally
            {
                objJson = null;
                objdt = null;
                objAccess = null;
            }
            return null;
        }


        public HttpResponseMessage getAllOrder(string SalesmanID, string CompanyID)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            SqlParameter[] objParam = new SqlParameter[2];
            objParam[0] = new SqlParameter("@SALESMANID", SqlDbType.VarChar, 50);
            objParam[0].Value = SalesmanID;

            objParam[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar, 10);
            objParam[1].Value = CompanyID;

            ret = objAccess.GetDataTable(ApiConstant.getAllOrders, ApiConstant.apiConnectionString, ref objParam, ref objdt);
            DataToJson objJson = new Repository.DataToJson();
            strJsonData = objJson.DataTableToJSON(objdt);

            StringContent sc = new StringContent(strJsonData);
            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage data = new HttpResponseMessage();
            data.Content = sc;
            return data;
        }
    }
}