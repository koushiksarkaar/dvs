﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DVSWebApi.Repository
{
    public static class CustomRoles
    {
        /// <summary>
        /// This Roles are defined in table [OAuthServiceDB].[dbo].[AspNetRoles]
        /// </summary>
        public const string BrkRole = "Broker01,Broker03,Broker15,Broker17,Broker18,Broker20,Broker80,Admin,GrpMgr,DivMgr,SrSalesMgr,Driver," +
    "CustomGrp01,CustomGrp02,CustomGrp03,CustomGrp04,CustomGrp05,CustomGrp06,CustomGrp07,CustomGrp08,CustomGrp09,CustomGrp10,CustomGrp11,CustomGrp12,CustomGrp13,CustomGrp14,CustomGrp15," +
    "CustomGrp16,CustomGrp17,CustomGrp18,CustomGrp19,CustomGrp20,CustomGrp21,CustomGrp22,CustomGrp23,CustomGrp24,CustomGrp25";
        public const string AdmRole = "Admin";
    }
}