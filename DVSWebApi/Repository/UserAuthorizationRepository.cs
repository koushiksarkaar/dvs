﻿using DVSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DVSWebApi.Repository
{
    public class UserAuthorizationRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public int GetRoleIdFromUserId(string userId)
        {
            DataTable dt = null;
            int roleId = 0;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetRoleIdFromUserId, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    roleId = Convert.ToInt32(dr["RoleId"]);
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

                return roleId; 
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return roleId;
        }

        public string GetUserFullNameFromUserId(string userId)
        {
            DataTable dt = null;
            string userFName = string.Empty;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetUserFullName, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userFName = dr["UserFullName"].ToString();
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

                return userFName;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return userFName;
        }

        public UserProfile GetUserProfile(string userId, string CompanyID, string DeviceID, string TokenID)
        {
            UserProfile userObj = null;

            try
            {
                userObj = new UserProfile();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetUserMyProfile, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                com.Parameters.AddWithValue("@CompanyID", CompanyID);
                com.Parameters.AddWithValue("@DeviceID", DeviceID);
                com.Parameters.AddWithValue("@TokenID", TokenID);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserName = dr["UserName"].ToString();
                    userObj.UserFullName = dr["UserFullName"].ToString();
                    userObj.CompanyId = dr["CompanyId"].ToString();
                    userObj.CompanyName = dr["CompanyName"].ToString();
                    userObj.Role = dr["Role"].ToString();
                    userObj.Email = dr["Email"].ToString();
                    userObj.Phone = dr["PhoneNumber"].ToString();
                    userObj.BrokerPicName = dr["BrokerPicName"].ToString();
                    userObj.UserType = dr["UserType"].ToString();
                    if (dr["IsNewReleaseFlag"] is DBNull)
                        userObj.IsNewReleaseFlag = false;
                    else
                        userObj.IsNewReleaseFlag = Convert.ToBoolean(dr["IsNewReleaseFlag"]);
                    userObj.BrokerDivisionCode = dr["BrokerDivisionCode"].ToString();
                    userObj.DeliveryCharge = dr["DeliveryCharge"].ToString();
                    userObj.MinimumDeliveryChargeAmount = dr["MinimumDeliveryChargeAmount"].ToString();
                    userObj.LoginCompanyId = dr["LoginCompanyId"].ToString();
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }

        public bool UpdateUserProfile(UserProfile userObj)
        {
            bool success = false;
            SqlDataReader reader = null;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateMyProfile, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", userObj.UserName);
                com.Parameters.AddWithValue("@Email", userObj.Email);
                com.Parameters.AddWithValue("@Phone", userObj.Phone);
                com.Parameters.AddWithValue("@Comments", "Phone and Email got updated.");

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }

        public bool UpdateNewUser(CreateNewUserBindingModel userObj)
        {
            bool success = false;
            SqlDataReader reader = null;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateUser, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", userObj.UserName);
                com.Parameters.AddWithValue("@UserFullName", userObj.UserFullName);
                com.Parameters.AddWithValue("@Domain", userObj.Domain);
                com.Parameters.AddWithValue("@UserTypeCode", userObj.UserTypeCode);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }

        public bool UpdateUserPassword(CreateNewUserBindingModel userObj)
        {
            bool success = false;
            SqlDataReader reader = null;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.spResetPasswordUser, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", userObj.UserName);
                com.Parameters.AddWithValue("@UserFullName", userObj.UserFullName);
                com.Parameters.AddWithValue("@Domain", userObj.Domain);
                com.Parameters.AddWithValue("@UserTypeCode", userObj.UserTypeCode);
                com.Parameters.AddWithValue("@CompanyID", userObj.CompanyID);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }

        public bool UpdateUpdatedBy(string userName)
        {
            bool success = false;
            SqlDataReader reader = null;
            string comments = "Password is been reset.";

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateUpdatedBy, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                com.Parameters.AddWithValue("@comments", comments);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }


        public string GetUserIDFromUserName(string userName)
        {
            DataTable dt = null;
            string userId = string.Empty;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetUserIdFromUserName, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userId = dr["Id"].ToString();
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetUserIDFromUserName");

                return userId;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return userId;
        }

        public bool IfUserExistInOAuthDB(string userName, string email)
        {
            DataTable dt = null;
            bool isExist = false;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetIfUserOrEmailExist, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                com.Parameters.AddWithValue("@email", email);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt32(dr[0]) == 1)
                        isExist = true;
                }

                log.Debug("Method -> IfUserExistInOAuthDB");

                return isExist;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return isExist;
        }

        public UserProfile GetUserInfoByUserID(string userName, String companyID)
        {
            DataTable dt = null;
            UserProfile userObj = null;

            try
            {
                userObj = new UserProfile();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.spGetUserProfile, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                com.Parameters.AddWithValue("@CompanyID", companyID);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach(DataRow dr in dt.Rows)
                {
                    userObj.UserFullName = dr["UserFullName"].ToString();
                    userObj.UserType = dr["UserTypeCode"].ToString();
                    userObj.Email = dr["Email"].ToString();
                    userObj.Domain = dr["Domain"].ToString();
                }

                log.Debug("Method -> GetUserInfoByUserID");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt.Dispose();
            }

            return userObj;
        }

        public bool IfUserValidEmailOAuthDB(string userName, string email)
        {
            DataTable dt = null;
            bool isExist = false;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocCheckIfValidEmailExist, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                com.Parameters.AddWithValue("@email", email);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt32(dr[0]) == 1)
                        isExist = true;
                }

                log.Debug("Method -> IfUserExistInOAuthDB");

                return isExist;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return isExist;
        }

        //  Reset Release Update flag
        public string ResetReleaseUpdateFlag(string userId)
        {
            DataTable dt = null;
            int roleId = 0;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocResetReleaseUpdateFlag, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose(); 

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> ResetReleaseUpdateFlag");

                return "Updated";
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return "Failed";
        }

        // Upload User Profile Image
        public string sendFileData(UserProfile userObj)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                //dbConnectionObj = new DBConnection();
                //SqlCmd = new SqlCommand(ApiConstant.SProcInsertSalesCommunicator, dbConnectionObj.ApiConnection);
                // SqlCmd.CommandType = CommandType.StoredProcedure;
                string userProfileImagePath = WebConfigurationManager.AppSettings["UserProfileImagePath"];

                string base64BinaryStr = userObj.ImageFileData;

                byte[] bytes = Convert.FromBase64String(base64BinaryStr);

               
                string ext = Path.GetExtension(userObj.FileName); // Get file Extension from file name
                System.IO.FileStream stream = new FileStream(userProfileImagePath + "//"+ userObj.UserId+".jpg", FileMode.Append);
                System.IO.BinaryWriter writer =
                new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();

                // SqlCmd.Parameters.AddWithValue("@WeekNo", fileName);
                //  SqlCmd.Parameters.AddWithValue("@Year", fileData);
                //  SqlCmd.Parameters.AddWithValue("@StartDate", fileType);
                //  SqlCmd.Parameters.AddWithValue("@EndDate", sentDate);

                //   da = new SqlDataAdapter(SqlCmd);
                //   dt = new DataTable();
                //   dbConnectionObj.ConnectionOpen();
                //   da.Fill(dt);
                //   dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //   jsonString = dtToJson.convertDataTableToJson(dt, "insertSalesCommunicatorData", true);
                // return jsonString;
                return "";
            }
            catch (Exception ex)
            {
                //jsonString = dtToJson.convertDataTableToJson(dt, "insertSalesCommunicatorData", false);
            }
            finally
            {
                dbConnectionObj = null;
              //  SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
    }
}