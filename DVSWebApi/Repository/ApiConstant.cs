﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DVSWebApi.Repository
{
    public class ApiConstant
    {
        /// <summary>
        /// Conncetion String to connect to database 
        /// </summary> 
        public static string apiSB3_DVSconnString = ConfigurationManager.ConnectionStrings["SB3DVSconn"].ToString();
        public static string apiConnectionStringOAuth = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        public static string SB3ConfigDBonn = ConfigurationManager.ConnectionStrings["SB3ConfigDBonn"].ToString();
        /// <summary>
        /// User Authentication and if success response with Tokenl
        /// </summary> 
        public static string userTokenApiUri = WebConfigurationManager.AppSettings["UserTokenApiUri"];
        public static string ldapAuthenticationApiUri = WebConfigurationManager.AppSettings["LDAPAuthenticationApiUri"];
        public static string userVerifyApiUri = WebConfigurationManager.AppSettings["UserVerifyApiUri"];
        public static string userAuthorizationApiUri = WebConfigurationManager.AppSettings["UserAuthorizationApiUri"];
        public static string getUserFullNameApiUri = WebConfigurationManager.AppSettings["GetUserFullNameApiUri"];
        public static string getClientName = WebConfigurationManager.AppSettings["ClientName"];

        public static string FileDirectory = WebConfigurationManager.AppSettings["ImageData"] + "\\Files"; // "E:\\OMSmobile_API\\OMSmobilePublished\\Files";    // WebConfigurationManager.AppSettings["FileDirectoryPath"];   E:\\OMSmobile_API\\OMSmobilePublished\\Files    D:\Koushik\OMSmobile_API\OMSmobileImg\Files\EDI
        public static string SectionOutsideInside_ImagePath = WebConfigurationManager.AppSettings["ImageData"] + "\\Customer\\StoreImage";  // "E:\\OMSmobile_API\\OMSmobilePublished\\UploadedFiles";   //E:\OMSmobile_API\OMSmobilePublished\UploadedFiles
        public static string DVSmobileImage = WebConfigurationManager.AppSettings["DVSmobileImage"]; // "http://107.21.119.157/DVSmobileImg/";   //WebConfigurationManager.AppSettings["CustPortalImagePath"]; 
        public static string StorePhotoPath = WebConfigurationManager.AppSettings["ImageData"] + "\\Customer\\ProfileImage";
        public static string ImageData = WebConfigurationManager.AppSettings["ImageData"];
        public static string StopCheckIn_imagePath = WebConfigurationManager.AppSettings["ImageData"] + "\\Stops";
        public static string Barcode_imagePath = WebConfigurationManager.AppSettings["ImageData"] + "\\BarcodeImages\\";
        public static string ProductImage = WebConfigurationManager.AppSettings["ProductImage"]; 
        public static string apiConnectionString = ConfigurationManager.ConnectionStrings["SB3DVSconn"].ToString();
        /// <summary>
        /// Token verification
        /// </summary>
        public static string userVerifyTokenApiUri = WebConfigurationManager.AppSettings["UserVerifyTokenApiUri"];

        /// <summary>
        /// Change Password
        /// </summary>
        public static string changePasswordApiUri = WebConfigurationManager.AppSettings["ChangePasswordApiUri"];

        public static string brokerPicUri = WebConfigurationManager.AppSettings["BrokerPicUri"];
         
        public static string DefaultBrokerPwdPrefix = WebConfigurationManager.AppSettings["DefaultBrokerPwdPrefix"];
        public static string DefaultPwdPrefix = WebConfigurationManager.AppSettings["DefaultPwdPrefix"];
        public static string apiNewUserUri = WebConfigurationManager.AppSettings["apiNewUserUri"];
        public static string apiResetUserUri = WebConfigurationManager.AppSettings["apiResetUserUri"];
        public static string apiChangePassword = WebConfigurationManager.AppSettings["apiChangePassword"];

        // Setting Folder and IMAGES
        //public static string FileDirectory = WebConfigurationManager.AppSettings["ImageData"] + "\\Files"; // "E:\\OMSmobile_API\\OMSmobilePublished\\Files";    // WebConfigurationManager.AppSettings["FileDirectoryPath"];   E:\\OMSmobile_API\\OMSmobilePublished\\Files    D:\Koushik\OMSmobile_API\OMSmobileImg\Files\EDI
        //public static string SectionOutsideInside_ImagePath = WebConfigurationManager.AppSettings["ImageData"] + "\\Customer\\StoreImage";  // "E:\\OMSmobile_API\\OMSmobilePublished\\UploadedFiles";   //E:\OMSmobile_API\OMSmobilePublished\UploadedFiles
        //public static string CustPortalImage = WebConfigurationManager.AppSettings["CustPortalImage"]; // "http://portal.goya.com/CustomerPortalImages/";   //WebConfigurationManager.AppSettings["CustPortalImagePath"]; 
        //public static string StorePhotoPath = WebConfigurationManager.AppSettings["ImageData"] + "\\Customer\\ProfileImage";
        //public static string ImageData = WebConfigurationManager.AppSettings["ImageData"];

        //   Send Email 
        public static string strSenderEmailId = "koushik@sb3inc.com";//WebConfigurationManager.AppSettings["strSenderAddress"]; 
        public static string smtppasswd = "welcome123#";//WebConfigurationManager.AppSettings["smtppasswd"];
        public static string SMTPServer = "smtp.1and1.com";

        //  Configuration SPs 
        public static string SPocGetLookupMappingTableList = "sp_GetLookupMappingTableList";
        public static string SPocGetRoleApplicationAccessMappingList = "sp_GetRoleApplicationAccessMappingList";
        public static string SPocGetUserAppRoleAssignedList = "sp_GetUserAppRoleAssignedList";
        public static string SPocGetUserAppRoleNotAssignedList = "sp_GetUserAppRoleNotAssignedList";
        public static string SpInsertDefaultScreen = "sp_InsertDefaultScreen";
        public static string SPGetRollDetails = "SP_GetRollDetails";
        public static string SPGetDefaultScreenDetails = "SP_GetDefaultScreenDetails";
        public static string SPocGetUserSearchList = "sp_GetUserSearchList";
        public static string SPocUpdateUserProfileFromAdmin = "sp_UpdateUserProfileFromAdmin";
        public static string SPocGetGeneralLookupList = "sp_GetGeneralLookupList";
        
        
        //OAuth Section SPs
        public static string SPocGetUserMyProfile = "sp_GetUserMyProfile";
        public static string SProcGetUserDetails = "sp_GetUserDetails";
        public static string SPocGetRoleIdFromUserId = "sp_GetRoleIdFromUserId";
        public static string SPocGetUserFullName = "sp_GetUserFullName";
        public static string SPocUpdateMyProfile = "sp_UpdateMyProfile";
        public static string SPocUpdateUser = "sp_UpdateUser";
        public static string spResetPasswordUser = "sp_ResetPasswordUser";
        public static string SPocUpdateUpdatedBy = "sp_UpdateUpdatedBy";
        public static string SPocGetUserIdFromUserName = "sp_GetUserIdFromUserName";
        public static string SPocGetIfUserOrEmailExist = "sp_GetIfUserOrEmailExist";
        public static string SPocCheckIfValidEmailExist = "sp_CheckIfValidEmailExist";
        public static string SPocResetReleaseUpdateFlag = "sp_ResetReleaseUpdateFlag";
        public static string spGetUserProfile = "sp_GetUserProfile";
        //  Firebase Notification
        public static string spGetRefreshToken = "sp_GetRefreshToken";

        // Driver login ckecked
        public static string SProcGetLoginInfo = "SProc_GetLoginInfo";

        // Dashboard Info
        public static string SProcGetLebelImageByScreenWise = "SProc_GetLebelImageByScreenWise";

        // DVS Company Config param ON / OFF
        public static string SProcGetCompanyConfigList = "SProc_GetCompanyConfigList";

        // Driver Record Inserted 
        public static string SProcInsertDriverRecord = "SProc_InsertDriverRecord";

        // Driver TripInformation
        public static string SProcDriverTripInformation = "SProcDriverTripInformation";

        // RouteSpotDetails
        public static string SProcRouteSpotDetail = "SProc_GetRouteSpotDetail";

        // Get Trip Stop Summery
        public static string SProcGetTripSummery = "SProc_GetTripSummery";

        // Show the Trip Map View
        public static string SProcGetTripMapView = "SProc_GetTripMapView";

        // To get the trip custoemr check In 
        public static string SProcGetTripCustomerCheckIn = "SProc_GetTripCustomerCheckIn";
        // Trip Stop check in details to be inserted by the customer wise 
        public static string SProcInsertStopCheckIn = "SProc_InsertStopCheckIn";
        // To get the stop summery 2
        public static string SProcGetStopSummery2 = "SProc_GetStopSummery2";
        // Order 
        // To get the order summery by order number 
        public static string SProcGetOrderSummery = "SProc_GetOrderSummery";
        // Insert DSD generation
        public static string SProcDVSUpdateStopSummery = "SProcDVS_UpdateStopSummery";
        // To insert delivery
        public static string SProcInsertDelivery = "SProc_InsertDelivery";
        public static string SProcGetCancelReasonText = "SProc_GetCancelReasonText";

        // To insert invoice
        public static string SProcGenerateInvoice = "SProc_GenerateInvoice";
        public static string SProcSaveInvoice = "SProc_SaveInvoice";
        public static string SProcInvoiceGeneration = "SProc_InvoiceGeneration";
        
        // View Invoice data by number
        public static string SProcGetInvoiceHeaderByNo = "SProc_GetInvoiceHeaderByNo";
        // Insert DSD generation 
        public static string SProcInsertDSD = "SProc_InsertDSD";
        // Invoice View
        public static string SProcGetInvoiceByDriverCompany = "SProc_GetInvoiceByDriverCompany";
        public static string SProcViewSavedInvoice = "SProc_ViewSavedInvoice";
        public static string SProcGetInvoiceDetail_ByDVSInvoiceNo = "SProc_GetInvoiceDetail_ByDVSInvoiceNo";



        //Dashboard
        public static string getTotalOrderCountforDashboard = "SProc_getOrderCountForDashboard"; //get order count for dashboard
        public static string getTripCountForDashboard = "Sproc_GetTripCountForDashBoard"; //get trips count for dashboard
        public static string getListOfCompletedTrips_forDashboard = "SProc_ListOfCompletedTrips_forDashboard"; //get list of completed trips for dashboard
        public static string getListofOnGoingTrip_forDashboard = "SProc_ListofOnGoingTrip_forDashboard"; //get list of ongoing trips for dashboard
        public static string getListOfRecentTrips_forDashboard = "SProc_ListOfRecentTrips_forDashboard"; //get list of recent trips for dashboard

        //Vehicles
        public static string getAllActiveVehiclesType = "SP_getAllVehiclesType"; //gel all vehicles list and search
        public static string insertVehiclesType = "sp_insertVehiclesType"; //Save vehicles type into the databse
        public static string getAllActiveVehiclesList = "SP_getAllActiveVehicles"; //get all active vehicles

        //Driver
        public static string getAllDriverList = "SProc_getAllDriversList"; // To get all drivers list
        public static string insertDriverDetails = "SProc_InsertDriverRecord"; // insert driver details
        public static string getDriverDetailsByID = "SProc_getDriverDetailsByID"; // To get particular driver details
        public static string GetWarehouseListByComp = "SProc_OMS_WarehouseListByComp"; // To get warehouse  details
        public static string ValidateLoginUser = "Sproc_ValidateLoginUser"; // To get warehouse  details
        
        

        //Trip
        public static string getAllTripInformation = "SProc_GetAllTripInfo"; // To get all trip information and filter by date and trip status..
        public static string getAllInProgressTrip = "SProc_TripTracking"; // Get All In-Progress trip 
        public static string searchInProgressTrip = "SProc_SearchTripTracking"; // search in progress trip by search text
        public static string getTripSummeryByTripId = "SProc_GetTripSummery"; // Get trip summery by Trip Id
        public static string getTripMapViewBytripId = "SProc_GetTripMapView"; // Trip map view by trip id
        public static string GetTripCustomerCheckIn = "SProc_GetTripCustomerCheckIn"; // Get Trip Customer CheckIn
        public static string DriverTripInformation = "SProcDriverTripInformation"; // Driver Trip Information
        public static string SProcDVSUpdateSkippedTripSummery = "SProcDVS_UpdateSkippedTripSummery"; // to skip the partilar customer
        public static string SProcGetTripListOrderCount = "SProc_GetTripListOrderCount";

        //Order
        public static string getOrderDetailsbyOrderNumber = "SProc_GetOrderDetailsByOrderNumber"; // get cust and order details by order number..
        public static string getItemDetailsbyOrderNumber = "SProc_GetItemDetailsByOrderNumber"; //get items details by order number..
        public static string getAllOrders = "SProc_GetAllOrders"; //To get all order for All Order List


        // Invoice 
        public static string SProc_DVS_UploadInvoice = "SProc_DVS_UploadInvoice";
        public static string SProc_DVS_UploadInvoiceImage = "SProc_DVS_UploadInvoiceImage";
        public static string InvoicePDF = WebConfigurationManager.AppSettings["InvoicePDF"];
        public static string SProc_DVS_GetUplodedInvoiceList = "SProc_DVS_GetUplodedInvoiceList";

        // Payment
        public static string SProcGetPaymentTermList = "SProc_GetPaymentTermList";

        //Customer Dashboard
        public static string SProcGetCustomerDashboardList = "SProc_GetCustomerDashboardList";

        // Offline download data syncing
        public static string SProcGetLebelImageByScreenWiseOffline = "SProc_GetLebelImageByScreenWiseOffline";
        public static string SprocGetAllTrips = "Sproc_GetAllTrips";
        public static string SprocGetAllOrders = "Sproc_GetAllOrders";
        public static string SProcGetAllCustomers = "SProc_GetAllCustomers";
        public static string SProcGetAllOrdersDetails = "SProc_GetAllOrdersDetails";

        // Offline Upload data by syncing
        public static string DVS_TripUpload = "SProc_DVS_TripUpload";
        public static string DVS_CustomerUpload = "SProc_DVS_CustomerUpload";
        public static string GetInvoiceNoByOrderNo = "SProc_GetInvoiceNoByOrderNo";
        public static string DVS_StopsCheckInUpload = "SProc_DVS_StopsCheckInUpload";

        public static string SProc_DVS_GetCompanyByID = "SProc_DVS_GetCompanyByID";
        public static string SProc_DVS_OrderDetailsUpload = "SProc_DVS_OrderDetailsUpload";
        public static string SProc_DVS_InvoiceDSDUpload = "SProc_DVS_InvoiceDSDUpload";
        public static string SProc_DVS_CustomerStatusUpload = "SProc_DVS_CustomerStatusUpload";
        public static string SProc_GetInvoiceDSDImagePDF = "SProc_GetInvoiceDSDImagePDF";

        // AR Invoice related Sp
        public static string InvoiceMedia = WebConfigurationManager.AppSettings["InvoiceMedia"];
        public static string apiConnectionDB2 = ConfigurationManager.ConnectionStrings["conStrDB2"].ToString();
        public static string GetCustomerList = "SProc_AR_GetCustomerList"; // Get customer list for AR App
        public static string GetOrderAndLinvoiceList = "SProc_AR_GetOrderAndInvoiceList"; // Get order and invoice list by customer code
        public static string SProcGetTripDetailsByOrderNo = "USP_GET_ORDER_TRIP_DATA_BY_ORDERNO"; // get invoice details by BasketId
        public static string InsertARInvoice = "SProc_AR_InsertInvoiceData";
        //public static string InsertARInvoiceDetails = "SProc_AR_InsertInvoiceDetails";
        public static string GetInsertedInvoiceList = "SProc_AR_GetInsertedInvoiceList";
        public static string GetARInvoiceStatus = "SP_AR_GetARInvoiceStatus";
        public static string ARInvoiceApproveReject = "SP_AR_ApproveRejectInvoice";

    }
}