﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DVSWebApi.Models;
using System.Xml.Serialization;
//using System.Xml.Serialization;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Web.Helpers;
using Newtonsoft.Json;
using System.Web.Mvc;
using Newtonsoft.Json.Converters;
using System.Text;


namespace DVSWebApi.Repository
{
    /// <summary>
    /// Convert Datatable to Json format 
    /// </summary>
    public class DataToJson 
    {        
        public String convertDataTableToJson(DataTable dt,string eventName,bool status)
        {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            string jsonData;
            JsonData jsData = new JsonData();
            jsData.domain = eventName;  // 1
            jsData.events = eventName;  // 2
            StringBuilder sb = new StringBuilder();

            //foreach (DataRow dr in dt.Rows)
            //{

            //    row = new Dictionary<string, object>();
            //    foreach (DataColumn col in dt.Columns)
            //    {
            //        row.Add(col.ColumnName, dr[col]);
            //    }
            //    rows.Add(row);
            //}
            sb.Append("{");
            sb.Append("\"Domain\":" + "\"" + eventName + "\",");
            sb.Append("\"Event\":" + "\"" + eventName + "\",");
            sb.Append("\"ClientData\":" + "\"test2\",");
            sb.Append("\"Status\":" + "\"" + status + "\",");
            ///////////
            string[] jsonArray = new string[dt.Columns.Count];
            string headString = string.Empty;

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                jsonArray[i] = dt.Columns[i].Caption; // Array for all columns
                headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
            }

            if(headString.Length>0)
                  headString = headString.Substring(0, headString.Length - 1);
          
            sb.Append("\"Payload\":[");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0;i< dt.Rows.Count; i++)
                {
                    string tempString = headString;
                    sb.Append("{");

                    // To get each value from the datatable
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        tempString = tempString.Replace(dt.Columns[j] + j.ToString().Replace("\"", "") + "%", dt.Rows[i][j].ToString().Replace("\"", "")).Replace("\\", "/").Replace("'", "-");
                    }

                    sb.Append(tempString + "},");
                }
            }
            else
            {
                string tempString = headString;
                sb.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    tempString = tempString.Replace(dt.Columns[j] + j.ToString() + "%", "-");
                }

                sb.Append(tempString + "},");
            }

            sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
            sb.Append("]}");       
            ////////////////

            //string data = jss.Serialize(rows); //3
            jsData.payload = sb.ToString(); ;
               
            jsData.clientData = "";  // 4
            jsData.status = "Sucess";
      
         //  string jsonDataResult = jss.Serialize(jsData);
          // return jsonDataResult;

         //   JavaScriptSerializer json_serializer = new JavaScriptSerializer();

        
         //   var jObjectData = JObject.Parse(jsonDataResult);
            return sb.ToString();

           

           // jsonData = jss.Serialize(rows);
           // string finalJsonData = addHeaderToJson(jsonData);
           
            //return jss.Serialize(rows);                    

        }

        /// <summary>
        /// Add Header Detail to Json object
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public string addHeaderToJson(string jsonData)
        {
        
            //JObject jsonObject = JObject.Parse(jsonData);

            //((JObject)jsonObject["RCPT_HEADER"][0])
            //  .Properties()
            //  .Last()
            //  .AddAfterSelf(new JProperty("RCPT_DETAIL", jsonObject["RCPT_DETAIL"]));
            //   jsonObject.Remove("RCPT_DETAIL");


            string jsonFinalResult = jsonData.ToString();
            return jsonFinalResult;

        }

        public string convertRegulartDataToJson(DataTable orderData)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            //if (_ds.Tables.Count > 0)
            {
               // DataTable orderData = _ds.Tables[0];

                foreach (DataRow dr in orderData.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in orderData.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
            }
            return jss.Serialize(rows);
           // return;
        }

        public string DataTableToJSON(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }

    }


}