﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DVSWebApi.Repository
{
    public class DBConnConfigSB3
    {
        protected string _connString = null;
        protected SqlConnection _conn = null;
        protected bool _disposed = false;
        protected SqlTransaction _trans = null;

        /// <summary>
        /// Constructor using global connection string.
        /// </summary>
        public DBConnConfigSB3()
        {
            _connString = ApiConstant.SB3ConfigDBonn;
            ConnectConfig();
        }

        /// <summary>
        /// Constructure using connection string override
        /// </summary>
        /// <param name="connString">Connection string for this instance</param>
        public DBConnConfigSB3(string connString)
        {
            _connString = connString;
            ConnectConfig();
        }

        // Creates a SqlConnection using the current connection string
        protected void ConnectConfig()
        {
            _conn = new SqlConnection(_connString);
            _conn.Open();
        }

        public DataSet ExecDataSetProcConfig(string qry, params object[] args)
        {
            using (SqlCommand cmd = CreateCommandConfig(qry, CommandType.StoredProcedure, args))
            {
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                return ds;
            }
        }

        public DataTable ExecDataTabletProcConfig(string qry, params object[] args)
        {
            using (SqlCommand cmd = CreateCommandConfig(qry, CommandType.StoredProcedure, args))
            {
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                return dt;
            }
        }

        public SqlCommand CreateCommandConfig(string qry, CommandType type, params object[] args)
        {
            SqlCommand cmd = new SqlCommand(qry, _conn);

            // Associate with current transaction, if any
            if (_trans != null)
                cmd.Transaction = _trans;

            // Set command type
            cmd.CommandType = type;

            // Construct SQL parameters
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] is string && i < (args.Length - 1))
                {
                    SqlParameter parm = new SqlParameter();
                    parm.ParameterName = (string)args[i];
                    parm.Value = args[++i];
                    cmd.Parameters.Add(parm);
                }
                else if (args[i] is SqlParameter)
                {
                    cmd.Parameters.Add((SqlParameter)args[i]);
                }
                else throw new ArgumentException("Invalid number or type of arguments supplied");
            }
            return cmd;
        }

    }
}