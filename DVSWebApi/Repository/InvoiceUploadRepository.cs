﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DVSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using DVSWebApi.Controllers;
using DVSWebApi.Providers;
using DVSWebApi.Results;
using DVSWebApi.Repository;
using System.Net.Mail;
using System.Globalization;
using System.Text;
using System.Web.Script.Serialization;


using System.Reflection;
using System.IO;
using Newtonsoft.Json;

namespace DVSWebApi.Repository
{
    public class InvoiceUploadRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        SqlDataAdapter da = null;
        public DataTable SaveImageListAll(String CustId, String CompId, dynamic frmData)// String[] SectionSrl, String[] SectionNm, String[] OutsideSrl, String[] OutsideNm, String[] InsideSrl, String[] InsideNm)
        {
            Common common = new Common();
            DataTable dt = null;
            List<ImgReuslt> ImgObj = null;
            DataTable dtl = new DataTable();
            try
            {
                ImgObj = new List<ImgReuslt>();

                //     Outside
                foreach (var ot in frmData.outsideDet)
                {
                    String OutsideImgName = ot.OutsideImgName;
                    if (!String.IsNullOrEmpty(OutsideImgName))
                    {
                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SProc_DVS_CustomerStatusUpload, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.AddWithValue("@CustomerId", CustId);
                        SqlCmd.Parameters.AddWithValue("@CompanyID", CompId);
                        SqlCmd.Parameters.AddWithValue("@UploadShopName", OutsideImgName);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImagePath", "http://107.21.119.157/CustomerPortalImages/");
                        SqlCmd.Parameters.AddWithValue("@UploadShopImageName", OutsideImgName);
                        SqlCmd.Parameters.AddWithValue("@action", 'I');

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ImgObj.Add(new ImgReuslt
                        {
                            Msg = Convert.ToString(dt.Rows[0]["Msg"]),
                            CustomerId = CustId
                        });
                    }
                }

                ListtoDataTable lsttodt = new ListtoDataTable();
                dtl = lsttodt.ToDataTable(ImgObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                ImgObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public string UploadDVSInvoice(InvoiceUploadModel invModel)
        {
            InvoiceUploadModel InvModel = new InvoiceUploadModel();
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtImage = new DataTable();
            DataSet dsImage = new DataSet();
            string jsonString;
            string time = DateTime.Now.ToString("mmssff");
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_DVS_UploadInvoice, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@DriverId", string.IsNullOrEmpty(invModel.DriverId) ? DBNull.Value : (object)invModel.DriverId);
                SqlCmd.Parameters.AddWithValue("@CustomerId", string.IsNullOrEmpty(invModel.CustomerId) ? DBNull.Value : (object)invModel.CustomerId);
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", string.IsNullOrEmpty(invModel.InvoiceNo) ? DBNull.Value : (object)invModel.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@UploadDate", invModel.UploadDate == DateTime.MinValue ? DBNull.Value : (object)invModel.UploadDate);
                SqlCmd.Parameters.AddWithValue("@SignatoryName", string.IsNullOrEmpty(invModel.SignatoryName) ? DBNull.Value : (object)invModel.SignatoryName);

                System.Drawing.Image img;
                var saveSignImage = "";
                if (invModel.SignBase64Image != null)
                {

                    string SignImageSavePath = string.Empty;
                    var path = System.Web.Hosting.HostingEnvironment.MapPath("~/InvoicePDF");
                    if (!String.IsNullOrEmpty(invModel.SignBase64Image))
                    {
                        saveSignImage = invModel.InvoiceNo + time + Path.GetExtension(invModel.SignImage);
                        SignImageSavePath = path + "\\" + saveSignImage;
                        if (!String.IsNullOrEmpty(invModel.SignBase64Image))
                        {
                            img = Base64ToImage(Convert.ToString(invModel.SignBase64Image));
                            img.Save(SignImageSavePath);
                        }
                    }
                }
                SqlCmd.Parameters.AddWithValue("@SignImage", saveSignImage);
                SqlCmd.Parameters.AddWithValue("@SignImagePath", ApiConstant.InvoicePDF);

                var savePdfName = "";
                if (invModel.PDFBase64 != null)
                {
                    String StorePdfPath = System.Web.Hosting.HostingEnvironment.MapPath("~/InvoicePDF");
                    string PdfSavePath = string.Empty;

                    var base64Pdf = invModel.PDFBase64;
                    var PdfName = invModel.PDFName;
                    if (!String.IsNullOrEmpty(base64Pdf))
                    {
                        savePdfName = invModel.InvoiceNo + time + Path.GetExtension(PdfName);
                        PdfSavePath = StorePdfPath + "\\" + savePdfName;
                        if (!String.IsNullOrEmpty(base64Pdf))
                        {
                            byte[] PDFDecoded = Convert.FromBase64String(base64Pdf);
                            FileStream obj = File.Create(PdfSavePath);
                            obj.Write(PDFDecoded, 0, PDFDecoded.Length);
                            obj.Flush();
                            obj.Close();
                        }
                    }
                }
                SqlCmd.Parameters.AddWithValue("@PDFName", savePdfName);
                SqlCmd.Parameters.AddWithValue("@PDFPath", ApiConstant.InvoicePDF);

                SqlCmd.Parameters.AddWithValue("@Status", string.IsNullOrEmpty(invModel.Status) ? DBNull.Value : (object)invModel.Status);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (Convert.ToString(dt.Rows[0]["InvoiceNo"]) != null)
                {
                    var saveImageName = "";
                    string InvoiceNo = Convert.ToString(dt.Rows[0]["InvoiceNo"]);
                    IEnumerable<InvoiceImage> ImageLst = invModel.InvoiceUploadImage;
                    DataTable dtImageList = new DataTable();
                    dtImageList.Columns.Add("ImageName", typeof(string));
                    dtImageList.Columns.Add("ImageBase64String", typeof(string));
                    int count = 1;
                    String StoreImgPath = System.Web.Hosting.HostingEnvironment.MapPath("~/InvoicePDF");
                    string imageSavePath = string.Empty;
                    foreach (var itms in ImageLst)
                    {
                        var base64Image = itms.ImageBase64String;
                        var imageName = itms.ImageName;
                        if (!String.IsNullOrEmpty(base64Image))
                        {
                            saveImageName = InvoiceNo + time + count.ToString() + Path.GetExtension(imageName);
                            imageSavePath = StoreImgPath + "\\" + saveImageName;
                            if (!String.IsNullOrEmpty(base64Image))
                            {
                                img = Base64ToImage(Convert.ToString(base64Image));
                                img.Save(imageSavePath);
                            }
                            if (saveImageName != null || saveImageName != "")
                            {
                                SqlCmd = new SqlCommand(ApiConstant.SProc_DVS_UploadInvoiceImage, dbConnectionObj.ApiConnection);
                                SqlCmd.CommandType = CommandType.StoredProcedure;

                                SqlCmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
                                SqlCmd.Parameters.AddWithValue("@InvImageName", saveImageName);
                                SqlCmd.Parameters.AddWithValue("@InvImagePath", ApiConstant.InvoicePDF);

                                da = new SqlDataAdapter(SqlCmd);
                                dbConnectionObj.ConnectionOpen();
                                da.Fill(dtImage);
                                dbConnectionObj.ConnectionClose();
                            }
                            count++;
                        }
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UploadDVSInvoice", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UploadDVSInvoice", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                dtImage = null;
            }
            return null;
        }
        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        public string GetUplodedInvoiceList(SearchUplodedInvoice search)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtImage = new DataTable();
            string jsonString;
            string filePath = string.Empty;
            string path = string.Empty;
            String file = String.Empty;
            List<InvoiceResponse> invoices = new List<InvoiceResponse>();
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_DVS_GetUplodedInvoiceList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@DriverId", string.IsNullOrEmpty(search.DriverId) ? DBNull.Value : (object)search.DriverId);
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", string.IsNullOrEmpty(search.InvoiceNo) ? DBNull.Value : (object)search.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@Status", string.IsNullOrEmpty(search.Status) ? DBNull.Value : (object)search.Status);
                SqlCmd.Parameters.AddWithValue("@CustomerId", string.IsNullOrEmpty(search.CustomerId) ? DBNull.Value : (object)search.CustomerId);
                SqlCmd.Parameters.AddWithValue("@Fromdate", (search.Fromdate) == DateTime.MinValue ? DBNull.Value : (object)search.Fromdate);
                SqlCmd.Parameters.AddWithValue("@Todate", (search.Todate) == DateTime.MinValue ? DBNull.Value : (object)search.Todate);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                //System.Data.DataColumn newColumn = new System.Data.DataColumn("PDFBase64", typeof(System.String));
                //dt.Columns.Add(newColumn);
                //if (dt.Rows.Count > 0)
                //{
                //    if (dt.Rows[0]["PDFName"] != "" || dt.Rows[0]["PDFName"] != null)
                //    {
                //        path = System.Web.Hosting.HostingEnvironment.MapPath("~/InvoicePDF");

                //        for (int i = 0; i < dt.Rows.Count; i++)
                //        {
                //            filePath = path + "\\" + dt.Rows[i]["PDFName"];
                //            Byte[] bytes = File.ReadAllBytes(filePath);
                //            file = Convert.ToBase64String(bytes);

                //            invoices.Add(new InvoiceResponse
                //            {
                //                DriverId = Convert.ToString(dt.Rows[i]["DriverId"]),
                //                CustomerId = Convert.ToString(dt.Rows[i]["CustomerId"]),
                //                InvoiceNo = Convert.ToString(dt.Rows[i]["InvoiceNo"]),
                //                UploadDate = Convert.ToString(dt.Rows[i]["UploadDate"]),
                //                SignatoryName = Convert.ToString(dt.Rows[i]["SignatoryName"]),
                //                Status = Convert.ToString(dt.Rows[i]["Status"]),
                //                PDFBase64 = file
                //            });
                //        }
                //    }
                //}

                //ListtoDataTable lsttodt = new ListtoDataTable();
                //dt = lsttodt.ToDataTable(invoices);// list to datatable

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetUplodedInvoiceList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetUplodedInvoiceList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                dtImage = null;
            }
            return null;
        }
    }
    public class ImgReuslt
    {
        public string Msg { get; set; }
        public string CustomerId { get; set; }
    }
    public class ListtoDataTable
    {
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
    }
}