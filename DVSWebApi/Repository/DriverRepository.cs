﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Configuration;
using DVSWebApi.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DVSWebApi.Repository
{
    public class DriverRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnectionSB3 dbConnectionObj = null;
        DBConnectionOAuth dbOAuthConnectionObj = null;
        SqlCommand OAuthcmd = null;
        SqlCommand SqlCmd = null;
        DataTable dtl;
        DataToJson dtToJson = new DataToJson();

        //public string DriverLogin(Driver _Driver) // For DriverRegistration
        //{
        //    dbConnectionObj = new DBConnectionSB3();
        //    String jsonString = string.Empty;
        //    try
        //    {
        //        Boolean flag = false;
        //        dtl = new DataTable();
        //        string passwd = EncodePasswordToBase64(_Driver.Password);
        //        dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetLoginInfo, "@DriverLogID", _Driver.UserId, "@Password", passwd, "@CompanyID", _Driver.CompanyID);

        //        if (dtl.Rows.Count >= 0)
        //        {
        //            dtToJson = new DataToJson();
        //            // Convert Datatable to JSON string
        //            //jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dtl, "GetLoginInfo", true));
        //            jsonString = dtToJson.convertDataTableToJson(dtl, "GetLoginInfo", true);
        //        }
        //        else
        //            jsonString = dtToJson.convertDataTableToJson(dtl, "GetLoginInfo", false);
        //        return jsonString;
        //    }
        //    catch (Exception ex)
        //    {
        //        jsonString = dtToJson.convertDataTableToJson(dtl, "GetLoginInfo", false);

        //        //throw;
        //    }
        //    finally
        //    {
        //        dtl.Dispose();
        //    }
        //    return null;
        //}

        public string DriverRegistration(Driver _Driver) // For DriverRegistration
        {
            string jsonString;
            dbConnectionObj = new DBConnectionSB3();
            try
            {
                String EncodedPasswd = EncodePasswordToBase64(_Driver.UserPassword);
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcInsertDriverRecord, "@DriverLoginID", _Driver.DriverLoginId, "@CompanyID", _Driver.CompanyID, "@DriverName", _Driver.DriverName, "@Address",
                    _Driver.Address, "@City", _Driver.City, "@State", _Driver.State, "@Zip", _Driver.Zip, "@Country", _Driver.Country, "@Email", _Driver.Email, "@ContactNo", _Driver.ContactNo,
                    "@DrivingLicense", _Driver.DrivingLicense, "@WarehouseID", _Driver.WarehouseID,
                    "@DriverImage", _Driver.DriverImage, "@DriverImagePath", _Driver.DriverImagePathTxt, "@UserID", _Driver.UserID, "@UserType", 1, "@UserPassword", EncodedPasswd);

                //if (dtl.Rows.Count >= 0)
                //{
                //    flag = true;
                //    //FireBase_PushNitification fbPush_Notify = new FireBase_PushNitification();
                //    //fbPush_Notify.FB_PushNotification("Order Submitted Successfully.", "Order No. : " + Convert.ToString(dt.Rows[0]["PONumber"]));
                //}
                jsonString = dtToJson.convertDataTableToJson(dtl, "DriverRegistration", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "DriverRegistration", false);
                log.Error("->Method: DriverRegistration(Driver _Driver) " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                dtl.Dispose();
            }
            return "";
        }

        public string GetRefreshTokenData(string DeviceID, string TokenID, String UserID)
        {
            //dbOAuthConnectionObj = new DBConnectionOAuth();
            dbConnectionObj = new DBConnectionSB3();
            DataTable dt = null;
            SqlDataAdapter da = null;
            string jsonString;
            try
            {
                dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.spGetRefreshToken, "@DeviceID", DeviceID, "@TokenID", TokenID);
                //dbOAuthConnectionObj = new DBConnectionOAuth();
                //SqlCmd = new SqlCommand(ApiConstant.spGetRefreshToken, dbOAuthConnectionObj.ApiConnection);
                //SqlCmd.CommandType = CommandType.StoredProcedure;

                //SqlCmd.Parameters.AddWithValue("@DeviceID", DeviceID);
                //SqlCmd.Parameters.AddWithValue("@TokenID", TokenID);
                //SqlCmd.Parameters.AddWithValue("@UserID", UserID);
                //da = new SqlDataAdapter(SqlCmd);
                //dt = new DataTable();
                //dbOAuthConnectionObj.ConnectionOpen();
                //da.Fill(dt);
                //dbOAuthConnectionObj.ConnectionClose();

                log.Debug("-> Method: GetRefreshTokenData(string DeviceID, string TokenID) - Connection stablish to Firebase by the Device and token");

                jsonString = dtToJson.convertDataTableToJson(dt, "GetRefreshTokenData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetRefreshTokenData", false);
                log.Error("->Method: GetRefreshTokenData(string DeviceID, string TokenID) " + ex);
            }
            finally
            {
                //dbOAuthConnectionObj = null;
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }

            return "";
        }

        public static string EncodePasswordToBase64(string password)
        {
            try
            {
                byte[] encData_byte = new byte[password.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        } //this function Convert to Decord your Password

        public static string DecodeFrom64(string encodedData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encodedData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }




        public HttpResponseMessage GetAllDriverList(String CompanyID, String SearchTxt)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                //SqlParameter[] objParam = new SqlParameter[1];
                //objParam[0] = new SqlParameter("@SearchText", SqlDbType.VarChar, 100);
                //objParam[0].Value = Convert.ToString(search.SearchText);
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@CompanyID", SqlDbType.VarChar, 50);
                objParam[0].Value = CompanyID;
                objParam[1] = new SqlParameter("@SearchTxt", SqlDbType.VarChar, 100);
                objParam[1].Value = SearchTxt;
                ret = objAccess.GetDataTable(ApiConstant.getAllDriverList, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetAllDriverList", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }

        public HttpResponseMessage GetDriverDetailsByIDdata(String CompanyID, String DriverLoginID)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@CompanyID", SqlDbType.VarChar, 50);
                objParam[0].Value = CompanyID;
                objParam[1] = new SqlParameter("@DriverLoginID", SqlDbType.VarChar, 50);
                objParam[1].Value = DriverLoginID;
                ret = objAccess.GetDataTable(ApiConstant.getDriverDetailsByID, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetDriverDetailsByID", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }

        public HttpResponseMessage GetAllActiveVehiclesType(SearchBase search)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@SearchText", SqlDbType.VarChar, 100);
                objParam[0].Value = search.SearchText;

                ret = objAccess.GetDataTable(ApiConstant.getAllActiveVehiclesType, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetAllActiveVehiclesType", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }

        public HttpResponseMessage GetAllVehiclesList(SearchBase search)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@SearchText", SqlDbType.VarChar, 100);
                objParam[0].Value = search.SearchText;
                ret = objAccess.GetDataTable(ApiConstant.getAllActiveVehiclesList, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetAllVehiclesList", false);
            }
            finally
            {
                objJson = null;
                objdt = null;
                objAccess = null;
            }
            return null;
        }

        public Boolean insertVehiclesType(VehiclesType objVehiclesType)
        {
            int ret = 0;
            clsDataAccess objAccess = new clsDataAccess();
            SqlParameter[] objParam = new SqlParameter[6];
            objParam[0] = new SqlParameter("@CompanyID", SqlDbType.NVarChar, 10);
            objParam[0].Value = Convert.ToString(objVehiclesType.CompanyId);

            objParam[1] = new SqlParameter("@TypeName", SqlDbType.NVarChar, 50);
            objParam[1].Value = objVehiclesType.TypeName;

            objParam[2] = new SqlParameter("@TypeDescription", SqlDbType.NVarChar, 50);
            objParam[2].Value = objVehiclesType.TypeDescription;

            objParam[3] = new SqlParameter("@IsActive", SqlDbType.Bit);
            objParam[3].Value = Convert.ToBoolean(objVehiclesType.IsActive);

            objParam[4] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 50);
            objParam[4].Value = Convert.ToString(objVehiclesType.CreatedBy);

            objParam[5] = new SqlParameter("@CreatedAt", SqlDbType.DateTime);
            objParam[5].Value = Convert.ToDateTime(objVehiclesType.CreatedAt);

            ret = objAccess.ExecuteNonQuery(ApiConstant.insertVehiclesType, ApiConstant.apiConnectionString, ref objParam);
            if (ret > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public Boolean insertDriverDetails(DriverModel driver)
        {
            int ret = 0;
            clsDataAccess objAccess = new clsDataAccess();
            SqlParameter[] objParam = new SqlParameter[19];

            objParam[0] = new SqlParameter("@DriverLoginID", SqlDbType.VarChar, 50);
            objParam[0].Value = Convert.ToString(driver.DriverLoginId);

            objParam[1] = new SqlParameter("@CompanyID", SqlDbType.VarChar, 10);
            objParam[1].Value = Convert.ToString(driver.CompanyID);

            objParam[2] = new SqlParameter("@DriverName", SqlDbType.VarChar, 50);
            objParam[2].Value = Convert.ToString(driver.DriverName);

            objParam[3] = new SqlParameter("@Address", SqlDbType.VarChar, 100);
            objParam[3].Value = Convert.ToString(driver.Address);

            objParam[4] = new SqlParameter("@City", SqlDbType.VarChar, 50);
            objParam[4].Value = Convert.ToString(driver.City);

            objParam[5] = new SqlParameter("@State", SqlDbType.Int);
            objParam[5].Value = Convert.ToInt32(driver.State);

            objParam[6] = new SqlParameter("@Zip", SqlDbType.VarChar, 8);
            objParam[6].Value = Convert.ToString(driver.Zip);

            objParam[7] = new SqlParameter("@Country", SqlDbType.Int);
            objParam[7].Value = Convert.ToInt32(driver.Country);

            objParam[8] = new SqlParameter("@Email", SqlDbType.VarChar, 100);
            objParam[8].Value = Convert.ToString(driver.Email);

            objParam[9] = new SqlParameter("@ContactNo", SqlDbType.VarChar, 100);
            objParam[9].Value = Convert.ToString(driver.ContactNo);

            objParam[10] = new SqlParameter("@DrivingLicense", SqlDbType.VarChar, 50);
            objParam[10].Value = Convert.ToString(driver.DrivingLicenceNo);

            objParam[11] = new SqlParameter("@WarehouseID", SqlDbType.VarChar, 10);
            objParam[11].Value = Convert.ToString(driver.WarehouseID);

            objParam[12] = new SqlParameter("@FromDate", SqlDbType.VarChar, 10);
            objParam[12].Value = Convert.ToString(driver.FromDate);

            objParam[13] = new SqlParameter("@ToDate", SqlDbType.VarChar, 10);
            objParam[13].Value = Convert.ToString(driver.ToDate);

            objParam[14] = new SqlParameter("@RegistrationDate", SqlDbType.NVarChar, 10);
            objParam[14].Value = Convert.ToString(driver.RegistrationDate);

            objParam[15] = new SqlParameter("@DriverImage", SqlDbType.NVarChar, 150);
            objParam[15].Value = Convert.ToString(driver.DriverImage);

            objParam[16] = new SqlParameter("@DriverImagePath", SqlDbType.NVarChar, 150);
            objParam[16].Value = Convert.ToString(driver.DriverImagePath);

            objParam[17] = new SqlParameter("@UserID", SqlDbType.NChar, 50);
            objParam[17].Value = Convert.ToString(driver.UserID);

            objParam[18] = new SqlParameter("@UserType", SqlDbType.Int);
            objParam[18].Value = Convert.ToInt32(driver.UserType);

            ret = objAccess.ExecuteNonQuery(ApiConstant.insertDriverDetails, ApiConstant.apiConnectionString, ref objParam);
            if (ret > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public HttpResponseMessage GetAllWarehouseList(String CompanyID )
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@CompanyId", SqlDbType.VarChar, 10);
                objParam[0].Value = CompanyID;
                ret = objAccess.GetDataTable(ApiConstant.GetWarehouseListByComp, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetAllWarehouseList", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }

        public HttpResponseMessage ValidateLoginUserData(String LoginUserID, String CompanyID)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@LoginUserID", SqlDbType.VarChar, 50);
                objParam[0].Value = LoginUserID;
                objParam[1] = new SqlParameter("@CompanyId", SqlDbType.VarChar, 10);
                objParam[1].Value = CompanyID;
                ret = objAccess.GetDataTable(ApiConstant.ValidateLoginUser, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }

                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "ValidateLoginUser", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }
    }
}