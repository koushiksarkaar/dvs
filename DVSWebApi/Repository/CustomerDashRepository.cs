﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DVSWebApi.Repository
{
    public class CustomerDashRepository
    {

        public HttpResponseMessage GetCustomerDashboardData()
        {
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            string strJsonData = string.Empty;
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                ret = objAccess.GetDataTable(ApiConstant.SProcGetCustomerDashboardList, ApiConstant.apiConnectionString, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }
                
                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "TotalOrderCountforDashboard", false);
            }
            finally
            {
                objJson = null;
                objAccess = null;
                objdt = null;
            }
            return null;
        }


    }
}