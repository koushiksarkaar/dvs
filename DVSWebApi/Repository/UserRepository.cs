﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DVSWebApi.Repository
{
    public class UserRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        public void GetUserDetails(string brokerId, out int companyId, out string userName)
        {
            companyId = 0;
            userName = string.Empty;

            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
               // custList = new List<CustomerDetail>();
                dbConnectionObj = new DBConnectionOAuth();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetUserDetails, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@userId", brokerId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                log.Debug("-> Method: GetUserDetails(string brokerId, out int companyId, out string userName) - Connection stablish to OMSWebApi Database successfully");
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

               // companyId = Convert.ToInt32(dt[0][1]);

                foreach(DataRow dr in dt.Rows)
                {
                    companyId = Convert.ToInt32(dr["CompanyID"]);
                    userName = dr["Name"].ToString();
                }


             //   string jsonString;
             //   jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomersDetailList", true);
               // return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("->Method: GetUserDetails(string brokerId, out int companyId, out string userName) " + ex);
            }
            finally
            {
                dbConnectionObj = null;
               // custList = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }

           // return "";
        }



    }
}