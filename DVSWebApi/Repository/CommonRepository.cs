﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using System.Xml;
using System.Net.Http;
using System.Net.Http.Headers;
namespace DVSWebApi.Repository
{
    public class CommonRepository
    {

       

        //For this first we need to download JSON.Net DLL. 
        //We can download it from Nuget.org and then import the Newtonsoft.JSON namespace into our page as in
        //the following code.JSON.NET is a popular high-performance JSON framework for .NET.

        //public static HttpResponseMessage DataTableToJSONWithJSONNet(DataTable table)
        //{
        //    string JsonString = string.Empty;
        //    JsonString = JsonConvert.SerializeObject(table);
        //    StringContent Sc = new StringContent(JsonString);
        //    Sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //    HttpResponseMessage Data = new HttpResponseMessage();
        //    Data.Content = Sc;
        //    return Data;
        //    
        //}

        public static HttpResponseMessage GetJsonResponse(DataTable objDt,string strMethod)
        {
            DataToJson dtToJson = new DataToJson();
            string JsonString = string.Empty;
            JsonString = dtToJson.convertDataTableToJson(objDt, strMethod, true);
            StringContent Sc = new StringContent(JsonString);
            Sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage Data = new HttpResponseMessage();
            Data.Content = Sc;
            return Data;            
        }

        public static string GetSPNane(string XMLNode)
        {
            try
            {
                XmlDocument OMSXml = new XmlDocument();
                string xmlPath = HttpContext.Current.Server.MapPath("/App_Data/OMSXML.xml");
                OMSXml.Load(xmlPath);
                XmlNode Node = OMSXml.DocumentElement.SelectSingleNode(XMLNode);
                string strContent = Node.InnerText;
                return strContent;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}