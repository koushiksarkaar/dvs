﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DVSWebApi.Models;
using DVSWebApi.Repository;
using System.Data.SqlClient;
using System.IO;

namespace DVSWebApi.Repository
{
    public class OrderSummeryRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnectionSB3 dbConnectionObj = null;
        DataTable dtl;
        DBConnection dbConnection = null;
        DataToJson dtToJson = new DataToJson();
        SqlCommand SqlCmd = null;
        //DBConnectionOAuth dbOAuthConnectionObj = null;  
        public String GetOrderSummeryData(String OrderNo, String CompnayID, String TripID) // fetch Order Summery details respective OrderNo and Trip ID
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetOrderSummery, "@OrderNo", OrderNo, "@CompanyID", CompnayID, "@TripID", TripID);
                if (dtl != null)
                {
                    String ProductImagePath = ApiConstant.ProductImage;
                    foreach (DataRow _dr in dtl.Rows)//(int i =0; dtl.Rows.Count > 0; i++)
                    {
                        String ProductCode = Convert.ToString(_dr["ProductCode"]);
                        if (!File.Exists(ProductImagePath + ProductCode + ".JPG"))
                            _dr["HasProductImage"] = "0";
                        else
                            _dr["HasProductImage"] = "1";
                    }
                }
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetOrderSummeryData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetOrderSummeryData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetOrderSummeryData

        public string InsertStopOrderSummery(StopOrderSummeryModels objOrdSummry)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString = String.Empty;
            String TripID = String.Empty, CompID = String.Empty, CustID = String.Empty, DriverID = String.Empty, ActualEndTime = String.Empty;

            try
            {
                IEnumerable<OrderDetailsList> stopOrderLst = objOrdSummry.orderDetailsLists;
                DataTable dtlStopOrd = new DataTable();
                dtlStopOrd.Columns.Add("OrderNumber", typeof(string));
                dtlStopOrd.Columns.Add("ToDoComments", typeof(string));

                foreach (var itms in stopOrderLst)
                {
                    dtlStopOrd.Rows.Add(itms.OrderNumber, itms.ToDoComments);
                    if (String.IsNullOrWhiteSpace(TripID))
                    {
                        TripID = itms.TripID;
                        CompID = itms.CompanyID;
                        CustID = itms.CustomerID;
                        DriverID = itms.DriverID;
                        ActualEndTime = itms.ActualEndTime;
                    }
                }

                StopOrderSummeryModels stopOrdSummryyModel = new StopOrderSummeryModels();
                stopOrdSummryyModel.orderDetailsLists = objOrdSummry.orderDetailsLists;

                DataSet dsStopOrder = new DataSet();
                dsStopOrder.Tables.Add(dtlStopOrd);
                String orderLst = dsStopOrder.GetXml();
                dt = new DataTable();
                dbConnection = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDVSUpdateStopSummery, dbConnection.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;

                SqlCmd.Parameters.AddWithValue("@CustomerID", CustID);
                SqlCmd.Parameters.AddWithValue("@TripID", TripID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                SqlCmd.Parameters.AddWithValue("@DriverID", DriverID);
                SqlCmd.Parameters.AddWithValue("@ActualEndTime", ActualEndTime);

                SqlCmd.Parameters.AddWithValue("@XML_OrderList", orderLst);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnection.ConnectionOpen();
                da.Fill(dt);
                dbConnection.ConnectionClose();

                //dt = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcDVSUpdateStopSummery, "@TripID", TripID, "@CompanyID", CompID, "@OrderNo", OrderNo, "@Invoice_Number", Invoice_Number,
                //    "@OrderQty", OrdQty, "@OrderAmount", OrderAmount, "@Latitude", Lat, "@Longitude", Lng, "@Location", Location, "@DeliveredBy", DeliveredBy, "@XML_OrderDtl", DelvryItms);

                jsonString = dtToJson.convertDataTableToJson(dt, "InsertStopOrderSummery", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertStopOrderSummery", false);
            }
            finally
            {
                dbConnection = null;
                SqlCmd = null;
                dt.Dispose();
                da = null;
            }
            return null;
        }

        public String GetCancelReasonTextData(String CompnayID ) // fetch Order Summery details respective OrderNo and Trip ID
        {
            dbConnectionObj = new DBConnectionSB3();
            String jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetCancelReasonText,  "@CompanyID", CompnayID);
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetCancelReasonTextData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetCancelReasonTextData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetCancelReasonTextData


    }
}