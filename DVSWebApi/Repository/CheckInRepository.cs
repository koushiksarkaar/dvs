﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Configuration;
using DVSWebApi.Models;
using System.IO;

namespace DVSWebApi.Repository
{
    public class CheckInRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnectionSB3 dbConnectionObj = null;
        DBConnectionOAuth dbOAuthConnectionObj = null;
        SqlCommand OAuthcmd = null;
        SqlCommand SqlCmd = null;
        DataTable dtl;
        DataToJson dtToJson = new DataToJson();

        public String GetCheckIn(string TripID, String CompnayID, String CustomerID) // fetch Check In details respective customer ID
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetTripCustomerCheckIn, "@CompanyID", CompnayID, "@TripID", TripID, "@CustomerID", CustomerID);
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetCheckInData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetCheckInData", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetCheckIn

        public String InsertStopCheckIn(TripStopCheckIn objStopCheckIn) // insert Stop Check In details respective customer ID
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcInsertStopCheckIn, "@CustomerID", objStopCheckIn.CustomerID, "@CompanyID", objStopCheckIn.CompnayID, "@TripID", objStopCheckIn.TripID,
                    "@Latitude", Convert.ToDecimal(objStopCheckIn.Latitude), "@Longitude", Convert.ToDecimal(objStopCheckIn.Longitude), "@StartingStop", objStopCheckIn.StartingStop, "@CurrentStop", objStopCheckIn.CurrentStop, "@Address", objStopCheckIn.Address, "@StopDateTime", objStopCheckIn.StopDateTime, "@Mileage", objStopCheckIn.Mileage,
                    "@StopImageName", objStopCheckIn.StopImageName, "@Remarks", objStopCheckIn.Remarks, "@UserID", objStopCheckIn.UserID);

                try
                {
                    if (Convert.ToString(dtl.Rows[0]["InsertedStatus"]) == "Ok")
                    {
                        String ModelData = "Name: " + objStopCheckIn.CustomerID + ", ImageName: " + objStopCheckIn.StopImageName + ", TripID: " + objStopCheckIn.TripID;
                        log.Info("-> api/CheckIn/InsertStopCheckIn : Image Data: " + Convert.ToString(ModelData));

                        System.Drawing.Image img;
                        String path = ApiConstant.StopCheckIn_imagePath;
                        //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";
                        String ImgPath = path + "\\";
                        if (!String.IsNullOrEmpty(objStopCheckIn.StopImageData) && !string.IsNullOrEmpty(objStopCheckIn.StopImageName) && objStopCheckIn.StopImageName != "~" && objStopCheckIn.StopImageData != "~")
                        {
                            string extension = Path.GetExtension(ImgPath + objStopCheckIn.StopImageName);
                            ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                            img = objBase64.Base64ToImage(objStopCheckIn.StopImageData);                        //if (extension == ".png" || extension == ".gif")    img = Base64ToImage(objImg.ImageBinaryData.Substring(22)); else    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));
                            img.Save(ImgPath + objStopCheckIn.StopImageName, System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Method:InsertStopCheckIn" + ex.Message.ToString());
                }
                jsonString = dtToJson.convertDataTableToJson(dtl, "InsertStopCheckIn", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "InsertStopCheckIn", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//InsertStopCheckIn


        public String GetStopSummery2Data(string TripID, String CustomerID,String CompnayID, String UserID) // fetch Check In details respective customer ID
        {
            dbConnectionObj = new DBConnectionSB3();
            string jsonString;
            try
            {
                dtl = new DataTable();
                dtl = dbConnectionObj.ExecDataTabletProc(ApiConstant.SProcGetStopSummery2, "@TripID", TripID, "@CustomerID", CustomerID, "@UserName", UserID, "@CompanyID", CompnayID);
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetStopSummery2Data", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetStopSummery2Data", false);
                throw;
            }
            finally
            {
                dtl.Dispose();
                dbConnectionObj = null;
            }
        }//GetStopSummery2Data 




    }
}