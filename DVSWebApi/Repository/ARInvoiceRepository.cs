﻿using DVSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace DVSWebApi.Repository
{
    public class ARInvoiceRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// Get Customer List
        /// </summary>
        /// <param name="Customer"></param>
        /// <returns></returns>
        public string GetCustomerList(CustomerModels Customer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.GetCustomerList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CUSTOMERNAME", string.IsNullOrEmpty(Customer.CustomerName) ? DBNull.Value : (object)Customer.CustomerName);
                SqlCmd.Parameters.AddWithValue("@CUSTOMERCODE", string.IsNullOrEmpty(Customer.CustomerCode) ? DBNull.Value : (object)Customer.CustomerCode);
                SqlCmd.Parameters.AddWithValue("@PageSize", (Customer.PageSize <= 0) ? DBNull.Value : (object)Customer.PageSize);
                SqlCmd.Parameters.AddWithValue("@PageNumber", (Customer.PageNumber <= 0) ? DBNull.Value : (object)Customer.PageNumber);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerList", true);
                return jsonString;

            }
            catch (Exception)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Order Invoice List By Customer Code
        /// </summary>
        /// <param name="Customer"></param>
        /// <returns></returns>
        public string GetOrderInvoiceListByCustCode(CustomerModels Customer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.GetOrderAndLinvoiceList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerCode", string.IsNullOrEmpty(Customer.CustomerCode) ? DBNull.Value : (object)Customer.CustomerCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderInvoiceListByCustCode", true);
                return jsonString;

            }
            catch (Exception)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderInvoiceListByCustCode", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// Get Trip Details by Order No
        /// </summary>
        /// <param name="Customer"></param>
        /// <returns></returns>
        public string GetTripDetailsByOrderNo(CustomerModels Customer)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionDB2);
                OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetTripDetailsByOrderNo.ToString(), con);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                DB2cmd.Connection = con;
                Customer.basketId = Customer.OrderNo;
                DB2cmd.Parameters.AddWithValue("@ORDERNO", Customer.basketId);
                //  SqlCmd.Parameters.AddWithValue("@Action", action);
                con.Open();
                var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                dt.Load(dr);
                string newColumnName;

                // Change column name coming from DB2 which have double quotes in all column name
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    newColumnName = dt.Columns[i].ColumnName.ToString();
                    dt.Columns[i].ColumnName = newColumnName.Replace("\"", "");
                    dt.AcceptChanges();
                }
                con.Close();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetTripDetailsByOrderNo", true);
                return jsonString;
            }
            catch (Exception)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetTripDetailsByOrderNo", false);
                return null;
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                dt = null;
            }
            return null;
        }


        public string InsertARInvoiceData(InvoiceModel invoice)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtImage = new DataTable();
            //DataSet dsImage = new DataSet();
            string jsonString;
            var savePdfName = ""; //var signimageSavePath = "";
            string PdfSavePath = string.Empty;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.InsertARInvoice, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerNo", string.IsNullOrEmpty(invoice.CustomerNo) ? DBNull.Value : (object)invoice.CustomerNo);
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", string.IsNullOrEmpty(invoice.InvoiceNo) ? DBNull.Value : (object)invoice.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@CustName", string.IsNullOrEmpty(invoice.CustName) ? DBNull.Value : (object)invoice.CustName);
                SqlCmd.Parameters.AddWithValue("@InvoiceDate", invoice.UploadDate == DateTime.MinValue ? DBNull.Value : (object)invoice.UploadDate);
                SqlCmd.Parameters.AddWithValue("@IsDSD", invoice.IsDSD);
                SqlCmd.Parameters.AddWithValue("@TripID", string.IsNullOrEmpty(invoice.TripID) ? DBNull.Value : (object)invoice.TripID);
                SqlCmd.Parameters.AddWithValue("@StopID", string.IsNullOrEmpty(invoice.StopID) ? DBNull.Value : (object)invoice.StopID);
                SqlCmd.Parameters.AddWithValue("@DriverID", string.IsNullOrEmpty(invoice.DriverID) ? DBNull.Value : (object)invoice.DriverID);
                SqlCmd.Parameters.AddWithValue("@CheckerID", string.IsNullOrEmpty(invoice.CheckerID) ? DBNull.Value : (object)invoice.CheckerID);
                SqlCmd.Parameters.AddWithValue("@TruckId", string.IsNullOrEmpty(invoice.TruckId) ? DBNull.Value : (object)invoice.TruckId);
                SqlCmd.Parameters.AddWithValue("@BrokerID", string.IsNullOrEmpty(invoice.BrokerID) ? DBNull.Value : (object)invoice.BrokerID);
                SqlCmd.Parameters.AddWithValue("@Status", string.IsNullOrEmpty(invoice.Status) ? DBNull.Value : (object)invoice.Status);
                SqlCmd.Parameters.AddWithValue("@OrderNo", string.IsNullOrEmpty(invoice.OrderNo) ? DBNull.Value : (object)invoice.OrderNo);
                SqlCmd.Parameters.AddWithValue("@OrderDate", invoice.OrderDate == DateTime.MinValue ? DBNull.Value : (object)invoice.OrderDate);
                SqlCmd.Parameters.AddWithValue("@CreatedBy", string.IsNullOrEmpty(invoice.CreatedBy) ? DBNull.Value : (object)invoice.CreatedBy);

                if (!String.IsNullOrEmpty(invoice.PDFBase64))
                {
                    String StorePath = System.Web.Hosting.HostingEnvironment.MapPath("~/InvoiceMedia");
                    savePdfName = invoice.InvoiceNo + Path.GetExtension(invoice.PDFName);
                    PdfSavePath = StorePath + "\\" + savePdfName;
                    if (!String.IsNullOrEmpty(invoice.PDFBase64))
                    {
                        byte[] PDFDecoded = Convert.FromBase64String(invoice.PDFBase64);
                        FileStream obj = File.Create(PdfSavePath);
                        obj.Write(PDFDecoded, 0, PDFDecoded.Length);
                        obj.Flush();
                        obj.Close();
                    }
                }

                SqlCmd.Parameters.AddWithValue("@PDFName", savePdfName);
                SqlCmd.Parameters.AddWithValue("@PDFPath", ApiConstant.InvoiceMedia);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //if (Convert.ToString(dt.Rows[0]["InvoiceNo"]) != null)
                //{
                //    //var saveImageName = ""; var signImageName = "";
                //    string InvoiceNo = Convert.ToString(dt.Rows[0]["InvoiceNo"]);
                //    //IEnumerable<InvoiceDetails> ImageLst = invoice.invoiceDetails;
                //    //int count = 1;
                //    //System.Drawing.Image img;
                //    String StorePath = System.Web.Hosting.HostingEnvironment.MapPath("~/InvoiceMedia");
                //    //var savePdfName = ""; //var signimageSavePath = "";
                //    //string PdfSavePath = string.Empty;
                //    //foreach (var itms in ImageLst)
                //    //{
                //    var pdfName = invoice.PDFName;
                //    var pdf64 = invoice.PDFBase64;
                //    //var InvoiceimageName = itms.InvoiceImage;
                //    //var InvoiceImage64 = itms.InvoiceImageBase64;
                //    //var signimgName = itms.SignatureName;
                //    //var signimg64 = itms.SignImageBase64;

                //    if (!String.IsNullOrEmpty(pdf64))
                //    {
                //        //saveImageName = InvoiceNo + count.ToString() + Path.GetExtension(InvoiceimageName);
                //        //imageSavePath = StorePath + "\\" + saveImageName;
                //        //if (!String.IsNullOrEmpty(InvoiceImage64))
                //        //{
                //        //    img = Base64ToImage(Convert.ToString(InvoiceImage64));
                //        //    img.Save(imageSavePath);
                //        //}

                //        savePdfName = InvoiceNo + Path.GetExtension(pdfName);
                //        PdfSavePath = StorePath + "\\" + savePdfName;
                //        if (!String.IsNullOrEmpty(pdf64))
                //        {
                //            byte[] PDFDecoded = Convert.FromBase64String(pdf64);
                //            FileStream obj = File.Create(PdfSavePath);
                //            obj.Write(PDFDecoded, 0, PDFDecoded.Length);
                //            obj.Flush();
                //            obj.Close();
                //        }

                //        //signImageName = InvoiceNo + Path.GetExtension(signimgName);
                //        //signimageSavePath = StorePath + "\\" + signImageName;
                //        //if (!String.IsNullOrEmpty(signimg64))
                //        //{
                //        //    img = Base64ToImage(Convert.ToString(signimg64));
                //        //    img.Save(signimageSavePath);
                //        //}

                //        SqlCmd = new SqlCommand(ApiConstant.InsertARInvoiceDetails, dbConnectionObj.ApiConnection);
                //        SqlCmd.CommandType = CommandType.StoredProcedure;

                //        SqlCmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
                //        SqlCmd.Parameters.AddWithValue("@PDFName", savePdfName);
                //        SqlCmd.Parameters.AddWithValue("@PDFPath", ApiConstant.InvoiceMedia);
                //        //SqlCmd.Parameters.AddWithValue("@SignatureName", signImageName);
                //        //SqlCmd.Parameters.AddWithValue("@SignImage", ApiConstant.InvoiceMedia);
                //        //SqlCmd.Parameters.AddWithValue("@InvoiceImage", saveImageName);
                //        //SqlCmd.Parameters.AddWithValue("@InvoiceImagePath", ApiConstant.InvoiceMedia);

                //        da = new SqlDataAdapter(SqlCmd);
                //        dbConnectionObj.ConnectionOpen();
                //        da.Fill(dtImage);
                //        dbConnectionObj.ConnectionClose();
                //        //count++;
                //    }
                //    //}
                //}
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertARInvoiceData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertARInvoiceData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                dtImage = null;
            }
            return null;
        }

        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        public string GetInsertedInvoiceList(SearchInvoiceList search)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.GetInsertedInvoiceList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustNo", string.IsNullOrEmpty(search.CustomerNo) ? DBNull.Value : (object)search.CustomerNo);
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", string.IsNullOrEmpty(search.InvoiceNo) ? DBNull.Value : (object)search.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@FromDt", (search.FromDt) == DateTime.MinValue ? DBNull.Value : (object)search.FromDt);
                SqlCmd.Parameters.AddWithValue("@ToDt", (search.ToDate) == DateTime.MinValue ? DBNull.Value : (object)search.ToDate);
                SqlCmd.Parameters.AddWithValue("@DriverId", string.IsNullOrEmpty(search.DriverId) ? DBNull.Value : (object)search.DriverId);
                SqlCmd.Parameters.AddWithValue("@BrokerId", string.IsNullOrEmpty(search.BrokerId) ? DBNull.Value : (object)search.BrokerId);
                SqlCmd.Parameters.AddWithValue("@CreatedBy", string.IsNullOrEmpty(search.CreatedBy) ? DBNull.Value : (object)search.CreatedBy);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetInsertedInvoiceList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetInsertedInvoiceList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get invoice status for AR
        /// </summary>
        /// <returns></returns>
        public string GetOrderInvoiceStatus()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.GetARInvoiceStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderInvoiceStatus", true);
                return jsonString;

            }
            catch (Exception)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderInvoiceStatus", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// InsertApproveReject
        /// </summary>
        /// <param name="approve"></param>
        /// <returns></returns>
        public string InsertApproveReject(ApproveReject approve)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.ARInvoiceApproveReject, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@InvoiceNo", string.IsNullOrEmpty(approve.InvoiceNo) ? DBNull.Value : (object)approve.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@RejectReason", string.IsNullOrEmpty(approve.RejectReason) ? DBNull.Value : (object)approve.RejectReason);
                SqlCmd.Parameters.AddWithValue("@Action", string.IsNullOrEmpty(approve.Action) ? DBNull.Value : (object)approve.Action);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertApproveReject", true);
                return jsonString;

            }
            catch (Exception)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertApproveReject", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
    }
}