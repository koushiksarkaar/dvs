﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSWebApi.Repository
{
    public class clsDataAccess
    {
        public byte[] GetImageData(string strDSDImageData)
        {
            byte[] imgData = Encoding.ASCII.GetBytes(strDSDImageData);
            return imgData;
        }

        public int ExecuteNonQuery(string strSQL, string strConnection)
        {
            int ret = 0;
            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.Text;
                    objCom.CommandText = strSQL;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    try
                    {
                        objCon.Open();
                        objCom.ExecuteNonQuery();
                        ret = 1;
                    }
                    catch (SqlException ex)
                    {
                        ret = 0;
                    }
                    finally
                    {
                        if (objCom != null)
                        {
                            objCom.Dispose();
                        }
                        if (objCon.State == ConnectionState.Open)
                        {
                            objCon.Close();
                        }
                        if (objCon != null)
                        {
                            objCon.Dispose();
                        }
                    }
                }
            }
            return ret;
        }

        public int ExecuteNonQuery(string strStoredProc, string strConnection, ref SqlParameter[] objParams)
        {
            int ret = 0;

            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.StoredProcedure;
                    objCom.CommandText = strStoredProc;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    for (int i = 0; i < objParams.Length; i++)
                        objCom.Parameters.Add(objParams[i]);

                    try
                    {
                        objCon.Open();
                        objCom.ExecuteNonQuery();
                        ret = 1;
                    }
                    catch (SqlException ex)
                    {
                        ret = 0;
                    }
                    finally
                    {
                        objCom.Parameters.Clear();
                        if (objCom != null)
                        {
                            objCom.Dispose();
                        }
                        if (objCon.State == ConnectionState.Open)
                        {
                            objCon.Close();
                        }
                        if (objCon != null)
                        {
                            objCon.Dispose();
                        }
                    }
                }
            }
            return ret;
        }

        public string ExecuteScalar(string strSQL, string strConnection)
        {
            string strReturnData;

            strReturnData = "";
            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.Text;
                    objCom.CommandText = strSQL;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    try
                    {
                        objCon.Open();
                        strReturnData = Convert.ToString(objCom.ExecuteScalar());
                    }
                    catch (SqlException ex)
                    {
                        //throw (ex);
                    }
                    finally
                    {
                        objCom.Parameters.Clear();
                        if (objCom != null)
                            objCom.Dispose();
                        if (objCon.State == ConnectionState.Open)
                            objCon.Close();
                        if (objCon != null)
                            objCon.Dispose();
                    }
                }
            }
            return strReturnData;
        }

        public string ExecuteScalar(string strStoredProc, string strConnection, ref SqlParameter[] objParams)
        {
            string strReturnData;

            strReturnData = "";
            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.StoredProcedure;
                    objCom.CommandText = strStoredProc;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    for (int i = 0; i < objParams.Length; i++)
                        objCom.Parameters.Add(objParams[i]);

                    try
                    {
                        objCon.Open();
                        strReturnData = Convert.ToString(objCom.ExecuteScalar());
                    }
                    catch (SqlException ex)
                    {
                        //throw (ex);
                    }
                    finally
                    {
                        //Do Nothing;
                        if (objCom != null)
                            objCom.Dispose();
                        if (objCon.State == ConnectionState.Open)
                            objCon.Close();
                        if (objCon != null)
                            objCon.Dispose();
                    }
                }
            }
            return strReturnData;
        }

        public SqlDataReader GetDataReader(string strSQL, ref SqlConnection objCon)
        {
            SqlDataReader objrdr = null;

            using (SqlCommand objCom = new SqlCommand())
            {
                objCom.CommandType = CommandType.Text;
                objCom.CommandText = strSQL;
                objCom.Connection = objCon;
                objCom.CommandTimeout = 0;

                try
                {
                    objCon.Open();
                    objrdr = objCom.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (SqlException ex)
                {
                    //throw (ex);
                }
                finally
                {
                    //Do Nothing;
                }
            }
            return objrdr;
        }

        public SqlDataReader GetDataReader(string strStoredProc, ref SqlConnection objCon, ref SqlParameter[] objParams)
        {
            SqlDataReader objrdr = null;

            using (SqlCommand objCom = new SqlCommand())
            {
                objCom.CommandType = CommandType.StoredProcedure;
                objCom.CommandText = strStoredProc;
                objCom.Connection = objCon;
                objCom.CommandTimeout = 0;

                for (int i = 0; i < objParams.Length; i++)
                    objCom.Parameters.Add(objParams[i]);

                try
                {
                    objCon.Open();
                    objrdr = objCom.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (Exception ex)
                {
                    //throw (ex);
                }
                finally
                {
                    objCom.Parameters.Clear();

                }
            }
            return objrdr;
        }

        public int GetDataSet(string strSQL, string strConnection, ref DataSet objds)
        {
            int ret = 0;
            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.Text;
                    objCom.CommandText = strSQL;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    SqlDataAdapter objAdp = new SqlDataAdapter();
                    objAdp.SelectCommand = objCom;

                    try
                    {
                        objCon.Open();
                        objAdp.Fill(objds);
                        ret = 1;
                    }
                    catch (SqlException ex)
                    {
                        //throw (ex);
                        ret = 0;
                    }
                    finally
                    {
                        objAdp.Dispose();
                        if (objCom != null)
                            objCom.Dispose();
                        if (objCon.State == ConnectionState.Open)
                            objCon.Close();
                        if (objCon != null)
                            objCon.Dispose();
                    }
                }
            }
            return ret;
        }

        public int GetDataSet(string strStoredProc, string strConnection, ref SqlParameter[] objParams, ref DataSet objds)
        {
            int ret = 0;
            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.StoredProcedure;
                    objCom.CommandText = strStoredProc;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    for (int i = 0; i < objParams.Length; i++)
                        objCom.Parameters.Add(objParams[i]);

                    SqlDataAdapter objAdp = new SqlDataAdapter();
                    objAdp.SelectCommand = objCom;

                    try
                    {
                        objCon.Open();
                        objAdp.Fill(objds);
                        ret = 1;
                    }
                    catch (Exception ex)
                    {
                        //throw (ex);
                        ret = 0;
                    }
                    finally
                    {
                        objCom.Parameters.Clear();
                        objAdp.Dispose();
                        if (objCom != null)
                            objCom.Dispose();
                        if (objCon.State == ConnectionState.Open)
                            objCon.Close();
                        if (objCon != null)
                            objCon.Dispose();
                    }
                }
            }
            return ret;
        }

        public int GetDataTable(string strSQL, string strConnection, ref DataTable objdt)
        {
            int ret = 0;
            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.Text;
                    objCom.CommandText = strSQL;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    SqlDataAdapter objAdp = new SqlDataAdapter();
                    objAdp.SelectCommand = objCom;

                    try
                    {
                        objCon.Open();
                        objAdp.Fill(objdt);
                        ret = 1;
                    }
                    catch (Exception ex)
                    {
                        //throw (ex);
                        ret = 0;
                    }
                    finally
                    {
                        objCom.Parameters.Clear();
                        objAdp.Dispose();
                        if (objCom != null)
                            objCom.Dispose();
                        if (objCon.State == ConnectionState.Open)
                            objCon.Close();
                        if (objCon != null)
                            objCon.Dispose();
                    }
                }
            }
            return ret;
        }

        public int GetDataTable(string strStoredProc, string strConnection, ref SqlParameter[] objParams, ref DataTable objdt)
        {
            int ret = 0;
            using (SqlConnection objCon = new SqlConnection(strConnection))
            {
                using (SqlCommand objCom = new SqlCommand())
                {
                    objCom.CommandType = CommandType.StoredProcedure;
                    objCom.CommandText = strStoredProc;
                    objCom.Connection = objCon;
                    objCom.CommandTimeout = 0;

                    for (int i = 0; i < objParams.Length; i++)
                        objCom.Parameters.Add(objParams[i]);

                    SqlDataAdapter objAdp = new SqlDataAdapter();
                    objAdp.SelectCommand = objCom;

                    try
                    {
                        objCon.Open();
                        objAdp.Fill(objdt);
                        ret = 1;
                    }
                    catch (Exception ex)
                    {
                        //throw (ex);
                        ret = 0;
                    }
                    finally
                    {
                        objCom.Parameters.Clear();
                        objAdp.Dispose();
                        if (objCom != null)
                            objCom.Dispose();
                        if (objCon.State == ConnectionState.Open)
                            objCon.Close();
                        if (objCon != null)
                            objCon.Dispose();
                    }
                }
            }
            return ret;
        }


    }
}