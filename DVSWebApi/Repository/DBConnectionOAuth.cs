﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DVSWebApi.Repository
{
    public class DBConnectionOAuth
    {
        private SqlConnection _con;

        /// <summary>
        /// Create instance of SqlConnection.
        /// </summary>
        public SqlConnection ApiConnection
        {
            get
            {
                _con = new SqlConnection(ApiConstant.apiConnectionStringOAuth);
                return this._con;
            }
        }

        /// <summary>
        /// Open SQL Connection to connect to Database.
        /// </summary>
        public void ConnectionOpen()
        {
            if (_con.State != ConnectionState.Open)
            {
                _con.Close();
                _con.Open();
            }

        }

        /// <summary>
        /// Close SQL Connection from the Database.
        /// </summary>
        public void ConnectionClose()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
            }
        }

    }
}