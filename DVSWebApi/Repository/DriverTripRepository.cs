﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using DVSWebApi.Models;

namespace DVSWebApi.Repository
{
    public class DriverTripRepository
    {
        public HttpResponseMessage GetAllTripInformation(TripSearchBase search)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            SqlParameter[] objParam = new SqlParameter[1];
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                objParam[0] = new SqlParameter("@SearchText", SqlDbType.VarChar, 50);
                objParam[0].Value = search.searchtext;

                ret = objAccess.GetDataTable(ApiConstant.getAllTripInformation, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }
                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetAllTripInformation", false);
            }
            finally
            {
                objJson = null;
                objdt = null;
                objAccess = null;
            }
            return null;
        }

        public HttpResponseMessage getAllInProgressTrip()
        {
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            string strJsonData = string.Empty;
            ret = objAccess.GetDataTable(ApiConstant.getAllInProgressTrip, ApiConstant.apiConnectionString, ref objdt);
            if (ret > 0)
            {
                DataToJson objJson = new Repository.DataToJson();
                strJsonData = objJson.DataTableToJSON(objdt);
            }

            StringContent sc = new StringContent(strJsonData);
            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage data = new HttpResponseMessage();
            data.Content = sc;
            return data;
        }

        public HttpResponseMessage searchInProgressTripBySearchText(TripSearchBase search)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@SearchText", SqlDbType.VarChar, 100);
                objParam[0].Value = search.searchtext;

                ret = objAccess.GetDataTable(ApiConstant.searchInProgressTrip, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }
                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "searchInProgressTripBySearchText", false);
            }
            finally
            {
                objJson = null;
                objdt = null;
                objAccess = null;
            }
            return null;
        }


        public HttpResponseMessage GetTripSummeryBytripId(TripModels Trip)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@CompanyID", SqlDbType.VarChar, 10);
                objParam[0].Value = Trip.CompanyID;

                objParam[1] = new SqlParameter("@TripID", SqlDbType.VarChar, 10);
                objParam[1].Value = Trip.TripId;

                ret = objAccess.GetDataTable(ApiConstant.getTripSummeryByTripId, ApiConstant.apiConnectionString, ref objParam, ref objdt);
                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }
                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetTripSummeryBytripId", false);
            }
            finally
            {
                objJson = null;
                objdt = null;
                objAccess = null;
            }
            return null;
        }


        public HttpResponseMessage getTripMapViewByTripID(TripModels Trip)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@CompanyID", SqlDbType.VarChar, 10);
                objParam[0].Value = Trip.CompanyID;

                objParam[1] = new SqlParameter("@TripID", SqlDbType.VarChar, 10);
                objParam[1].Value = Trip.TripId;

                ret = objAccess.GetDataTable(ApiConstant.getTripMapViewBytripId, ApiConstant.apiConnectionString, ref objParam, ref objdt);

                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }
                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "getTripMapViewByTripID", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }

        public HttpResponseMessage GetTripCustomerCheckIn(TripModels Trip)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];
                objParam[0] = new SqlParameter("@CompanyID", SqlDbType.VarChar, 10);
                objParam[0].Value = Trip.CompanyID;

                objParam[1] = new SqlParameter("@TripID", SqlDbType.VarChar, 10);
                objParam[1].Value = Trip.TripId;

                objParam[2] = new SqlParameter("@CustomerID", SqlDbType.VarChar, 50);
                objParam[2].Value = Trip.CustomerID;

                ret = objAccess.GetDataTable(ApiConstant.GetTripCustomerCheckIn, ApiConstant.apiConnectionString, ref objParam, ref objdt);

                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }
                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetTripCustomerCheckIn", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }




        public HttpResponseMessage getDriverTripInformation(TripModels Trip)
        {
            string strJsonData = string.Empty;
            int ret = 0;
            DataTable objdt = new DataTable();
            clsDataAccess objAccess = new clsDataAccess();
            DataToJson objJson = new Repository.DataToJson();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@UserName", SqlDbType.NVarChar, 256);
                objParam[0].Value = Trip.UserName;

                objParam[1] = new SqlParameter("@CompanyID", SqlDbType.VarChar, 10);
                objParam[1].Value = Trip.CompanyID;

                ret = objAccess.GetDataTable(ApiConstant.DriverTripInformation, ApiConstant.apiConnectionString, ref objParam, ref objdt);

                if (ret > 0)
                {
                    strJsonData = objJson.DataTableToJSON(objdt);
                }
                StringContent sc = new StringContent(strJsonData);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                strJsonData = objJson.convertDataTableToJson(objdt, "GetTripCustomerCheckIn", false);
            }
            finally
            {
                objdt = null;
                objAccess = null;
                objJson = null;
            }
            return null;
        }


    }
}