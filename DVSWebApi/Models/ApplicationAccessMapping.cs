﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVSWebApi.Models
{
    public class ApplicationAccessMapping
    {
        public int AccessId { get; set; }
        public int ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public int ParentAccessId { get; set; }
        public string AccessName { get; set; }
        public string Description { get; set; }
        public int SortOrder { get; set; }
        public int AccessType { get; set; }
        public string AccessTypeName { get; set; }
        public int AccessControl { get; set; }
        public string AccessControlName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }

        public string TempApplicationDDL { get; set; }
        public string SelectedApplicationDDL { get; set; }
        public string TempAccessTypeDDL { get; set; }
        public string TempAccessControlDDl { get; set; }
        public IEnumerable<SelectListItem> Client { get; set; }

        public IEnumerable<ApplicationAccessMapping> AppAccessMapList { get; set; }
        public List<LookupMappingTable> ApplicationList { get; set; }
        public IEnumerable<LookupMappingTable> ParentAccessList { get; set; }
        public IEnumerable<LookupMappingTable> AccessTypeList { get; set; }
        public IEnumerable<LookupMappingTable> AccessControlList { get; set; }
    }
}