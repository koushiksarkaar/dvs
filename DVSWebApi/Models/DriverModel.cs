﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class DriverModel
    {
        public int DriverLoginId { get; set; }
        public string CompanyID { get; set; }
        public string DriverName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int State { get; set; }
        public string Zip { get; set; }
        public int Country { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string DrivingLicenceNo { get; set; }
        public string WarehouseID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string RegistrationDate { get; set; }
        public string DriverImage { get; set; }
        public string DriverImagePath { get; set; }
        public string UserID { get; set; }
        public int UserType { get; set; }
    }
    public class SearchBase
    {
        public string SearchText { get; set; }
    }
}