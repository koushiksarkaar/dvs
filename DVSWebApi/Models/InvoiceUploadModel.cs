﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class InvoiceUploadModel
    {
        public string DriverId { get; set; }
        public string CustomerId { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime UploadDate { get; set; }
        public string SignatoryName { get; set; }
        public string SignImage { get; set; }
        public string SignBase64Image { get; set; }
        public string PDFName { get; set; }
        public string PDFBase64 { get; set; }
        public string Status { get; set; }
        public IEnumerable<InvoiceImage> InvoiceUploadImage { get; set; }
    }
    public class InvoiceImage
    {
        public string ImageName { get; set; }
        public string ImageBase64String { get; set; }
    }

    public class SearchUplodedInvoice
    {
        public string DriverId { get; set; }
        public string InvoiceNo { get; set; }
        public string Status { get; set; }
        public string CustomerId { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
    }

    public class InvoiceResponse
    {
        public string DriverId { get; set; }
        public string CustomerId { get; set; }
        public string InvoiceNo { get; set; }
        public string UploadDate { get; set; }
        public string SignatoryName { get; set; }
        public string Status { get; set; }
        public string PDFBase64 { get; set; }
    }
}