﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class RoleDetails
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }

    }
}