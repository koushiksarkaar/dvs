﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class CustomerModels
    {
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string basketId { get; set; }
        public string OrderNo { get; set; }
    }
}