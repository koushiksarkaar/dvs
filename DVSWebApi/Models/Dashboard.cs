﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class Dashboard
    {
    }
    public class LoginInfo
    {
        public string ConfigID { get; set; }
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string LabelText { get; set; }
        public string FolderHeaderImage { get; set; }
        public string SortOrder { get; set; }
        public string CompanyID { get; set; }
    }
    public class DashBoard
    {
        public string ConfigID { get; set; }
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string LabelText { get; set; }
        public string FolderHeaderImage { get; set; }
        public string SortOrder { get; set; }
        public string CompanyID { get; set; }
    }
    public class DashBoardMiddle
    {
        public string ConfigID { get; set; }
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string LabelText { get; set; }
        public string FolderHeaderImage { get; set; }
        public string SortOrder { get; set; }
        public string CompanyID { get; set; }
    }
    public class DashBoardBottom
    {
        public string ConfigID { get; set; }
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string LabelText { get; set; }
        public string FolderHeaderImage { get; set; }
        public string SortOrder { get; set; }
        public string CompanyID { get; set; }
    }
    public class Menu
    {
        public string ConfigID { get; set; }
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string LabelText { get; set; }
        public string FolderHeaderImage { get; set; }
        public string SortOrder { get; set; }
        public string CompanyID { get; set; }
    }
    public class Header
    {
        public string ConfigID { get; set; }
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string LabelText { get; set; }
        public string FolderHeaderImage { get; set; }
        public string SortOrder { get; set; }
        public string CompanyID { get; set; }
    }
    public class Footer
    {
        public string ConfigID { get; set; }
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string LabelText { get; set; }
        public string FolderHeaderImage { get; set; }
        public string SortOrder { get; set; }
        public string CompanyID { get; set; }
    }

    public class CompanyConfig
    {
        public string CompanyID { get; set; }
        public string ImageFullPath { get; set; }
        public string DriverPhoneOnOff { get; set; }
        public string DollarSignOnOff { get; set; }
        public string ThemeColor { get; set; }
        public string PaymentDueDateOnOff { get; set; }
        public string PaymentTermOnOff { get; set; }
        public string PaymentDueDatePeriod { get; set; }
        public string TripAmountOnOff { get; set; }
        public string TripStopOnOff { get; set; }
        public string TripHeaderOnOff { get; set; }
        public string TripOrderCountOnOff { get; set; }
        public string TripDateOnOff { get; set; }
        public string CleanInvItemNameOnOff { get; set; }
        public string CustDashBackDatePeriod { get; set; }

        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyLogo { get; set; }
    }

}