﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class OrderModel
    {
        public string SearchText { get; set; }
        public string SalesmanID { get; set; }
        public int CompanyID { get; set; }
    }
}