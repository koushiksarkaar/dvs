﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DVSWebApi.Models
{
    public class Driver
    {
        [Required]
        [Display(Name = "DriverLoginId")]
        public string DriverLoginId { get; set; }

        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "DriverName")]
        public string DriverName { get; set; }
        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        public Int32 State { get; set; }
        [Required]
        [Display(Name = "Zip")]
        public string Zip { get; set; }
        public Int32 Country { get; set; }


        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "ContactNo")]
        public string ContactNo { get; set; }
        [Required]
        [Display(Name = "DrivingLicenseNo")]
        public string DrivingLicense { get; set; }
        [Required]
        [Display(Name = "WarehouseID")]
        public String WarehouseID { get; set; }
        public String FromDate { get; set; }
        public String ToDate { get; set; }
        public String RegistrationDate { get; set; }
        public String DriverImage { get; set; }
        public String DriverImagePathTxt { get; set; }
        public String CompanyID { get; set; }
        public string UserID { get; set; }
        public Int32 UserType { get; set; }
        [Required]
        [Display(Name = "UserPassword")]
        public string UserPassword { get; set; }
        //public String DeviceID { get; set; }
        //public String TokenID { get; set; }
    }

    public class RouteStopDetails
    {

        public string StopID { get; set; }
        public string SequenceNO { get; set; }
        public string Location { get; set; }

    }

    public class Trip
    {
        public string UserName { get; set; }
        public string RouteID { get; set; }
        public string CompanyID { get; set; }
        public string TripID { get; set; }
        public string CustomerID { get; set; }
        public string IsCustomerSkipped { get; set; }
        public string ActualStartTime { get; set; }
    }
    public class TripStopCheckIn
    {
        public string CustomerID { get; set; }
        public string TripID { get; set; }
        public string CompnayID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string StartingStop { get; set; }
        public string CurrentStop { get; set; }
        public string Address { get; set; }
        public DateTime StopDateTime { get; set; }
        public Decimal Mileage { get; set; }
        public string StopImageName { get; set; }
        public string StopImageData { get; set; }
        public string Remarks { get; set; }
        public string UserID { get; set; }
    }
    public class TripCount
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string DriverID { get; set; }
        public string CompanyID { get; set; }
    }


}