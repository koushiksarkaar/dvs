﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class RoleApplicationAccessMapping
    {
        public int ApplicationId { get; set; }
        public int? RoleId { get; set; }
        public int AccessId { get; set; }
        public string UserId { get; set; }
        public string AppId { get; set; }
        public string AccessName { get; set; }
        public string Description { get; set; }
        public string AccessTypeName { get; set; }
        public string AccessControlName { get; set; }
        public string PageURL { get; set; }
        public string SelectedRoleIds { get; set; }
        public string NotSelectedRoleIds { get; set; }
        public bool IsAccess { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string UserListToAssigne { get; set; }
        public IEnumerable<RoleApplicationAccessMapping> RoleAppAccessMapList { get; set; }
        public IEnumerable<RoleApplicationAccessMapping> UserAppRoleAssignedList { get; set; }
        public IEnumerable<RoleApplicationAccessMapping> UserAppRoleNotAssignedList { get; set; }
        public List<LookupMappingTable> ApplicationList { get; set; }
        public List<LookupMappingTable> RoleList { get; set; }
        
    }
}