﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class OrderSummeryModels
    {
        public string UserName { get; set; }
        public string OrderNo { get; set; }
        public string CompnayID { get; set; }
        public string TripID { get; set; }
    }
    
    public class StopOrderSummeryModels
    {
        public IEnumerable<OrderDetailsList> orderDetailsLists { get; set; }
    }
    
    public class OrderDetailsList
    {
        public string TripID { get; set; }
        public string CompanyID { get; set; }
        public string CustomerID { get; set; }
        public string DriverID { get; set; }
        public string OrderNumber { get; set; }
        public string ToDoComments { get; set; }
        public string ActualEndTime { get; set; }
    }


}