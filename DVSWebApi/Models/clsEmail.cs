﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net.Mail;

/// <summary>
/// Summary description for clsEmail
/// </summary>
public class clsEmail
{
    private  string strMailTo;
    private string strLanguageId;   
    private  int intCompanyId;
    private  int intMailId;
    private string strCustomerName;
    private string strCommends;
    private string strMailFrom;
    private string strMailSubject;
    private string strMailBody;
    //private string strAttach;
    //clsDataAccess objData = new clsDataAccess();

    public string CustomerName
    {
        get { return strCustomerName; }
        set { strCustomerName = value; }
    }

    public string LanguageId
    {
        get { return strLanguageId; }
        set { strLanguageId = value; }
    }

    public int CompanyId
    {
        get { return intCompanyId; }
        set { intCompanyId = value; }
    }

    public string Commends
    {
        get { return strCommends; }
        set { strCommends = value; }
    }

    public int MailID
    {
        get { return intMailId; }
        set { intMailId = value; }
    }

    public string MailTo
    {
        get { return strMailTo; }
        set { strMailTo = value; }
    }

    public string MailFrom
    {
        get { return strMailFrom; }
        set { strMailFrom = value; }
    }

    public string MailSubject
    {
        get { return strMailSubject; }
        set { strMailSubject = value; }
    }

    public string MailBody
    {
        get { return strMailBody; }
        set { strMailBody = value; }
    }

    //public string Attachment
    //{
    //    get { return strAttach; }
    //    set { strAttach = value; }
    //}

	public clsEmail()
	{
		//
		// TODO: Add constructor logic here
		//
	} 

    public void SendAuthorizationEmail(string strConnection)
    {
        StringBuilder strQuery = new StringBuilder();       
        string strMailBody;
        string strCC;
        string strSMTPServer;
        string strSMTPUser;
        string strSMTPPassword;
        string strSubject;
        string strCompanyName;
        string strLogoPath;
        string HTMLBody;

        strQuery.Append("SELECT tbl_CompanyMaster.CompanyName,tbl_OMS_MailInfo.Subject,tbl_OMS_MailInfo.BodyHTML,tbl_OMS_MailInfo.SMTPServer, ");
        strQuery.Append("tbl_OMS_MailInfo.SMTPUserId,tbl_OMS_MailInfo.SMTPPassword,tbl_OMS_MailInfo.Logo,tbl_OMS_MailInfo.CCTO ");
        strQuery.Append("FROM tbl_CompanyMaster INNER JOIN ");
        strQuery.Append("tbl_OMS_MailInfo ON tbl_CompanyMaster.CompanyID =tbl_OMS_MailInfo.CompanyId ");
        strQuery.Append("WHERE (tbl_OMS_MailInfo.CompanyId =" + intCompanyId + ") AND (tbl_OMS_MailInfo.MailId =" + intMailId + ") AND (tbl_OMS_MailInfo.LanguageId = '" + strLanguageId + "')");

        //DataTable dtMail = objData.getDataTable(strQuery.ToString(), strConnection);
        //if (dtMail.Rows.Count > 0)
        //{
        strCompanyName = "";//Convert.ToString(dtMail.Rows[0]["CompanyName"]);
        strMailBody =  "";//Convert.ToString(dtMail.Rows[0]["BodyHTML"]);
        strCC =  "";//Convert.ToString(dtMail.Rows[0]["CCTO"]);
        strSMTPServer =  "";//Convert.ToString(dtMail.Rows[0]["SMTPServer"]);
        strSMTPUser = "";// Convert.ToString(dtMail.Rows[0]["SMTPUserId"]);
        strSMTPPassword =  "";//Convert.ToString(dtMail.Rows[0]["SMTPPassword"]);
        strSubject = "";// Convert.ToString(dtMail.Rows[0]["Subject"]);
        strLogoPath = "";//Convert.ToString(dtMail.Rows[0]["Logo"]);
        HTMLBody = string.Format(strMailBody, strLogoPath, strCustomerName, strCompanyName);
        //}
        //else
        //{
        //    return;
        //}

        MailMessage mailMsg = new MailMessage();
        mailMsg.To.Add(strMailTo);
        if (strCC != "")
        {
            mailMsg.CC.Add(strCC);
        }
        mailMsg.Subject = strSubject;
        mailMsg.Body = HTMLBody;
        mailMsg.IsBodyHtml = true;
        MailAddress fromMail = new MailAddress(strSMTPUser);
        mailMsg.From = fromMail;
        //Attachment pdfReport = new Attachment(strPDFPath);
        //mailMsg.Attachments.Add(pdfReport);
        System.Net.Mail.SmtpClient SMTP = new System.Net.Mail.SmtpClient();
        SMTP.Host = strSMTPServer;
        SMTP.Port = 587;
        SMTP.Timeout = 600000;
        SMTP.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        SMTP.Credentials = new System.Net.NetworkCredential(strSMTPUser, strSMTPPassword);
        SMTP.Send(mailMsg);
    }


    public int SendGeneralizedMail(string strSMTPServer, string strSMTPUser, string strSMTPPassword)
    {
        int status;
        MailMessage objMessage;
        MailAddress objMailAddress;
        System.Net.Mail.SmtpClient objSmtpClient;
        status = 0;
        objMessage = new MailMessage();
        objMailAddress = new MailAddress(strMailFrom);
        objSmtpClient = new System.Net.Mail.SmtpClient();
        objMessage.To.Add(strMailTo);

        objMessage.Subject = strMailSubject;
        objMessage.IsBodyHtml = true;
        objMessage.Body = strMailBody;
        objMessage.From = objMailAddress;
        //Attachment pdfReport = new Attachment(strAttach);
        //objMessage.Attachments.Add(pdfReport);
        try
        {
            objSmtpClient.Host = strSMTPServer;
            objSmtpClient.Port = 587;
            objSmtpClient.Timeout = 600000;
            objSmtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            objSmtpClient.Credentials = new System.Net.NetworkCredential(strSMTPUser, strSMTPPassword);
            objSmtpClient.Send(objMessage);
            status = 1;
        }
        catch (HttpException ex)
        {
            status = 0;
        }
        return status;
    }

    public string GenerateMailBody(string strUserId, string strBody)
    {
        StringBuilder strMailBody = new StringBuilder();
        String MailPhoto = String.Empty, MailBody = String.Empty, MailFooter = String.Empty, Header = String.Empty;
        
        strMailBody.Append("<div style='padding:20px 20px 20px 20px; width:100%;'>");
        strMailBody.Append("<div style='text-align:center; width:90%;'> ");
        //strMailBody.Append("<img alt='Logo' src='" + MailPhoto + "' /> ");
        strMailBody.Append("</div> <br />");
        strMailBody.Append("<div style='text-align:left; width:90%; margin-top:20px;'>");
        strMailBody.Append("Hi " + strUserId + ",<br /><br /> ");
        strMailBody.Append(strBody  + MailFooter);
        strMailBody.Append("</div> <br /> <br />");
        strMailBody.Append("<br /><br /> Thanks,<br />Admin</div></div> ");
        return strMailBody.ToString();
    }


}