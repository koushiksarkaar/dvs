﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class TripModels
    {
        public DateTime StartDate { get; set; }
        public int TripStatus { get; set; }
        public int CompanyID { get; set; }
        public string UserName { get; set; }
        public string TripId { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
    }
    public class TripSearchBase
    {
        public string searchtext { get; set; }
    }
}