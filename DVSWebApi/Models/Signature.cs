﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class Signature
    {
        public SignatureDetails[] sign { get; set; }
        public string CreatedDate { get; set; }

        // public string UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int Creditid { get; set; }
    }

    public class SignatureDetails
    {
        public string SignatureType { get; set; }
        public string SignatureData { get; set; }

        public string SignatureName { get; set; }

    }
}