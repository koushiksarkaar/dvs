﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{

    public class DeliveryModels
    {
        public IEnumerable<DeliveryLoadItem> deliveryLoadItems { get; set; }
    }

    public class DeliveryLoadItem
    {
        public string TripID { get; set; }
        public string CompanyID { get; set; }
        public string OrderNo { get; set; }
        public string Invoice_Number { get; set; }
        public Int16 OrderQty { get; set; }
        public decimal OrderAmount { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Location { get; set; }
        public string DeliveredBy { get; set; }
        public string DeliveryDate { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Int32 Qty { get; set; }
        public Int32 DelQty { get; set; }
        public decimal ItemPrice { get; set; }
        public string Remarks { get; set; }
        public string CancelReason { get; set;  }
    }




    public class InvoiceModels
    {
        public string InvoiceNo { get; set; }
        public string OrderNumber { get; set; }
        public string TripID { get; set; }
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public string InvoiceDate { get; set; }
        public string DueDate { get; set; }
        public string DriverID { get; set; }
        public string Dept { get; set; }

        public int InvoiceTotalQty { get; set; }
        public decimal SubTotalAmount { get; set; }
        public decimal ShippingCharge { get; set; }
        public decimal Tax { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotalAmount { get; set; }

        public IEnumerable<InvoiceLoadItem> invoiceLoadItems { get; set; }
    }
    
    public class InvoiceLoadItem
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Int32 Qty { get; set; }
        public Int32 DespQty { get; set; }
        public Int32 DelQty { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal ItemTotalPrice { get; set; }

    }

    public class OrderView
    {
        public string OrderNo { get; set; }
        public string CompanyID { get; set; }
    }

    public class InvoiceView
    {
        public string TripID { get; set; }
        public string OrderNo { get; set; }
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public string DriverID { get; set; }
        public string InvoiceNo { get; set; }
    }

    public class GenerateInvoice
    {
        public string OrderNo { get; set; }
        public string CustomerID { get; set; }
        public string InvoiceNo { get; set; }
        public string TripID { get; set; }
        public string DriverID { get; set; }
        public string CompanyID { get; set; }
        public string StopLocation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

    public class DeliveryView
    {
        public string OrderNumber { get; set; }
        public string CompanyID { get; set; }
    }
    public class GenerateDSD
    {
        public string InvoiceNo { get; set; }
        public string TripID { get; set; }
        public string CompanyID { get; set; }
        public string CustomerID { get; set; }
        public string DriverID { get; set; }
        public DateTime DSDDate { get; set; }
        
        public string SignatureImageName { get; set; }
        public string SignatureImageBinaryData { get; set; }
        public string CaptureImageName { get; set; }
        public string CaptureImageBinaryData { get; set; }
        public string WhoSigned { get; set; }
    }

    public class AddSignature
    {
        public string InvoiceNo { get; set; }
        public string TripID { get; set; }
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public string DriverID { get; set; }
        public string SignedForTheDelivery { get; set; }
        public string SignatureImageName { get; set; }
        public string SignatureImageBinaryData { get; set; }
    }

    

}