﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class UserLoginDetails
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string Domain { get; set; }
        public string EmailId { get; set; }
        public string UserType { get; set; }
        public string UserFullName { get; set; }
        public string SelectedUserInputType { get; set; }
        public string SelectedUserInputValue { get; set; }
        public string ResponseMessage { get; set; }
        public string CompanyID { get; set; }
    }
}