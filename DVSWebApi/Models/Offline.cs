﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class Offline
    {
    }
    public class TripsInfo
    {
        public string TripID { get; set; }
        public string TripName { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        public string ActualStartTime { get; set; }
        public string ActualEndTime { get; set; }
        public string OrdersCount { get; set; }
        public string StopsCount { get; set; }
        public string ItemsCount { get; set; }
        public string StatusText { get; set; }
        public string StatusID { get; set; }
    }
    public class InputParameters
    {
        public string CompanyID { get; set; }
        public string DriverID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class OrdersInfo
    {
        public string TripID { get; set; }
        public string TripDetailID { get; set; }
        public string StartLocation { get; set; }
        public string CustomerNo { get; set; }
        public string ToDoComments { get; set; }
        public string IsStopSummeryDone { get; set; }

        public string IsToDo { get; set; }
        public string IsCustomerSkipped { get; set; }
        public string OrderDate { get; set; }
        public string OrderTotal { get; set; }
        public string TotalDiscount { get; set; }
        public string OrderQty { get; set; }
        public string OrderNo { get; set; }
        public string ItemReturn { get; set; }
        public string ItemExtra { get; set; }
        public string DeliveryFlag { get; set; }
        
    }
    public class CustomersInfo
    {
        public string TripID { get; set; }
        public string TripDetailID { get; set; }
        //public string StartDate { get; set; }
        //public string StartTime { get; set; }
        //public string EndDate { get; set; }
        //public string EndTime { get; set; }

        public string ActualStartTime { get; set; }
        public string ActualEndTime { get; set; }
        public string StartLocation { get; set; }
        public string CustomerNo { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public string Country { get; set; }
        public string ContactNo { get; set; }
        public string SalesmanID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string SpecificCustomer { get; set; }
        public string IsCustomerSkipped { get; set; }
        public string EmailId { get; set; }
        public string CustomerCheckInFlag { get; set; }
        public string CustomerStatusFlag { get; set; }

        public string DeliveryCharges { get; set; }
        public string CustomerNotes { get; set; }
        public string StopImageName { get; set; }
        public string StopImagePath { get; set; }
        

    }

    public class OrderDetails
    {
        public string TripID { get; set; }
        public string TripDetailID { get; set; }
        public string OrderNo { get; set; }
        public string ProductCode { get; set; }
        public string ItemName { get; set; }
        public string OrdQty { get; set; }

        public string CasePrice { get; set; }
        public string RetailPrice { get; set; }
        public string CompanyId { get; set; }
        public string CustomerID { get; set; }
        public string WHID { get; set; }
        public string Extra { get; set; }
        public string Returned { get; set; }
        public string ProductImage { get; set; }
        public string CaseUPC { get; set; }
        

    }
    public class InvoiceDSDImage
    {
        public string DSDHeader_Id { get; set; }
        public string InvoiceNo { get; set; }
        public string TripID { get; set; }
        public string CustomerID { get; set; }
        public string SignatureImageName { get; set; }
        public string CaptureImageName { get; set; }
        public string InvoiceFilePDF { get; set; }
        public string DSDPDFName { get; set; }
        public string ImagePath { get; set; }
    }

    public class InvoiceNoByOrderNo
    {
        public string TripID { get; set; }
        public string TripDetailID { get; set; }
        public string OrderNo { get; set; }
        public string CompanyID { get; set; }
        public string InvoiceNo { get; set; }
    }

    public class TripsModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<PayLoadTrip> payLoadTrips { get; set; }
    }

    public class PayLoadTrip
    {
        public String TripID { get; set; }
        public String TripStatus { get; set; }
        public String ActualStartDate { get; set; }
        public String ActualEndDate { get; set; }
    }

    public class CustomersModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<PayLoadCustomer> payLoadCustomers { get; set; }
    }

    public class PayLoadCustomer
    {
        public String TripID { get; set; }
        public Int64 TripDetailID { get; set; }
        public String CustomerNo { get; set; }
        public String ActualStartTime { get; set; }
        public String ActualEndTime { get; set; }
        public String IsCustomerSkipped { get; set; }
        public Boolean SpecificCustomer { get; set; }
        public String ActualStartDate { get; set; }
        public String ActualEndDate { get; set; }
        
    }

    public class StopCheckInModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<PayLoadCheckIn> payLoadCheckIns { get; set; }
    }

    public class PayLoadCheckIn
    {
        public String TripID { get; set; }
        public String CustomerNo { get; set; }
        public String Latitude { get; set; }
        public String Longitude { get; set; }
        public String Address { get; set; }
        public String StartingStop { get; set; }

        public String CurrentStop { get; set; }
        public String StopDateTime { get; set; }
        public Decimal Mileage { get; set; }
        public String StopImageName { get; set; }
        public String StopImagePath { get; set; }
        public String Remarks { get; set; }
        public String CreatedAt { get; set; }
        public String CreatedBy { get; set; }
    }


    public class OrderDetailsModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<OrderDetailsUpload> orderDetailsUploads { get; set; }
    }

    public class OrderDetailsUpload
    {
        public String TripID { get; set; }
        public String OrderNo { get; set; }
        public String CustomerID { get; set; }
        public String ProductCode { get; set; }
        public Int16 OrdQty { get; set; }
        public decimal CasePrice { get; set; }
        public decimal TotalPrice { get; set; }
        public Int16 TotalQty { get; set; }
        public String Reason { get; set; }
        public String Comments { get; set; }
        public Int16 Extra { get; set; }
        public Int16 Return { get; set; }
        public string ReturnDamageImage { get; set; }
    }

    public class InvoicDSDModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<InvoicDSDUpload> invoicDSDUploads { get; set; }
    }

    public class InvoicDSDUpload
    {
        public String InvoiceNo { get; set; }
        public String TripID { get; set; }
        public String CustomerID { get; set; }
        public String DriverID { get; set; }
        public DateTime DSDDate { get; set; }
        public String SignatureImageName { get; set; }
        public String CaptureImageName { get; set; }
        public String WhoSigned { get; set; }
        public String CleanInvoicePdfName { get; set; }
        public String DSDPdfName { get; set; }
    }

    public class DSDImageModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<DSDImage> DSDImages { get; set; }
    }

    public class DSDImage
    {
        public String SignatureImageName { get; set; }
        public String SignatureImageBinaryData { get; set; }
        public String CaptureImageName { get; set; }
        public String CaptureImageBinaryData { get; set; }
    }
    public class DSDMultipleImageModel
    {
        public string CompanyID { get; set; }
        public String InvoiceNo { get; set; }
        public String TripID { get; set; }
        public String CustomerID { get; set; }
        public String DriverID { get; set; }
        public DateTime DSDDate { get; set; }
        public IEnumerable<DSDMultipleImage> DSDMultipleImages { get; set; }
    }
    public class DSDMultipleImage
    {
        public String CaptureImageName { get; set; }
        public String CaptureImageBinaryData { get; set; }
    }
    public class CustomerStatusModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<CustomerStatusUpload> CustomerStatusUploads { get; set; }
    }

    public class CustomerStatusUpload
    {
        public String TripID { get; set; }
        public String CustomerID { get; set; }
        public String CustomerStatus { get; set; }


    }

}