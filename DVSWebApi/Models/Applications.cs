﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class Applications
    {
        public int ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string Description { get; set; }
      //  public string CurrentVersion { get; set; } // To be use for future

        //public DateTime? StartDate { get; set; }
        //public DateTime? EndDate { get; set; }
        //  public DateTime? CreateDate { get; set; }
        //public DateTime? UpdateDate { get; set; }
      //  public string StartDate { get; set; } // To be use for future
     //   public string EndDate { get; set; } // To be use for future
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
    }

    public class LookupMappingTable
    {
        public int CodeId { get; set; }
        public string Name { get; set; }
        public string Value1 { get; set; }
        public string Description { get; set; }
        public int ParentCodeId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
        public string SelectedValue { get; set; }
    }
}