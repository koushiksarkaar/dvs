﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class LoginUser
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string AppId { get; set; }
        public string CompanyID { get; set; }
        public string DeviceID { get; set; }
        public string TokenID { get; set; }
    }
}