﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class UserRoleAccessMap
    {
        public int AccessId { get; set; }
        public string AccessName { get; set; }
        public string Description { get; set; }
        public string AccessTypeName { get; set; }
        public string AccessControlName { get; set; }
        public bool IsAccess { get; set; }
    }
}