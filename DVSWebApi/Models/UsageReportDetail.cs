﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class UsageReportDetail
    {
        public string CompanyId { get; set; }
        public string DivisionId { get; set; }
        public string GroupId { get; set; }
        public string BrokerId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CustomerName { get; set; }
        public string Comments { get; set; }
        public string FileName { get; set; }
        public string FileData { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}