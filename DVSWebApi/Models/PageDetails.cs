﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;

namespace DVSWebApi.Models
{
    public class PageDetails
    {
        public String DefaultID { get; set; }
        public int ApplicationID { get; set; }
        public string ScreenID { get; set; }
        public string ControlID { get; set; }
        public string ControlType { get; set; }
        public string Description { get; set; }
        public string ColumnID { get; set; }
        public string ApplicationName { get; set; }
        public int AccessID { get; set; }
        public string RollID { get; set; }
        public String Visibility { get; set; }


    }
}