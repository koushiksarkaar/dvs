﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVSWebApi.Models
{
    public class InvoiceModel
    {
        public string CustomerNo { get; set; }
        public string InvoiceNo { get; set; }
        public string CustName { get; set; }
        public DateTime UploadDate { get; set; }
        public Boolean IsDSD { get; set; }
        public string TripID { get; set; }
        public string StopID { get; set; }
        public string DriverID { get; set; }
        public string CheckerID { get; set; }
        public string TruckId { get; set; }
        public string BrokerID { get; set; }
        public string Status { get; set; }
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string PDFName { get; set; }
        public string PDFBase64 { get; set; }
        public string CreatedBy { get; set; }
    }

    public class SearchInvoiceList
    {
        public string CustomerNo { get; set; }
        public string BrokerId { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime FromDt { get; set; }
        public DateTime ToDate { get; set; }
        public string DriverId { get; set; }
        public string CreatedBy { get; set; }

    }

    public class ApproveReject
    {
        public string InvoiceNo { get; set; }
        public string RejectReason { get; set; }
        public string Action { get; set; }
    }
}