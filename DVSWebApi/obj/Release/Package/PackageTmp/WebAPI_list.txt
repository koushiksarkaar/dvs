1> Login API 
http://107.21.119.157/DVSmobile/api/User									-- Login			{ "UserId":"D01",     "Password":"Pwd@d01",    "CompanyID":"2", "DeviceID":"864829031031542", "TokenID":"d23kCzUirLg:APA91bF5pTnJ-AHVoX8vs3ml-VfuL8VD3Ip0ZinoMlGuP_ojLVj7ockPJKJpUm0IxerQNPHMAZGPQkE5O5e7qgsEVoVCPcIvnJjXjLyZrJLaoCTMoPNCZsbW-sKvEHI2rhrPkrHULUFG" }
http://107.21.119.157/DVSmobile/api/Driver/Login							-- Login			{ "UserId":"D01",   "Password":"Pwd@d01",    "CompanyID":"2", }
																								  "UserId":"dvsadmin01",    "Password":"Pwd@dvsadmin01",    "CompanyID":"2",			
2> DVS user sign up 
http://107.21.119.157/DVSmobile/api/Driver/Register							-- Driver Registration {"DriverLoginId":"D02", "Name": "Name D02", "Address": "Address2", "City":"NJ", "State":31, "Zip":"09876", "Country":243, "Email" : "name02@test.com", "ContactNo": "1234567", "DrivingLicenseNo":"Lic0001", "WarehouseID":"01", "FromDate":"01-01-2019", "ToDate":"01-01-2030", "RegistrationDate":"07-07-2019", "DriverImage":"","DriverImagePath":"", "CompanyID":"2", "UserId":"dvsadmin", "UserType":1 }

3> Reset Password
http://localhost:50313/api/UserAuthorization/ResetPassword					-- Reset password      {"UserId": "D01", "CompanyID": "2", "Password":"Driver@01", "NewPassword":"Pwd@d01" }

4> To get refresh token for firebase
http://107.21.119.157/DVSmobile/api/Driver/GetRefreshToken					-- Refresh			{  "DeviceID":"",  "TokenID":"" }

5> To get the dashboard all data like Top, Middle & Bottom section,  menu, footer, header, login image,
http://107.21.119.157/DVSmobile/api/Dashboard/GetDVSAllInformation			--  		{ "CompanyID":"2"}

6> Get TRIP info
http://107.21.119.157/DVSmobile/api/Trip/GetTripInfo						-- 	{"UserName" : "D01", "CompnayID" : "2" }
7> To Get Trip Stop Summery
http://107.21.119.157/DVSmobile/api/Trip/GetTripStopSummery		 			--   { "CompnayID" : "2", "TripID" :"1" }

8> Trip STOP only
http://107.21.119.157/DVSmobile/api/Trip/GetTripStopDetails					-- 		{ "CompnayID" : "2", "TripID" :"1" }

	Map view of Delivery route
http://107.21.119.157/DVSmobile/api/Trip/GetTripMapView						--		{ "TripID" : "1", "CompanyID":"2" }

9> To get the check in info  
http://107.21.119.157/DVSmobile/api/CheckIn/GetCheckInData					--		{ "TripID" : "1", "CompanyID":"2", "CustomerID":"006969" }

10> insert stops check in
http://localhost:50313/api/CheckIn/InsertStopCheckIn						-- 	{"CustomerID":"003925", "TripID":1, "CompnayID":"2", "Latitude":"","Longitude":"", "Address":"":"", "StopDateTime":"","Mileage":10, "StopImageName":"", "StopImageData":"", "Remarks": "Remarks", "UserID":"d01" }

11> To get the STOP summery 2
http://localhost:50313/api/CheckIn/GetStopSummeryByCustomer					-- 	{"TripID":"1", "CustomerID":"006969", "CompanyID":"2", "UserName":"d01"}


12>	To get the Order Summery & Insert Delivery
http://localhost:50313/api/OrderSummery/GetOrderDetailSummery				-- To get the Order Summery {"OrderNo":"5902000251", "CompnayID":"2", "TripID":"1"}
http://localhost:50313/api/Delivery/InsertDelivery							-- Insert Delivery  {"payLoadItems":[{"TripID":"1","CompanyID":"2","OrderNo":"10011","Invoice_Number":"","OrderQty":3,"OrderAmount":37.50,"Latitude":22.5711866,"Longitude":88.4341312,"Location":"location","DeliveredBy":"d01","DeliveryDate":"2019-08-01 11:12:13", "ItemCode":"1103","ItemName":"item name","Qty":3, "ItemPrice": 12.50}]}

13> Generate Invoice 
http://localhost:50313/api/InvoiceDet/GenerateInvoice						-- Generate Invoice  {"invoiceLoadItems":[{"InvoiceNo":"0","OrderNumber":"5902000082","TripID":"1","CustomerID":"006969","CompanyID":"2","InvoiceDate":"2019-08-05 16:01:02","CustomerAddress":"315 W. FERRY ST","CustomerPhone":"7168851740","CustomerEmail":"", "CustomerNotes":"","InvoiceTotalQty":1,"InvoiceTotalAmount":96.80, "ShippingCharge":0.00, "DriverID":"d01" , "ItemCode":"1111","ItemName":"EXTRA VIRGIN OLIVE OIL 50.7 OZ","Qty":1, "ItemPrice": 96.80}]}

14> View Delivery															-- { "OrderNumber":"5902000082" , "CompanyID" :"2" }
http://localhost:50313/api/InvoiceDet/ViewDelivery					

15> http://localhost:50313/api/InvoiceDet/InvoiceList						--  { "CompanyID":"2", "DriverID":"d01" }

16> http://localhost:50313/api/InvoiceDet/GenerateDSD						-- { "InvoiceNo":"INV1901000009", "TripID":"1", "CustomerID":"006969", "CompanyID":"2", "DriverID":"d01", "DueDate":"2019-09-30 11:12:13","StopLocation":"006969", "DSDDate":"2019-09-27 11:12:13", "CustomerAddress":"315 W. FERRY ST", "CustomerPhone":"7168851740", "CustomerEmail":"", "PaymentMode":"", "CustomerNotes":"", "TotalQty":1,"DSDSubTotal":12.30, "CountryTax":3, "Discount":1, "GrandTotal": 16.30,  "SignatureImageName":"signaturecapture.png", "SignatureImageBinaryData":"iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAIAAABu2d1/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAydJREFUeNrsmEtvElEUx2EGKAMMTym0pC3FR5Vo0jTExMaFG2M0xp07F7rShV/Ab+A3MC7c6sJETerGuOjKNjESJTFNQaoNQ22LbbUdhukwD5wYg3CZ171lLE3u2d2TO9xfDv9z/nfGmclkHEcnCMeRCoyLcTEuxsW4GBfjHlK4LO5TPN5mNGlLwZr7np2NfuKqoLUrd1Rim2oWn39GVZb7JgY2e8E+VjWkQKSf2hUSaVsVObTxrW+4al2lQNhW3H5qV0hClNZL+XIzM1Cs3N7u9xt320snz3rePkXHbUZHLB6cGEldn80FXHDDscDGVjtw/9Zbh9j8p/ctV/fs1ElYVjV2xBaQaVE0unat99kx2o8g3M0miEsyJURcKGtI+jywrJzsaCpgkix+QMQVkpMWD56YPI5Q2g0BLC3BlJwNFrHVDKp7+9KbCC20l5dT56bD/84W5PqTaopV0rDCJZki+iDTE+79q3ORkPrPkO1MNng+7pnt3HNr9OWjahpauMU8ok0YGESop3ej7jNAZp2vIYwFslJExNUziKmRVYLoenB4yE+R48C2pXoUtrROvk4wqLh6BpGOg545Rg33bisJ16D7zLC0Jrh6BjGR2AEyGd9pIPOjuYCiBP0RZkEMOn0WC0qgQXhOAJlKw/z+2isGl2GfGeHqjTCa4lxuErxfd88ENRheND74p9jqNQhj4Rrh6hnE9MQXIJOlNSReqF80U4IDyiBMcNVXKM18KrYNZryjQGZX+mxsEGpdV3gFyiBMbMJf/qjO3d53Hl9unduVu3w//G6lwXVm1jihwD4wOJXZb2n1Wf5A13N6aVHjATdd/9U9QMbny41XnZkX1XsFVoG9P5AV8+rCXU+jp8je5B5PAZk8dxOW1b0wRxxEDJoRHNPAff81HaL49lIWia2Hj30wP6t2mBVWaFzN6ooyuVUPtJe1gmQ67f/TRydNXFAbjOKwLSBwqRjhopzm1lqSBgLXSmn/4MoDgRscIw6XFQ7XihLWFkVbcSEmw/Jzgd9uGWxgq/LmJ2lQcMVGq/xacBxq4I/9GBfjYlyMi3ExLsa1Er8FGAABfxc09S1slgAAAABJRU5ErkJggg==", "CaptureImageName": "captureimage.png", "CaptureImageBinaryData":"iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAIAAABu2d1/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAydJREFUeNrsmEtvElEUx2EGKAMMTym0pC3FR5Vo0jTExMaFG2M0xp07F7rShV/Ab+A3MC7c6sJETerGuOjKNjESJTFNQaoNQ22LbbUdhukwD5wYg3CZ171lLE3u2d2TO9xfDv9z/nfGmclkHEcnCMeRCoyLcTEuxsW4GBfjHlK4LO5TPN5mNGlLwZr7np2NfuKqoLUrd1Rim2oWn39GVZb7JgY2e8E+VjWkQKSf2hUSaVsVObTxrW+4al2lQNhW3H5qV0hClNZL+XIzM1Cs3N7u9xt320snz3rePkXHbUZHLB6cGEldn80FXHDDscDGVjtw/9Zbh9j8p/ctV/fs1ElYVjV2xBaQaVE0unat99kx2o8g3M0miEsyJURcKGtI+jywrJzsaCpgkix+QMQVkpMWD56YPI5Q2g0BLC3BlJwNFrHVDKp7+9KbCC20l5dT56bD/84W5PqTaopV0rDCJZki+iDTE+79q3ORkPrPkO1MNng+7pnt3HNr9OWjahpauMU8ok0YGESop3ej7jNAZp2vIYwFslJExNUziKmRVYLoenB4yE+R48C2pXoUtrROvk4wqLh6BpGOg545Rg33bisJ16D7zLC0Jrh6BjGR2AEyGd9pIPOjuYCiBP0RZkEMOn0WC0qgQXhOAJlKw/z+2isGl2GfGeHqjTCa4lxuErxfd88ENRheND74p9jqNQhj4Rrh6hnE9MQXIJOlNSReqF80U4IDyiBMcNVXKM18KrYNZryjQGZX+mxsEGpdV3gFyiBMbMJf/qjO3d53Hl9unduVu3w//G6lwXVm1jihwD4wOJXZb2n1Wf5A13N6aVHjATdd/9U9QMbny41XnZkX1XsFVoG9P5AV8+rCXU+jp8je5B5PAZk8dxOW1b0wRxxEDJoRHNPAff81HaL49lIWia2Hj30wP6t2mBVWaFzN6ooyuVUPtJe1gmQ67f/TRydNXFAbjOKwLSBwqRjhopzm1lqSBgLXSmn/4MoDgRscIw6XFQ7XihLWFkVbcSEmw/Jzgd9uGWxgq/LmJ2lQcMVGq/xacBxq4I/9GBfjYlyMi3ExLsa1Er8FGAABfxc09S1slgAAAABJRU5ErkJggg==" }






http://airstine.com

17> http://107.21.119.157/DVSmobile/api/InvoiceDet/GetPaymentTerm			-- {"CompanyID":"2" } To get Payment Term list for the Clear Invoice screen
18> http://107.21.119.157/DVSmobile/api/Trip/GetTripListOrderCount			-- { "CompanyID":"2", "DriverID": "D01", "StartDate": "2020-01-01", "EndDate": "2020-03-03" }  To show date wise total trip count for Trip screen




Download
To get Dashboard data
http://107.21.119.157/DVSmobile/api/Offline/GetDVSDashboard 
{ "CompanyID":"2" }

To get ALL Trip data
http://107.21.119.157/DVSmobile/api/Offline/GetAllTrips
{ "CompanyID":"2", "DriverID":"D01", "StartDate":"2020-02-01","EndDate":"2020-11-22" }

To get Order Invoice details
http://107.21.119.157/DVSmobile/api/Offline/GetAllOrderInvoiceDetails
{ "CompanyID":"2", "DriverID":"D01", "StartDate":"2020-02-01","EndDate":"2020-04-22" }

Upload 
To upload the all trip data
http://107.21.119.157/DVSmobile/api/Offline/TripDataUpload
{"CompanyID":"2", "payLoadTrips":[{"TripID":"1", "TripStatus" : "6"}, {"TripID":"10", "TripStatus" : "1"} ]}

To upload the all customer data
http://107.21.119.157/DVSmobile/api/Offline/CustomerDataUpload
{"CompanyID":"2", "payLoadCustomers":[{"TripID":"1", "TripDetailID" : 1, "CustomerNo":"006969", "ActualStartTime" : "2020-03-06 18:33:10", "ActualEndTime" : "2020-05-14 18:45:10" , "IsCustomerSkipped" : "1", "SpecificCustomer" : "false" } ]}

To send the invoice generate number
http://107.21.119.157/DVSmobile/api/Offline/GetInvoiceNoByOrderNo
{ "CompanyID":"2", "DriverID":"D01", "StartDate":"2020-02-01","EndDate":"2020-04-22" }

To upload the customer check in data
http://107.21.119.157/DVSmobile/api/Offline/StopsCheckInUpload
{"CompanyID":"2", "payLoadCheckIns":[{"TripID":"1", "CustomerNo" : "003925", "Latitude": "22.610072600", "Longitude" : "88.420487100", "Address" : "KEVIN PRISE,2200 HARLEM ROAD,CHEEKTOWAGA,NY,14225, CHEEKTOWAGA, NY, 14225", "StartingStop" :  "1254 Paterson Plank Rd, Secaucus, NJ 07094, USA","CurrentStop" : "225, Shyam Nagar Rd, Dum Dum Park, South Dumdum, West Bengal 700055, India", "StopDateTime":"2020-05-19 23:49:55", "Mileage": 24, "StopImageName" : "Stop003925.jpg", "StopImagePath" : "NULL", "Remarks":"NULL", "CreatedAt":"2020-05-19 19:19:57", "CreatedBy":"D01"} ]}


19Nov, 2020 
1>	Order Detail API for uploading the data from mobile to SQL table
http://107.21.119.157/DVSmobile/api/Offline/OrderDetailsUpload
{"CompanyID":"2", "orderDetailsUploads":[{"BasketID":"0159372019711162643613", "CustomerID" : "006969", "ProductCode": "2626", "OrdQty" : 5, "CasePrice" : 29.75, "TotalPrice" :  1159.60,"TotalQty" : 15 },
{"BasketID":"0159372019711162643613", "CustomerID" : "006969", "ProductCode": "2623", "OrdQty" : 5, "CasePrice" : 1103.75, "TotalPrice" :  1159.60,"TotalQty" : 15 }]}

2>	To upload invocie DSD data ( To sync Invoice detail and DSD data for uploading in sql table
 )
http://localhost:50313/api/Offline/InvoiceDSDUpload
{"CompanyID":"2", "invoicDSDUploads":[{"InvoiceNo":"INV20010000082", "TripID" : "1", "CustomerID": "003925", "DriverID" : "D01                                               ", "DSDDate" : "2020-11-20 02:05:23.000", "SignatureImageName" :  "signaturecapture.png","CaptureImageName" : "captureimage.png" , "WhoSigned":"Edwaed" } ]}


http://107.21.119.157/DVSmobile/api/Offline/DSDPhotosUpload
{"CompanyID":"2", "DSDImages":[{"SignatureImageName" : "signaturecapture.png", "SignatureImageBinaryData":"iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAIAAABu2d1/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAydJREFUeNrsmEtvElEUx2EGKAMMTym0pC3FR5Vo0jTExMaFG2M0xp07F7rShV/Ab+A3MC7c6sJETerGuOjKNjESJTFNQaoNQ22LbbUdhukwD5wYg3CZ171lLE3u2d2TO9xfDv9z/nfGmclkHEcnCMeRCoyLcTEuxsW4GBfjHlK4LO5TPN5mNGlLwZr7np2NfuKqoLUrd1Rim2oWn39GVZb7JgY2e8E+VjWkQKSf2hUSaVsVObTxrW+4al2lQNhW3H5qV0hClNZL+XIzM1Cs3N7u9xt320snz3rePkXHbUZHLB6cGEldn80FXHDDscDGVjtw/9Zbh9j8p/ctV/fs1ElYVjV2xBaQaVE0unat99kx2o8g3M0miEsyJURcKGtI+jywrJzsaCpgkix+QMQVkpMWD56YPI5Q2g0BLC3BlJwNFrHVDKp7+9KbCC20l5dT56bD/84W5PqTaopV0rDCJZki+iDTE+79q3ORkPrPkO1MNng+7pnt3HNr9OWjahpauMU8ok0YGESop3ej7jNAZp2vIYwFslJExNUziKmRVYLoenB4yE+R48C2pXoUtrROvk4wqLh6BpGOg545Rg33bisJ16D7zLC0Jrh6BjGR2AEyGd9pIPOjuYCiBP0RZkEMOn0WC0qgQXhOAJlKw/z+2isGl2GfGeHqjTCa4lxuErxfd88ENRheND74p9jqNQhj4Rrh6hnE9MQXIJOlNSReqF80U4IDyiBMcNVXKM18KrYNZryjQGZX+mxsEGpdV3gFyiBMbMJf/qjO3d53Hl9unduVu3w//G6lwXVm1jihwD4wOJXZb2n1Wf5A13N6aVHjATdd/9U9QMbny41XnZkX1XsFVoG9P5AV8+rCXU+jp8je5B5PAZk8dxOW1b0wRxxEDJoRHNPAff81HaL49lIWia2Hj30wP6t2mBVWaFzN6ooyuVUPtJe1gmQ67f/TRydNXFAbjOKwLSBwqRjhopzm1lqSBgLXSmn/4MoDgRscIw6XFQ7XihLWFkVbcSEmw/Jzgd9uGWxgq/LmJ2lQcMVGq/xacBxq4I/9GBfjYlyMi3ExLsa1Er8FGAABfxc09S1slgAAAABJRU5ErkJggg==", "CaptureImageName" : "captureimage.png", "CaptureImageBinaryData": "~� } ] }


http://localhost:50313/api/FileInvoice/
POST
Files		test.JPG