﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace OMSWebApi.Repository
{
   public interface IDataAccess
    {
        void ExecuteQuery(String strSQL);
        void ExecuteQuery(String strSPName, ref SqlParameter[] param);
        SqlDataReader GetDataReader(string StrSQL);
        DataTable GetDataTable(string strSQL);
        DataTable GetDataTable(string strSPName, SqlParameter[] param);
        DataTable GetDataTable(string strSPName, Boolean isSP);
        DataSet GetDataSet(string strSQL);
        DataSet GetDataSet(string strSPName, SqlParameter[] param);
        DataSet GetDataSet(string strSPName, Boolean isSP);
    }
}
