﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DVSWebApi.Providers
    {

    /*******************************************/
    //This is the Oauth provider to allow passing the token in the query string
    /*******************************************/
    public class QueryStringOAuthBearerProvider : OAuthBearerAuthenticationProvider
        {
        readonly string _name;

        public QueryStringOAuthBearerProvider(string name)
            {
            _name = name;
            }

        public override Task RequestToken(OAuthRequestTokenContext context)
            {
            var value = context.Request.Query.Get(_name);
            if (!string.IsNullOrEmpty(value))
                {
                context.Token = value;
                }

            return Task.FromResult<object>(null);
            }
        }
    }