﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DVSWebApi.Providers
    {
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
        {


        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
            {
            context.Validated();
            }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
            {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var userStore = new UserStore<IdentityUser>(new IdentityDbContext());
            var userManager = new UserManager<IdentityUser>(userStore);
            userManager.UserValidator = new UserValidator<IdentityUser>(userManager);
            var user = userManager.Find(context.UserName, context.Password);

            if (user != null)
                {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ExternalBearer);
                context.Validated(userIdentity);
                }
            else
                {
                }

            }
        }
    }